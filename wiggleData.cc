/*
 * wiggleData.cc
 *
 *  Created on: Mar 1, 2009
 *      Author: sven
 */
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <stdlib.h>
#include <numeric>
#include "guli/wiggleData.h"
#include "guli/gzstream.h"

namespace GULI {
	// parser for the data-files
	class wiggleStatement {
		typedef int (wiggleStatement::*proc)(const string &);
		typedef struct {
			const char *keyword;
			proc processor;
		} wigCmd;

		static const wigCmd _list[];

		int fixedStep(const string &) {
			return 0;
		}
		int chrom(const string &arg) {
			_chr = arg;
			return 0;
		}
		int start(const string &arg) {
			_start = atol(arg.c_str());
			return 0;
		}
		int step(const string &arg) {
			_stepsize = atol(arg.c_str());
			return 0;
		}

	public:
		bool isWiggleStatement(const string &line) {
			if (line.size() < 1) return false;
			char first = line[0];
			if (first == '-' || (first >= '0' && first <= '9')) return false; // Data line

			string toparse = line;

			while (toparse != "") {
				string arg = "";
				size_t stop = toparse.find_first_of(" =");
				string key = toparse.substr(0, stop);

				if (stop != string::npos && (toparse[stop] == '=')) {
					size_t eoarg = toparse.substr(stop + 1).find_first_of(" ");
					arg = toparse.substr(stop + 1, eoarg);
					toparse = (eoarg != string::npos) ? toparse.substr(stop
							+ eoarg + 2).c_str() : "";
				} else {
					toparse
							= (stop != string::npos) ? toparse.substr(stop + 1).c_str()
									: "";
				}

				int i;
				int r = 0;
				for (i = 0; *_list[i].keyword; ++i) {
					if (key == _list[i].keyword) {
						r = (this ->* _list[i].processor)(arg);
						break;
					}
				}
				if (r || !*_list[i].keyword) {
					std::cerr << "Warning, problem parsing line " << line
							<< std::endl;
					return false;
				}
			}
			return true;
		}

		int start() {
			return _start - 1;
		}
		int stepsize() {
			return _stepsize;
		}
		const string &chromosome() {
			return _chr;
		}
	private:
		int _start;
		int _stepsize;
		string _chr;
	};

	const wiggleStatement::wigCmd 	wiggleStatement::_list[] = {
			{ "fixedStep",	&wiggleStatement::fixedStep },
			{ "chrom",  	&wiggleStatement::chrom },
			{ "start",		&wiggleStatement::start },
			{ "step", 		&wiggleStatement::step },
			{ "",			&wiggleStatement::step }
	};

	wiggleData::wiggleData()  {}

	wiggleData::~wiggleData() {}

	static string extractChrFromFname(string name) {
		const string error = "error";
		if (name.length() < 4) return error;
		if (name.substr(0, 3) != "chr") return error;
		size_t found = name.find_first_of(".");
		if (found == string::npos) return error;
		return name.substr(0, found).c_str();
	}

	void wiggleData::addFile(const string &chromosome, const string &fnam,
			int sizeHint) {
		gvalueLT empty;
		unsigned int i = _chrMap.idIdx(chromosome, true);
		if (i == _c.fnames.size()) {
			_c.fnames.push_back(fnam);
			_c.loaded.push_back(false);
			_c.density.push_back(empty);
			_c.sizeHint.push_back(sizeHint);
			_c.maxPos.push_back(-1);
			_c.nHits.push_back(-1.);

		} else {
			_c.fnames[i] = fnam;
			_c.loaded[i] = false;
			_c.density[i].swap(empty);
			_c.sizeHint[i] = sizeHint;
			_c.nHits[i] = -1.;
			_c.maxPos[i] = -1;
		}
	}

	int wiggleData::openDir(const string &d) {
		DIR *dir = opendir(d.c_str());
		if (!dir) {
			std::cerr << "Can not open directory " << d << std::endl;
			return -1;
		}

		while (dirent * e = readdir(dir)) {
			struct stat info;
			string fqn = d + "/" + e->d_name;
			if (stat(fqn.c_str(), &info)) {
				std::cerr << "ERROR: Can not stat file " << e->d_name
						<< std::endl;
				closedir(dir);
				return -1;
			}

			string name = e->d_name;
			if (S_ISREG(info.st_mode)) {
				string chr = extractChrFromFname(name);
				if (chr == "error") continue;
				addFile(chr, fqn);
			}

		}
		closedir(dir);
		return 0;
	}

	void wiggleData::relaxDensity(const string &chrm) const {
		gvalueLT empty;
		const int chr = _chrMap.idIdx(chrm);
		if(chr < 0) return;
		_c.density[chr].swap(empty);
		_c.loaded[chr] = false;
		return;
	}



	const wiggleData::gvalueLT &wiggleData::density(const string  &chrm) const {
		static const wiggleData::gvalueLT empty;
		const int chr = _chrMap.idIdx(chrm);
		if(chr < 0) return empty;
		if (_c.loaded[chr]) return _c.density[chr];

		std::cerr << "Loading data " << _c.fnames[chr] << std::endl;
		igzstream I(_c.fnames[chr].c_str());
		if (!I.is_open()) {
			std::cerr << "Could not open " << _c.fnames[chr] << std::endl;
			return _c.density[chr];
		}
		if (_c.sizeHint[chr] > 0) _c.density[chr].reserve(_c.sizeHint[chr]);

		wiggleStatement stmt;

		gvalueLT &cur = _c.density[chr];
		cur.clear();
		vector<float> buffer;
		buffer.reserve(10240000);
		int start = -1;
		int stepsize = 1;
		while (!I.eof()) {
			char b[1024];
			I.getline(b, 1024);
			if (stmt.isWiggleStatement(b)) {
				if (start >= 0) {
					if (stepsize == 1) cur.insert(cur.end(), buffer.begin(),
							buffer.end());
					else {
						vector<float> b1(buffer.size() * stepsize);
						for (unsigned int i = 0; i < buffer.size(); ++i) {
							for (int j = 0; j < stepsize; ++j) {
								b1[i * stepsize + j] = buffer[j] / stepsize;
							}
						}
						cur.insert(cur.end(), b1.begin(), b1.end());
					}
				}
				start = stmt.start();
				stepsize = stmt.stepsize();
				// pad with zeros to new start position
				if ((int) cur.size() < start) {
					vector<float> b(start - cur.size(), 0.);
					cur.insert(cur.end(), b.begin(), b.end());
				}
				buffer.clear();
			} else {
				buffer.push_back(atof(b));
			}
		}
		I.close();
		_c.nHits[chr] = std::accumulate(cur.begin(), cur.end(), 0.);
		_c.sizeHint[chr] = cur.size();
		_c.maxPos[chr] = cur.size() - 1;
		_c.loaded[chr] = true;
		return cur;
	}

	wiggleData::gvalueT wiggleData::nHits(const  string &chr) const {
		const int idx = _chrMap.idIdx(chr);
		if(idx < 0) return 0;
		if (_c.nHits[idx] < 0) {
			density(chr);
			relaxDensity(chr);
		}
		return _c.nHits[idx];
	}

	int wiggleData::maxpos(const string &chrm) const {
		const int chr = _chrMap.idIdx(chrm);
		if(chr < 0) return 0;

		if (_c.maxPos[chr] < 0) {
			density(chrm);
			relaxDensity(chrm);
		}
		return _c.maxPos[chr];
	}

} // NAMESPACE
