/***************************************************************************
 *   Copyright (C) 2005-2007 by Sven Bilke  	                           *
 *   bilkes@mail.nih.gov   					                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "guli/seqsetwholegenome.h"

	seqSetWholeGenome::seqSetWholeGenome() :
		seqSet(), _dsc(0) {
		_size = 0;
	}

	seqSetWholeGenome::seqSetWholeGenome(const ucscGenome &f) :
		seqSet() {
		selectGenome(f);
	}

	seqSetWholeGenome::~seqSetWholeGenome() {}

	const sequence seqSetWholeGenome::operator[](const string &s) const {
		static const sequence::seqData empty;
		static const sequence _emptySequence(empty);
		int i = _dsc->chrIndex(s);
		if (i < 0) return _emptySequence;
		return this->operator[](i);
	}

	void seqSetWholeGenome::selectGenome(const ucscGenome &f) {
		_dsc = &f;
		_size = f.nChromosomes();
		_cache.select(f);
		return;
	}

	void seqSetWholeGenome::cache::select(const ucscGenome &f) {
		_dsc = &f;
		resize(f.nChromosomes());
		return;
	}

	void seqSetWholeGenome::cache::_minimizeMemFootprint(sequence::seqData &D,
			size_t i) const {
		_usedmem -= _dsc->chrLength(i);
		--_nmem;
		sequence::seqData swapedOut;
		D = swapedOut; // assign empty sequence to free memory
	}

	void seqSetWholeGenome::cache::_load2mem(sequence::seqData &D, size_t i) const {
		assert(_dsc);
		const string &fn = _dsc->fnam(i);
		ifstream IN(fn.c_str());
		if (!IN) {
			std::cerr << "Could not open genome file " << fn[i] << std::endl;
			assert(false);
		}
		IN >> D;
		IN.close();
		++_nmem;
		_usedmem += _dsc->chrLength(i);
		return;
	}
