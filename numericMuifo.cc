/*
 * numericMuifo.cc
 *
 *  Created on: Mar 11, 2010
 *      Author: sven
 */
#include <math.h>
#include "guli/numericMuifo.h"

double numericMuifo::kernel2d(const std::pair<vector<int> , vector<int> > &rankorder, float radius) {
	static const double PI     = 3.14159265359;

	const vector<int> &A = rankorder.first;
	const vector<int> &B = rankorder.second;
	const unsigned int N = A.size();
#ifdef DEBUG
	assert(N == B.size());
#endif


	// Estimate radius from data if no user-supplied estimate is given
	// [Silverman B.W., Density Estimation for Statistics and Data Analysis (Chapman and Hall, NY, 1986)]
	if(radius <= 0.) radius = pow(N, -1./6.);

	// calculate co-variance matrix, determinant and inverse
	double Cij = 0., Cii = 0.;
	for (unsigned int i = 0; i < N; ++i) {
		const double da = (A[i] / (double) N) - 0.5;
		const double db = (B[i] / (double) N) - 0.5;
		Cij += da * db;
		Cii += da * da;
	}
	Cij /= N;
	Cii /= N;

	//  Det(Cij)
	const float detS = Cii * Cii - Cij * Cij;

	// inverse co-variance matrix
	/*TODO: Make sure S _is_ actually invertible (det(Cij) is finite) */
	const double iCii =  Cii   / detS;
	const double iCij = -Cij   / detS;

	// to speed up computation, pre-calculate some products needed frequently
	// 1 / (2 r**2)
	const double r2   = 2.* radius * radius;
	// co-ordinates are in rank-order but not mapped to unit-square.
	// do transformation to unit-square in combination with v^T S^-1 v operation (divide by N from left and right)
	// and also include 1/r**2 needed when calculating the standard bivariate gaussian
	const double iCii_r2 = iCii  / (r2 * N * N);
	const double iCij_r2 = iCij  / (r2 * N * N);
	const double gaussian_norm = 1. / (r2 * PI * sqrt(detS));


	double mi = 0.;
	for (unsigned int i = 0; i < N; ++i) {
		const int x1 = A[i];
		const int x2 = B[i];
		double JPD = 0.;
		for (unsigned int j = 0; j < N; j++) {
			const int y1  = x1 - A[j];
			const int y2  = x2 - B[j];
			const int y11 = y1 * y1;
			const int y22 = y2 * y2;
			const int y12 = y1 * y2;
			const double u = (iCii_r2 * (y11 + y22) + 2. * iCij_r2 * y12);
			JPD +=  exp(-u);
		}
		JPD *= gaussian_norm;
		JPD /= N;
		mi += JPD * log(JPD);
	}
	// norm by partition function
	const double Z    = 1. / ( N * log(2.));
	mi *= Z; // base 2 & number of bins

	return mi;
}

