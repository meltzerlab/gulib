/***************************************************************************
 basicData.cpp  -  description
 -------------------
 begin                : Tue Jul 23 2002
 copyright            : (C) 2002 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <functional>
#include <algorithm>
#include "guli/stdinc.h"
#include "guli/dataBasic.h"
namespace GULI {

		dataBasic::dataBasic(int cols, int rows) :
			mutableIF(), _cols(cols), _rows(rows) {
			const string empty("");
			_content = std::make_unique<vector<vector<float> > > (rows);
			std::for_each(_content->begin(), _content->end(), [cols](vector<float> &v){v.resize(cols); });
			_rowId = std::make_unique<vector<string> > (_rows, empty);
			_colId = std::make_unique<vector<string> > (_cols, empty);
		}

		dataBasic::dataBasic(dataIF &init) {
			_initcpy(init);
		}

		dataBasic::dataBasic(dataBasic &init) {
			_initcpy(init);
		}

		dataBasic::~dataBasic() {
		}

		void dataBasic::colPushBack(const string &id, vector<float> &D) {
			if (D.size() != _rows) throw new sizeMismatch;
			for (unsigned int i = 0; i < _rows; i++)
				(*_content)[i].push_back(D[i]);
			_colId->push_back(id);
			++_cols;
			return;
		}
		;

		void dataBasic::colPushBack(const string &id, float init) {
			for (unsigned int i = 0; i < _rows; i++)
				(*_content)[i].push_back(init);
			_colId->push_back(id);
			++_cols;
			return;
		}

		void dataBasic::colPopBack() {
			for (unsigned int i = 0; i < _rows; i++)
				(*_content)[i].pop_back();
			_colId->pop_back();
			--_cols;
		}

		void dataBasic::rowPushBack(const string &id, vector<float> &D) {
			if (D.size() != _cols) throw new sizeMismatch;
			_content->push_back(D);
			_rowId->push_back(id);
			++_rows;
		}
		;

		void dataBasic::rowPushBack(const string &id, float init) {
			vector<float> D(_cols, init);
			rowPushBack(id, D);
		}

		void dataBasic::rowPopBack() {
			_content->pop_back();
			_rowId->pop_back();
			--_rows;
		}

		void dataBasic::resize(int cols, int rows) {
			const string empty("");
			_cols = cols;
			_rows = rows;
			_content = std::make_unique<vector<vector<float> > >(_rows);
			std::for_each(_content->begin(), _content->end(), [this](vector<float> &v){v.resize(this->_cols);});
			_rowId = std::make_unique<vector<string> >(_rows, empty);
			_colId = std::make_unique<vector<string> >(_cols, empty);
		}

		void dataBasic::_initcpy(dataIF &init) {
			_cols = init.cols();
			_rows = init.rows();
			_content = std::make_unique<vector<vector<float> > > (_rows);
			std::for_each(_content->begin(), _content->end(), [this](vector<float> &v){v.resize(this->_cols);});
			_rowId = std::make_unique<vector<string> >(_rows);
			_colId = std::make_unique<vector<string> >(_cols);
			std::copy(init.beginRowLabel(), init.endRowLabel(), beginRowLabel());
			std::copy(init.beginColLabel(), init.endColLabel(), beginColLabel());
			for (unsigned int i = 0; i < _cols; i++) {
				std::copy(init.beginCol(i), init.endCol(i), beginCol(i));
			}
		}
} // namespace GULI::data
