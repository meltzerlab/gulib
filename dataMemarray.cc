/***************************************************************************
 dataMemarray.h  -  description
 -------------------
 begin                : Fri May 3 2002
 copyright            : (C) 2002 by sven
 email                : sven@gonzo.thep.lu.se
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "guli/stdinc.h"
#include "guli/dataMemarray.h"

#ifndef __APPLE_CC__
#include <malloc.h>
#endif

namespace GULI {
	dataMemarray::~dataMemarray() {
		free (_data);
		delete[] _rowid;
		delete[] _colid;
	}

	dataMemarray::dataMemarray(unsigned int cols, unsigned int rows, int align) :
		_cols(cols), _rows(rows) {
		int memsize;
		if (!align) {
			int memsize = cols * rows * sizeof(float);
			_entries_per_col = rows;
			_data = (float *) malloc(memsize);
		} else {
			unsigned int mask = 0;
			for (int i = 0; i < align; i++) mask = (mask << 1) | 1;
			_entries_per_col = ((rows + mask) / (mask + 1)) * (mask + 1);
			memsize = cols * _entries_per_col * sizeof(float);
#ifdef __APPLE_CC__
			// Malloc is aligned, but not the general way. This may or may not work
			assert(align == 16);
			_data = (float *)  malloc(memsize);
#else
			_data = (float *)  memalign(mask + 1, memsize);
#endif
			//TODO: this is NOT correct, memory is not aligned. Just for debugging on the mac
			// did this to run some other code on the mac whcih refused to compile
		}
		_rowid = new std::string[_rows];
		_colid = new std::string[_cols];
	}
} // namespace SB::data

