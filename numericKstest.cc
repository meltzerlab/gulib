/***************************************************************************
 numericKstest.cc  -  description
 -------------------
 begin                : Fri Jan 3 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/** Implementation of the Kolmogorov Smirnov Test
 Inspired by "Numerical Recipes in C, Chapter 14.3
 **/
#include "guli/stdinc.h"
#include "guli/numericKstest.h"

#undef DEBUG
#ifdef DEBUG
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#endif
namespace GULI {
	double KolmogorovSmirnov::_calcP(double alam) {
		double a2, fac = 2.0, sum = 0.0, term, termbf = 0.0;
		a2 = -2.0 * alam * alam;
		_probability = 1.0;
		for (int j = 1; j <= _MAXSUM; j++) {
			term = fac * exp(a2 * j * j);
			sum += term;
			if (fabs(term) <= _EPS1 * termbf || fabs(term) <= _EPS2 * sum) {
				_probability = sum;
				break;
			}
			fac = -fac;
			termbf = fabs(term);
		}
		return _probability;
	}
//TODO:: IS THIS A BUG OR A FEATURE?????
	double KolmogorovSmirnov::density(double x) {
		int n1 = 0;
		int n2 = _csize - 1;
		if (x <= _compare[0]) return 0.0;
		if (x >= _compare[n2]) return 1.0;
		int tr, tr1 = -1;
		while (true) {
			if (1 == (n2 - n1)) { // Well, rounding is important.
				// is there a smarter way than this ?
				if (tr1 == n1) tr = n2;
				else tr = n1;
			} else {
				tr = (n2 + n1) / 2;
			}
			if (_compare[tr] >= x && _compare[tr - 1] <= x) return (tr - 0.5)
					/ (double) _csize;
			if (_compare[tr] < x) n1 = tr;
			else n2 = tr;
			tr1 = tr;
		}
	}
} // namespace GULI::numeric

#ifdef DEBUG
using namespace GULI::numeric;

int main (void) {
	const gsl_rng_type * T;
	gsl_rng * r;

	vector<double> A, B;
	int i, n = 10000;

	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc (T);

	GULI::numeric::KolmogorovSmirnov test;

	for (i = 0; i < n; i++) {
		A.push_back(gsl_ran_flat (r, -1.0, 1.0));
	}

	test.setCompare(A.begin(), A.end());

	for(int k =0; k < 20000; k++) {
		B.clear();

		for (i = 0; i < 50; i++)
		B.push_back(gsl_ran_flat (r, -1.0, 1.0));
		double P = test.test(B.begin(), B.end(), A.begin(), A.end());
		cout << P << " " << test.maxDist() << endl;

	}
	gsl_rng_free (r);
}

#endif

