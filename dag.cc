//TEST
/*
 * dag.cc
 *
 *  Created on: Sep 30, 2012
 *      Author: sven
 */

#include "guli/dag.h"
void GULI::Dag::_updateRelations(Dag::Node &cur,
						 const vector<string> &l,
						 Dag::Link::directionT d,
						 Dag::Link::userdataT  u) {
	if(!l.size()) return;

	for (unsigned int j = 0; j < l.size(); j++) {
		Node *t =  (*this)[l[j]];
		if (!t) {
			std::cerr << "WARNING: reference from " << cur.id() << " to an unknown dag-node \"" << l[j] << "\""
					  << std::endl;
			return;
		}

		Link::directionT d1 = (d == Link::INCOMING ? Link::OUTGOING : Link::INCOMING);

		Link l1(*t,   d,  u);
		Link l2(cur, d1, u);
		cur.addLink(l1);
		t->addLink(l2);
	}
}

void GULI::Dag::clear() {
	vector<Node *> deathrow(_data.size());
	size_t j = 0;
	for(auto i = _data.begin(); i != _data.end(); ++i) {
		deathrow[j++] = i->second;
	}
	_data.clear();
	for(size_t i =0; i < deathrow.size(); ++i) delete deathrow[i];
}
