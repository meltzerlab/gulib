/*
 * ngsBamPileupT.cc
 *
 *  Created on: Dec 12, 2012
 *      Author: sven
 */

#include <math.h>
#include "guli/ngsPileupT.h"
#include "guli/ngsBamUtils.h"


static const short nt_bam2idx[] = {  -1,   0,   1,  -1,   2,  -1,  -1,  -1,
									  3,  -1,  -1,  -1,  -1,  -1,  -1,   4,
									  5,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
									 -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
									 -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
									 -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
									 -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
									 -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6,
									 6,  6,  6,  6,  6,  6,  6,  6
};

static const short nt_ascii2idx[] = { -1, -1, -1, -1, -1, -1, -1, -1, // 0x00
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x08
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x10
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x18
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x20
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x28
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x30
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x38
									  -1,  0, -1,  1,  5, -1, -1,  2, // 0x40
									  -1,  6, -1, -1, -1, -1,  4, -1, // 0x48
									  -1, -1, -1, -1,  3, -1, -1, -1, // 0x50
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x58
									  -1,  0, -1,  1,  5, -1, -1,  2, // 0x60
									  -1,  6, -1, -1, -1, -1,  4, -1, // 0x68
									  -1, -1, -1, -1,  3, -1, -1, -1, // 0x70
									  -1, -1, -1, -1, -1, -1, -1, -1, // 0x78
};

namespace GULI {

	namespace NGS {
		void pileup_t::clear() {
			_reference = '\0';
			_clientReject = _baseQualityReject =  _overlapReject = _dupReject = 0;
			_statUpdated = false;
			static_cast<vector<ntRead_t> *>(this)->clear();
		}

		void pileup_t::_updateStat() const {
			static const int maxidx = 7;
			if(_statUpdated) return;
			const unsigned int N = size();

			std::fill(_ntFreq, _ntFreq+maxidx, 0);
			std::fill(_ntFwdFreq, _ntFwdFreq+maxidx, 0);
			_coverage = 0;
			for(unsigned int i=0; i < N; ++i) {
				const ntRead_t &R = this->operator[](i);
				if(R.is_del) continue;
				_coverage++;
				const int idx = nt_bam2idx[R.bnt()];
				++_ntFreq[idx];
				if(!R.strand()) ++_ntFwdFreq[idx];
				if(R.indel < 0) {
					++_ntFreq[5]; // DELETION
					if(!R.strand()) ++_ntFwdFreq[5];
				}
			}

			std::fill(_gmnBqual, _gmnBqual+maxidx, 0);
			std::fill(_gmnAqual, _gmnAqual+maxidx, 0);

			for(unsigned int i=0; i < N; ++i) {
				const ntRead_t &R = this->operator[](i);
				int x = nt_bam2idx[R.bnt()];
				_gmnBqual[x] += BAMUTILS::baseQuality(R);
				_gmnAqual[x] += R.b->core.qual;
			}
			for(int i=0; i < maxidx; ++i) {
				_gmnBqual[i] = pow(10., -_gmnBqual[i] / (10. * _ntFreq[i]));
				_gmnAqual[i] = pow(10., -_gmnAqual[i] / (10. * _ntFreq[i]));
			}

			_statUpdated = true;



		}

		int pileup_t::_ntidx(const char nt) const {
			assert(nt >= 0);
			const int i = nt_ascii2idx[(short int) nt];
			if(i < 0) {
				std::cerr << "Mapping illegal character in pileup_t::_ntidx \"" << nt << "\"" <<  std::endl;
				assert(i >= 0);
			}
			return i;
		}

		int pileup_t::ntCoverage() const {
			_updateStat();
			return _coverage;
		}

		int pileup_t::ntCount(const char nt) const {
			_updateStat();
			return _ntFreq[_ntidx(nt)];
		}

		int pileup_t::ntFwdCount(const char nt) const {
			_updateStat();
			return _ntFwdFreq[_ntidx(nt)];
		}

		int pileup_t::ntBwdCount(const char nt) const {
			_updateStat();
			return _ntFreq[_ntidx(nt)] - _ntFwdFreq[_ntidx(nt)];
		}

		double pileup_t::gmnNtQual(const char nt) const {
			_updateStat();
			return _gmnBqual[_ntidx(nt)];
		}

		double pileup_t::gmnAlQual(const char nt) const {
			_updateStat();
			return _gmnAqual[_ntidx(nt)];
		}

	}
}
