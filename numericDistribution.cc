/***************************************************************************
 *   Copyright (C) 2006, 2012 by Sven Bilke   *
 *   bilkes@mail.nih.gov   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <math.h>
#include "guli/numericDistribution.h"
namespace GULI {
	unsigned int distribution::CBDuseBeta = 16;
	int distribution::LNFACT_CACHESIZE = 4096;
	int distribution::FACT_CACHESIZE = 20;
	int distribution::cdHYPERGEOMETRIC_EXACT_TERMS = 10;
	double distribution::cdHYPERGEOMETRIC_APPROX_PREC = 1e-3;

	double distribution::FPMIN = 1e-50;
	double distribution::EPS = 3e-10;
	int distribution::MAXIT = 200;

	// A few forward declarations of local helper routines
	static double betacf(double a, double b, double x);
	static double gcf(double, double);
	static double gser(double, double);
	static void error(const char *errormessage);

	distribution::distribution() : fCacheTop(1){
		lnf_cache.resize(LNFACT_CACHESIZE);
		std::fill(lnf_cache.begin(), lnf_cache.end(), 0.);
		f_cache.resize(FACT_CACHESIZE);
		f_cache[0] = f_cache[1] = 1.;
	}

	/** Calculate the incomplete beta-function I_x(a,b)
	 **/
	double distribution::betai(double a, double b, double x) {
		if (x < 0. || x > 1.) error(
				"x out of range in incomplete beta function");
		double bt = (x == 0. || x == 1.0) ? 0. : exp(gammln(a + b) - gammln(a)
				- gammln(b) + a * log(x) + b * log(1.0 - x));

		return (x < (a + 1.0) / (a + b + 2.0)) ? bt * betacf(a, b, x) / a : 1.0
				- bt * betacf(b, a, 1.0 - x) / b;
	}

	/** logarithm of the gamma function */
	double distribution::gammln(double x) {
		static double cof[] = { 76.18009172947146, -86.50532032941677,
				24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
				-0.5395239384953e-5 };

		double y, tmp, ser;
		y = x;
		tmp = x + 5.5;
		tmp -= (x + 0.5) * log(tmp);
		ser = 1.000000000190015;
		for (int j = 0; j < 6; j++)
			ser += cof[j] / ++y;
		return -tmp + log(2.5066282746310005 * ser / x);
	}

	/* logarithm of the factorial */
	double distribution::lnfactorial(long int n) {
		if (n < 0) {
			std::cerr << "Negative value in factorial" << std::endl;
			assert(1 == -1);
		}
		if (n >= LNFACT_CACHESIZE) return gammln(n + 1.);
		return lnf_cache[n] ? lnf_cache[n] : (lnf_cache[n] = gammln(n + 1.));
	}

	/** A cached factorial calculator, it keeps the first "cachesize" factorial
	 *  in an array for re-use
	 */
	double distribution::factorial(int n) {
		if (n < 0) {
			error("Negative value in factorial");
			assert(-1 == 1);
		}
		if (n >= FACT_CACHESIZE) return exp(gammln(n + 1.));
		for (; fCacheTop <= n; fCacheTop++)
			f_cache[fCacheTop + 1] = f_cache[fCacheTop] * fCacheTop;
		return f_cache[n];

	}

	double distribution::binomial(unsigned long int n, unsigned long int k) {
		return floor(0.5 + exp(lnfactorial(n) - lnfactorial(k) - lnfactorial(n
				- k)));
	}

	double distribution::cdBinomial(double p, unsigned long int k,
			unsigned long int n) {
		if (k < 1) return 1.;
		if (n >= CBDuseBeta) return betai(k, n - k + 1., p);
		double sum = 0.;
		for (unsigned long int i = k; i <= n; i++)
			sum += binomial(n, i) * pow(p, i) * pow(1. - p, n - i);
		return sum;
	}

	double distribution::hypergeometric(unsigned long int N,
			unsigned long int S, unsigned long int n, unsigned long int s) {
		// N items, out of which S are "white", choose n (of N) bowls, return probability to find s "white"
		double r;
		r = lnfactorial(S) - lnfactorial(s) - lnfactorial(S - s);
		r += lnfactorial(N - S) - lnfactorial(n - s) - lnfactorial(N - S - n
				+ s);
		r -= lnfactorial(N) - lnfactorial(n) - lnfactorial(N - n);
		return exp(r);
	}

	double distribution::cdHypergeometric(unsigned long int N,
			unsigned long int S, unsigned long
			int n, unsigned long int s) {
		int upper = n < S ? n : S;
		long int delta = n - s;
		if (delta <= cdHYPERGEOMETRIC_EXACT_TERMS) {
			double sum = 0.;
			for (int i = s; i <= upper; i++)
				sum += hypergeometric(N, S, n, i);
			return sum;
		} else {
			double sum, last;
			last = 0.;
			sum = 0.;
			for (int i = s; i <= upper; i++) {
				double add, err;
				add = hypergeometric(N, S, n, i);
				sum += add;
				err = (add / sum) * (upper - i);
				if (err < cdHYPERGEOMETRIC_APPROX_PREC && add < last) return sum;
				last = add;
			}
			return sum;
		}
	}

	double distribution::incompleteGamma(double a, double x) {
		if (x < 0. || a <= 0.) error("Invalid parameters in incompleteGamma");
		if (x < (a + 1.)) return gser(a, x);
		return 1. - gcf(a, x);
	}

	double distribution::incompleteGamma1(double a, double x) {
		return 1. - incompleteGamma(a, x);
	}

	double distribution::errf(double x) {
		return x < 0. ? -incompleteGamma(0.5, x * x) : incompleteGamma(0.5, x
				* x);
	}

	void error(const char *message) {
		std::cerr << "Numerical error in distribution.cc: " << message
				<< std::endl;
	}

	/*****************************************************************************/
	/* It follow implementation specific functions.                              */
	/*****************************************************************************/

	static inline double checkPrecision(double &d) {
		return d = (fabs(d) < distribution::FPMIN) ? distribution::FPMIN : d;
	}

	/** A helper routine implementing the continued fraction evaluation method
	 **/
	double betacf(double a, double b, double x) {
		double c = 1.;
		double d = 1. - (a + b) * x / (a + 1.);
		checkPrecision(d);
		double h = d = 1. / d;
		for (int m = 1; m <= distribution::MAXIT; m++) {
			int m2 = 2 * m;
			double aa = m * (b - m) * x / ((a - 1. + m2) * (a + m2));
			d = 1. + aa * d;
			d = 1. / checkPrecision(d);
			c = 1. + aa / c;
			checkPrecision(c);
			h *= d * c;
			aa = -(a + m) * (a + b + m) * x / ((a + m2) * (a + 1. + m2));
			d = 1. + aa * d;
			d = 1. / checkPrecision(d);
			c = 1. + aa / c;
			checkPrecision(c);
			h *= d * c;
			if (fabs(d * c - 1.) < distribution::EPS) return h;
		}
		error("BetaCF: did not converge");
		return h;
	}

	/** A helper routine implementing the series expansion of the incomplete Gamma function
	 **/
	static double gser(double a, double x) {
		if (x == 0.) return 0.;
		double delta = 1. / a;
		double sum = delta;
		double ap = a;
		for (int i = 0; i < distribution::MAXIT; i++) {
			delta *= x / ++ap;
			sum += delta;
			if (fabs(delta) < fabs(sum) * distribution::EPS) return sum * exp(
					-x * a * log(x) - distribution::gammln(a));
		}
		error("gser: a too large or ITMAX too small");
		return 0.;
	}

	/** A helper routine implementing the continued fraction representation
	 ** of the incomplete Gamma function
	 **/
	static double gcf(double a, double x) {
		double b = x + 1. - a;
		double c = 1. / distribution::FPMIN;
		double d = 1. / b;
		double h = d;
		for (int i = 1; i <= distribution::MAXIT; i++) {
			b += 2.;
			d = -i * (i - a) * d + b;
			d = 1. / checkPrecision(d);
			c = b - i * (i - a);
			checkPrecision(c);
			h *= d * c;
			if (fabs(d * c - 1.) < distribution::EPS) return exp(-x * a
					* log(x) - distribution::gammln(a)) * h;
		}
		error("gcf: a too large or ITMAX too small");
		return 0.;
	}

}
