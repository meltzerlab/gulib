/*
 * TableBase.cc
 *
 *  Created on: Feb 21, 2015
 *      Author: sven
 */
#ifdef HAVE_WT
#include <limits.h>
#include <algorithm>
#include "guli/dboTableBase.h"
#include <Wt/Json/Parser.h>
#include <Wt/Json/Serializer.h>

const std::string  json_field_id = "_json_"; //

template <> const int &GULI::Dbo::tableBase::nullValue<int>()  { static const int R = INT_MIN;  return R; }
template <> const float &GULI::Dbo::tableBase::nullValue<float>() { static const float R = 1e30;  return R; }
template <> const double &GULI::Dbo::tableBase::nullValue<double>()  { static const double R = 1e30;  return R; }
template <> const std::string &GULI::Dbo::tableBase::nullValue<std::string>()  { static const std::string R ="_NULL_"; return R; }
#endif

