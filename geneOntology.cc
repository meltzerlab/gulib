/*
 * geneOntology.cc
 *
 *  Created on: Sep 26, 2012
 *      Author: sven
 */


// Initialize an empty geneOntology with the classes implicit in the GO file
#include "guli/geneOntology.h"
namespace GULI {
	void geneOntology::clear() {
		Dag::clear();

		static const string rootDsc = "The virtual root of the tree";
		static const string allDsc =  "A virtual ontology associated to all genes";
		static const string obsDsc =  "A virtual ontology collecting obsolete ontologies ";
		static const string unkDsc =  "A virtual ontology associated to otherwise unassigned genes";

		Node *ROOT     = new Node(rootId,     "root",    rootDsc);
		Node *ALL      = new Node(allId,      "All",     allDsc);
		Node *UNKNOWN  = new Node(unknownId,  "Unknown", unkDsc);
		Node *OBSOLETE = new Node(obsoleteId, "Obsolete",obsDsc);


		_addNode(ROOT);
		_addNode(ALL);
		_addNode(UNKNOWN);
		_addNode(OBSOLETE);

		ALL->addLink     (Link(*ROOT,     Link::INCOMING, (Link::userdataT) Node::linkType::PARTOF));
		UNKNOWN->addLink (Link(*ROOT,     Link::INCOMING, (Link::userdataT) Node::linkType::PARTOF));
		OBSOLETE->addLink(Link(*ROOT,     Link::INCOMING, (Link::userdataT) Node::linkType::PARTOF));
		ROOT->addLink    (Link(*ALL,      Link::OUTGOING, (Link::userdataT) Node::linkType::PARTOF));
		ROOT->addLink    (Link(*UNKNOWN,  Link::OUTGOING, (Link::userdataT) Node::linkType::PARTOF));
		ROOT->addLink    (Link(*OBSOLETE, Link::OUTGOING, (Link::userdataT) Node::linkType::PARTOF));

		return;
	}

	void geneOntology::textual2Node(textual &T) {
		clear();

		for (unsigned int i = 0; i < T.size(); i++) {
			textualNode &cur = T[i];
			if(exists(cur.identifier)) {
				std::cerr << "WARNING: Ontology " << cur.identifier << " defined more than once." << std::endl;
				continue;
			}

			// Create Node and put into data repository. Links will be filled in later
			_addNode(new Node(cur.identifier, cur.name, cur.description));
		}

		Node &root = *this->operator[](rootId);
		// fill in links
		for (unsigned int i = 0; i < T.size(); i++) {
			textualNode &curt = T[i];

			Node &curn = *dynamic_cast<Node *>((*this)[curt.identifier]);

			if (!(curt.isa.size() || curt.partof.size())) { // linked to root
				Link link2root(root, Dag::Link::INCOMING,  (Dag::Link::userdataT) Node::linkType::PARTOF);
				Link linkFroot(curn, Dag::Link::OUTGOING,  (Dag::Link::userdataT) Node::linkType::PARTOF);
				curn.addLink(link2root);
				root.addLink(linkFroot);
			}

			updateRelations(curn, curt.isa,      Dag::Link::INCOMING, Node::linkType::ISA);
			updateRelations(curn, curt.partof,   Dag::Link::INCOMING, Node::linkType::PARTOF);
			updateRelations(curn, curt.contains, Dag::Link::INCOMING, Node::linkType::CONTAINS);
		}
	}

	const string geneOntology::rootId = "ROOT";
	const string geneOntology::allId = "ALL";
	const string geneOntology::unknownId = "UNKNOWN";
	const string geneOntology::obsoleteId = "OBSOLETE";
} // NAMESPACE
