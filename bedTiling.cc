/*
 * bedTiling.cc
 *
 *  Created on: Oct 23, 2014
 *      Author: sven
 */
#include "guli/bedTiling.h"

namespace GULI {
	void bedTilingBase::closest(const bedRegion &B, targetListT &r, size_t maxdist) const {
		r.clear();
		if (!_tiling.size()) return;
		typename tileListT::const_iterator F = std::lower_bound(_tiling.begin(),
				_tiling.end(), B);
		if (F == _tiling.end()) {
			--F;
			if (F->chromosome() != B.chromosome()
					|| (size_t) B.outerDistance(*F) > maxdist) return;
			r = F->member();
			return;
		}
		if (F != _tiling.begin() && B.overlap(*(F - 1))) --F;

		if (F == _tiling.begin()
				|| labs(B.outerDistance(*F))
						< labs(B.outerDistance(*(F - 1)))) {
			// closest (or overlap) lies to the right,
			if (!B.overlap(*F)
					&& (F->chromosome() != B.chromosome()
							|| (size_t) labs(B.outerDistance(*F)) > maxdist)) return;
			unsigned int a = _newMagic();
			do {
				const targetListT &M = F->member();
				for (size_t s = 0; s < M.size(); ++s) {
					int i = M[s];
					if (_magic[i] == a) continue;
					_magic[i] = a;
					r.push_back(i);
				}
				++F;
			} while (F != _tiling.end() && B.overlap(*F));
			return;
		}

		// closest lies to the left
		--F;
		if (F->chromosome() != B.chromosome()
				|| (size_t) labs(B.outerDistance(*F)) > maxdist) return;
		r = F->member();
		return;
	}

	// return indices of bedRegions (utilizing the bedList order presented to this  object overlapping B
	void bedTilingBase::overlaps(const bedRegion &B, targetListT &r, unsigned int maxOuterDist) const {
		r.clear();
		if (!_tiling.size()) return;
		unsigned int a = _newMagic();
		auto F = std::lower_bound(_tiling.begin(), _tiling.end(), B);
		while(F != _tiling.begin() && _overlap(B, *(F - 1), maxOuterDist)) --F;
		for (; F != _tiling.end() && _overlap(B, *F, maxOuterDist); ++F) {
			const targetListT &M = F->member();
			for (size_t s = 0; s < M.size(); ++s) {
				int i = M[s];
				if (_magic[i] == a) continue;
				_magic[i] = a;
				r.push_back(i);
			}
		}
		return;
	}

	// return Indices of the TILES overlapping a bedRegion
	void bedTilingBase::overlapingTiles(const bedRegion &B, bedRegion::listT &r, unsigned int maxOuterDist) const {
		r.clear();
		if (!_tiling.size()) return;
		auto F = std::lower_bound(_tiling.begin(), _tiling.end(), B);
		while(F != _tiling.begin() && _overlap(B, *(F - 1), maxOuterDist)) --F;
		for (; F != _tiling.end() && _overlap(B, *F, maxOuterDist); ++F)
			r.push_back(*F);
		return;
	}

	void bedTilingBase::overlapingTiles(const bedRegion &B, targetListT &r, unsigned int maxOuterDist) const {
		r.clear();
		if (!_tiling.size()) return;
		auto F = std::lower_bound(_tiling.begin(), _tiling.end(), B);
		while(F != _tiling.begin() && _overlap(B, *(F - 1), maxOuterDist)) --F;
		for (; F != _tiling.end() && _overlap(B, *F, maxOuterDist); ++F)
			r.push_back(F - _tiling.begin());
		return;
	}

	bool bedTilingBase::doesOverlap(const bedRegion &b) const {
		targetListT r;
		overlaps(b, r);
		return 0 != r.size();
	}

}
