/***************************************************************************
 numericPca.cc  -  description
 -------------------
 begin                : Wed Mar 4 2004
 copyright            : (C) 2004-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <assert.h>
#include <math.h>
#include <string.h>
#include "guli/stdinc.h"
#include "guli/numericPca.h"
#include "guli/numericZscore.h"
#include "guli/dataIF.h"
#include "guli/dataMemarray.h"
#include <gsl/gsl_linalg.h>

#define F77NAME(a) a ## _
// We use LINPACK for the diagonalization of the correlation Matrix
extern "C" {
	int F77NAME(ilaenv)(int *, const char *, const char *, int *, int *, int *,
			int *, int, int);

	void F77NAME(dsyev)(char *, char *, long int *, double *, long int *,
			double *, double *, long int *, long int *);
}

namespace GULI {
		/** A helper class used to keep an array of double-precision numbers
		 ** it actually re-implements the data::memearry for doubles. The dataIF does
		 ** not easily allow to store double-prec numbers, but we need those because
		 ** LINPACK works on double-precision mem-arrays
		 **/
	namespace PCA {
		class dmemarray {
		public:
			~dmemarray() {
				delete[] _data;
			}
			dmemarray(unsigned int cols, unsigned int rows) :
				_cols(cols), _rows(rows) {
				int memsize = cols * rows;
				_entries_per_col = rows;
				_data = new double[memsize];
			}
			const unsigned int cols() const {
				return _cols;
			}
			const unsigned int rows() const {
				return _rows;
			}
			double &Data(int col, int row) {
				return _data[col * _entries_per_col + row];
			}
			double &operator()(int col, int row) {
				return Data(col, row);
			}
			double *memstart() {
				return _data;
			}
		private:
			double *_data;
			unsigned int _cols, _rows, _entries_per_col;
		};

		////////////////////////////////////////////////////////////////////////////
		// Eigenvectorization
		/////////////////////////////////////////////////////////////////////////////
		/** Diagonolize the Matrix stored in "evec" and return Eigenvectors in EVEC,
		 ** Eigen-Values in EVAL
		 **/
		int _diagonolize(dmemarray &evec, dmemarray &eval) {
			double *eigenvector = evec.memstart();
			double *eigenvalue = eval.memstart();

			long int N = evec.rows();
			char jobz = 'V';
			char uplo = 'L';
			long int info;
			long int lda = N;

			char opts[] = "U";
			char fname[] = "SSYTRD";
			int one = 1;
			int M = N;
			int O = N;
			int junk = -1;

			long int w = (F77NAME(ilaenv)(&one, fname, opts, &M, &O, &junk,
					&junk, strlen(fname), strlen(opts)) + 2) * N;

			double *Work = new double[w];

			F77NAME(dsyev)(&jobz, &uplo, &N, eigenvector, &lda, eigenvalue,
					Work, &w, &info);

			delete[] Work;
			return info ? 1 : 0;
		}

		classic::classic(const dataIF &D, int P) {
			_eval = new dmemarray(1, D.rows());
			_evec = new dmemarray(D.rows(), D.rows());
			_setupCorrelationMatrix(D, P);
			_diagonolize(*_evec, *_eval);
		}

		classic::~classic() {
			delete _eval;
			delete _evec;
		}

		void classic::_setupCorrelationMatrix(const dataIF &D, int P) {
			int i, j, k;
			dmemarray &EVEC(*_evec);
			const double N = (double) D.cols();
			const int Din = D.rows();
			dataMemarray TMP(D.cols(), D.rows());
			for (int i = 0; i < Din; i++) {
				switch (P) {
					case zscore:
						GULI::zscore(D.beginRow(i), D.endRow(i),
								TMP.beginRow(i));
						break;
					case center:
						zero_av(D.beginRow(i), D.endRow(i),
								TMP.beginRow(i));
						break;
					case raw:
						std::copy(D.beginRow(i), D.endRow(i), TMP.beginRow(i));
						break;
					default:
						assert(0);
						break;
				}
			}
			for (i = 0; i < Din; i++) {
				for (j = 0; j < Din; j++) {
					EVEC(i, j) = 0.;
					for (k = 0; k < N; k++) {
						EVEC(i, j) += TMP(k, i) * TMP(k, j);
					}
				}
			}
			return;
		}

		dataIF *classic::projectData(const dataIF &Dt, int DTarget, int P) {
			// Project down the data, keep only "DTarget" dimensions
			// Client must delete return-Matrix
			assert(Dt.rows() == _evec->rows());
			const int sx = _evec->cols() - 1;
			const int rows = _evec->rows();
			dmemarray &EVEC(*_evec);

			if ((unsigned int) DTarget > EVEC.cols()) DTarget = EVEC.cols();
			dataMemarray *result = new dataMemarray(Dt.cols(), DTarget);
			dataMemarray TMP(Dt.cols(), Dt.rows());

			for (unsigned int i = 0; i < Dt.rows(); i++)
			// Center zero data to be projected
			{
				switch (P) {
					case zscore:
						GULI::zscore(Dt.beginRow(i), Dt.endRow(i),
								TMP.beginRow(i));
						break;
					case center:
						zero_av(Dt.beginRow(i), Dt.endRow(i),
								TMP.beginRow(i));
						break;
					case raw:
						std::copy(Dt.beginRow(i), Dt.endRow(i), TMP.beginRow(i));
						break;
					default:
						assert(0);
						break;
				}
			}

			for (unsigned int i = 0; i < Dt.cols(); i++) {
				for (int j = 0; j < DTarget; j++) {
					(*result)(i, j) = 0.;
					for (int k = 0; k < rows; k++)
						(*result)(i, j) += static_cast<float> (EVEC(sx - j, k)
								* TMP(i, k));
				}
			}
			return result;
		}

		double classic::eigenValue(int i) {
			return (*_eval)(0, _evec->rows() - i - 1);
		}

		double classic::eigenVector(int vec, int comp) {
			return (*_evec)(_evec->cols() - vec - 1, comp);
		}

		/************************************************************************/
		/* Singular Value Decomposition                                         */
		/************************************************************************/

		svd::svd(const dataIF &D, int P) {
			_L = gsl_vector_alloc(D.cols());
			_V = gsl_matrix_alloc(D.cols(), D.cols());
			_A = gsl_matrix_alloc(D.rows(), D.cols());

			dataMemarray TMP(D.cols(), D.rows());
			for (unsigned int i = 0; i < D.rows(); i++) {
				switch (P) {
					case zscore:
						GULI::zscore(D.beginRow(i), D.endRow(i),
								TMP.beginRow(i));
						break;
					case center:
						zero_av(D.beginRow(i), D.endRow(i),
								TMP.beginRow(i));
						break;
					case raw:
						std::copy(D.beginRow(i), D.endRow(i), TMP.beginRow(i));
						break;
					default:
						assert(0);
						break;
				}
			}
			// Copy Data to gsl-matrix
			for (unsigned int i = 0; i < D.cols(); i++)
				for (unsigned int j = 0; j < D.rows(); j++)
					gsl_matrix_set(_A, j, i, TMP(i, j));
			gsl_linalg_SV_decomp_jacobi(_A, _V, _L);
		}

		svd::~svd() {
			gsl_matrix_free(_A);
			gsl_matrix_free(_V);
			gsl_vector_free(_L);
		}

		dataIF *svd::projectData(const dataIF &Dt, int DTarget, int P) {
			// Project down the data, keep only "DTarget" dimensions
			// Client must delete return-Matrix
			assert(Dt.rows() == _A->size1);
//			const int sx = _A->size2 - 1;
			const int rows = _A->size1;

			dataMemarray TMP(Dt.cols(), Dt.rows());
			for (unsigned int i = 0; i < Dt.rows(); i++)
			// Center zero data to be projected
			{
				switch (P) {
					case zscore:
						GULI::zscore(Dt.beginRow(i), Dt.endRow(i),
								TMP.beginRow(i));
						break;
					case center:
						zero_av(Dt.beginRow(i), Dt.endRow(i),
								TMP.beginRow(i));
						break;
					case raw:
						std::copy(Dt.beginRow(i), Dt.endRow(i), TMP.beginRow(i));
						break;
					default:
						assert(0);
						break;
				}
			}

			if ((unsigned int) DTarget > _A->size2) DTarget = _A->size2;
			dataMemarray *result = new dataMemarray(Dt.cols(), DTarget);

			for (unsigned int i = 0; i < Dt.cols(); i++) {
				for (int j = 0; j < DTarget; j++) {
					(*result)(i, j) = 0.;
					for (int k = 0; k < rows; k++)
						(*result)(i, j)
								+= static_cast<float> (eigenVector(j, k) * TMP(
										i, k));
				}
			}
			return result;
		}

		/****************************************************************/
		/* Kernel based PCA                                             */
		/****************************************************************/
		kernelBased::kernelBased(const dataIF &D, Kernel &k) {
			_kernel = &k;
			_eval = new dmemarray(1, D.cols());
			_evec = new dmemarray(D.cols(), D.cols());
			_setupCorrelationMatrix(D);
			_diagonolize(*_evec, *_eval);
		}

		kernelBased::~kernelBased() {
			delete _eval;
			delete _evec;
		}

		void kernelBased::_setupCorrelationMatrix(const dataIF &D) {
			int i, j, k, l;
			const int N = D.cols();
			const double M = 1 / (double) N;
			double M2, P;
			_generatorData = &D;
			dmemarray &EVEC(*_evec);
			// this (probably) is a HUGE matrix !
			// We store the correlation-matrix
			// temporarily in the Eigenvector-Matrix
			M2 = 0.;
			for (k = 0; k < N; k++) {
				for (l = 0; l < N; l++) {
					P = _kernel->DotProduct(*_generatorData, k,
							*_generatorData, l);
					M2 += P;
					EVEC(k, l) = P;
				}
			}
			M2 *= M * M;
			// Data has not been centered in Feature-Space.
			// Calculate CENTERED correlation-Matrix
			// K~ = K - 1/M ONE K - 1/M K ONE - 1/M^2 ONE K ONE
			// (ONE)ij = 1.
			// We have to optimize somewhat..... It may be  boring slow if we dont
			// do this !
			// OK thats an old remark. Since we rotated the matrix I dont expect
			// much trouble, but now it is optimized (and more difficult to read)...

			for (i = 0; i < N; i++) {
				P = 0.;
				for (k = 0; k < N; k++) {
					P += _kernel->DotProduct(*_generatorData, i,
							*_generatorData, k);
				}
				P *= -M;
				for (j = 0; j < N; j++) {
					EVEC(i, j) += P;
					EVEC(j, i) += P;
				}
			}

			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					EVEC(i, j) = (EVEC(i, j) + M2) * M;
				}
			}
			return;
		}

		dataIF *kernelBased::projectData(const dataIF &TestData, int TDim,
				int IGNORED) {
			dmemarray &EVEC(*_evec);
			const int Ntest = TestData.cols();
			const int Ngen = _generatorData->cols();

			const int D = TestData.rows();
			const int sx = EVEC.cols() - 1;
			const double M = 1 / (double) Ngen;

			double M2, P;
			dataIF *result;
			int i, j, k;

			vector<double> Kxj(Ngen);
			vector<double> Vj(Ngen);

			if (D != _generatorData->rows()) return 0;
			if ((unsigned int) TDim > EVEC.cols()) TDim = EVEC.cols();
			result = new dataMemarray(Ntest, TDim);
			for (unsigned int i = 0; i < result->cols(); i++)
				std::fill(result->beginCol(i), result->endCol(i), 0.);

			M2 = 0.;
			for (j = 0; j < Ngen; j++) {
				Kxj[j] = 0.;
				for (i = 0; i < Ngen; i++)
					Kxj[j] += _kernel->DotProduct(*_generatorData, i,
							*_generatorData, j);
				M2 += Kxj[j];
				Kxj[j] *= M;
			}
			M2 *= M * M;

			for (i = 0; i < Ntest; i++) {
				P = 0.;
				for (j = 0; j < Ngen; j++)
					P += _kernel->DotProduct(TestData, i, *_generatorData, j);
				for (j = 0; j < Ngen; j++)
					Vj[j]
							= _kernel->DotProduct(TestData, i, *_generatorData,
									j) - Kxj[j] - M * P + M2;

				for (k = 0; k < TDim; k++) {
					for (j = 0; j < Ngen; j++)
						(*result)(i, k) += static_cast<float> (EVEC(sx - k, j)
								* Vj[j]);
					(*result)(i, k) /= static_cast<float> (sqrt((*_eval)(0, sx
							- k) * (double) Ngen)); // Normalization of Eigenvectors
				}
			}
			return result;
		}

		double kernelBased::eigenValue(int i) {
			return (*_eval)(0, i);
		}
		double kernelBased::eigenVector(int i, int j) {
			return (*_evec)(i, j);
		}

		/******************************************************************/
		/* Implementation of Kernels                                      */
		/******************************************************************/
		/******************************************************************/
		/* Linear Kernel                                                  */
		/******************************************************************/
		kernelBased::Linear::Linear() {
			npar = 0;
		}

		double kernelBased::Linear::DotProduct(const dataIF &d1, int i, const dataIF &d2,
				int j) {
			int k;
			const int D = d1.rows();
			double r = 0.;

			for (k = 0; k < D; k++)
				r += d1(i, k) * d2(j, k);
			return r;
		}

		/******************************************************************/
		/* Polynomial Kernel                                              */
		/******************************************************************/
		kernelBased::Polynomial::Polynomial() {
			npar = 1;
			parameter = new double;
			defaultvalue = new double;
			*defaultvalue = 1.;
		}

		kernelBased::Polynomial::~Polynomial() {
			delete parameter;
			delete defaultvalue;
		}

		double kernelBased::Polynomial::DotProduct(const dataIF &d1, int i,
				const dataIF &d2, int j) {
			int k;
			const int D = d1.rows();
			double r = 0.;

			for (k = 0; k < D; k++)
				r += d1(i, k) * d2(j, k);

			r = pow(r / (double) D, parameter[0]);
			return r;
		}

		/******************************************************************/
		/* Gaussian Kernel                                                */
		/******************************************************************/
		kernelBased::Gaussian::Gaussian() {
			npar = 1;
			parameter = new double;
			defaultvalue = new double;
			*defaultvalue = 1.;
		}

		kernelBased::Gaussian::~Gaussian() {
			delete parameter;
			delete defaultvalue;
		}

		double kernelBased::Gaussian::DotProduct(const dataIF &d1, int i, const dataIF &d2,
				int j) {
			int k;
			const int D = d1.rows();
			double r = 0.;

			for (k = 0; k < D; k++)
				r += (d1(i, k) - d2(j, k)) * (d1(i, k) - d2(j, k));

			r = exp(-r / (2. * *parameter * (double) D));
			return r;
		}

		/******************************************************************/
		/* Sigmoid               								          */
		/******************************************************************/
		kernelBased::Sigmoid::Sigmoid() {
			npar = 2;
			parameter = new double[2];
			defaultvalue = new double[2];
			defaultvalue[0] = 1.;
			defaultvalue[1] = 0.;
		}

		kernelBased::Sigmoid::~Sigmoid() {
			delete[] parameter;
			delete[] defaultvalue;
		}

		double kernelBased::Sigmoid::DotProduct(const dataIF &d1, int i, const dataIF &d2,
				int j) {
			int k;
			const int D = d1.rows();
			double r = 0.;

			for (k = 0; k < D; k++)
				r += d1(i, k) * d2(j, k);

			r /= (double) D;
			r = tanh(parameter[0] * r + parameter[1]);
			return r;
		}
	} // NAMESPACE
} // NAMESPACE
