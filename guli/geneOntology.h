/*
 * geneOntology.h
 *
 *  Created on: Sep 26, 2012
 *      Author: sven
 */

#ifndef GENEONTOLOGY_H_
#define GENEONTOLOGY_H_

#include "dag.h"
#include "dagIterator.h"
namespace GULI {
	class geneOntology : public Dag {
	public:
		/** The definition of a Gene Ontology Node
		 **/
		class Node : public Dag::Node {
		public:
			Node(const string id, const string name, const string dsc) :
	                    	  _id(id),  _name(name), _dsc(dsc) {}

			virtual ~Node()  {}
			/** The name of this ontology  **/
			virtual const string &name() const { return _name; }
			/** The description (definition in GOs wordings)  **/
			virtual const string &description() const { return _dsc; }
			virtual const string &id() const { return _id; }
			enum  class linkType { ISA, PARTOF, CONTAINS };
		private:
			string  _id, _name, _dsc;
		};


		/** Definition of the TEXT representation for Ontologies. This is
		 ** (at least for now) the interface to any I/O Routine for Ontologies.
		 ** Every implementation of the ontology  API defined below is required
		 ** to implement import (and one day export) functionality
		 ** to this least common denominator
		 **/
		typedef struct  {
			string name;
			string description;
			string identifier;
			vector<string> isa, partof, contains;
		} textualNode;
		typedef vector<textualNode> textual;


		/** Translate a textual representation to the internal representation
		 **/
		void textual2Node(textual &);

		// virtual textual &Node2textual() = 0;

		/** Clear the Ontology, setup a minimal Ontology (ALL, UNKNOWN)
		 */
		void clear();

		virtual ~geneOntology() {}

		static const string rootId, allId, unknownId, obsoleteId;

		typedef dagTemplates::forwardIterator<Node>  forwardIterator;
		typedef dagTemplates::backwardIterator<Node> backwardIterator;
		typedef dagTemplates::uniqueForwardIterator<Node>  uniqueForwardIterator;
		typedef dagTemplates::uniqueBackwardIterator<Node> uniqueBackwardIterator;

		Node *root() const {
			return this->operator[](rootId);
		}

		Node *end() const {
			return (Node *) -1;
		}

		Node *operator[](string s) const {
			return dynamic_cast<Node *> (Dag::operator[](s));
		}

	private:
//		dagIterator::StaticIterator _root_it, _end_it;

	};


}	 // NAMESPACE
#endif /* GENEONTOLOGY_H_ */
