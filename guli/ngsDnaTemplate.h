/*
 * ngsDnaTemplate.h
 *
 *  Created on: Dec 12, 2013
 *      Author: sven
 */

#ifndef NGSDNATEMPLATE_H_
#define NGSDNATEMPLATE_H_
#include <string>
#include "bedRegion.h"
namespace GULI {
	namespace NGS {

		class DNAtemplate {
		public:
			class Read {
			public:
				typedef std::vector<uint32_t> cigarT;
				typedef struct seg_core_t {
				        int32_t  tid;
				        int32_t  pos;
				        uint32_t flag:16, bin:16;
				        int32_t  isize;
				        cigarT   cigar;
				        vector<uint8_t> auxData;
				        uint8_t  qual;
				        seg_core_t(bam1_t *, bool keepAux=false);
				} seg_core_t;

				Read(const Read &R);
				virtual ~Read();
				size_t nSegment(size_t al)                      const { return _alns.size() > al ? _alns[al]->size() : 0; }
				const seg_core_t  &segment(size_t al, size_t s) const { return (*_alns[al])[s]; }

				uint32_t seqlen() 								const { return _qual.size(); }
				uint8_t seqi(size_t qpos,  size_t seg, size_t al) const {
					const size_t _seqlen = _qual.size();
					if(qpos >= _seqlen) return 0xF;
					if(segment(al,seg).flag & BAM_FREVERSE) {
						size_t p1 = _seqlen - qpos - 1;
						uint8_t nt = (_seq[p1 >> 1] >> ( (p1 & 0x1)   ? 0 : 4)) & 0xF;
						return _complement[nt];
					} else {
						return (_seq[qpos >> 1]    >> ( (qpos & 0x1)  ? 0 : 4)) & 0xF;
					}
				}
				static const uint8_t    _complement[];

			private:
				typedef std::vector<seg_core_t>  _alignmentDataT;
				vector<_alignmentDataT *> _alns;
				std::vector<uint8_t>    _seq, _qual;
				bool                    _keepSequence;
				bool                    _keepAux;

				friend class DNAtemplate;
				Read(vector<vector<bam1_t *> > &segments, bool keepSequence=false, bool keepAux=false);
				Read(bam1_t * segment, bool keepSequence=false, bool keepAux=false);
				bam1_t                  *_bam(size_t s, size_t al, const char *id) const;
		        void                     _covers(bedRegion::listT &R, size_t al, size_t seg) const;
				size_t _nAL()                                   const { return _alns.size(); }
			};

			virtual ~DNAtemplate();
			DNAtemplate(std::vector<bam1_t *> &alignments, bool keepSequence=false,  bool keepAuxFields=false);
			DNAtemplate(bam1_t *b, bool keepSequence=false,  bool keepAuxFields=false);
			DNAtemplate(const DNAtemplate &);
			const std::string &id()  const        { return _id; }
			const Read &read(bool rd)       const { return *_R[rd ? 1 : 0]; }
			bam1_t *read(bool rd, size_t al, size_t s) const;
			size_t nAlignment() const;
			// return the outline of segment rd,al,s, the ORIENTATION of the region is
			// with respect to the FIRST read (i.e. we hard-code in that the mate-pair
			// reads in the opposite direction and swap back to the first read orientation!)
			int covers(bool rd, size_t al, size_t s, char **chrIds, bedRegion::listT &) const;
		private:
			std::string _id;
			Read       *_R[2];
		};
	}
}




#endif /* NGSDNATEMPLATE_H_ */
