/*
 * peakfinderMode.h
 *
 *  Created on: Dec 26, 2008
 *      Author: sven
 */

#ifndef __OPERATION_MODE_H_
#define __OPERATION_MODE_H_
#include "stdinc.h"
#include "uiParms.h"
#include <map>

/**
 * This object is used to define the major operation modes of the program. It essentially is a
 * main() routine decorated with a id() method to name the mode and a brief description.
 * Instantiating a peakfinderMode derived object will register the module and make it available
 * for lookup by the static \a mode(string id) method.
 **/

namespace GULI {
	class uiOperationMode {
	public:

		uiOperationMode(const string &i, const string dsc = "No description provided") :
			_id(i), _dsc(dsc) {
			_regstr();
		}
		virtual ~uiOperationMode() {
			_unregstr();
		}

		virtual int main(int argc, char **argv) = 0;

		virtual string describeMode() {
			return _dsc;
		}

		const string &id() {
			return _id;
		}

		static vector<string> knownModes();
		static uiOperationMode *mode(const string &id);
		static int entryPoint(int argc, char **argv);

// Some useful definitions
		typedef uiParms::Variable<char> charvarT;
		typedef uiParms::Variable<int> intvarT;
		typedef uiParms::Variable<double> doublevarT;
		typedef uiParms::Variable<string> stringvarT;
		typedef uiParms::Variable<bool> boolvarT;
		typedef uiParms::VariableVector<int> intvecT;
		typedef uiParms::VariableVector<double> doublevecT;
		typedef uiParms::VariableVector<string> stringvecT;
		typedef uiParms::VariableVector<bool> boolvecT;
		typedef uiParms::TouchVar touchVarT;

	protected:
		virtual const string &_modeId() { return _id; }

	private:
		void _regstr();
		void _unregstr();
		typedef std::map<string, uiOperationMode *> sT;
		static sT *instances;
		const string _id, _dsc;
	};

	class uiHelpMode: public uiOperationMode {
	public:
		uiHelpMode() :
			uiOperationMode("help") {
		}
	private:
		static const unsigned int _TABSIZE_ = 8;
		virtual string describeMode() {
			return "Guess What";
		}

		virtual int main(int argc, char **argv) {
			std::cerr << "Available modes are:" << std::endl;
			vector<string> m = knownModes();
			for (unsigned int i = 0; i < m.size(); ++i) {
				std::cerr << m[i] << "\t";
				if (m[i].length() < _TABSIZE_) std::cerr << "\t";
				std::cerr << "\t" << uiOperationMode::mode(m[i])->describeMode();
				std::cerr << std::endl;
			}
			return 0;
		}
	};

	class uiVersionMode: public uiOperationMode {
	public:
		uiVersionMode(const string &v) :
			uiOperationMode("version"),
			_versionInfo(v) {}
		uiVersionMode() :
			uiOperationMode("version"),
			_versionInfo(std::string("Compiled at ") + __DATE__ + " at " + __TIME__) {}
	private:
		virtual string describeMode() {
			return "Print version information";
		}

		virtual int main(int argc, char **argv) {
			std::cerr << _versionInfo << std::endl;
			return 0;
		}
		const string _versionInfo;
	};

}
#endif /* PEAKFINDERMODE_H_ */
