/***************************************************************************
 ontology.h  -  description
 -------------------
 begin                : Wed Nov 19 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2012 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_DAG
#define __GULI_DAG
#include <unordered_map>
#include "guliconfig.h"
#include "stdinc.h"

namespace GULI {
	class Dag {
	protected:
		class baseIterator;
		class uniqueBaseIterator;
		class uniqueIteratorState;
	public:
		class forwardIterator;
		class backwardIterator;
		class uniqueForwardIterator;
		class uniqueBackwardIterator;
		class Node;

		class Link  {
		public:
			typedef enum {
				INCOMING, OUTGOING
			} directionT;
			typedef void *userdataT;

			template<class it> Link(it &ref, directionT d, userdataT u = 0) :
					 _target ((Node *) ref), _userdata(u), _direction(d) {
			}

			directionT direction() const {
				return _direction;
			}
			userdataT &userdata() {
				return _userdata;
			}
			userdataT userdata() const {
				return _userdata;
			}

			operator Node *()  const {return _target;  }

			Node &operator*()  const {return  *_target;}

			Node *operator->() const {return  _target; }

			template<class it> bool operator==(it &B) const {
				return _target == (Node *) B;
			}
			template<class it> bool operator!=(it &B) const {
				return _target != (Node *) B;
			}

		protected:
			Node *_target;
			userdataT _userdata;
			directionT _direction;
		};

		class Node {
		public:
			virtual ~Node() {}

			Node() : _userVariable(0) {}

			virtual const string &id() const = 0;

			/** The template API to make this a static iterator
			 **/


			template<class it> bool operator==(const it &B) const {
				return this  == B.target();
			}

			template<class it> bool operator!=(const it &B) const {
				return this != B.target();

			}

			size_t nIncoming() const {
				return _incoming.size();
			}

			size_t nOutgoing() const {
				return _outgoing.size();
			}

			const Link &incoming(size_t n) const {
				return _incoming[n];
			}
			const Link &outgoing(size_t n) const {
				return _outgoing[n];
			}

			operator Node *() { return this; }

			void addLink(Link l) {
				if (l.direction() == Link::INCOMING) _incoming.push_back(l);
				else _outgoing.push_back(l);
			}

			void setUserVariable(void *v) { _userVariable = v; }
			void *getUserVariable()       { return _userVariable; }
		protected:

		private:
			vector<Link> _incoming, _outgoing;
			void *_userVariable;
		};

		Dag() {}
		virtual ~Dag() {
			clear();
		}

		virtual void clear();

		/** The number of DAG nodes
		 **/
		unsigned int size() const {
			return _data.size();
		}

		/** Get DAG Node with identifier id
		 **/
		Node *operator[](const string &id) const {
			_storageT::const_iterator f = _data.find(id);
			if(f == _data.end()) return 0;
			return f->second;
		}

		void addLink(const string &id, Link l) { this->operator[](id)->addLink(l); }

		virtual bool exists(const string &id) const {
			_storageT::const_iterator f = _data.find(id);
			return f != _data.end();
		}

	private:
		void _updateRelations(Node &cur, const vector<string> &t, Link::directionT d, Link::userdataT);

	public:
		template <class udat> void updateRelations(Node &cur, const vector<string> &t, Link::directionT d, udat u) {
			Dag::_updateRelations(cur, t, d, (Link::userdataT) u);
		}

	protected:
		typedef std::unordered_map<string, Node *> _storageT;

		// provide an extra id if the lookup-id in the hash differs from the Node-id
		Node *_addNode(Node *N, string id="") {
			if(id=="") id = N->id();
			_storageT::const_iterator f = _data.find(id);
			assert(f == _data.end()); // require NEW node id
			_data[id] = N;
			return N;
		}

	private:
		_storageT _data;
	};

} // NAMSEPACE GULI

#endif
