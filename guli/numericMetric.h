/***************************************************************************
 numericMetric.h  -  description
 -------------------
 begin                : Fri Jan 30 2004
 copyright            : (C) 2004-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_NUMERIC_METRIC_H
#define __MULI_NUMERIC_METRIC_H
#include <math.h>
#include "guliconfig.h"
#include "numericDscstat.h"

namespace GULI {
		class euclidean {
		public:
			typedef float unit;
			template<class it1, class it2>
			unit operator()(it1 b1, it1 e1, it2 b2) {
				unit sum = 0;

				while (b1 < e1) {
					float d1 = *b1;
					float d2 = *b2;
					sum += (d1 - d2) * (d1 - d2);
					++b1;
					++b2;
				}
				return sqrt(sum);
			}
		};

		class manhatten {
		public:
			typedef float unit;
			template<class it1, class it2>
			unit operator()(it1 b1, it1 e1, it2 b2) {
				unit sum = 0;

				while (b1 < e1) {
					float d1 = *b1;
					float d2 = *b2;
					sum += sqrt((d1 - d2) * (d1 - d2));
					++b1;
					++b2;
				}
				return sum;
			}
		};

		class pearson {
		public:
			typedef float unit;
			template<class it1, class it2>
			unit operator()(it1 b1, it1 e1, it2 b2) {
				int n = e1 - b1;
				unit sum = 0;
				dscstat A(b1, e1);
				dscstat B(b2, b2 + n);

				while (b1 < e1) {
					float d1 = static_cast<float> (*b1 - A.average());
					float d2 = static_cast<float> (*b2 - B.average());
					sum += d1 * d2;
					++b1;
					++b2;
				}
				sum /= n;
				sum /= static_cast<float> (sqrt(A.variance() * B.variance()
						+ 1e-12));
				// Regularize constant data
				return sum;
			}
		};

		class uncenteredCorr {
		public:
			typedef float unit;
			template<class it1, class it2>
			unit operator()(it1 b1, it1 e1, it2 b2) {
				int n = e1 - b1;
				unit sum = 0;
				while (b1 < e1) {
					sum += *b1 * *b2;
					++b1;
					++b2;
				}
				sum /= n;
				return sqrt(sum);
			}
		};

} //NAMESPACE
#endif
