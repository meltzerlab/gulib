/***************************************************************************
 *   Copyright (C) 2005-2007 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __RANDOM_SEQUENCESET_H
#define __RANDOM_SEQUENCESET_H
#include "stdinc.h"
#include "seqsetmulti.h"

/** Select random subsets of sequences. Note that re-visiting the same set-index does not
 * guarantee to get the same set of sequences. **/
class seqSetRandom: public seqMultiSet {
public:
	seqSetRandom(int provideSets = -1, const seqSet *source = 0);
	virtual ~seqSetRandom();

	void setSubsetSize(int i) {
		_n_selected = i;
	}
	void setNSet(unsigned int i) {
		_nset = i;
	}
	void setSource(const seqSet *s) {
		_data = s;
	}
	virtual size_t size() const {
		return _n_selected;
	}

	virtual const sequence operator[](int i) const {
		const seqSet &D = *_data;
		return D[_index[i]];
	}

	virtual int nSet() const{ return _nset; }
	virtual void selectFirst() const {
		_randomize();
		_cur = 0;
	} // select first sub-set
	virtual bool select(const string &) const {
		_randomize();
		_cur++;
		return true;
	}
	virtual void next(int stepsize) const {
		_randomize();
		_cur += stepsize;
	}
	virtual bool end() const {
		return (!_data) || ((_cur > 0) && (_cur > _nset));
	}
	//    virtual unsigned int index(unsigned int i) const { return _index[i]; }
	void transparent();

private:
	void _randomize() const;
	const seqSet *_data;
	mutable vector<int> _index;
	mutable int _n_selected, _nset, _cur;
};

istream &operator>>(istream &A, seqSetRandom &b);
ostream &operator<<(ostream &A, seqSetRandom &b);
#endif
