/*
 * wiggleData.h
 *
 *  Created on: Mar 1, 2009
 *      Author: sven
 */

#ifndef WIGGLEDATA_H_
#define WIGGLEDATA_H_
#include "utilIdMap.h"

namespace GULI {
	struct _wiggleDataState;
	class wiggleData  {
	public:
		typedef float gvalueT;
		typedef vector<gvalueT> gvalueLT;

		wiggleData();
		virtual ~wiggleData();

		int openDir(const string &);

		void addFile(const string &chromosome, const string &fnam,
				int sizeHint = -1);

		const gvalueLT &density(const string &chr) const;
		/** free memory allocated for density */
		virtual void relaxDensity(const string &chr) const;
		virtual int maxpos(const string &chr) const;
		gvalueT nHits(const string &chr) const;

	private:
		struct _wiggleDataState {
			vector<wiggleData::gvalueLT> density;
			vector<string> fnames;
			vector<bool> loaded;
			vector<int> sizeHint;
			vector<int> maxPos;
			vector<float> nHits;
		};
		mutable struct _wiggleDataState _c;
		utilIdMap _chrMap;
	};
}
#endif /* WIGGLEDATA_H_ */
