/*
 * errorcodes.h
 *
 *  Created on: Dec 20, 2008
 *      Author: sven
 */

#ifndef ERRORCODES_H_
#define ERRORCODES_H_

#include <string>
#define ESUCCESS       0
#define ENOTFOUND      1
#define ECANNOTREAD    2
#define ECREATE        3
#define EUNKNOWNFORMAT 4
#define EINSUFFARG     5
#define ENEEDHELP      6
#define EUNKNOWNOPTION 7
#define ESYNTAX        8
#define EWAITING       0
#define EFILESDONTMATCH   8



namespace GULI {
	class guliException : public std::exception {
	public:
		guliException(std::string errmsg, int code, std::string extramessage="") :
			_err(errmsg), _extra(extramessage), _code(code) {} ;
		~guliException() throw() {}
		const std::string errmsg() const { return _err; }
		const std::string extramsg() const { return _extra; }
		int   errcode() { return _code; }
	private:
		std::string _err, _extra;
		int _code;
	};

	class guliNotFoundException : public  guliException {
	public:
		guliNotFoundException(std::string msg="") : guliException("Not Found", ENOTFOUND, msg){}
	};



}
#endif /* ERRORCODES_H_ */

