/***************************************************************************
 zscore.h  -  description
 -------------------
 begin                : Fri Jan 30 2004
 copyright            : (C) 2004-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004-2005 by Sven Bilke                               *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_ZSCORE
#define __GULI_ZSCORE
#include <algorithm>
#include "numericDscstat.h"

namespace GULI {
	struct _zscore: public std::unary_function<double, double> {
		_zscore(double av, double stddev) :
			_av(av), _stddev(stddev) {
		}
		double operator()(double v) {
			return (v - _av) / _stddev;
		}
		double _av, _stddev;
	};

	/** Z-Transform a vector, the output is stored in target **/
	template<class inIt, class outIt>
	void zscore(inIt begin, inIt end, outIt target) {
		dscstat A(begin, end);
		_zscore Z(A.average(), A.stddev());
		std::transform(begin, end, target, Z);
		return;
	}

	/** Z-Transfrom a vector in place **/
	template<class inIt>
	void zscore(inIt begin, inIt end) {
		zscore(begin, end, begin);
	}

	template<class inIt, class outIt>
	void zero_av(inIt begin, inIt end, outIt target) {
		dscstat A(begin, end);
		_zscore Z(A.average(), 1.);
		std::transform(begin, end, target, Z);
		return;
	}

	/** Zeor-Averag-Transform a vector in place **/
	template<class inIt>
	void zero_av(inIt begin, inIt end) {
		zero_av(begin, end, begin);
	}
} //namespace SB::statistic
#endif
