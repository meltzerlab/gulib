/*
 * ngsBamRandomAccessPileup.h
 *
 *  Created on: Jan 19, 2013
 *      Author: sven
 */

#ifndef NGSBAMRANDOMACCESSPILEUP_H_
#define NGSBAMRANDOMACCESSPILEUP_H_
#include "ngsBamPiler.h"

namespace GULI {
	namespace NGS {
		class randomAccessPileup: public piler, private piler::streamingClient {
		public:
			randomAccessPileup();
			virtual ~randomAccessPileup(){};
			virtual void suggestReadahead(int i);


			const pileup_t &get(int tid, int pos, size_t s);
			bool get(pileup_t &result, int tid, int pos, const char *rg,
					size_t s);
		protected:
			virtual void _flush();
			typedef vector<pileup_t> _cacheT;
			vector<_cacheT> _cache;
			int _cache_tid, _cache_start, _cache_len;

		private:
			virtual pileup_t *pilerStreamingSetup(uint32_t tid, uint32_t pos, size_t fileIdx, bool &manage);
		};

	}
}

#endif /* NGSBAMRANDOMACCESSPILEUP_H_ */
