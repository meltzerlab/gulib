/***************************************************************************
 ontologyIteratorBasic.h  -  description
 -------------------
 begin                : Wed Nov 19 2003
 copyright            : (C) 2003-2009 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_ONTOLOGY__ITERATOR_BASIC_H
#define __GULI_ONTOLOGY__ITERATOR_BASIC_H
#include <iterator>
#include "guliconfig.h"
#include <unordered_map>
#include "stdinc.h"
#include "dag.h"

namespace GULI {

	 /** A base class suitable for many type of iterators traversing the ontology tree.
	 * it contains definition of a "status" type and a stack, which is useful in most
	 * cases to traverse the tree. If this is not of use for you, you are probably better up
	 * re-implementing the iterator from the scratch rather than  deriving from here!
	 * The class contains a "template" API (index(), getDag()) which should be present
	 * in all iterators if you plan to interact (e.g. instantiate from) with this class.
	 * In this way we have simple means to exchange the _current_ location (but not
	 * direction information) between different types of iterators. Samples are the
	 * staticIterator and the Node itself.
	 **/

	class Dag::baseIterator
		: public std::iterator<std::forward_iterator_tag, Node, int, Node *, Node &> {
	public:
		baseIterator() :   _dir(Dag::Link::INCOMING) {
			__current.target = 0;
		}

		/** The copy constructor for iterators following the same template API
		 **/
		template<class it> 	baseIterator(it &B, Dag::Link::directionT d) :  _dir(d) {
			this->assign(B, d);
		}

		baseIterator(Dag::Link::directionT d) : _dir(d) {}

		virtual ~baseIterator()  {}

		/* Assignment operation from an iterator in a different mode. This does NOT copy
		 * the "kinetic" information, i.e. increasing the iterator (even in the same mode)
		 * can lead to different results
		 */
		template<class it> baseIterator &assign(it &B, Dag::Link::directionT dir)
		{
			__current.target =  (Node *) B;
			__current.aidx = 0;
			__stack.clear();
			_dir = dir;
			return *this;
		}

		template<class it> 	bool operator==(const it &B) const {
			return __current.target == (Dag::Node *) B;
		}

		template<class it> 	bool operator!=(const it &B) const {
			return __current.target != (Dag::Node *) B;
		}

		Dag::Node &operator*()  const {return  *__current.target;}

		Dag::Node *operator->() const {return  __current.target; }

		virtual void increment();

		/** The <template> API
		 **/
		operator Dag::Node *() const { return __current.target; }

		Dag::Link::directionT dir() const { return _dir; }

	protected:
		/** The assign operator works ONLY for iterators operating in the same mode. Use the
		 * assign method to operate on iterators in different mode. Forward from the derived class
		 * operator= to use type-checking at compile time.
		 **/
		virtual void equal_assign(const baseIterator &B) {
			__current.target = B.__current.target;
			__current.aidx = B.__current.aidx;
			__stack = B.__stack;
			_dir = B._dir;
		}

		typedef struct {
			Dag::Node *target;
			int aidx; // Ancestor index
		} cur;

		cur __current;
		std::vector<cur> __stack;

		cur &_current()  { return __current; }
		std::vector<cur> &_stack() { return __stack; }

		virtual bool visited() const {
			return false;
		}
		virtual void markVisited()  {
			return;
		}
	private:
		Dag::Link::directionT _dir;
	};

	/** The straight forward Downward (to root) Iterator potentially
	 ** visiting a Dag::Node several times
	 **/
	class Dag::forwardIterator : public baseIterator {
	public:
		/** Forward Constructors to the base class
		 **/
		template<class it> 	forwardIterator(const it &B) : baseIterator(B, Dag::Link::OUTGOING) {	}
		forwardIterator(const forwardIterator &B) : baseIterator() { *this = B; }
		virtual ~forwardIterator(){}

		/** Implementation of methods returning Downward Iterators
		 **/
		forwardIterator operator++(int) {
			forwardIterator r(*this);
			increment();
			return r;
		}
		forwardIterator &operator++() {
			increment();
			return *this;
		}
		forwardIterator &operator=(const forwardIterator &B) {
			equal_assign(B);
			return *this;
		}
	};

	/** The straight forward Upward (to leafs) Iterator potentially
	 ** visiting a Dag::Node several times
	 **/
	class Dag::backwardIterator : public baseIterator {
	public:
		/** Forward Constructors to the base class
		 **/
		backwardIterator() {}
		template<class it> 	backwardIterator(const it &B) : baseIterator(B, Dag::Link::INCOMING) {}

		backwardIterator(const backwardIterator &B) : baseIterator() {
			*this = B;
		}

		virtual ~backwardIterator() {}

		backwardIterator operator++(int) {
			backwardIterator r(*this);
			increment();
			return r;
		}
		backwardIterator &operator++() {
			increment();
			return *this;
		}
		backwardIterator &operator=(const backwardIterator &B) {
			equal_assign(B);
			return *this;
		}
	};

	class Dag::uniqueIteratorState {
	public:
		uniqueIteratorState() : _magic(0) {}

		~uniqueIteratorState() {
			releaseBuffer();
		}

		void releaseBuffer() {
			_buffer.clear();
		}
		unsigned int newMagic();

		void mark(Node *N) {
			_buffer[N->id().c_str()] = _magic;
		}

		bool state(Node *N) const {
			_hashT::const_iterator f = _buffer.find(N->id().c_str());
			if(f == _buffer.end()) return false;
			return f->second == _magic;
		}
	private:
		// The threshold to re-init the magic buffer. Almost certainly we will never get there, anyway.
		static const unsigned int reinit_magic = 2147483645;
		typedef std::unordered_map<const char *, unsigned int> _hashT;
		_hashT _buffer;

		unsigned int _magic;
	};

	/** A iterator base class providing elementary means to visit every node
	 * only once.
	 **/
	class Dag::uniqueBaseIterator : public Dag::baseIterator {
	protected:
		typedef Dag::baseIterator itBase_t;

		uniqueBaseIterator (Dag::Link::directionT d) : Dag::baseIterator(d) {
			_b.newMagic();
		}

		/** The copy constructor is here only for completeness. It generates an error message !
		 *  The iterator is unique, so you should (and can) not copy this type of iterator
		 **/
		uniqueBaseIterator (const uniqueBaseIterator  &)  {
			int unique_copy_constructor_called = -1;
			assert(unique_copy_constructor_called > 0);
		}

		virtual ~uniqueBaseIterator () {};

		/** Return true if THIS iterator has visited this node before, mark as visited.
		 **/
		virtual bool visited() const { return _b.state(this->operator->()); }

		virtual void markVisited()  { _b.mark(this->operator->()); }


	public:
		using itBase_t::_stack;
		using itBase_t::_current;

		/** Assign multiple start-points to the iterator. This is useful to visit
		 ** all dag nodes
		 **/
		template<typename itit> void massign(const itit &begin, const itit &end) {
			itit cur;

			_stack().clear();
			_current().aidx = -1;

			// initialize to end() in  case we got an empty list
			_current().target = (Node *) -1;
			for (cur = begin; cur != end; ) {
				_current().target = (Node *) *cur;
				if(++cur != end) _stack().push_back(_current());  // all but the last go onto the stack
			}
			_current().aidx = 0;
			_b.newMagic();
			return;
		}

	protected:
		void newMagic() {
			_b.newMagic();
		}
	private:
		uniqueIteratorState _b;
	};

	/** An iterator traversing DOWNWARDS all possible path's to the root.
	 *  It visits every node exactly once.
	 **/
	class Dag::uniqueForwardIterator : public Dag::uniqueBaseIterator  {
	public:
		// It is not possible to copy a unique (!) iterator ! We use the copy constructor for ASSIGNMENT
		template<typename it> uniqueForwardIterator(it B) : uniqueBaseIterator (Dag::Link::OUTGOING) {
			assign(B);
		}

		template<class itit> uniqueForwardIterator(const itit &begin, const itit &end)
							 : uniqueBaseIterator (Dag::Link::OUTGOING) {
			massign(begin, end);
		}


		virtual ~uniqueForwardIterator() {}

		/** We support ONLY pre-increment, because we can not copy a unique iterator
		 **/
		uniqueForwardIterator &operator++() {
			static_cast<baseIterator * >(this)->increment();
			return *this;
		}

		template<class it> 	void assign(it &B) {
			baseIterator::assign(B, Dag::Link::OUTGOING);
			uniqueBaseIterator ::newMagic();
		}

	protected:
		friend class uniqueBaseIterator ;
		uniqueForwardIterator() : uniqueBaseIterator (Dag::Link::OUTGOING) {}
	};

	/** An iterator traversing DOWNWARDS all possible path's to the root.
	 *  It visits every node at most once.
	 **/
	class Dag::uniqueBackwardIterator : public Dag::uniqueBaseIterator  {
	public:
		// It is not possible to copy a unique (!) iterator ! We use the copy constructor for ASSIGNMENT
		template<class it> 	uniqueBackwardIterator (it &B) : uniqueBaseIterator (Dag::Link::INCOMING) {
			assign(B);
		}

		template<class itit> uniqueBackwardIterator(const itit &begin, const itit &end)
								 : uniqueBaseIterator (Dag::Link::INCOMING) {
				massign(begin, end);
			}

		virtual ~uniqueBackwardIterator () {};

		/** We support ONLY pre-increment, because we can not copy a unique iterator
		 **/

		uniqueBackwardIterator  &operator++() {
			increment();
			return *this;
		}

		template<class it>
		void assign(it &B) {
			baseIterator::assign(B, Dag::Link::INCOMING);
			newMagic();
		}

	protected:
		friend class uniqueBaseIterator ;
		uniqueBackwardIterator () : 	uniqueBaseIterator (Dag::Link::INCOMING) {}
	};

	// some convenience iterators with overloaded operator*(), operator->() to return user-defined
	// objects derived from Dag::Node
	namespace dagTemplates {
		template <class NDE> class forwardIterator : public Dag::forwardIterator  {
		public:
			template<typename it> forwardIterator(it B) : Dag::forwardIterator (B) {}

			template<class itit> forwardIterator(const itit &begin, const itit &end) :
								 Dag::forwardIterator(begin, end) {}

			virtual ~forwardIterator() {}

			operator NDE*()   const { return dynamic_cast<NDE *>(Dag::forwardIterator::operator->()); }

			NDE &operator*()  const {return  *dynamic_cast<NDE *>(&Dag::forwardIterator::operator*()); }

			NDE *operator->() const {return  dynamic_cast<NDE *>(Dag::forwardIterator::operator->()); }
		};

		template<class NDE> class backwardIterator : public Dag::backwardIterator  {
		public:
			template<typename it> backwardIterator(it B) : Dag::backwardIterator (B) {}

			template<class itit> backwardIterator(const itit &begin, const itit &end) :
								 Dag::backwardIterator(*begin, end) {}

			virtual ~backwardIterator() {}


			operator NDE*()   const { return dynamic_cast<NDE *>(Dag::backwardIterator::operator->()); }

			NDE &operator*()  const {return  *dynamic_cast<NDE *>(&Dag::backwardIterator::operator*()); }

			NDE *operator->() const {return  dynamic_cast<NDE *>(Dag::backwardIterator::operator->()); }
		};

		template <class NDE> class uniqueForwardIterator : public Dag::uniqueForwardIterator  {
		public:
			template<typename it> uniqueForwardIterator(it B) :
								  Dag::uniqueForwardIterator (B) {}

			template<class itit> uniqueForwardIterator(const itit &begin, const itit &end) :
								 Dag::uniqueForwardIterator(begin, end) {}


			virtual ~uniqueForwardIterator() {}


			operator NDE*()   const { return dynamic_cast<NDE *>(Dag::uniqueForwardIterator::operator->()); }

			NDE &operator*()  const {return  *dynamic_cast<NDE *>(&Dag::uniqueForwardIterator::operator*()); }

			NDE *operator->() const {return  dynamic_cast<NDE *>(Dag::uniqueForwardIterator::operator->()); }
		};

		template <class NDE> class uniqueBackwardIterator : public Dag::uniqueBackwardIterator  {
		public:
			template<typename it> uniqueBackwardIterator(it B) :
								  Dag::uniqueBackwardIterator (B) {}

			template<class itit> uniqueBackwardIterator(const itit &begin, const itit &end) :
								 Dag::uniqueBackwardIterator(begin, end) {}

			virtual ~uniqueBackwardIterator() {}

			operator NDE*()   const { return dynamic_cast<NDE *>(Dag::uniqueBackwardIterator::operator->()); }

			NDE &operator*()  const {return  *dynamic_cast<NDE *>(&Dag::uniqueBackwardIterator::operator*()); }

			NDE *operator->() const {return  dynamic_cast<NDE *>(Dag::uniqueBackwardIterator::operator->());  }
		};
	} // NAMESPACE dagTemplates
} // NAMSEPACE GULI
#endif //IFDEF
