/*
 * ontologySimplifiedAPI.h
 *
 *  Created on: Apr 23, 2009
 *      Author: sven
 */

#ifndef ONTOLOGYSIMPLIFIEDAPI_H_
#define ONTOLOGYSIMPLIFIEDAPI_H_
#include "stdinc.h"
#include "geneOntology.h"
#include "dagIterator.h"

namespace GULI {
	class ontologySimplifiedAPI {
	public:
		typedef vector<string> one2many;
		typedef vector<int> one2manyI;
		typedef map<string, one2many> one2manyMap;
		typedef geneOntology::Node Node;

		void setId2goMap(const one2manyMap &);

		int getTreeFromXML(const string &file);

		geneOntology &tree() const {
			return *dagGO;
		}

		/** Return a index list of features contained in a GO term
		 *
		 * @param goid the gene ontology ID to be searched
		 * @param ids  list of features to be tested for membership
		 * @param r    return index variable
		 * @param ascend if true, higher level (more specific) GO terms  are probed as well
		 * @param b     set this variable to a per-thread buffer in a multi-threaded environment
		 */
		void go2Id(const string goid, const vector<string> ids, one2manyI &r,
				bool ascend = true) const;

		/** get an index list \a r of GO terms associated with \a id.
		 *
		 * @param id    feature id to be mapped to GO terms
		 * @param r     array returning the indices of GO term containing feature
		 * @param descend if true, lower level (less specific) GO terms are included in the list (default true)
		 * @param b      set this variable to a per-thread buffer in a multi-threaded environment
		 */
		void id2Go(const string &id, one2many &r, bool descend = true) const;

		unsigned int goSize() const;
//		const string &goId(int i) const;
//		const string &goDescription(int i) const;
	//	const Node &node(int i) const;

		/** generate a path of names from the root up to this node
		 The path is not unique if multiple paths exist from the root.
		 In this case, one  is chosen by random (or, by the order they were
		 defined in the process of creating the tree)
		 If you are working in a multi-threaded environment you want to provide
		 the second argument, holding a reference to a visited_buffer, which
		 can not be shared over different threads.
		 **/

		static string path(const Node &n, const geneOntology &go);

		/** a very simplistic way of reading a map id2go, a tab-delimited id,go pair per line with some
		 * Limited sanity checking
		 *
		 * @param fnam file name to read
		 * @param M    the variable to receive the input file
		 * @return  zero on success, -1 otherwise
		 */
		static int readId2GO(const string fnam, one2manyMap &M);

	private:
		one2manyMap _id2go, _go2id;
		geneOntology *dagGO;
	};
}
#endif /* ONTOLOGYSIMPLIFIEDAPI_H_ */
