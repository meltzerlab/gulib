/***************************************************************************
                          dataIFtorus.h  -  description
                             -------------------
    begin                : Wed Nov 19 2003
    copyright            : (C) 2003 by Sven Bilke
    email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2004 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __GULI_dataifswap
#define __GULI_dataifswap

#include "dataIF.h"
namespace GULI
{
     /** A wrapper forcing any dataIF object into a toroidal data structure, i.e.
      ** the first and the last row/column in the data entries are identified.
      ** For the details of the API see the annotation of the base class dataIF.
      **/
      class dataTorus : public dataIF
      {
      public:
        /** if own is true, the child object is owned by the class and
          ** will be destructed if the wrapper gets destructed
          **/
        dataTorus(dataIF &t, bool own = true) : _target(t), _own(own) {}

        /** If the object is instantiated with a pointer the class _always_
          ** owns the target.
          **/
        dataTorus(dataIF *t) : _target(*t), _own(true) {}

        virtual ~dataTorus() {if(_own) delete &_target; }
        virtual const unsigned int cols() const {return _target.cols(); }
        virtual const unsigned int rows() const {return _target.rows(); }
        virtual float &Data(int col, int row)
        {
          return _target.Data(_adjustCol(col), _adjustRow(row));
        }
        virtual float Data(int col, int row) const
        {
          return _target.Data(_adjustCol(col), _adjustRow(row));
        }
        virtual float &operator()(int col, int row) { return Data(col, row); }
        virtual std::string &rowId(int i) { return _target.rowId(_adjustRow(i)); }
        virtual std::string &colId(int i) { return _target.colId(_adjustCol(i)); }
        virtual const std::string &rowId(int i) const { return _target.rowId(_adjustRow(i)); }
        virtual const std::string &colId(int i) const { return _target.colId(_adjustCol(i)); }

	/** return a pointer to the underlying dataIF structure.
	 ** This function is obsolete and should not be used in new implementations.
	 ** It certainly should go away soon.
	 **/
        dataIF *core() { return &_target; }

      private:
        int _adjustCol(int C)
        {
          int c = cols();
          while(C < 0) C += c;
          while(C >= c) C -= c;
          return C;
        }
        int _adjustRow(int R)
        {
          int r = rows();
          while(R < 0) R += r;
          while(R >= r) R -= r;
          return R;
        }
        dataIF &_target;
        bool _own;
      };
} // namespace GULI::data::wrapper
#endif
