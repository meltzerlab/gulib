/***************************************************************************
 numericDscstat.h  -  description
 -------------------
 begin                : Thu Jan 2 2002
 copyright            : (C) 2002-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_NUMERIC_DSCSTAT
#define __MULI_NUMERIC_DSCSTAT
#include <numeric>
#include <functional>
namespace GULI {
	class dscstat {
	public:
		dscstat() :
			_avg(0.), _err(0), _var(0.), _stddev(0.), _dof(0) {
		}

		template<class it>
		dscstat(it begin, it end) : _avg(0.), _err(0.), _var(0.), _stddev(0.), _dof(0) {
			calculate(begin, end);
		}

		dscstat(const dscstat &a) : _avg(0.), _err(0.), _var(0.), _stddev(0.), _dof(0) {
			*this = a;
		}

		dscstat &operator=(const dscstat &b) {
			_avg = b._avg;
			_err = b._err;
			_var = b._var;
			_dof = b._dof;
			return *this;
		}

		template<class it>
		dscstat &calculate(it begin, it end) {
			_dof = end - begin;
			_avg = std::accumulate(begin, end, 0.);
			_avg /= _dof;
			_var = std::accumulate(begin, end, 0., _varc(_avg));
			_var /= _dof;
			_stddev = sqrt(_var);
			_err = sqrt(_var / (_dof - 1.));
			return *this;
		}

		double average() {
			return _avg;
		}
		double error() {
			return _err;
		}
		double variance() {
			return _var;
		}
		double stddev() {
			return _stddev;
		}
		int dof() {
			return _dof;
		}

	private:
		double _avg, _err, _var, _stddev;
		int _dof;
		struct _varc: public std::binary_function<double, double, double> {
			_varc(double av) :
				_avg(av) {
			}
			;
			double operator()(double r, double v) {
				r += (v - _avg) * (v - _avg);
				return r;
			}
			double _avg;
		};
	};
}
#endif

