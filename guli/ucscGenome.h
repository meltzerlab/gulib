			/***************************************************************************
 *   Copyright (C) 2006 by Sven Bilke   *
 *   bilkes@mail.nih.gov   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef UCSC_GENOME_H
#define UCSC_GENOME_H
#include <map>
#include <fstream>
#include "stdinc.h"
#include "guliconfig.h"
/**
    @author Sven Bilke <bilkes@mail.nih.gov>
*/


class ucscGenome {
public:
    ucscGenome();
    ucscGenome(const string &);
    ~ucscGenome();

    unsigned int nChromosomes() const { return _knownChromosomes.size(); }
    int chrIndex(const string ) const;
    string chrId(unsigned int i) const;
    int chrLength(unsigned int i) const;
    string getSeq(unsigned int chr, unsigned int pos, unsigned int len) const;

    void setBaseName(const string &b) { _base = b; }
    static const string &defaultBaseDir(const string &b) {return  _defaultBase = b; }
    static const string &defaultBaseDir() { return _defaultBase; }

    int select(const string & g);
    string fnam(unsigned int i) const;

    bool error() const { return _error; }
    void clearError() { _error = false; }

protected:

private:
    static string _defaultBase;
     string _base;
    string _curDir;
//    unsigned int  _n;

    std::map<string, int> _index;
      typedef struct {
    	string id;
    	int chrlen;
    	int offset;
    	int linelen;
    } _chrInfoT;
    vector<_chrInfoT> _knownChromosomes;

    mutable ifstream _CHRSTREAM;
    mutable int _curopen;
    mutable bool _error;
};
#endif
