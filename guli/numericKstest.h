/***************************************************************************
 numericKstest.h  -  description
 -------------------
 begin                : Fri Jan 3 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/** Implementation of the Kolmogorov Smirnov Test
 Inspired by "Numerical Recipes in C, Chapter 14.3
 **/
#ifndef __MULI_NUMERIC_KOLMOGOROV_SMIRNOV_H
#define __MULI_NUMERIC_KOLMOGOROV_SMIRNOV_H
#include "guliconfig.h"
#include "stdinc.h"
#include <algorithm>
#include <iterator>
#include <math.h>

using std::iterator_traits;
using std::sort;

namespace GULI {
		class KolmogorovSmirnov {
		public:
			KolmogorovSmirnov() :
				_EPS1(0.001), _EPS2(1.0e-8), _MAXSUM(100) {
			}
			;

			/** Compare an observed distribution to a (theoretical) density
			 *  !! Data is modified in place !
			 *  1. template argument is an interface with a density  method
			 *  2. template argument is a random access iterator to float
			 **/
			template<class dens, class it>
			float testIP(it begin, it end, dens *Q) {
				sort(begin, end);
				_distance = 0.0;
				double en = end - begin;
				double f0 = 0;
				for (int j = 0; j < end - begin; j++) {
					double fn = j / en;
					double ff = Q->density(begin[j]);
					double d1 = fabs(fn - ff);
					double d2 = fabs(f0 - ff);
					f0 = fn;
					double dt = (d1 > d2) ? d1 : d2;
					_distance = (_distance > dt) ? _distance : dt;
				}
				en = sqrt(en);
				_calcP((en + 0.12 + 0.11 / en) * _distance);
				return _probability;
			}

			/** Compare an observed distribution to a (theoretical) density
			 *     Data is not modified
			 *  1. template argument is an interface with a density(float) method
			 *  2. template argument is a random access iterator to float
			 **/
			template<class dens, class it>
			float test(it begin, it end, dens *Q) {
				typedef	typename iterator_traits<it>::value_type vt;
				vector<vt> temp(end - begin);
				std::copy(begin, end, temp.begin());
				return testIP(temp.begin(), temp.end(), Q);
			}

			/** Compare two observed distributions
			 *  !! Data is modified in place
			 *  1. template argument is a random access iterator to float
			 *  2. template argument is a random access iterator to float
			 **/
			template <class it1, class it2>
			float testIP(it1 b1, it1 e1, it2 b2, it2 e2) {
				it1 j1;
				it2 j2;
				_distance = 0.0;
				double en1 = e1 - b1;
				double en2 = e2 - b2;

				sort(b1, e1);
				sort(b2, e2);
				for(j1=b1, j2=b2; j1 < e1 && j2 < e2; ) {
					double fn1=0., fn2=0.;
					typename iterator_traits<it1>::value_type d1 = *j1;
					typename iterator_traits<it2>::value_type d2 = *j2;
					fn1 = (j1 - b1) / en1;
					fn2 = (j2 - b2) / en2;
					double dt = fabs(fn2 - fn1);
					_distance = ( _distance > dt) ? _distance : dt;
					if(d1 <= d2)
					j1++;
					if(d2 <= d1)
					j2++;
				}
				double en = sqrt( en1 * en2 / ( en1 + en2));
				_calcP( (en + 0.12 + 0.11 / en) * _distance );
				return _probability;
			}

			/** Compare two observed distributions
			 *     Data is not modified
			 *  1. template argument is a random access iterator to float
			 *  2. template argument is a random access iterator to float
			 **/
			template <class it1, class it2>
			float test(it1 b1, it1 e1, it2 b2, it2 e2) {
				vector<typename iterator_traits<it1>::value_type> t1( e1 - b1);
				vector<typename iterator_traits<it2>::value_type> t2( e2 - b2);
				std::copy(b1, e1, t1.begin());
				std::copy(b2, e2, t2.begin());
				return testIP(t1.begin(), t1.end(), t2.begin(), t2.end());
			}

			/** Compare distribution to an earlier registered distribution
			 *  this makes sense ONLY if one wants to perform MULTIPLE
			 *  different compariosons AND the size of the "registered" dataset
			 *  is significantly larger then the ones of the repeated sets.
			 *  !! data is modified !
			 *  template argument is a random access iterator to float
			 */
			template <class it>
			float testIP(it begin, it end) {
				return testIP(begin, end, this);
			}

			/** Compare distribution to an earlier registered distribution
			 *  this makes sense ONLY if one wants to perform MULTIPLE
			 *  different compariosons AND the size of the "registered" dataset
			 *  is significantly larger then the ones of the repeated sets.
			 *  Data is not modified
			 *  template argument is a random access iterator to float
			 **/
			template <class it>
			float test(it begin, it end) {
				return test(begin, end, this);
			}

			template <class it>
			void setCompare(it begin, it end) {
				_compare.resize(end - begin);
				int i = 0;
				for(it I=begin; I < end; ++I)
				_compare[i++] = *I;
				sort(_compare.begin(), _compare.end());
				_csize = _compare.size();
			};

			double maxDist() {
				return _distance;
			}

		protected:
			double _calcP( double );

			/** look up the observation in the previously registered distribution
			 performance grows like O(log(N1)), while the full compariosn is O(N1)
			 */
			double density(double);
			double _distance, _probability;

		private:
			double _EPS1, _EPS2;
			int _MAXSUM, _csize;
			vector<double> _compare;
		};
} // NAMSEPACE SB::statistic
#endif

