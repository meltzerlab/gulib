/***************************************************************************
 randomData  -  description
 -------------------
 begin                : Mon Mar 29 2004
 copyright            : (C) 2004 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __MULI_DATA_RANDOM_H
#define __MULI_DATA_RANDOM_H
#ifdef HAVE_GULICONFIG
#include <guliconfig.h>
#endif
#include "stdinc.h"
#include <gsl/gsl_rng.h>
#include "dataIF.h"
#include "dataBasic.h"

namespace GULI {
	/** a dataIF providing random data
	 **/
	class dataRandom: public dataBasic {
	public:
		virtual ~dataRandom() {
		}
		;

		/**
		 Setup a datamatrix with gaussian iid data. Sigma and av can be used to set the
		 two distribution parameters
		 **/
		dataRandom(int cols, int rows, double sigma = 1., double av = 0.);

		/** Generate new data content **/
		void randomize(double sigma = 1., double av = 0.);

		void resize(int cols, int rows) {
			dataBasic::resize(cols, rows);
			randomize();
			_setLabel();
		}

	private:
		void _setLabel();
		const gsl_rng_type *ranT;
		gsl_rng *rangen;
	};
} // namespace SB::data
#endif // defined __redirectdata
