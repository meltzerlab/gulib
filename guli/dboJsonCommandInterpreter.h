/*
 * JsonCommandInterpreter.h
 *
 *  Created on: Mar 4, 2015
 *      Author: sven
 */

#ifndef JSONCOMMANDINTERPRETER_H_
#define JSONCOMMANDINTERPRETER_H_
#include <string>
#include <iostream>
#include <map>
#include <memory>
#include <Wt/Json/Object.h>
#include "dboSession.h"
#include "dboJson2Table.h"


namespace GULI {
	namespace Dbo {
		class _InstantiatorBase;
		class jsonCommandInterpreter {
		public:
			jsonCommandInterpreter(Session &S) : _S(S) {};
			virtual ~jsonCommandInterpreter(){}

			void add(const Wt::Json::Object &O) const;
			void include(const Wt::Json::Object &O) const;
			void remove(const Wt::Json::Object &O) const;
			void update(const Wt::Json::Object &O) const;

			void parseObjectString(const std::string &) const;
			std::istream &stream(std::istream &) const;
		private:
			static const std::string whoAmI;


			enum class _modeT {
				CREATE,
				UPDATE,
				UPDATEorCREATE,
				DELETE
			} ;

			class _InstantiatorBase {
			public:
				virtual ~_InstantiatorBase(){}
				virtual void instantiate(const Wt::Json::Object &O, Wt::Dbo::Session &S, _modeT mde) const = 0;
			protected:
			};

			template <class T>
			class _Instantiator : public _InstantiatorBase  {
			public:
				virtual ~_Instantiator(){};
				_Instantiator(Session &S) : _S(S) {}

				virtual void instantiate(const Wt::Json::Object &O, Wt::Dbo::Session &S, _modeT mde) const {
					Json2Table J2T;
					dbo::ptr<T> p;
					std::string err = "";

					if(mde == _modeT::UPDATE || mde ==  _modeT::UPDATEorCREATE || mde == _modeT::DELETE) {
						std::unique_ptr<T> tmp = std::make_unique<T>(); // make sure global variables (like idfield) in class T are initialized
						std::string idField = T::idField();

						if(idField != "" && O.contains(idField) && O.type(idField) ==  Wt::Json::Type::String) {
							const std::string &id = O.get(idField);
							p = S.find<T>().where(idField +  " = \'" + id + "'");
							if(p.get()) {
								if(mde == _modeT::DELETE) {
									// do something
									return;
								} else {
									J2T.deserialize(O, p);
									err = "Object not found: " + id;
									return;
								}
							} else {
								err = "Json does not contain a valid Object id-field: " + idField;
							}
						} else {
							err = "idField not found in Json-description";
						}
					}
					if(mde == _modeT::UPDATE || mde == _modeT::DELETE) throw  Wt::Dbo::ObjectNotFoundException(S.tableName<T>(),whoAmI + "instantiate: " + err);

					if(T::idField() != "id") {
						if(O.contains(T::idField())) {
							std::string id = O.get(T::idField()).toString();
							dbo::ptr<T> f = S.find<T>().where(T::idField() +  " = \'" + id + "'");
							if(f.get()) throw Wt::Dbo::Exception(whoAmI + "Instantiate::" + S.tableName<T>() + ": Item already exists " + id, "");
						}
					}
					p = S.add(std::make_unique<T>());
					J2T.deserialize(O, p);
				}
			private:
				Session &_S;
			};

			typedef void (jsonCommandInterpreter::*_serviceRoutine)(const Wt::Json::Object &arg) const;

			static const std::map<std::string, _serviceRoutine> _commands;


		protected:
			template <class T> void mapClass(const char *name) {
				if(_tables.find(name) != _tables.end()) return;
				_tables[name] = std::make_unique<_Instantiator<T> >(_S);
			}
		private:
			std::map<std::string, std::unique_ptr<_InstantiatorBase> >    _tables;
			Session &_S;
		};
	}
}
#endif /* JSONCOMMANDINTERPRETER_H_ */
