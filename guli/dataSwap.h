/***************************************************************************
 dataWrapperSwap.h  -  description
 -------------------
 begin                : Wed May 5 2003
 copyright            : (C) 2004 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __GULI_dataifswap
#define __GULI_dataifswap
#include "dataIF.h"
namespace GULI {
	/** A wrapper around any  dataIF object that swaps columns/rows.
	 ** For the details of the API see the annotation of the base class dataIF.
	 **/
	class dataSwap: public dataIF {
	public:
		/** if own is true, the child object is owned by the class and
		 ** will be destructed if the wrapper gets destructed
		 **/
		dataSwap(dataIF &t, bool own = false) :
			_target(t), _own(own) {
		}

		/** If the object is instantiated with a pointer the class _always_
		 ** owns the target.
		 **/
		dataSwap(dataIF *t) :
			_target(*t), _own(true) {
		}
		;

		virtual ~dataSwap() {
			if (_own) delete &_target;
		}

		virtual const unsigned int cols() const {
			return _target.rows();
		}
		virtual const unsigned int rows() const {
			return _target.cols();
		}
		virtual float &Data(int col, int row) {
			return _target.Data(row, col);
		}

		virtual float Data(int col, int row) const {
			return _target.Data(row, col);
		}

		virtual float &operator()(int col, int row) {
			return _target(row, col);
		}
		virtual std::string &rowId(int i) {
			return _target.colId(i);
		}
		virtual std::string &colId(int i) {
			return _target.rowId(i);
		}


		virtual const std::string &rowId(int i) const {
			return _target.colId(i);
		}
		virtual const std::string &colId(int i) const {
			return _target.rowId(i);
		}

		/** return a pointer to the underlying dataIF structure.
		 ** This function is obsolete and should not be used in new implementations.
		 ** It certainly should go away soon.
		 **/
		dataIF *core() {
			return &_target;
		}

	private:
		dataIF &_target;
		bool _own;
	};
} // namespace SB::data::wrapper
#endif
