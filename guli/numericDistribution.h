/***************************************************************************
 *   Copyright (C) 2006 by Sven Bilke   *
 *   bilkes@mail.nih.gov   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GULIB__DISTRIBUTION_H
#define GULIB__DISTRIBUTION_H
#include "stdinc.h"
namespace GULI {
		class distribution {
		public:
			distribution();
			/** Binomial distribution **/
			double binomial(unsigned long int n, unsigned long int k);

			/** Cummulative Binomial Distribution **/
			double cdBinomial(double p, unsigned long int k,
					unsigned long int n);

			/** Hypergeometric distribution **/
			double hypergeometric(unsigned long int N, unsigned long int S,
					unsigned long int n, unsigned long int s);

			double cdHypergeometric(unsigned long int N, unsigned long int S,
					unsigned long int n, unsigned long int s);

			double factorial(int n);
			double lnfactorial(long int n);
			/** Incomplete beta function **/
			static double betai(double a, double b, double x);
			/** Logarithm of the Gamma function */
			static double gammln(double x);
			static double incompleteGamma(double a, double x);
			static double incompleteGamma1(double a, double x);

			double errf(double x);

			// smallest numbers to be processes
			static double FPMIN;
			// desired precision (convergence criterion in series expansions)
			static double EPS;
			// maximal number of iterations
			static int MAXIT;
		private:

			static unsigned int CBDuseBeta;
			static int LNFACT_CACHESIZE;
			static int FACT_CACHESIZE;
			static int cdHYPERGEOMETRIC_EXACT_TERMS;
			static double cdHYPERGEOMETRIC_APPROX_PREC;

			int fCacheTop;
			vector<double> lnf_cache, f_cache;
		};
}
//#undef CBDuseBeta
#endif

