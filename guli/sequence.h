#ifndef __SEQUENCE_H
#define __SEQUENCE_H
#include "stdinc.h"
#include "mmu.h"


class sequence;
class inverseSequence;

class sequence: private mmu::referer {

protected:
	friend class inverseSequence;
	template <class it> class _asciiIterator : public it {
	public:
		char operator[](int i) const {
			return n2c[(int) it::operator[](i)];
		}

		char operator*() {
			return n2c[(int) it::operator*()];
		}

		_asciiIterator(const it &c) : it(c) {}
	};

public:
	class seqData;
	enum {
		A, C, G, T, N
	};
	typedef char nucleotide;
	typedef vector<nucleotide> storageType;
	typedef storageType::const_iterator iterator;
	typedef storageType::const_reverse_iterator riterator;
	typedef _asciiIterator<iterator>  aIterator;
	typedef _asciiIterator<riterator> aRiterator;


	sequence(const seqData &);
	sequence(const sequence &);
	sequence(const inverseSequence &);
	sequence(const storageType &s, const string id = "");
	sequence(const char *s, const string id = "");
	~sequence();

	bool valid() const { return _sd->valid(); }

	nucleotide operator[](unsigned int i) const {
		return _sd->content()[i];
	}

	char operator()(unsigned int i) const {
		return n2c[(int) _sd->content()[i]];
	}

	const string &id() const {
		return _sd->id();
	}
	unsigned int length() const {
		return static_cast<unsigned int> (_sd->content().size());
	}

	iterator begin() const {
		return _sd->content().begin();
	}
	iterator end() const {
		return _sd->content().end();
	}
	riterator rbegin() const {
		return _sd->content().rbegin();
	}
	riterator rend() const {
		return _sd->content().rend();
	}


	aIterator aBegin() const {
//		return _sd->content().begin();
		return aIterator(begin());
	}
	aIterator aEnd() const {
		return aIterator(end());
//		return _sd->content().end();
	}
	aRiterator aRbegin() const {
		return _sd->content().rbegin();
	}
	aRiterator aRend() const {
		return _sd->content().rend();
	}



	const sequence subSequence(unsigned int start, unsigned int len, const string &id = "") const;


	static bool maskRepeats() {
		return _maskRepeats;
	}
	static void maskRepeats(bool m) {
		_maskRepeats = m;
	}

	ostream &operator<<(ostream &O) const {
		return _sd->operator<<(O);
	}

	// If storage capacity exceeds (1. + MARGIN) size, reallocate proper
	//    memory to save space
	static float MEM_OVERALLOCATION_MARGIN;
	static const nucleotide c2n[256];
	static const nucleotide c2n_MASKED[256];
	static const char n2c[5];

	class seqData: public mmu::managed {
		friend class sequence;
	public:
		seqData();
		seqData(const seqData &);
		~seqData();

		const storageType &content() const {	return _content; }
		storageType &content() { return _content; }

		string &id() { 	return _id; }
		inline const string &id() const {return _id; }

		const bool &valid() const { return _valid; }
		bool &valid() { return _valid; }

		seqData &operator=(const seqData &);
		seqData &operator=(const sequence &s) {
			*this = *s._sd;
			return *this;
		}

		istream &operator>>(istream &);
		ostream &operator<<(ostream &) const;

private:
		storageType _content;
		string _id;
		bool _valid;
	};
private:
	const seqData *_sd;
	const bool _ownSeqData;
	static bool _maskRepeats;
};

/** \inverseSequence implements transparent access to the reverse complement
 ** nucleotide sequence of a \a sequence object
 **/
class inverseSequence {
	// Reverse complement access to a sequence !
	typedef sequence::nucleotide nucleotide;
	typedef sequence::storageType storageType;
	typedef sequence::riterator rit;
public:
	class iterator: public rit {
	public:
		iterator(const iterator &c) :
			rit(c) {
		}
		iterator(const rit &c) :
			rit(c) {
		}

		const nucleotide &operator[](int i) const {
			return _trans[(int) rit::operator[](i)];
		}

		const nucleotide &operator*() {
			return _trans[(int) rit::operator*()];
		}

		iterator &operator++() {
			rit::operator++();
			return *this;
		}
		iterator operator++(int) {
			iterator tmp(*this);
			rit::operator++(1);
			return tmp;
		}

		iterator &operator--() {
			rit::operator--();
			return *this;
		}
		iterator operator--(int) {
			iterator tmp(*this);
			rit::operator--(1);
			return tmp;
		}

	};

	typedef class sequence::_asciiIterator<iterator> aIterator;

	/** TODO: we may need a reverse iterator for inverse sequences **/

	/** Instantiate \a inverseSequence of \a sequence **/
	inverseSequence(const sequence &s) :
		_content(s), _l(s.length()), _l1(s.length() - 1) {
	}
	virtual ~inverseSequence() {	}

	bool valid() const { return _content.valid(); }

	sequence::nucleotide operator[](unsigned int i) const {
		return _trans[(int) _content[_l1 - i]];
	}

	char operator()(unsigned int i) const {
		return sequence::n2c[(int) this->operator[](i)];
	}
	unsigned int length() const {
		return _l;
	}

	const string &id() const {
		return _content.id();
	}

	iterator begin() const {
		return iterator(_content.rbegin());
	}
	iterator end() const {
		return iterator(_content.rend());
	}

	aIterator aBegin() const {
		return aIterator(begin());
	}

	aIterator aEnd() const {
		return aIterator(end());
	}


	private:
	friend class sequence;
	const sequence &_content;
	int _l, _l1;
	static const sequence::nucleotide _trans[5];
};

istream &operator>>(istream &A, sequence::seqData &b);
ostream &operator<<(ostream &A, const sequence::seqData &b);
ostream &operator<<(ostream &A, const sequence &b);

#endif // IFDEF
