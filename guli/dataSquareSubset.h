/***************************************************************************
 dataSquareSubset.h  -  description
 -------------------
 Created on           : Mar 28, 2012
 copyright            : (C) 2012 by sven
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2012 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef DATASQUARESUBSET_H_
#define DATASQUARESUBSET_H_
#include "stdinc.h"
#include "dataIF.h"

namespace GULI {
	/** a dataIF  subsselecting a square region of another datIF dataIF
	 **/
	class dataSquareSubset: public GULI::dataIF {
	public:
		virtual ~dataSquareSubset(){};
		dataSquareSubset(dataIF &target, int cs, int cl, int rs, int rl) :
				_target(target), _cstart(cs), _clen(cl), _rstart(rs), _rlen(rl) {}

		/** Return the number of Columns in the Data
		 *  Re-implemented for internal reasons
		 **/
		virtual const unsigned int cols() const {
			return _clen;
		}

		/** Return the number of Rows in the Data
		 *  Re-implemented for internal reasons
		 **/
		virtual const unsigned int rows() const {
			return _rlen;
		}

		/** return the Data content at column, row
		 *  Re-implemented for internal reasons
		 **/
		virtual float &Data(int col, int row) {
			return _target(col + _cstart, row + _rstart);
		}

		virtual float Data(int col, int row) const {
			return _target(col + _cstart, row + _rstart);
		}

		/** return the String ID for a row
		 *  Re-implemented for internal reasons
		 **/
		virtual std::string &rowId(int i) {
			return _target.rowId(i + _rstart);
		}

		/** return the String ID for a column
		 *  Re-implemented for internal reasons
		 **/
		virtual std::string &colId(int i) {
			return _target.colId(i + _cstart);
		}

		/** return the String ID for a row (const)
		 **/
		virtual const std::string &rowId(int i) const {
			return _target.rowId(i + _rstart);
		}

		/** return the String ID for a column (const)
		 **/
		virtual const std::string &colId(int i) const {
			return _target.colId(i + _cstart);
		}

	private:
		dataIF &_target;
		int _cstart, _clen, _rstart, _rlen;
	};
} // namespace
#endif // defined __GULI_DATA_SQUARSESUBSET_H
