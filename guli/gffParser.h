/*
 * gffParser.h
 *
 *  Created on: Oct 9, 2012
 *      Author: sven
 */

#ifndef GFFPARSER_H_
#define GFFPARSER_H_
#include "gffDag.h"
#include "tabDelimited.h"

namespace GULI {
	// map them to the conventions needed in your client
	class gffParser  {
	public:
		// this parser reads a generic GFF formatted file, and applies a rule-based set of transformations
		// regarding naming conventions of attributes and synthesizing implicit objects. To set up a new
		// input format, the parserMode type defined below needs to be setup with the appropriate set of
		// rules. Take a look at the .cc file complementing this header for a few examples
		typedef struct  {
			typedef struct {
				const char *featureRegex; // This is the feature type of the object TRIGGERING synthesis
				const char *feature;      // the object to be synthesized
				const char *idAttribute;  // which attribute to use as the ID. If it is "", synthesize an
				                          // id based on the feature type of the synthesized object
			} synthesizeFeatureT ;

			typedef struct {
				const char *featureRegex;
				const char *idAttribute;   // 0-> drop feature, "" -> SYNTHESIZE name
				const char *uniqifyRegex;  // 0-> do nothing, regex-> merge features with the same outline into
				                           // one object adding information of the attributes identified in the regex pointed
				                           // to by uniquify
			} featureSelectorT;

			typedef struct {
				const char *featureRegex;
				const char *attributeRegex;
				bool       keep;
			} attributeSelectorT;


			typedef struct {
				const char *featureRegex;     // apply rule to features described in this regex
				const char *attributeRegex;   // rename attributes matching this regex
				const char *targetAttribute;  // new attribute id
			} namespaceMapT;



			const synthesizeFeatureT *synthesis;
			const featureSelectorT   *idSelector;
			const attributeSelectorT *attributeSelector;
			const namespaceMapT      *mapNames;
		} parserMode;

		static const parserMode Native;
		static const parserMode Ensembl;
		static const parserMode Gencode;

		static istream &parse(istream &I, gffDag::Node::table &target, const gffParser::parserMode &M);
		static istream &parse(istream &I, gffDag &target, const gffParser::parserMode &M);
		static void textual2Gff(const gffDag::Node::table &, gffDag &target);
	protected:
	private:
	};
}



#endif /* GFFPARSER_H_ */
