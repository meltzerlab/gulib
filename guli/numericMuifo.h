/*
 * numericMuifo.h
 *
 *  Created on: Mar 9, 2010
 *      Author: sven
 */

/** Estimate mutual information
 */
#ifndef NUMERICMUIFO_H_
#define NUMERICMUIFO_H_
#include "stdinc.h"

class numericMuifo {
	template <class it> class sorter {
	public:
		sorter(it &b) : _p(b) {};
		bool operator()(const int &a, const int &b) { return *(_p +a) < *(_p + b); }
		it _p;
	};

public:
	typedef std::pair<vector<int> &, vector<int> & > data2dT;


	template <class it> static double kernel2d(it b1, it e1, it b2,  float radius = -1.) {
		// do copola transform
		const unsigned int N = e1 - b1;
		vector<int> ii1(N), ii2(N), i1(N), i2(N);
		for(unsigned int i=0; i < N; ++i) i1[i] = i2[i] = i;
		sorter<it> S1(b1), S2(b2);
		std::sort(i1.begin(), i1.end(), S1);
		std::sort(i2.begin(), i2.end(), S2);
		for(unsigned int i=0; i < N; ++i) ii1[i1[i]] = ii2[i2[i]] = i;

		// let method for copola transformed data take over
		data2dT D(ii1, ii2);
		// DOES NOT COMPILE ON MAC??? return kernel2d(D, radius);
		return -1;
	}

	/** Implements kernel based estimator of mutual information **/
	/* Following Moon, Y.I., Balaji R., Lall, U.,  PhysRev E. (52)3 2318ff (1995) **/
	static double kernel2d(const std::pair<vector<int>, vector<int> > &rankorder,  float radius = -1.);
};

#endif /* NUMERICMUIFO_H_ */
