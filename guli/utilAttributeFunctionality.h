/*
 * atrigbutedDagNode.h
 *
 *  Created on: Oct 18, 2013
 *      Author: sven
 */

#ifndef ATRIGBUTEDDAGNODE_H_
#define ATRIGBUTEDDAGNODE_H_
#include <string>
#include <vector>
#include <unordered_map>
using std::string;
using std::vector;

namespace GULI {
	class utilAttributeFunctionality  {
	public:
		static const string UNKNOWNATTRIBUTE;
		virtual ~utilAttributeFunctionality() {}
		utilAttributeFunctionality(const utilAttributeFunctionality &P) : __attributes(P.__attributes) {}
		utilAttributeFunctionality() {}
		size_t nAttribute() const { return __attributes.size(); }
		vector<string> knownAttributes() const;
		const string &attribute(const string &id) const;
		void removeAttribute(const string &id) {__attributes.erase(id); }
		void addReplaceAttribute(const string &id, const string &value) {__attributes[id] = value;}
		const char *getAttribute(const string &id) const;
	private:
		typedef std::unordered_map<string, string> _attributeT;
		_attributeT __attributes;
	};
}



#endif /* ATRIGBUTEDDAGNODE_H_ */
