/***************************************************************************
 dataMemarray.h  -  description
 -------------------
 begin                : Fri May 3 2002
 copyright            : (C) 2002 by sven
 email                : sven@gonzo.thep.lu.se
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2004 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GULI_DATA_MEMARRAY_H
#define GULI_DATA_MEMARRAY_H
#include <string>
#include "guliconfig.h"
#include "dataIF.h"

namespace GULI {
	/** a Data container based on (standard) allocated memory. This is
	 ** somewhat faster than the basicData (using std::vector as a base).
	 ** The only real advantage of this class is if you need to align
	 ** rows on some (say 16byte) boundary. In some instances this can
	 ** dramatically increase performance. (And, one day I learn to
	 ** re-define the _allocator for vectors so we can do the same thing
	 ** with the verctor class)
	 ** This class is optimized for ROW access, the main access direction.
	 **/
	class dataMemarray: public dataIF {
	public:
		virtual ~dataMemarray();

		/** Instantiate a mem-array based dataIF container with \a cols columns and
		 * \a rows rows. If the argument \a align is non-zero each row is aligned
		 * to a memory location with an intergr multiple of \a align starting
		 * adress.
		 **/
		dataMemarray(unsigned int cols, unsigned int rows, int align = 0);

		/** Return the number of Columns in the Data
		 **/
		virtual const unsigned int cols() const {
			return _cols;
		}

		/** Return the number of Rows in the Data
		 **/
		virtual const unsigned int rows() const {
			return _rows;
		}

		/** return the Data content at column, row
		 **/
		virtual float &Data(int col, int row) {
			return _data[col * _entries_per_col + row];
		}

		virtual float Data(int col, int row) const {
			return _data[col * _entries_per_col + row];
		}

		/** a shortcut for the Data function
		 **/
		virtual float &operator()(int col, int row) {
			return Data(col, row);
		}

		/** return the String ID for a row
		 **/
		virtual std::string &rowId(int i) {
			return _rowid[i];
		}

		/** return the String ID for a column
		 **/
		virtual std::string &colId(int i) {
			return _colid[i];
		}

		/** return the String ID for a row (const)
		 **/
		virtual const std::string &rowId(int i) const {
			return _rowid[i];
		}

		/** return the String ID for a column (const)
		 **/
		virtual const std::string &colId(int i) const {
			return _colid[i];
		}

		/** return a pointer to the beginning of row \a row */
		float *column(int row) {
			return _data + row * _entries_per_col;
		}
	private:
		float *_data;
		unsigned int _cols, _rows, _entries_per_col;
		std::string *_rowid, *_colid;
	};
} // namespace MULI::data
#endif // defined __memarrayData
