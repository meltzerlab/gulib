/***************************************************************************
 numericFtest.h  -  description
 -------------------
 begin                : Thu Jan 2 2002
 copyright            : (C) 2002-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_NUMERIC_FTEST_H
#define __MULI_NUMERIC_FTEST_H
#include <algorithm>
#include <math.h>
#include "guliconfig.h"
#include "numericDscstat.h"
#include "specialFunction.h"

namespace GULI {
		class ftest {
		protected:
			double _calcP(dscstat &P1, dscstat &P2) {
				int df1, df2;
				if (P1.variance() > P2.variance()) {
					_F = P1.variance() / P2.variance();
					df1 = P1.dof() - 1;
					df2 = P2.dof() - 1;
				} else {
					_F = P2.variance() / P1.variance();
					df1 = P2.dof() - 1;
					df2 = P1.dof() - 1;
				}
				_P = Probability(_F, df1, df2);
				return _P;
			}

		public:
			template<class it>
			double test(it begin1, it end1, it begin2, it end2) {
				dscstat P1(begin1, end1);
				dscstat P2(begin2, end2);
				return _calcP(P1, P2);
			}

			template<class it>
			double test(it begin, it end) {
				dscstat P(begin, end);
				return _calcP(P, _P1);
			}

			template<class it>
			void setCompare(it begin, it end) {
				_P1.calculate(begin, end);
			}
			;

			static double Probability(double F, int df1, int df2) {
				double P;
				int n1, n2;
				if (F > 1) { // Make F larger than one
					n1 = df1;
					n2 = df2;
				} else {
					n1 = df2;
					n2 = df1;
					F = 1. / F;
				}

				P = 2.0 * SB::specialFunction::betai(0.5 * n2, 0.5 * n1, n2
						/ (n2 + n1 * F));
				P = (P > 1.) ? 2. - P : P;
				return P;
			}

			double FValue() {
				return _F;
			}
			double Probability() {
				return _P;
			}

		protected:
			double _P, _F;
			dscstat _P1;
		};
} // Namespace
#endif
