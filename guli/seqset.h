/***************************************************************************
 *   Copyright (C) 2005 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "stdinc.h"
#ifndef __SEQUENCE_SET_H
#define __SEQUENCE_SET_H
#include "sequence.h"

class seqSet {
public:
	virtual ~seqSet() {
	}
	virtual size_t size() const = 0; // Number of Sequences
	virtual const sequence operator[](size_t i) const = 0; // Get Sequence i

	/** Some control entry points to manage cached data which may (or may not)
	 ** be swapped in and out at runtime. Calling these routines is merely a hint
	 ** to the cache
	 **/
	virtual void idle()         {} // Signal that the data will not be used for some time
	virtual void prefetch(bool) {} // Pre-loading the data is recommended if it fits into memory
};

/** a simple adaptor to make a single sequence appear as a set **/
class seqSetAdaptor: public seqSet {
public:
	virtual ~seqSetAdaptor() {}
	seqSetAdaptor(const sequence &s) : 	_content(s) {}
	virtual size_t size() const {return 1;}
	virtual const sequence operator[](size_t i) const { assert ( !i ); return _content; }
private:
	const sequence &_content;
};

class seqMemSet: public seqSet {
public:
	virtual size_t size() const {
		return static_cast<unsigned int> (_content.size());
	}

	virtual const sequence operator[](size_t i) const {
		return sequence(_content[i]);
	}

	istream &operator>>(istream &A);
	ostream &operator<<(ostream &A) const;

	seqSet &operator=(const seqSet &);

	void clear() {
		_content.clear();
	}

private:
	vector<sequence::seqData> _content;
};

istream &operator>>(istream &A, seqMemSet &b);
ostream &operator<<(ostream &A, const seqMemSet &b);

#endif
