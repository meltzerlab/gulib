/***************************************************************************
 *   Copyright (C) 2006 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SEQUENCEWHOLEGENOMESET_H
#define SEQUENCEWHOLEGENOMESET_H
#include "seqset.h"
#include "ucscGenome.h"
/**
 Represent a whole Genome
 @author Sven Bilke <bilkes@mail.nih.gov>
 */

class seqSetWholeGenome: public seqSet {
public:
	seqSetWholeGenome();
	~seqSetWholeGenome();
	seqSetWholeGenome(const ucscGenome &);
	void selectGenome(const ucscGenome &);
	virtual size_t size() const {
		return _size;
	}
	virtual const sequence operator[](size_t i) const {
		return _cache[i];
	}
	virtual const sequence operator[](const string &) const;
	const ucscGenome &genomeDescr() const {
		return *_dsc;
	}


	/** Limit the memory this object may use. Note that we will
	 * allocate the memory for a single chromosome, even if this
	 * exceeds this limit. In this case, though, we will keep only
	 * a single chromosome in memory,  other chromosomes are swapped in and
	 * out when needed. Also, in the process of loading a chromosome into memory
	 * the limit may be exceeded by at most by the size of the data to be loaded.
	 * Note that this is a "soft" limit. In a multi-threaded environment we
	 * may not be able to remove sequences when they are needed by another
	 * thread.
	 * A zero value sets no limit on memory allocation.
	 **/
	void setMaxMem(int mem) {
		_cache.setMaxMem(mem);
	}

	virtual void idle() {
		_cache.empty();
	}

private:
	typedef sequence::seqData datT;

	class cache: public mmu::swapedVector<datT, sequence> {
	public:
		cache(size_t m = 0) :
			_usedmem(0), _nmem(0), _maxmem(m), _dsc(0) {
		}
		void setMaxMem(size_t s) {
			_maxmem = s;
		}
		void select(const ucscGenome &f);

		virtual void empty() {
			mmu::swapedVector<datT, sequence>::empty();
			_nmem = 0;
			_usedmem = 0;
		}

	protected:
		virtual void _load2mem(datT &, size_t i) const;
		virtual void _minimizeMemFootprint(datT &, size_t i) const;
		virtual void _clearingCache(datT &d, size_t i) {
			_minimizeMemFootprint(d, i);
		}
		virtual bool _needSpace() const {
			return (_maxmem && (_nmem > 1) && (_usedmem > _maxmem));
		}

	private:
		mutable size_t _usedmem;
		mutable unsigned int _nmem;
		size_t _maxmem;
		const ucscGenome *_dsc;
	};

	cache _cache;
	const ucscGenome *_dsc;
	size_t _size;
};
#endif
