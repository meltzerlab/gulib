/***************************************************************************
 *   Copyright (C) 2007 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MMU_H
#define __MMU_H
#include "stdinc.h"
#ifdef HAVE_THREADS
#include <pthread.h>
#endif

/**
Admittedly, MMU may be a little too promising. This object provides
some very limited functionality to swap in and out large objects (sequences)
at run-time in a thread-safe way.

The class "SwapedVector" is a replacement for std::vector and for the most
part behaves identical to std::vector, extended to demand loading.
This object essentially implements the "place holder" design pattern, where
the vector stores objects derived from the \a referer class, incomplete variants of the desired full object,
with the \a managed derived classes completing the (typically very large memory footprint) information.
It is probably most useful maintain at least some of the inexpensive parts of the \a referer class and only
out-source large memory foot print to \a managed class, which can be loaded on demand.

@author Sven Bilke <bilkes@mail.nih.gov>
*/


class mmu {
public:
    class managed;
    class referer;

    /**     A vector class enhanced to on-demand loading. \a stType has to
         be derived from \a referer (see summary for mmu-class), \a accType
         has to be derived from \a managed. **/
    template <class stType, class accType> class swapedVector {
    public:
        swapedVector() {
#ifdef HAVE_THREADS
            pthread_mutex_init ( &_mtx, NULL );
#endif
            _head = new managed;
            _init();
        }

        virtual ~swapedVector() {
#ifdef HAVE_THREADS
            pthread_mutex_destroy ( &_mtx );
#endif
            delete _head;
        }

        unsigned int size() const {
            return (unsigned int) _content.size();
        }

        void empty() {
            resize(size());
        }

        void resize(size_t s) {
            _lockThread();
            const size_t cs = _content.size();
            for (size_t i=0; i < cs; ++i) {
                stType &c = _content[i];
                if (c.locked() )
                    std::cerr << "DEBUG::MMU There are still locked items while clearing the cache ?\n";
                if (c.inMem())  c.remove();
                _clearingCache(c, i);
            }
            _content.resize(s);
            _init();
            _unlockThread();
        }

        void clear() {
            resize(0);
        }

        const accType operator[] ( size_t i ) const  {
            _lockThread();
            stType &c = _content[i];

            if ( !c.inMem() ) { // NOT currently in memory
//         std::cerr << "DEBUG::MMU: loading " << i << std::endl;
                c.insert ( _tail, i );
                _load2mem (c, i);
                _tail = &c;
            } else {
                // update LRU
                if ( &c != _tail ) {
                    c.move2(_tail);
                    _tail = &c;
                }
            }

            while ( _needSpace() ) { // We have to remove a managed object
                managed *cur = _head;
                while ( cur->NEXT ) { // search for an object not currently locked
                    cur = cur->NEXT;
                    if ( !cur->locked() ) break;
                }
                if ( ( !cur ) || ( cur == _tail ) ) break; // No object freeable
                stType *current = dynamic_cast<stType *>(cur);
                assert(current);
//          std::cerr << "DEBUG::MMU: removing " << current->idx() << std::endl;
                _minimizeMemFootprint(*current, current->idx());
                cur->remove();
            }

            accType r(_content[i]);
            _unlockThread();
            return r;
        }

    protected:
        /***********************************************************/
        /* The API for client classes. Inherit mmu<> and implement */
        /* these methods to make this useful. Sorry, no default    */
        /* {return} implementations, to avoid typos in the client  */
        /* so that methods don;t get called. Thats a pain to dataDebug */
        /***********************************************************/
        virtual void _load2mem ( stType &, size_t i ) const = 0;
        virtual void _minimizeMemFootprint ( stType &, size_t i ) const = 0;
        virtual void _clearingCache ( stType &, size_t i ) = 0;
        virtual bool _needSpace() const = 0;

    private:
        void _init() {
            _tail = _head;
            _head->NEXT = 0;
        }
#ifndef HAVE_THREADS
        inline void _lockThread()  const  { }
        inline void _unlockThread() const { }
#else
        inline void _lockThread() const {
            pthread_mutex_lock ( &_mtx );
            return;
        }
        inline void _unlockThread() const {
            pthread_mutex_unlock ( &_mtx );
            return;
        }
        mutable pthread_mutex_t _mtx;
#endif
        mutable std::vector<stType> _content;

        /** managed objects are stored in a linked list to maintain
           a least recently used  (LRU) list, objects close to the
           \a _head have not been accessed for the longest period of time
           and are candidates for removal. \a _tail points to the
           last used managed object
        **/
        mutable managed *_head;
        mutable managed *_tail;
    };

    class referer {
    protected:
        referer ( const managed *m  ) {
            _target = m;
            if (_target) _target->lock();
        }

        ~referer() {
            if (_target) _target->unlock();
        }
        bool isManaged() const {
            return 0 != _target;
        }
        const managed *_target;
    };


    class managed {
    public:
#ifndef HAVE_THREADS
        static void initMutex() {}
#else
        static void initMutex() {
            if (_mutex_initialized) return;
            pthread_mutex_init ( &_mtx, NULL );
            _mutex_initialized = true;
        }
#endif

    protected:
        managed() : PREVIOUS ( 0 ), NEXT ( 0 ), _locked ( 0 ), _idx(0) {
            _checkMutex();
        }
        managed ( const managed &o )  : PREVIOUS ( 0 ), NEXT ( 0 ), _locked ( 0 ) {
            _checkMutex();
        }

        virtual ~managed() {
            if ( _locked ) {
                std::cerr << "DEBUG::MMU: managed class destroyed while there is at "
                << "least one referer (idx=" << _idx << ")" << std::endl;
            }
        }

    private:
        friend class referer;
        template <class stType, class accType> friend class swapedVector;


        void insert ( managed *where, size_t idx ) {
            NEXT = where->NEXT;
            if ( NEXT ) NEXT->PREVIOUS = this;
            where->NEXT = this;
            PREVIOUS = where;
            _locked = 0;
            _idx = idx;
            return;
        }

        void move2 ( managed *where ) {
            PREVIOUS->NEXT = NEXT;
            if (NEXT) NEXT->PREVIOUS = PREVIOUS;
            NEXT = where->NEXT;
            if ( NEXT ) NEXT->PREVIOUS = this;
            where->NEXT = this;
            PREVIOUS = where;
            return;
        }

        void remove() {
            assert ( !_locked );
            PREVIOUS->NEXT = NEXT;
            if ( NEXT ) NEXT->PREVIOUS = PREVIOUS;
            PREVIOUS = 0;
            return;
        }

        friend int main(int, char **);

#ifndef HAVE_THREADS
    private:
        void _checkMutex() {}
        inline void _lockThread()  const  { }
        inline void _unlockThread() const { }
#else

    private:
        // Only one mutex for all, we can not instantiate 100000 mutexes....
        static bool _mutex_initialized;
        static pthread_mutex_t _mtx;

        void _checkMutex() {
            assert(_mutex_initialized);
        }

        inline void _lockThread() const {
            pthread_mutex_lock ( &_mtx );
            return;
        }

        inline void _unlockThread() const {
            pthread_mutex_unlock ( &_mtx );
            return;
        }
#endif

        void lock (bool lockThread = true)  const {
            if (lockThread) {
                _lockThread();
                ++_locked;
                _unlockThread();
            } else {
                ++_locked;
            }
        }

        void unlock (bool lockThread = true)  const {
            if (lockThread) {
                _lockThread();
                --_locked;
                _unlockThread();
            } else {
                --_locked;
            }
        }

        bool locked() {
            return (0 != _locked);
        }

        bool inMem()  const {
            return PREVIOUS != 0;
        }

        size_t idx() {
            return _idx;
        }

        managed *PREVIOUS, *NEXT;
        mutable int  _locked;
        mutable size_t _idx;
    };
};

#endif
