/***************************************************************************
 numericMedian.h  -  description
 -------------------
 begin                : Fri Jan 30 2004
 copyright            : (C) 2004-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_NUMERIC_MEDIAN_H
#define __GULI_NUMERIC_MEDIAN_H
#include <algorithm>
#include <assert.h>
#include "guliconfig.h"
namespace GULI {
		template<class inputIt>
		double median(inputIt begin, inputIt end) {
			const int N = end - begin;
			assert(N);
			double *t = new double[N];
			std::copy(begin, end, t);
			std::sort(t, t + N);
			double r = (N & 0x1) ? t[N / 2] : (t[N / 2] + t[N / 2 + 1]) / 2.;
			delete[] t;
			return r;
		}
} //namespace
#endif

