/***************************************************************************
 dataWrapperDebug.h  -  description
 -------------------
 begin                : Wed Nov 19 2003
 copyright            : (C) 2003 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_DATA_WRAPPER_DEBUG
#define __MULI_DATA_WRAPPER_DEBUG
#include "guliconfig.h"
#include "dataIF.h"
namespace GULI {
	/** A wrapper around any dataIF class which checks that an access is within
	 ** the allocated size limits.
	 ** For the details of the API see the annotation of the base class dataIF.
	 **/
	class dataDebug: public dataIF {
	public:
		/** if own is true, the child object is owned by the class and
		 ** will be destructed together with  the wrapper
		 **/
		dataDebug(dataIF &t, bool own = true) :
			_target(t), _own(own) {
		}

		/** If the object is instantiated with a pointer the class _always_
		 ** owns the target.
		 **/
		dataDebug(dataIF *t) :
			_target(*t), _own(true) {
		}
		;

		virtual ~dataDebug() {
			if (_own) delete &_target;
		}

		virtual const unsigned int cols() const {
			return _target.cols();
		}
		virtual const unsigned int rows() const {
			return _target.rows();
		}
		virtual float &Data(int col, int row) {
			return _target.Data(_checkCol(col), _checkRow(row));
		}

		virtual float Data(int col, int row) const {
			return _target.Data(_checkCol(col), _checkRow(row));
		}

//		virtual float &operator()(int col, int row) {
//			return Data(col, row);
//		}

		virtual std::string &rowId(int i) {
			return _target.rowId(_checkRow(i));
		}
		virtual std::string &colId(int i) {
			return _target.colId(_checkCol(i));
		}

		/** return the String ID for a row (const)
		 **/
		virtual const std::string &rowId(int i) const {
			return _id[_checkRow(i)];
		}

		/** return the String ID for a column (const)
		 **/
		virtual const std::string &colId(int i) const {
			return _id[_checkCol(i)];
		}

	private:
		int _checkCol(int C) {
			int c = cols();
			assert(C >=0 && C < c);
			return C;
		}
		int _checkRow(int R) {
			int r = rows();
			assert(R >=0 && R < r);
			return R;
		}
		dataIF &_target;
		bool _own;
	};
} //namespace MULI::data::wrapper
#endif

