/***************************************************************************
 ontologyXmlIO.h  -  description
 -------------------
 begin                : Wed Apr 16 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                               *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __ontologyXmlIO_h
#define __ontologyXmlIO_h
#include "geneOntology.h"
#include "xmlGeneric.h"
namespace GULI {
	class ontologyXmlIO: private xmlGeneric {
	public:
		geneOntology::textual *read(const char *filename);

	private:
		void _process_accession(geneOntology::textualNode &t, tokentype &to) {
			t.identifier = to.text;
		}

		void _process_name(geneOntology::textualNode &t, tokentype &to) {
			t.name = to.text;
		}

		void _process_description(geneOntology::textualNode &t, tokentype &to) {
			t.description = to.text;
		}

		void _process_relation(geneOntology::textualNode &, tokentype &);

		geneOntology::textual *_target;
		virtual bool _eatToken(tokentype &token);

		typedef void (ontologyXmlIO::* methodtype)(geneOntology::textualNode &target, tokentype &token);

	public:
		typedef struct KEYWORD {
			const char *key;
			methodtype method;
		} keyword;

		static const keyword Keys[];
	};
} //NAMESPACE
#endif
