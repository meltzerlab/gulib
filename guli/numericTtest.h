/***************************************************************************
 numericTtest.h  -  description
 -------------------
 begin                : Wed Jan 15 2002
 copyright            : (C) 2002-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/** Implementation of the T-Test
 */
#ifndef __GULI_TTEST
#define __GULI_TTEST
#include <algorithm>
#include <math.h>
#include "numericDscstat.h"
#include "numericDistribution.h"

namespace GULI {
	class ttest {
	protected:
		double calcSameVar(dscstat &P1, dscstat &P2) {
			_dof = P1.dof() + P2.dof() - 2.;
			double sd = ((P1.dof() - 1.) * P1.variance() + (P2.dof() - 1.)
					* P2.variance()) / _dof;
			_T = (P1.average() - P2.average()) / sqrt(sd * (1. / P1.dof() + 1.
					/ P2.dof()));
			_P = distribution::betai(0.5 * _dof, 0.5, _dof / (_dof + _T * _T));
			return _P;
		}

		double calcDiffVar(dscstat &P1, dscstat &P2) {
			_T = (P1.average() - P2.average()) / sqrt(P1.variance() / P1.dof()
					+ P2.variance() / P2.dof());
			double v1 = P1.variance() / P1.dof();
			double v2 = P2.variance() / P2.dof();
			_dof = pow(v1 + v2, 2) / (pow(v1, 2) / (P1.dof() - 1.) + pow(v2, 2)
					/ (P2.dof() - 1.));
			_P = distribution::betai(0.5 * _dof, 0.5, _dof / (_dof + _T * _T));
			return _P;
		}

	public:
		template<class it> double SameVar(it begin1, it end1, it begin2,
				it end2) {
			dscstat P1(begin1, end1);
			dscstat P2(begin2, end2);
			return calcSameVar(P1, P2);
		}

		template<class it> double SameVar(it begin, it end) {
			dscstat P(begin, end);
			return calcSameVar(P, _P1);
		}

		template<class it> double DiffVar(it begin1, it end1, it begin2,
				it end2) {
			dscstat P1(begin1, end1);
			dscstat P2(begin2, end2);
			return calcDiffVar(P1, P2);
		}

		template<class it> double DiffVar(it begin, it end) {
			dscstat P(begin, end);
			return calcDiffVar(P, _P1);
		}

		template<class it> void setCompare(it begin, it end) {
			_P1.calculate(begin, end);
		}

		double TValue() {
			return _T;
		}
		double Dof() {
			return _dof;
		}
		double Probability() {
			return _P;
		}

	protected:
		double _dof, _P, _T;
		dscstat _P1;
	};
} //namespace SB::statistic
#endif
