#ifndef __GULI__GZSTREAM_H
#define __GULI__GZSTREAM_H 1

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <iostream>
#include <fstream>
#include <zlib.h>


namespace GULI {
	class igzstream : public boost::iostreams::filtering_stream<boost::iostreams::input> {
	public:
		igzstream(std::istream &I) : _I(0), _own(false) {
			if(_checkGzMagic(I))
				push(boost::iostreams::gzip_decompressor());
			push(I);
		}

		igzstream(std::ifstream &I) : _I(&I),  _own(false) {
			if(_checkGzMagic(I))
				push(boost::iostreams::gzip_decompressor());
			push(I);
		}

		igzstream(const char* name, std::ios_base::openmode open_mode =  std::ios::in | std::ios::binary) : _own(true)   {
			_I = new std::ifstream(name, open_mode);
			if(_checkGzMagic(*_I))
				push(boost::iostreams::gzip_decompressor());
			push(*_I);
		}

		void close() {
			if(_I && _I->is_open()) {
				_I->close();
			}
		}

		bool is_open() {if(_I) return _I->is_open(); if(_own) return false; return true;}

		~igzstream() {
			if(_own) {
				_I->close();
				delete _I;
			}
		}
	private:
		static bool _checkGzMagic(std::istream &I) {
			unsigned char c1, c2;
			c1 = I.get();
			c2 = I.get();
			I.putback(c2);
			I.putback(c1);
			return (c1 == 0x1F) && (c2 == 0x8B);
		}

		std::ifstream *_I;
		bool   _own;
	};

	class ogzstream : public boost::iostreams::filtering_stream<boost::iostreams::output> {
	public:
		ogzstream(std::ostream &O) : _O(0), _own(false) {
			push(boost::iostreams::gzip_compressor());
			push(O);
		}

		ogzstream(std::ofstream &O) : _O(&O),  _own(false) {
			push(boost::iostreams::gzip_compressor());
			push(O);
		}

		ogzstream(const char* name, std::ios_base::openmode open_mode =  std::ios::out | std::ios::binary) : _own(true)   {
			_O = new std::ofstream(name, open_mode);
			push(boost::iostreams::gzip_compressor());
			push(*_O);
		}

		void close() {
			boost::iostreams::close(*this);
			if(_O && _O->is_open()) {
				_O->close();
			}
		}

		bool is_open() {if(_O) return _O->is_open(); if(_own) return false; return true;}

		~ogzstream() {
			if(_own) {
				_O->close();
				delete _O;
			}
		}
	private:
		std::ofstream *_O;
		bool   _own;
	};
} // NAMESPACE

#endif // __GULI_GZSTREAM_H

