/***************************************************************************
 dataWrapperRandomSubset.h  -  description
 -------------------
 begin                : Sun Feb 8 2004
 copyright            : (C) 2004 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_DATA_RANDOMSUBSET_H
#define __MULI_DATA_RANDOMSUBSET_H
#include "stdinc.h"
#include "dataIF.h"

namespace GULI {
	/** a dataIF  introducing randomized access to another dataIF
	 **/
	class dataRandomSubset: public dataIF {
	public:
		virtual ~dataRandomSubset() {
		}

		/** Instantiate the new dataIF and connect to \a target
		 **/
		dataRandomSubset(dataIF &target) :
			_target(target) {
			reset();
		}

		/** Transparent access to the full dataset in both directions **/
		void reset();

		/** Select a random subset in row-direction, where every row is selected with probability P
		 * Thes method is depreciated and only provided for backward compatibility with an older version of randomSubset
		 **/
		void randomRows(double P);

		/** Select a random subset in row-direction, where every row is selected with probability P
		 * Thes method is depreciated and only provided for backward compatibility with an older version of randomSubset
		 **/
		void randomCols(double P);

		/** Present a subset of N  randomly chosen rows via this dataIF interface. With every call a new random
		 * subset is selected
		 **/
		void randomRows(int N = -1);

		/** Present a subset of N  randomly chosen columns via this dataIF interface. With every call a new random
		 * subset is selected
		 **/
		void randomCols(int N = -1);

		/** Return the number of Columns in the Data
		 *  Re-implemented for internal reasons
		 **/
		virtual const unsigned int cols() const {
			return _subC;
		}

		/** Return the number of Rows in the Data
		 *  Re-implemented for internal reasons
		 **/
		virtual const unsigned int rows() const {
			return _subR;
		}

		/** return the Data content at column, row
		 *  Re-implemented for internal reasons
		 **/
		virtual float &Data(int col, int row) {
			return _target(_colIdx[col], _rowIdx[row]);
		}

		virtual float Data(int col, int row) const {
			return _target(_colIdx[col], _rowIdx[row]);
		}

		/** return the String ID for a row
		 *  Re-implemented for internal reasons
		 **/
		virtual std::string &rowId(int i) {
			return _target.rowId(_rowIdx[i]);
		}

		/** return the String ID for a column
		 *  Re-implemented for internal reasons
		 **/
		virtual std::string &colId(int i) {
			return _target.colId(_colIdx[i]);
		}

		/** return the String ID for a row (const)
		 **/
		virtual const std::string &rowId(int i) const {
			return _target.rowId(_rowIdx[i]);

		}

		/** return the String ID for a column (const)
		 **/
		virtual const std::string &colId(int i) const {
			return _target.colId(_colIdx[i]);
		}

		enum { ROW = 1, COL=2 };
		virtual void syncRandomSelections(dataRandomSubset &r, int mode);

	private:
		dataIF &_target;
		vector<int> _rowIdx, _colIdx;
		int _subC, _subR;
	};
} // namespace GULI::data::wrapper
#endif // defined __randomsubsetdata
