/***************************************************************************
 numericHistogram.h  -  description
 -------------------
 begin                : Thu Jul 18 2002
 copyright            : (C) 2002-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_NUMERIC_HISTOGRAM_H
#define __MULI_NUMERIC_HISTOGRAM_H
#include <functional>
#include <algorithm>
#include <math.h>
#include "guliconfig.h"
#include "stdinc.h"
using std::vector;
/**
 *@author sven
 */
namespace GULI {
		struct Partition {
			class API {
			public:
				API() {
				}
				virtual ~API() {
				}
				/** Return the id for the bin containing X
				 **/
				virtual int bin(double x) const = 0;
				/** Return the Number of Bins
				 **/
				virtual int Nbin() const = 0;

				/** Return the Center for bin #b
				 **/
				virtual double center(int b) const = 0;

				/** return the wodth for bin #b
				 **/
				virtual double width(int b) const = 0;

				/** A unique number iendifying the type of Partition. This is used to
				 *  compare two partitions
				 */
				virtual const int signature() const = 0;

				/** Return the lower bound
				 **/
				virtual double lower() const = 0;

				/** Return the upper bound
				 **/
				virtual double upper() const = 0;

				/** Return the width parameter (in linear partitions: width)
				 **/
				virtual double delta() const = 0;

				/** Clone the current partition, the routine invoking is the OWNER (must delete)
				 *  the returned Object.
				 **/
				virtual API *clone() const = 0;

				/** Compare two partitions
				 **/
				bool operator==(const API &B);

			protected:
				/** If one needs to extend the comparison of two partitions of the same type
				 *  do it here
				 **/
				virtual bool _specific_compare(const API &) {
					return true;
				}
			};

			class linear: public API {
			public:
				linear(double x0, double xN, double delta) :
					_x0(x0), _xN(xN), _delta(delta) {
				}
				int bin(double x) const;
				int Nbin() const;
				double center(int b) const;
				double width(int b) const;
				const int signature() const {
					return 123456;
				}
				double lower() const {
					return _x0;
				}
				double upper() const {
					return _xN;
				}
				double delta() const {
					return _delta;
				}
				API *clone() const {
					return new linear(_x0, _xN, _delta);
				}
			private:
				double _x0, _xN, _delta;
			};

			class logarithmic: public API {
			public:
				logarithmic(double x0, double xN, double delta, double x0Min =
						1e-4);
				int bin(double x) const;
				int Nbin() const;
				double center(int b) const;
				double width(int b) const;
				API *clone() const;
				const int signature() const {
					return 6543210;
				}
				double lower() const {
					return _x0;
				}
				double upper() const {
					return _xN;
				}
				double delta() const {
					return _delta;
				}
			private:
				double _x0, _xN, _delta;
				double _x0Min, _c0, _w0;
			};
		};

		class histogram {
		public:
			histogram(Partition::API &part) {
				_partition = part.clone();
				buffer = new vector<double> (partition().Nbin());
			}

			histogram(const histogram &B) {
				_partition = 0;
				buffer = new vector<double> ;
				*this = B;
			}

			virtual ~histogram() {
				delete buffer;
				delete _partition;
			}

			inline void event(double value) {
				int l = partition().bin(value);
				if (l < 0 || l >= partition().Nbin()) return;
				(*buffer)[l] += 1.;
			}

			template<class it> void event(it begin, it end) {
				for (; begin != end; ++begin)
					event(*begin);
			}

			inline double entries(int n) const {
				double r;
				if (n < 0 || n >= partition().Nbin()) return 0;
				r = (*buffer)[n] / partition().width(n);
				return r;
			}

			inline double entries(double x) const {
				return entries(partition().bin(x));
			}

			template<class it> void copyEntries(it target) {
				const int np = buffer->size();
				for (int i = 0; i < np; i++)
					*target++ = entries(i);
			}

			inline double cummulativeEntries(int n) const {
				const int np = buffer->size();
				double r = 0.;
				if (n < 0) return 0;
				n = (n >= np) ? np - 1 : n;
				for (int i = 0; i < n; i++)
					r += (*buffer)[i] / partition().width(i);
				return r;
			}

			template<class it> void copyCummulativeEntries(it target) {
				const int np = buffer->size();
				double r = 0;
				for (int i = 0; i < np; i++) {
					r += entries(i);
					*target++ = r;
				}
			}

			inline void clear() {
				std::fill(buffer->begin(), buffer->end(), 0);
			}
			;

			inline void normalize() {
				*this /= cummulativeEntries(buffer->size() - 1);
			}

			int Nbin() const {
				return partition().Nbin();
			}

			double center(int n) const {
				return partition().center(n);
			}

			Partition::API &partition() const {
				return *_partition;
			}
		protected:
			struct constMult {
				constMult(double fac) :
					_fac(fac) {
				}
				;
				double operator()(double dummy, double a) {
					return a * _fac;
				}
				double _fac;
			};

			void check_arithmetic_argument(const histogram &B) const {
#ifdef HAVE_ASSERT
				assert(partition() == B.partition());
#endif

			}

			template<class op>
			histogram &self_arit(const histogram &B, op O) {
				check_arithmetic_argument(B);
				std::transform(buffer->begin(), buffer->end(),
						B.buffer->begin(), buffer->begin(), O);
				return *this;
			}

			template<class op>
			histogram other_arit(const histogram &B, op O) const {
				check_arithmetic_argument(B);
				histogram result(partition());
				std::transform(buffer->begin(), buffer->end(),
						B.buffer->begin(), result.buffer->begin(), O);
				return result;
			}

		public:
			template<class OP>
			void transform(OP t) {
				for (unsigned int i = 0; i < buffer->size(); i++)
					(*buffer)[i] = t(center(i), (*buffer)[i]);
			}

			// Some simple maths to make histograms a bit more versatile
			inline histogram &operator +=(const histogram &B) {
				return self_arit(B, std::plus<double>());
			}
			inline histogram &operator -=(const histogram &B) {
				return self_arit(B, std::minus<double>());
			}
			inline histogram &operator *=(const histogram &B) {
				return self_arit(B, std::multiplies<double>());
			}
			inline histogram &operator /=(const histogram &B) {
				return self_arit(B, std::divides<double>());
			}
			inline histogram operator+(const histogram &B) const {
				return other_arit(B, std::plus<double>());
			}
			inline histogram operator-(const histogram &B) const {
				return other_arit(B, std::minus<double>());
			}
			inline histogram operator*(const histogram &B) const {
				return other_arit(B, std::multiplies<double>());
			}
			inline histogram operator/(const histogram &B) const {
				return other_arit(B, std::divides<double>());
			}
			inline histogram &operator *=(const double fac) {
				transform(constMult(fac));
				return *this;
			}
			inline histogram &operator /=(const double fac) {
				return (*this *= 1. / fac);
			}
			histogram &operator =(const histogram &B);

		private:
			Partition::API *_partition;
			vector<double> *buffer;
		};
}//NAMESPACE
#endif
