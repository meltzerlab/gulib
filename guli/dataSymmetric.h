/***************************************************************************
 dataMemarray.h  -  description
 -------------------
 begin                : Thu Jun 5 2003
 copyright            : (C) 2003 by sven
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_DATA_SYMMETRIC_H
#define __GULI_DATA_SYMMETRIC_H
#include "stdinc.h"
#include "dataIF.h"
namespace GULI {
	/** a Data container based on (standard) allocated memory.
	 for SYMMETRIC matrices. This implementation uses only 50% of
	 the generic implementation when used on symmetric data.
	 **/
	class dataSymmetric: public dataIF {
	public:
		virtual ~dataSymmetric();

		/** Construct a symmetric dataIF with \a size x \a size rows and columns */
		dataSymmetric(unsigned int size);

		/** Copy constructor for symmetric databases */
		dataSymmetric(dataSymmetric &);

		/** Copy constructor for general dataIF objects
		 ** Method copies the upper triangular matrix to the symmetric matrix
		 ** copies the rowIds as the new idetifiers.
		 **/
		dataSymmetric(dataIF &);

		/** Return the number of Columns in the Data
		 **/
		virtual const unsigned int cols() const {
			return _size;
		}

		/** Return the number of Rows in the Data
		 **/
		virtual const unsigned int rows() const {
			return _size;
		}

		/** return the Data content at column, row
		 **/
		virtual float &Data(int col, int row);

		/** return the Data content at column, row
		 **/
		virtual float Data(int col, int row) const;


		/** a shortcut for the Data function
		 **/
		virtual float &operator()(int col, int row) {
			return Data(col, row);
		}

		/** return the String ID for a row
		 **/
		virtual std::string &rowId(int i) {
			return _id[i];
		}


		/** return the String ID for a column
		 **/
		virtual std::string &colId(int i) {
			return _id[i];
		}

		/** return the String ID for a row (const)
		 **/
		virtual const std::string &rowId(int i) const {
			return _id[i];
		}

		/** return the String ID for a column (const)
		 **/
		virtual const std::string &colId(int i) const {
			return _id[i];
		}
	private:
		float *_data;
		unsigned int _size;
		vector<string> _id;
	};
} // namespace GULI::data

#endif // defined __symmetricData
