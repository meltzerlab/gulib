/***************************************************************************
 numericAnovaOneway.h  -  description
 -------------------
 begin                : Mon Jan 19 2004
 copyright            : (C) 2004-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __ANOVA_OW
#define __ANOVA_OW
#include <vector>
#include <assert.h>
#include "numericDscstat.h"
#include "numericFtest.h"
using std::vector;

namespace GULI {
	class anovaOneWay {
	public:
		anovaOneWay() :
			_SSwg(-1.), _SSbg(-1.), _DOFwg(-1), _DOFbg(-1) {
		}

		/** Do ANOVA on a set of groups. The template argument refers
		 ** to a class with a "NGroup()" method, returning the number of
		 ** groups, "begin(int n)" and "end(int n)} returning a iterator at the
		 ** start or end respectively of the data for the n-th group
		 ** The templated class "groups" needs to provide a method NGroup returning
		 ** the number of classes, and begin(i), end(i) returning iterators pointing
		 ** to the begin/end of the data for group i.
		 **/
		template<class groups> double calculate(groups &s) {
			int r = s.NGroup();
			assert(r >= 2);
			vector<double> av(r);
			vector<int> N(r);

			double avall = 0.;
			int Nt = 0;
			_SSwg = 0.;
			_SSbg = 0.;

			for (unsigned int i = 0; i < r; i++) {
				dscstat D(s.begin(i), s.end(i));
				int n = D.dof();
				av[i] = D.average();
				_SSwg += D.variance() * (n - 1);
				avall += av[i] * n;
				Nt += n;
				N[i] = n;
			}
			avall /= Nt;

			for (unsigned int i = 0; i < r; i++)
				_SSbg += N[i] * (av[i] - avall) * (av[i] - avall);

			_DOFwg = Nt - r;
			_DOFbg = r - 1;
			return Pvalue();
		}

		/** The sum of squares Within the Classes **/
		double sumOfSquaresWithin() {
			return _SSwg;
		}
		/** The sum of squares between classes **/
		double sumOfSquaresBetween() {
			return _SSbg;
		}
		/** The degrees of freedom within classes **/
		int dofWithin() {
			return _DOFwg;
		}
		/** The degrees of freedom between classes **/
		int dofBetween() {
			return _DOFbg;
		}
		/** The F-ratio between/within classes **/
		double F() {
			return (_SSbg * _DOFwg) / (_SSwg * _DOFbg);
		}
		/** The P-value estimated for this classifications **/
		double Pvalue() {
			return ftest::Probability(F(), _DOFwg, _DOFbg);
		}
	protected:
		double _SSwg, _SSbg, _P;
		int _DOFwg, _DOFbg;
	};
}
#endif
