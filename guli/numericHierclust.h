/***************************************************************************
 numericHierclust.h  -  description
 -------------------
 begin                : Wed Apr 16 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_NUMERIC_HIERCLUST_H
#define __MULI_NUMERIC_HIERCLUST_H
#include "guliconfig.h"
#include "stdinc.h"
#include "dataIF.h"
#include "numericMetric.h"
#include "numericZscore.h"

namespace GULI {
		class hierClust {
		public:
			typedef vector<float> data;
			typedef enum {
				COLUMNS, ROWS
			} orientation;

			/**
			 ** This class calculates the new pattern from two linked patterns
			 **/
			class Mode {
			public:
				virtual ~Mode(){}
				virtual void operator()(data &in1, data &in2, data &out) = 0;
			};

			/** This class defines the Distance-Metric to be used
			 ** it operates on float-vectors
			 **/
			class Metric {
			public:
				virtual ~Metric() {}
				virtual float operator()(data &d1, data &d2) = 0;

				/**
				 ** In some cases it is more effective to pre-calculate some information
				 ** for each data-line (like, variance, average...). We support this by
				 ** telling the numericMetric.how many different data-lines we have (at most)
				 **/
				virtual void setCacheSize(int maxKey) {
					return;
				}

				/**
				 ** ... anounce each dataline BEFORE using it (with the possibility we overwrite
				 ** some line later re-using the same id)
				 **/
				virtual void prepareCache(data &d, int key) {
					return;
				}

				/**
				 ** return the distance between d1, d2. We supply the same keys which were used
				 ** when the data-lines were "prepared" by the metric
				 **/
				virtual float operator()(data &d1, data &d2, int key1, int key2) {
					return operator()(d1, d2);
				}
			};

			/** Normalization Object applied after copying the original data to a buffer
			 **  in the leaf-node
			 **/
			class normalizeData {
			public:
				virtual ~normalizeData(){}
				virtual void operator()(data::iterator begin,
						data::iterator end) = 0;
			};

			class node;
			/** This class is used to traverse the tree
			 **/
			class visitor {
			public:
				virtual ~visitor(){}
				virtual void *visit(class node *n, void *leftRes,
						void *rightRes) = 0;
			};

			/** The representation of the nodes ihe hierarchical clustering dendogram
			 ** it contains information about children and also the pattern associated
			 ** with this node.
			 **/
			class node {
			public:
				node(int id, node *c1, node *c2, node *p, Mode &,
						const string &l = _intermediate, normalizeData *N = 0);

				template<class it> node(int id, it begin, it end,
						const string &l = _intermediate, normalizeData *N = 0) :
					_leftChild(0), _rightChild(0), _parent(0), _id(id), _label(
							l) {
					_p.resize(end - begin);
					std::copy(begin, end, _p.begin());
					if (N) (*N)(_p.begin(), _p.end());
				}

				virtual ~node();
				/** Return true if this node is an endpoint **/
				bool isLeaf() {
					if (!_leftChild) return true;
					return false;
				}

				/** Return a pointer to the "left" child of this node (NULL if leaf) **/
				node *leftChild() const {
					return _leftChild;
				}
				/** Return a pointer to the "right" child of this node (NULL if leaf) **/
				node *rightChild() const {
					return _rightChild;
				}
				/** Return a pointer to the parent of this node (NULL if root) **/
				node *parent() const {
					return _parent;
				}

				virtual void *visit(visitor *V);

				const string &label() const {
					return _label;
				}
				int id() const {
					return _id;
				}
				int &uservar() {
					return _uservar;
				}
			private:
				friend class hierClust;
				vector<float> _p;
				node *_leftChild, *_rightChild, *_parent;
				int _id, _uservar;
				const string _label;
				static const string _intermediate;
			};

			hierClust(Mode &mo, Metric &met) :
				metric(met), mode(mo), _distance(0) {
			}
			node *cluster(dataIF &D, orientation O = ROWS);
			vector<int> *indexTable(node *root);

		private:
			Metric &metric;
			Mode &mode;
			node *_cluster(int, int);
			dataIF *_distance;

			/**
			 ** The data structure is designed to optimize performance, so its a bit difficult to read....
			 ** The problem is, that we have to shrink the quadratic distance metric at run-time (or, grow
			 ** it with every new node and keep track of "removed" entries). Doing this is terribly
			 ** expensive, therefore we introduce an index-space, pointing from a range, wich is _always_
			 ** continuous [the "linear" space we would like to have] to the coordinates in the "real"
			 ** matrix space, which is messy. At each node, the method id() returns the
			 ** coordinate used to address the symmetric similarity metric array. For "active" nodes, this
			 ** is the inverse operation of _index. they point back to the  continuous space.
			 ** The id() method is _undefined_ for nodes not referenced (anymore) from _index (and, indices WILL be
			 ** re-used, because we want to re-use the matrix !)
			 **/
			vector<int> _index;
			// Keeps all the nodes
			vector<node *> _nodes;

			/**
			 ** Did you ever try to find the smalles value in a huge matrix ? And, this, very often ?
			 ** Well, better not. Therefore, to improve this, we introduce a "distance-cache" which
			 ** holds for each node a reference and the distance to the closest neighbour. It is a bit
			 ** expensive to update, but fast to search. These arrays are reference by the _continous_ space
			 ** i.e. index space. So, the labels reside in the linear _index_space (see above).
			 ** when one wants to access the corresponding node, don't forget to _index[i] it...
			 **/

			vector<int> _next_nb;
			vector<float> _next_d;

			/**
			 ** remove references to abondoned nodes from the _next_nb and _next_d
			 **/
			void _mob_up(int, int);
			/**
			 ** move contents of _index, _next_d, _next_nb to "to" from "from", update references
			 **/
			void _move2(int to, int from);

			/**
			 ** Fill in the distance matrix for row i. The second can be used to start
			 ** from position 0 (the general case) or from i+1, which is useful at initialization
			 **/
			void _updateDistanceSlice(unsigned int i, unsigned int start2nd = 0);

			void _debug_cache_consistency();
		};

		class commonModes {
		public:
			class averageLinkage: public hierClust::Mode {
			public:
				virtual ~averageLinkage(){}
				void operator()(hierClust::data &i1, hierClust::data &i2,
						hierClust::data &t) {
					std::transform(i1.begin(), i1.end(), i2.begin(), t.begin(),
							*this);
				}
				float operator()(float x1, float x2) {
					return static_cast<float> ((x1 + x2) / 2.);
				}
			};
		};

		class commonNorms {
		public:
			class noNorm: public hierClust::normalizeData {
				virtual  ~noNorm(){}
				virtual void operator()(hierClust::data::iterator begin,
						hierClust::data::iterator end) {
				}
				;
			};

			class zscoreNorm: public hierClust::normalizeData {
				virtual  ~zscoreNorm(){}
				virtual void operator()(hierClust::data::iterator begin,
						hierClust::data::iterator end) {
					zscore(begin, end);
				}
			};
		};

		class commonMetric {
		private:
			template<class coreMetric> class metricAdaptor: public hierClust::Metric {
			public:
				virtual  ~metricAdaptor(){}
				virtual float operator()(hierClust::data &d1,
						hierClust::data &d2) {
					coreMetric M;
					return M(d1.begin(), d1.end(), d2.begin());
				}
			};

		public:
			typedef metricAdaptor<euclidean> _euclidean;
			typedef metricAdaptor<pearson> _pearson;
			typedef metricAdaptor<uncenteredCorr> _uncenteredCorr;
			typedef metricAdaptor<manhatten> _manhatten;

			class hcpearson: public _pearson {
				typedef hierClust::data data;
			public:
				virtual ~hcpearson(){}
				virtual void setCacheSize(int maxKey) {
					_avg.resize(maxKey);
					_var.resize(maxKey);
				}
				virtual void prepareCache(data &, int);
				virtual float operator()(data &, data &, int, int);
			private:
				vector<float> _var;
				vector<float> _avg;
			};
		};
} // Namespace
#endif

