/***************************************************************************
 numericPca.h  -  description
 -------------------
 begin                : Wed Mar 4 1999
 copyright            : (C) 1999-2009 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_PCA
#define __GULI_PCA
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

namespace GULI {
	class dataIF;
	namespace PCA {
		class dmemarray;

		/** The API for all PCA implementations
		 **/
		class API {
		public:
			/** valid values used to decide if data should be zscored, centered or
			 ** remain unprocessed  before performing PCA or projection
			 */
			typedef enum {
				zscore, center, raw
			} preproc;
			/** Return eigenvalue corresponding to eigenvector \a i
			 */
			virtual double eigenValue(int i) = 0;

			/** Return component \a comp of vector \a vec
			 */
			virtual double eigenVector(int vec, int comp)= 0;

			/** Project Data (ROWS) \a dat onto the first \a D components of the
			 **  Eigenvector-Space. The argument \s is oneof the enumerated values
			 ** of preproc.
			 **/
			virtual dataIF *projectData(const dataIF &dat, int D, int s = raw) =0;

			virtual ~API() {
			}
		};

		/** Principal Component Analysis for ROW Vectors. This class calculates
		 ** DD^T, diagonolizes this matrix and provides the Eigenvalues and Eigenvectors
		 ** of this matrix. It is generally a bad idea to use this object, singular
		 ** value compositionis more effective in most cases.
		 **/
		class classic: public API {
		public:
			/** Do PCA for the Data presented in the dataIF
			 **/
			classic(const dataIF &, int = raw);
			virtual ~classic();

			dataIF *projectData(const dataIF &, int, int = raw);
			double eigenValue(int);
			double eigenVector(int vec, int comp);
		protected:
			void _setupCorrelationMatrix(const dataIF &, int);
			dmemarray *_eval, *_evec;
		};

		/** Principal Component Analysis for ROW Vectors. This implementation
		 ** is based on singular value decomposition and is therefore more efficient
		 ** than the straight-forward diagonalization used in the "classic" object
		 **/
		class svd: public API {
		public:
			/** Do PCA for the Data presented in the dataIF
			 **/
			svd(const dataIF &, int = raw);
			virtual ~svd();

			/** Project Data (ROWS) on the first D components of the Eigenvector-Space
			 **/
			dataIF *projectData(const dataIF &, int D, int P = raw);

			/** Return Eigenvalue \a i
			 **/
			double eigenValue(int i) {
				double ev = gsl_vector_get(_L, i);
				return ev * ev;
			}

			/** Return component \a comp of eigenvector \a vec
			 **/
			double eigenVector(int vec, int comp) {
				return gsl_matrix_get(_A, comp, vec);
			}

			/** Return component \a comp of conjugate eigenvector \a vec
			 **/
			double complementEigenVector(int vec, int comp) {
				return gsl_matrix_get(_V, comp, vec);
			}

		protected:
			gsl_matrix *_V, *_A; // _U == _A by GSL convention
			gsl_vector *_L;
		};

		/** Kernel based PCA (see [xxxx] (INSERT CITATION)
		 ** This is quite old code, it may or may not work. Needs to be debugged
		 **/
		class kernelBased: public API {
		public:
			class Kernel {
			public:
				virtual double DotProduct(const dataIF &, int, const dataIF &, int)=0;
				virtual ~Kernel() {
				}
				int npar;
				double *parameter;
				double *defaultvalue;
			};

			kernelBased(const dataIF &, Kernel &);
			virtual ~kernelBased();
			dataIF *projectData(const dataIF &, int, int = raw);
			double eigenValue(int);
			double eigenVector(int, int);

			class Linear: public Kernel {
			public:
				Linear();
				virtual ~Linear() {
				}
				virtual double DotProduct(const dataIF &, int, const dataIF &, int);
			};

			class Polynomial: public Kernel {
			public:
				Polynomial();
				virtual ~Polynomial();
				virtual double DotProduct(const dataIF &, int, const dataIF &, int);
			};

			class Gaussian: public Kernel {
			public:
				Gaussian();
				virtual ~Gaussian();
				virtual double DotProduct(const dataIF &, int, const dataIF &, int);
			};

			class Sigmoid: public Kernel {
			public:
				Sigmoid();
				virtual ~Sigmoid();
				virtual double DotProduct(const dataIF &, int, const dataIF &, int);
			};
		protected:
			virtual void _setupCorrelationMatrix(const dataIF &);
			dmemarray *_eval, *_evec;
			Kernel *_kernel;
			const dataIF *_generatorData;
		};
	}

} // NAMSESPACE
#endif
