/*
 * numericKaplanMeier.h
 *
 *  Created on: Dec 30, 2015
 *      Author: sven
 */

#ifndef GULI_NUMERICKAPLANMEIER_H_
#define GULI_NUMERICKAPLANMEIER_H_
#include <gsl/gsl_sf_erf.h>
#include <cstddef>
#include <vector>
namespace GULI {
	class kaplanMeier {
	public:
		kaplanMeier();
		virtual ~kaplanMeier();

		class event {
		public:
			event() {}
			virtual ~event(){}

			virtual unsigned int T() 		const = 0;
			virtual unsigned int nCensored() const = 0;
			virtual unsigned int nEvent() 	const = 0;

			bool operator<(const event &b) const {return this->T() < b.T();}
		};

		void addEvent(const event &EV );

		size_t size() 	const;

		unsigned int T(size_t i) const;

		const event &operator[](size_t i) const;

		float eventFreeEstimate(size_t i) const;

		unsigned int Ni(size_t i) const;

		// return the number of _events_ (not censored) AT index i
		unsigned int Di(size_t i) const;

		float EventFreeEstimateAtTime_(unsigned int t) const { return eventFreeEstimate(_time2index(t)); }

		unsigned int NiAtTime(unsigned t) const;

		unsigned int DiAtTime(unsigned int t) const;

		static double logRank(const kaplanMeier &A, const kaplanMeier &B);

	private:
		class multiEvent;
		size_t _time2index(unsigned int t) const;
		std::vector<multiEvent> &_events;
		size_t _N;
	};
}
#endif /* GULI_NUMERICKAPLANMEIER_H_ */
