/***************************************************************************
 *   Copyright (C) 2006-2012 by Sven Bilke  							   *
 *   bilkes@mail.nih.gov   												   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SEQUENCEGENOMEREGIONSET_H
#define SEQUENCEGENOMEREGIONSET_H
#include "seqset.h"
#include "ucscGenome.h"
#include "bedRegion.h"
using GULI::bedRegion;
/**
 @author Sven Bilke <bilkes@mail.nih.gov>
 */
class seqSetGenomeRegion: public seqSet {
public:
	/** An object representing an entry in a BED file. It contains
	 ** a chromosome, start, end annotation. Object supports reading
	 ** from a stream containing BED data.
	 **/
	class compositeRegion : public bedRegion::listT {
	public:
		string &id() { return _id; }
		const string &id() const { return _id; }

		compositeRegion() {}

		template <class it> compositeRegion(it begin, it end) {
			size_t i = 0;
			resize(end - begin);
			while(begin != end) {
				(*this)[i++] = *begin++;
			}
		}
		compositeRegion(const bedRegion &b) { push_back(b); }
	private:
		string _id;
	};

	typedef vector<compositeRegion> selectionT;


	seqSetGenomeRegion();
	~seqSetGenomeRegion();

	/** Select regions from this list for future access **/
	void select(const bedRegion &B, const ucscGenome &);
	void select(const bedRegion::listT &, const ucscGenome &);
	void select(const selectionT &, const ucscGenome &);

	void setQuiet(bool t) { _quiet = t; }
	void clear();

	virtual void idle() { _cache.empty(); }

	virtual void prefetch(bool p) { _cache.prefetch(p);	}

	virtual size_t size() const {
		return _cache.size();
	}

	virtual const sequence operator[](size_t i) const { return _cache[i]; }

	/** limit the memory footprint of this object to maxmem **/
	void setMaxMem(size_t mem) { _cache.setMaxMem(mem);	}

	bool mask() const { return _cache.mask(); }
	bool mask(bool b) {  _cache.mask(b); return _cache.mask(); }
private:
	selectionT _region;
	const ucscGenome *_dsc;
	bool _quiet;

	typedef sequence::seqData datT;

	/** Implement IO functions for the MMU to load/unload regions */
	class cache: public mmu::swapedVector<datT, sequence> {
	public:
		cache(unsigned int m = 0) : _usedmem(0), _nmem(0), _maxmem(m), _mask(true) {}
		void setMaxMem(size_t s) {	_maxmem = s; }
		void select(const selectionT &, const ucscGenome &);
		virtual void empty();
		virtual void prefetch(bool p);

		void mask(bool b) { 	_mask = b; 	}
		bool mask() const { return _mask; }

	protected:
		virtual void _load2mem(datT &, size_t i) const;
		virtual void _minimizeMemFootprint(datT &, size_t i) const;
		virtual void _clearingCache(datT &d, size_t i) {
			_minimizeMemFootprint(d, i);
		}

		virtual bool _needSpace() const {
			return (_maxmem && (_nmem > 1) && (_usedmem > _maxmem));
		}


	private:
		mutable size_t _usedmem;
		mutable unsigned int _nmem;
		size_t _maxmem;
		const selectionT *_region;
		const ucscGenome *_dsc;
		bool _mask, _prf;
	};

	cache _cache;
};
istream &operator>>(istream &, seqSetGenomeRegion &b);
//istream &operator>>(istream &, seqSetGenomeRegion::region &b);

#endif
