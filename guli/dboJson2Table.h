/*
 * Json2Table.h
 *
 *  Created on: Feb 26, 2015
 *      Author: sven
 */

#ifndef DBOJSON2TABLE_H_
#define DBOJSON2TABLE_H_

#include <Wt/Dbo/Field.h>
#include <Wt/Json/Array.h>
#include <Wt/Dbo/Exception.h>

#include "dboTableBase.h"

using Wt::Dbo::FieldRef;
using Wt::Dbo::CollectionRef;
using Wt::Dbo::PtrRef;
using Wt::Dbo::WeakPtrRef;

namespace GULI {
	namespace Dbo {
		class Json2Table {
		public:
			Json2Table () :  _dateFormat("MM/dd/yy"),  _O(0), _S(0), _T(0) {}

			template <class V> void deserialize(const Wt::Json::Object &O, dbo::ptr<V> &v) {
				_S = v.session();
				_O = &O;
				_T = dynamic_cast<const tableBase *>(& (*v));
				// fill in database fields
				v.modify()->persist(*this);
				//call any methods
				for(std::string M : _O->names()) {
					if(_T->knownMethod(M)) v.modify()->template callMethod<V>(v, M, _O->get(M));
				}
			}

			const std::string &dateFormat() const { return _dateFormat; }
			std::string &dateFormat() { return _dateFormat; }


			template <typename V> void act(FieldRef<V>);
			template <typename V> void actCollection(CollectionRef<V> F);
			template <typename V> void actPtr(PtrRef<V> F);
			template <typename V> void actWeakPtr(WeakPtrRef<V> F){}
			template <typename V> void actId(V &v, const std::string &name, int size);
			Wt::Dbo::Session *session() { return _S; }
		protected:
			template <typename V> void _reference(const std::string &, dbo::ptr<V> &);
			template <typename V> void _instantiate(const Wt::Json::Object &, dbo::ptr<V> &);

		private:
			std::string _dateFormat;
			const Wt::Json::Object *_O;
			Wt::Dbo::Session *_S;
			const tableBase *_T;
			std::string _idLabel;
		};

		template <> void Json2Table::act<int>(FieldRef<int> F);

		template <> void Json2Table::act<long long int>(FieldRef<long long int> F);

		template <> void Json2Table::act<long long>(FieldRef<long long> F);

		template <> void Json2Table::act<bool>(FieldRef<bool> F);

		template <> void Json2Table::act<float>(FieldRef<float> F);

		template <> void Json2Table::act<double>(FieldRef<double> F);

		template <> void Json2Table::act<std::string>(FieldRef<std::string> F);

		template <> void Json2Table::act<Wt::Json::Object>(FieldRef<Wt::Json::Object> F);

		template <> void Json2Table::act<Wt::WDate>(FieldRef<Wt::WDate> F);
		template <> void Json2Table::actId<std::string>(std::string &v, const std::string &name, int size);

		template <typename V> void Json2Table::_instantiate(const Wt::Json::Object &O, dbo::ptr<V> &p) {
			if(V::idField() != "id") {
				if(O.contains(V::idField())) {
					std::string id = O.get(V::idField()).toString();
					dbo::ptr<V> f = session()->find<V>().where(V::idField() +  " = \'" + id + "'");
					if(f.get()) throw Wt::Dbo::NoUniqueResultException();
				}
			}
			p = session()->add(std::make_unique<V>());
			Json2Table D;
			D.deserialize(O, p); //
		}

		template <typename V> void Json2Table::_reference(const std::string &id, dbo::ptr<V> &p) {
			p = session()->find<V>().where(V::idField() +  " = \'" + id + "'");
			if(!p.get()) throw Wt::Dbo::ObjectNotFoundException(_S->tableName<V>(), id);
		}

		template <typename V> void Json2Table::actCollection(CollectionRef<V> F) {
			std::string name = V::tablename();
			Wt::Json::Type t = _O->type(name);
			dbo::ptr<V> p;
			switch(t) {
				case Wt::Json::Type::Null:  return;

				case Wt::Json::Type::String: {
					_reference(_O->get(name), p);
					F.value().insert(p);
					break;
				}

				case Wt::Json::Type::Object: {
					_instantiate(_O->get(name), p);
					F.value().insert(p);
					break;
				}

				case Wt::Json::Type::Array:  {
					const Wt::Json::Array &A = _O->get(name);

					for(size_t i=0; i < A.size(); ++i) {
						Wt::Json::Type at = A[i].type();

						switch(at) {
							case Wt::Json::Type::Null: break;

							case Wt::Json::Type::String:
								_reference(A[i].toString(), p);
								F.value().insert(p);
								break;

							case Wt::Json::Type::Object:
								_instantiate(A[i], p);
								F.value().insert(p);
								break;

							default:
								throw Wt::Dbo::Exception("Json2Table: Format error while parsing", name); // silently ignore arrays and the like
								break;
						}
					}
				} 	break;
				default: break; // Silently ignore things we do not understand
			}
		}

		template <typename V> void Json2Table::actPtr(PtrRef<V> F) {
			const std::string &name = F.name();
			Wt::Json::Type t = _O->type(name);
			dbo::ptr<V> p;
			switch(t) {
				case Wt::Json::Type::Null: return;
				case Wt::Json::Type::String: {
					_reference(_O->get(name), p);
					F.value() = p;
					break;
				}
				case Wt::Json::Type::Object: {
					_instantiate(*_O, p);
					F.value() = p;
					break;
				}

				default:
					throw Wt::Dbo::Exception("Json2Table: Format error while parsing", name); // silently ignore arrays and the like
					break;
			}
		}
	}
}
#endif /* DBOJSON2TABLE_H_ */
