/***************************************************************************
 numericHierclustObserv.h  -  description
 -------------------
 begin                : Wed Apr 16 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_NUMERIC_HIERCLUSTOBSERV_H
#define __MULI_NUMERIC_HIERCLUSTOBSERV_H
#include "guliconfig.h"
#include "stdinc.h"
#include "numericHierclust.h"
// Some forward declarations to avoid excessive includes...
namespace GULI {
	namespace hierClustTools {
		typedef hierClust::node node;

		/** The interface for a drawing device which is used to draw a  tree
		 ** with drawTree
		 **/
		class drawDevice {
		public:
			virtual ~drawDevice(){}
			virtual void start(int w, int h)= 0;
			virtual void drawNode(int, int) = 0;
			virtual void drawHorizontalLine(int x0, int x1, int y)=0;
			virtual void drawVerticalLine(int x, int y0, int y1)=0;
			virtual void drawLeaf(int x, int y, const string &id) = 0;
			virtual void end()=0;
		};

		/** Draw a tree starting from "start" on a drawing device "d"
		 **/
		void drawTree(node *start, drawDevice &d);

		/** Count the number of Leafs present below "start";
		 **/
		int countLeafs(node *start);

		/** Count the maximal depth to the leafs starting from "start"
		 **/
		int depth(node *start);

		/**  Calculate a matrix with distances between any two _leafs_ in the tree
		 **/
		dataIF *calcDistMatrix(node *start);

		/**  Calculate a matrix with density between any two _leafs_ in the tree
		 **/
		dataIF *calcDensMatrix(node *start);

		class leafFinder {
		public:
			leafFinder();
			void operator()(node *start);
			unsigned int size();
			node *operator[](int);
			/** get the Index (used in []) of node "nodeindex" (node::id()) in this table
			 **/
			int getIndex(int nodeindex);
		private:
			int _minidx, _maxidx;
			vector<node *> _leafs;
			vector<int> _reverseIndex;
		};

		extern leafFinder LEAFS;
	}
}
#endif
