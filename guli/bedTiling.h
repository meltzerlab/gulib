/*
 * bedFunctions.h
 *
 *  Created on: Feb 16, 2013
 *      Author: sven
 */

#ifndef BEDFUNCTIONS_H_
#define BEDFUNCTIONS_H_

#include <list>
#include <limits>
#include <limits.h>
#include "bedRegion.h"

static inline bool _overlap(const GULI::bedRegion &A, const GULI::bedRegion &B, unsigned int maxOuterDist) {
	if(!maxOuterDist) return A.overlap(B); // is slightly faster than the next statement
	return abs(A.outerDistance(B)) < (int) maxOuterDist;
}

namespace GULI {
	class bedTilingBase {
	public:
		bedTilingBase() : _nBins(0),  _cMagic(0) {}
		virtual ~bedTilingBase(){};

		typedef vector<size_t> targetListT;

		class tile : public bedRegion {
		public:
			tile(const bedRegion &b) : bedRegion(b) {};
			const targetListT &member() const { return _member; }
			targetListT &member()             { return _member; }
		protected:
			targetListT _member;
		};

		typedef vector<tile>   tileListT;


		virtual const tileListT &getTiling() const { return _tiling; }

		virtual void closest(const bedRegion &B, targetListT &r, size_t maxdist = std::numeric_limits<size_t>::max()) const;
		// return indices of bedRegions (utilizing the bedList order presented to this  object overlapping B
		virtual void overlaps(const bedRegion &B, targetListT &r, unsigned int maxOuterDist=0) const;

		// return Indices of the TILES overlapping a bedRegion
		virtual void overlapingTiles(const bedRegion &B, bedRegion::listT &r, unsigned int maxOuterDist=0) const;
		virtual void overlapingTiles(const bedRegion &B, targetListT      &r, unsigned int maxOuterDist=0) const;
		bool doesOverlap(const bedRegion &b) const;
		size_t nBins() const { return _nBins; } // Number of BED regions which generated this tiling

protected:
		static const unsigned int _maxMagic = UINT_MAX - 1;
		size_t _nBins;
		tileListT _tiling;
		mutable vector<unsigned int> _magic;
		mutable unsigned int _cMagic;
		unsigned int _newMagic() const {
			if(++_cMagic >= _maxMagic) {
				std::fill(_magic.begin(), _magic.end(), 0);
				_cMagic = 1;
			}
			return _cMagic;
		}
	};


	template<class it> class bedTiling : public bedTilingBase {
	public:

		class bedAccess {
		public:
			bedAccess(it i) : _i(i) {}
			operator   const bedRegion*() {return &(*_i);}
			const bedRegion *operator->() {return &(*_i);}
		private:
			it _i;
		};

		bedTiling(it start, it end) :  _start(start) {
			vector<size_t> ordered;
			_nBins = end - start;
			size_t s,e;

			ordered.resize(nBins());
			for(size_t i =0; i < nBins(); ++i) ordered[i] = i;
			std::sort(ordered.begin(), ordered.end(), _sorter(start));


			for(s = 0; s < nBins(); s = e) {
				typedef std::list<it> clusterT;
				clusterT  cluster;
				cluster.push_back(_start + ordered[s]);
				unsigned int lastPos = bedAccess(cluster.front())->end();
				const string &chr = bedAccess(cluster.front())->chromosome();
				// make e point to the first non-overlapping target
				for(e = s + 1; e < nBins() && bedAccess(start + ordered[e])->start() <= lastPos && bedAccess((start + ordered[e]))->chromosome() == chr; ++e) {
					cluster.push_back(start + ordered[e]);
					if(bedAccess(cluster.back())->end() > lastPos) lastPos = bedAccess(cluster.back())->end();
				}

				for(bedRegion outline(*bedAccess(cluster.front()));
					outline.start() < lastPos;
					outline.start(outline.end()), outline.end(lastPos))
				{
					// identify next segment
					for(typename clusterT::const_iterator j = cluster.begin(); j != cluster.end(); ++j) {
						if (bedAccess(*j)->start() > outline.start() && bedAccess(*j)->start() < outline.end()) outline.end(bedAccess(*j)->start());
						if (bedAccess(*j)->end()   > outline.start() && bedAccess(*j)->end()   < outline.end()) outline.end(bedAccess(*j)->end());
					}

					tile cur(outline);

					for(typename clusterT::iterator j = cluster.begin(); j != cluster.end(); ) {
						if(outline.overlap(*bedAccess(*j))) {
							cur.member().push_back(*j - _start);
							++j;
						} else {
							( *bedAccess(*j) < outline) ? j = cluster.erase(j) : ++j;
						}
					}
					_tiling.push_back(cur);
				}
			}
			_magic.resize(nBins());
			std::fill(_magic.begin(), _magic.end(), _cMagic);
			return;
		}

		virtual ~bedTiling(){}
	private:
		it _start;
		struct _sorter {
			_sorter(it start) : _start(start){}
			bool operator()(size_t i1, size_t i2) {
				return  *bedAccess(_start + i1) < *bedAccess(_start + i2);
			}
			it _start;
		};

	};
}// NAMESPACE

#endif /* BEDFUNCTIONS_H_ */
