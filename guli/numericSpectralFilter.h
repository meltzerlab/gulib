/*
 * numericSpectralFilter.h
 *
 *  Created on: Aug 27, 2013
 *      Author: sven
 */

#ifndef NUMERICSPECTRALFILTER_H_
#define NUMERICSPECTRALFILTER_H_
#include <assert.h>
#include <math.h>
#include <algorithm>

namespace GULI {
	namespace SPECTRAL {
		typedef struct {
			float re, im;
		} complex;

/**  The one static
 *  method filter does a Fourier-Transform, provides the spectrum to a
 *  filter-core which returns  filter coefficients. Subsequently
 *  the data undergoes inverse fourier transform and is returned in the
 *  __same__ place
 **/
		class filterCore;
		void _filter(double *, const int, filterCore &);
		void _spectralize(double *, const int);


		/** The filter core. The whole spectrum is presented to this class via the
		 * method "estimate", subsequently the method "coefficient" returns the
		 * weight for each spectral component
		 **/
		class filterCore {
		public:
			virtual ~filterCore(){}
			/** estimate filter coefficients for the spectrum starting at double * over
			 * the next N entries
			 **/
			virtual void estimate(double *, int) = 0;

			/** return the filter coefficient
			 * the next N entries
			 **/
			virtual double coefficient(int, double) = 0;
		};



		template<class in, class out> void spectralize(in begin, in end, out dst) {
			const int N = 2 * (end - begin);
			double *buffer = new double[N];
			std::copy(begin, end, buffer);
			//Zero-Pad data
			std::fill(buffer+N/2, buffer+N, 0.);
			_spectralize(buffer, N);
			std::copy(buffer, buffer+N, dst);
			delete [] buffer;
		}



		template<class it> complex getComplexCoeff(it begin, it end, int n) {
			complex result;
			if(!n) {
				result.re = *begin;
				result.im = 0.;
				return result;
			}

			int N = end - begin;
			int N2 = N / 2;
			assert(abs(n) <= N2);
			int m = (abs(n) - 1) * 2 + 1;
			result.re = *(begin + m);
			if((!(N & 0x1)) && (abs(n) == N / 2) ) {
				result.im = 0.;
			} else {
				result.im = *(begin + m + 1);
			}
			if(n < 0)  result.im *= -1.;
			return result;
		}



		template<class it> void filter(it begin, it end, filterCore &A) {
			const int N = 2 * (end - begin);
			double *buffer = new double[N];
			std::copy(begin, end, buffer);
			//Zero-Pad data
			std::fill(buffer+N/2, buffer+N, 0.);

			_filter(buffer, N, A);

			std::copy(buffer, buffer + N / 2, begin);
			delete [] buffer;
			return;
		  }

		  class percentileFilter : public filterCore {
		  public:
		    percentileFilter(double f) : thresh(1.), fraction(f) {}
		    virtual ~percentileFilter(){}
		    virtual void estimate(double *S, int N);
		    virtual double coefficient(int F, double C);
		  private:
		    double thresh, fraction;
		  };

		class highPassFilter : public filterCore {
		  public:
			highPassFilter(double f) : cutoff(0), fraction(f) {}
			virtual ~highPassFilter(){}
			virtual void estimate(double *S, int N){cutoff=(int) (fraction * N);}
			virtual double coefficient(int k, double C) { return  (k < cutoff) ? 1. : 0.; }
		  private:
			int cutoff;
			double fraction;
		};

		class lowPassFilter : public filterCore {
		  public:
			lowPassFilter(double f) : cutoff(0), fraction(f) {}
			virtual ~lowPassFilter(){}
			virtual void estimate(double *S, int N){cutoff=(int) (fraction * N);}
			virtual double coefficient(int k, double C) { return  (k >= cutoff) ? 1. : 0.; }
		  private:
			int cutoff;
			double fraction;
		};


		class cutoffFilter : public filterCore {
		public:
			cutoffFilter(double f0, double width, bool larger) :  _N(0), _width(width), _f0(f0), _larger(larger) {}
			virtual ~cutoffFilter(){}
			virtual void estimate(double *S, int N){_N=  N;}
			virtual double coefficient(int k, double C) {
				double c = tanh((k-_f0 * _N)/(_width * _N));
				return _larger ? c : 1 - c;
			}
		private:
			int _N;
			double  _width, _f0;
			bool _larger;

		};

	}
}

#endif /* NUMERICSPECTRALFILTER_H_ */
