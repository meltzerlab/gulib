/*
 * gffAlgo.h
 *
 *  Created on: Oct 30, 2012
 *      Author: sven
 */
#ifndef GFFALGO_H_
#define GFFALGO_H_
#include <regex.h>
#include "gffDag.h"
#include "dagIterator.h"
#include "bedTiling.h"

namespace GULI {
	class gffNodeListT : public vector<gffDag::Node *> {};
	typedef enum {GFF_UP, GFF_DOWN} gffDirectionT;
}

template <class initializerList, class dagIterator> static void  _gffFindFeatures(initializerList start, initializerList end,  string featureRegex, GULI::gffNodeListT &R) {
	regex_t regex;
	regmatch_t m;

	if(regcomp(&regex, featureRegex.c_str(), REG_EXTENDED)) {
		assert(0);
	}

	R.clear();
	for(dagIterator U(start, end);  U != GULI::gffDag::end(); ++U) {
		if(!regexec(&regex, U->feature().c_str(), 1, &m, 0)) {
			R.push_back(U);
		}
	}

   regfree( &regex);
   return;
}

namespace GULI {
	// return a list of vectors to features in direction dir of the (pointer to) nodes pointed to by start,end

	template <class initializerList> void gffFindFeatures(gffDirectionT dir, gffNodeListT &R, initializerList start, initializerList end, string featureRegex) {
		 if(dir == GFF_DOWN) {
			 _gffFindFeatures<initializerList, gffDag::uniqueForwardIterator>(start, end, featureRegex, R);
		 } else {
			 _gffFindFeatures<initializerList, gffDag::uniqueBackwardIterator>(start, end, featureRegex, R);
		 }
	}

	template <class initializerList> gffNodeListT gffFindFeatures(gffDirectionT dir, initializerList start, initializerList end, string featureRegex) {
		 GULI::gffNodeListT R;
		 gffFindFeatures(dir, R, start, end, featureRegex);
		 return R;
	}

	inline void gffFindFeatures(gffDirectionT dir, gffNodeListT &R, gffDag::Node *start, string featureRegex) {
		 gffFindFeatures<gffDag::Node**>(dir, R, &start, &start+1, featureRegex);
	}

	inline gffNodeListT gffFindFeatures(gffDirectionT dir,  gffDag::Node *start, string featureRegex) {
		 return gffFindFeatures<gffDag::Node**>(dir, &start, &start+1, featureRegex);
	}

	/** Starting from a node "start", find _all_ connected features of type \a feature in direction
	 * backward (\a direction = true) or forward (\a direction = false). Next, bounce back and find all
	 * features of the same featuretype as \a start and continue to bounce back anf forth until the
	 * table is stable. This can be used for example to identify all transcripts including a cDNA, and
	 * all other cDNA's which are needed to cover all related transcript.s
	 */
	void gffPingPong(gffDag::Node *start, gffNodeListT &same_level, gffNodeListT &feature_level, string feature, bool direction);


	// Decompose a list of gffFEatures into non-overlapping features encoded as childs (forward link targets) of the original nodes.
	// Most useful probably to find non-overlapping cDNA regions of exons, but we keep it general just in case....
	void gffAddNonOverlappingNodes(gffDag &D, const string featureSelection, string newFeatureType);


	typedef bedTiling<gffNodeListT::const_iterator> constGffNodeListTiling;
	typedef bedTiling<gffNodeListT::iterator> gffNodeListTiling;

	template<> bedTiling<gffNodeListT::iterator>::bedAccess::operator const bedRegion*();
	template<> const bedRegion *bedTiling<gffNodeListT::iterator>::bedAccess::operator->();
	template<> bedTiling<gffNodeListT::const_iterator>::bedAccess::operator const bedRegion*();
	template<> const bedRegion *bedTiling<gffNodeListT::const_iterator>::bedAccess::operator->();
}


#endif /* GFFALGO_H_ */
