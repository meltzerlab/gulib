/*
 * bedRegion.h
 *
 *  Created on: Dec 27, 2008
 *      Author: sven
 */

#ifndef BEDREGION_H_
#define BEDREGION_H_
#include <samtools/bam.h>
#include "stdinc.h"

namespace GULI {
	namespace NGS {
		class alignment;
	};

	class bedRegion {
	public:
		typedef vector<bedRegion> listT;
		typedef listT::const_iterator constListIterator;
		typedef listT::iterator listIterator;
		typedef struct exonT {
			exonT(int p, int s) : pos(p), size(s) {}
			unsigned int pos, size;
		} exonT;
		typedef std::vector<exonT> featureT;
		typedef struct { uint8_t r,g,b; } rgbT;


		bedRegion(const string chr, const string id, int start, int end, float val=1000., const char orient='?',
				  unsigned int fstart = 0, unsigned int fend = 0, const featureT &F = featureT(),
				  rgbT RGB = {255,255,255}) :
					  _chr(chr), _id(id), _start(start), _end(end), _thickStart(fstart), _thickEnd(fend), _val(val), _orient(orient),
					  _feature(F), _rgb(RGB)
			{
//				if(_thickStart < 0) _thickStart = _start;
//				if(_thickEnd   < 0) _thickEnd   = _end;
			}

		bedRegion() : _chr("NotSet"), _id("NotSet"), _start(0), _end(0), _thickStart(0), _thickEnd(0),
				      _val(0), _orient('?'),  _rgb({255,255,255}) {}


		bedRegion(const bam1_t *, const bam_header_t *);

		const string &chromosome() const {
			return _chr;
		}
		const string &id() const {
			return _id;
		}
		unsigned int start() const {
			return _start;
		}
		unsigned int end() const {
			return _end;
		}
		unsigned int thickStart() const {
			return _thickStart ? _thickStart : _start;
		}
		unsigned thickEnd() const {
			return _thickEnd ? _thickEnd : _end;
		}

		float val() const { return _val; }
		rgbT  rgb() const { return _rgb; }

		const featureT &feature() const { return _feature; }
		const featureT &feature(const featureT &f)  { return _feature = f; }
		featureT &feature()      { return _feature; }

		const char orient() const {
			return _orient;
		}

		const string &chromosome(const string &c) {
			return _chr = c;
		}
		const string &id(const string &i) {
			return _id = i;
		}

		string defaultId() const;

		unsigned int start(unsigned int s) {
			return _start = s;
		}
		unsigned int end(unsigned int e) {
			return _end = e;
		}

		unsigned int thickStart(unsigned int s) {
			return _thickStart = s;
		}
		unsigned  thickEnd(unsigned int e) {
			return _thickEnd = e;
		}

		float val(float v) {
			return _val = v;
		}

		const char orient(const char o) {
			return _orient = o;
		}

		const rgbT  &rgb(const rgbT &rgb) { return _rgb = rgb; }

		const bedRegion &operator=(const bedRegion &);

		bool overlap(const bedRegion &b) const; // True if *this (partially) overlaps b
		// return true if \a b is inside *this
		bool inside(const bedRegion &b) const;
		bool operator==(const bedRegion &b) const {
			return !(*this != b);
		}
		bool operator!=(const bedRegion &b) const {
			return (*this < b) || (b < *this);
		}
		bool operator<(const bedRegion &b) const;
		bool operator>(const bedRegion &b) const;

		int outerDistance(const bedRegion &b) const;
		int centerDistance(const bedRegion &b) const;


		static int readList(listT &R, const string &fnam);
		static int readList(listT &R, istream &i);
		static int writeList(const listT &R, const string &fnam);
		static int writeList(const listT &R, ostream &o);
		static bedRegion::listT randomize(listT);
		static bedRegion::listT mergeOverlap(listT);

		static const int OTHERCHROMOSOMELARGER;
		static const int OTHERCHROMOSOMESMALLER;
		static unsigned int maxRepresentablePos() { return -1; }

	protected:
		string _chr, _id;
		int _start, _end;
		int _thickStart, _thickEnd;
		float _val;
		char _orient;
		featureT     _feature;
		rgbT         _rgb;
	};
}

ostream &operator<<(ostream &, const GULI::bedRegion &);

#endif /* BEDREGION_H_ */
