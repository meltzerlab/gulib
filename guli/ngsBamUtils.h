/*
 * ngsBamUtils.h
 *
 *  Created on: Jun 9, 2011
 *      Author: sven
 */

#ifndef NGSBAMUTILS_H_
#define NGSBAMUTILS_H_
#include <samtools/bam.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
using std::vector;
using std::string;


namespace GULI {
	namespace NGS {
		namespace BAMUTILS {

			// return the number of aligned reads on chromosome #i
			long int idxaligned(bam_header_t *header, bam_index_t *idx, int i);

			// Associate another object with an instance of a bam1_t object
			class bamAssocOp {
			public:

				struct equalBam {
					bool operator()(const bam1_t *A, const bam1_t *B) const {
						if(A->core.pos == B->core.pos &&      // avoid the expensive strcmp down below by using other potential differences
						   A->core.tid == B->core.tid &&
						   A->core.flag == B->core.flag &&
						  !strcmp(bam1_qname(A), bam1_qname(B))
						) return true;
						return false;
					}
				};

				struct equalName { // equal operator which exclusively uses the ID to recognize pairs (e.g. to id a partner in a paired end read)
					bool operator()(const bam1_t *A, const bam1_t *B) const {
						  return !strcmp(bam1_qname(A), bam1_qname(B));
					}
				};

				struct hashS {
					 size_t operator()(const bam1_t *A) const {
						 std::hash<string> H;
						 return H(bam1_qname(A));
					 }
				};
			};


			template <typename payload, typename equal = struct bamAssocOp::equalBam> class bamAssoc :
				public std::unordered_map<const bam1_t *, payload,  struct bamAssocOp::hashS, equal> {
					typedef std::unordered_map<const bam1_t *, payload, bamAssocOp::hashS, equal> baseType;
					typedef std::pair<const bam1_t *, payload> contentT;
			    public:
					typedef typename baseType::iterator iterator;
					typedef struct  bamAssocOp::equalBam  equalBam;
					typedef struct  bamAssocOp::equalName equalName;

					std::pair<iterator, bool> insert(const bam1_t *b, const payload &u) {
						return static_cast<baseType *>(this)->insert(contentT(b,u));
					}

				};

			typedef char tagId_t[2];

			// Operators found in a MED string
			typedef enum {
				MATCH, MISSMATCH, DELETE
			} MDop_opT;

			// representation of the MD string in a more accessible format
			typedef struct {
				MDop_opT op;
				size_t len;
				vector<char> nt;
			} MDop_T;
			typedef vector<MDop_T> listMDop_T;

			typedef enum {
				LEFT, RIGHT
			} orient_t;



			listMDop_T parseMD(const char *);
			char *createMD(const listMDop_T &);

			/** Calculate the chromosome position associated with a query position **/
			uint32_t q2chrPos(const bam_pileup1_t &);

			int updateCigar(bam1_t *b, const uint32_t *cigar, const size_t len);

			int insertCigarOperations(bam1_t *b, const size_t cigarPosition, const size_t nOp, const uint32_t *op);

			int deleteCigarOperations(bam1_t *b, const size_t cigarPosition, const size_t nOp);

			int editTag(bam1_t *b, const tagId_t id, const string &text);

			const char *getStringTag(const bam1_t *b, const tagId_t id);

			// if pl is a deletion, return the deleted nt's
			vector<uint8_t> deletedNt(const bam_pileup1_t &pl);
			string deletedNtAscii(const bam_pileup1_t &pl);

			// return reference nucleotide (works only if MD/XD tag is present)
			uint8_t referenceNT(const bam_pileup1_t &pl, uint32_t offset = 0);


			bool softClip(bam1_t *b, const size_t position, orient_t orient);

			uint8_t mapAscii2Bam(char);
			char    mapBam2Ascii(uint8_t bnt);

			// fill vector with genomic coordinates of each query nucelotide (-1 if not aligned, clipped, etc)
			// return position of the right-most coordinate
			int32_t genomicCoords(const bam1_t *b, vector<int32_t> &result);

			char baseQuality(const bam_pileup1_t &);
		}
	}
}
#endif /* NGSBAMUTILS_H_ */
