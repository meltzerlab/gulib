/*
 * ngsPilupNtReadT.h
 *
 *  Created on: Dec 12, 2012
 *      Author: sven
 */

#ifndef NGSPILUPNTREADT_H_
#define NGSPILUPNTREADT_H_
#include "samtools/bam.h"

namespace GULI {
	namespace NGS {
		class ntRead_t: public bam_pileup1_t {
			/** representation of a single read in a pileup, it's just the corresponding BAM structure
			 * plus some management to keep one copy of multiple references to a  bam1_t object in one place plus some convenience functions
			 */
		public:
			ntRead_t(const bam_pileup1_t &pl, bool managed = false) : _managed(managed)  {
				this->b = 0;
				*this = pl;
			}

			ntRead_t(const ntRead_t &ntr) : _managed(ntr._managed) {
				this->b = 0;
				*this = ntr;
			}

			ntRead_t(bool managed = true) : _managed(managed)  {
				this->b = 0;
			}

			~ntRead_t();

			ntRead_t &operator=(const ntRead_t &pl) {
				return *this = *static_cast<const bam_pileup1_t *>(&pl);
			}

			ntRead_t &operator=(const bam_pileup1_t &);

			unsigned int apos() const; 							/** Position of the first sequenced nucleotide **/
			char nt() const; 									/** nucleotide at this location (in ASCII)     **/
			unsigned short int bnt() const; 					/** nucleotide at this location (BAM encoded)  **/
			char qual() const { return this->b->core.qual; } 	/** alignment quality                          **/
			bool strand() const {
				return bam1_strand(this->b);
			} /** true if alignment is on the inverse strand **/
			const char *rg() const;
			int   rgTag() const;
		private:
			bool _managed;
		};
	}
}


#endif /* NGSPILUPNTREADT_H_ */
