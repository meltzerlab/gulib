/*
 * Permissions.h
 *
 *  Created on: Nov 15, 2014
 *      Author: sven
 */

#ifndef PERMISSIONS_H_
#define PERMISSIONS_H_
#include <string>
#include <set>
#include <Wt/WDate.h>
#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/Field.h>
#include <Wt/Json/Object.h>
#include <Wt/Dbo/WtSqlTraits.h>

namespace  dbo = Wt::Dbo;
using dbo::FieldRef;
using dbo::CollectionRef; //
using dbo::PtrRef;
using dbo::WeakPtrRef;

extern const std::string json_field_id;

namespace Wt {
	namespace Json {
		class Parser;
		class Serializer;
	}
}
namespace GULI {
	namespace Dbo {

		class tableBase {
			public:
				virtual ~tableBase(){};
				virtual const std::set<std::string> &fieldNames() const  = 0;
				virtual const std::set<std::string> &methodNames() const  = 0;
				bool knownColumn(const std::string &n) const {return  fieldNames().find(n) != fieldNames().end();}
				bool knownMethod(const std::string &n) const {return methodNames().find(n) != methodNames().end(); }

				template <class V> void callMethod(Wt::Dbo::ptr<V> &p, const std::string &method, const Wt::Json::Object &arg)
				{ // Specialize for a given implementation. The generic type does not support methods calls and will
				  // throw an exception.
					throw new std::bad_function_call;
				}

				template <typename t> static const t &nullValue();

			protected:
				class getFieldNameAction {
				public:
					std::set<std::string> names;
					std::string idField;
					template <typename V> void act(FieldRef<V> F) {names.insert(F.name());}
					template <typename V> void actCollection(CollectionRef<V> F) {names.insert(V::tablename());}
					template <typename V> void actPtr(PtrRef<V> F){names.insert(F.name());}
					template <typename V> void actWeakPtr(WeakPtrRef<V> F){names.insert(F.joinName());}
					template <typename V> void actId(V &v, const std::string &name, int size){names.insert(name); idField=name;}
					Wt::Dbo::Session *session() { return 0; }
				};


			private:
		};

		template <> const int &tableBase::nullValue<int>();
		template <> const float &tableBase::nullValue<float>();
		template <> const double &tableBase::nullValue<double>();
		template <> const std::string &tableBase::nullValue<std::string>();

		class tableJSON {
		public:
			typedef  Wt::Json::Object myJson;
			const myJson &json() const { return _storage; }
			myJson &json()  { return _storage; }

		protected:
			template<class Action> void _jsonPersist(Action& a) {
				dbo::field(a, _storage, json_field_id);
			}

		private:
			myJson _storage;
		};
	}
}

/*
namespace Wt {
	namespace Dbo {
		template<> struct WTDBO_API sql_value_traits<GULI::Dbo::tableJSON::myJson, void> {
		  static const bool specialized = true;
		  static std::string type(SqlConnection *conn, int size);
		  static void bind(const GULI::Dbo::tableJSON::myJson &v, SqlStatement *statement, int column, int size);
		  static bool read(GULI::Dbo::tableJSON::myJson &v, SqlStatement *statement, int column, int size);
		};

	}
}
*/
#endif
