/*
 * gffDag.h
 *
 *  Created on: Sep 27, 2012
 *      Author: sven
 */

#ifndef GFFDAG_H_
#define GFFDAG_H_

#include "dag.h"
#include "utilAttributeFunctionality.h"
#include "dagIterator.h"
#include "bedRegion.h"
#include "tabDelimited.h"

namespace GULI {
	/** The definition of a (binary) GFF Node
	 **/

	class gffDag : public Dag {
	public:

		class Node;

		typedef dagTemplates::forwardIterator<Node>  forwardIterator;
		typedef dagTemplates::backwardIterator<Node> backwardIterator;
		typedef dagTemplates::uniqueForwardIterator<Node>  uniqueForwardIterator;
		typedef dagTemplates::uniqueBackwardIterator<Node> uniqueBackwardIterator;

		typedef struct idAndVersion {
			idAndVersion(string full_id) {
				size_t p = full_id.find_last_of('.');
				id = full_id.substr(0, p);
				if(p != string::npos) {
					version = full_id.substr(p+1);
				}
			}
			string id;
			string version;
		} idAndVersionT;

		void clear();

		Node *operator[]   (const string &id) const;
		virtual bool exists(const string &id) const;

		static Node *end()  { return (Node *) -1; }

		Node *root() const { return this->operator[](rootId); }

//		void addNonOverlapping(const string feature, const string newFeature);

		void addNode(Node *N);

		static const string rootId;

		template <class udat> void updateRelations(Node &cur, const vector<string> &t, Link::directionT d, udat u) {
			const size_t N= t.size();
			vector<string> woVersion(N);
			for(size_t i=0; i < N; ++i) woVersion[i] = idAndVersionT(t[i]).id;
			Dag::updateRelations(cur, woVersion, d, u);
		}

	private:
	};


	class gffDag::Node : public Dag::Node, public utilAttributeFunctionality, public bedRegion {
	public:
		/** Definition of the TEXT representation for GFF. This is
			 ** (at least for now) the interface to any I/O Routine for gffDAG.
			 **/
		class textual;
		typedef vector<textual> table;

		Node(const textual &);
		Node(const bedRegion &, 	const string &feature, const string &source,  char frame);
		const string &source()   	const { return _source; };
		const string &feature()  	const { return _type;  }
		float score()            	const { return bedRegion::val(); }
		char  frame()            	const { return _frame; }
		const string &seqname()  	const { return bedRegion::chromosome(); }
		virtual const string &id() 	const { return bedRegion::id(); }
		ostream &operator<<(ostream &);
	private:
		string  _type, _source;
		char _frame;
	};

	class gffDag::Node::textual {
	public:
		typedef std::unordered_map<string, string> attributeT;
		string source;
		string feature;
		string seqname;
		attributeT  attribute;
		int start, end;
		float score;
		char strand, frame;

		textual(const string &featureA=".")
		: feature(featureA), start(0), end(0), score(0.), strand('.'), frame('.') {}

//		const char *getAttribute(const string &);

		textual(const bedRegion &B, const string &featureA=".")
		: feature(featureA), seqname(B.chromosome()), start(B.start()), end(B.end()),
		  score(B.val()), strand(B.orient()), frame('.') {
			if(B.id() != "") attribute["ID"] = B.id();
		}

		const char *getAttribute(const string );

		static attributeT parseAttributeString(const string text);
		// list of attributes which are represented in the structure of the DAg (and can be extracted from there)
		// which are therefore NOT present in the attribute variable;
		static const char reservedAttributes [][20];
	};



}	 // NAMESPACE

#endif /* GFFDAG_H_ */
