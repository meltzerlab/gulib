#ifndef __GULI_UTIL_ID_MAP_H_
#define __GULI_UTIL_ID_MAP_H_
#include "stdinc.h"
#include <map>

using std::map;

namespace GULI {
	class utilIdMap {
	public:

		utilIdMap() {}
		virtual ~utilIdMap() {}

		void clear();

		/** The id  for  #i, **/
		const string &id(unsigned int i) const;

		/** Number of ids  **/
		unsigned int nId() const;
		/** map from chromosome id to an unique  numerical index, return -1 if id is unknown **/

		int idIdx(const string &) const;
		int idIdx(const string &, bool create);
		int idIdx(const string &, bool create, bool &created);

		bool idExist(const string &chr) const;


	protected:
		/** like chrIdx, but create an index if id is yet unknown  **/

	private:
		typedef map<string, int> _mapT;
		_mapT _id2idx;
		vector<string> _ids;
	};
} // NAMESPACE
#endif /*GENOME_DENSITY_H_*/
