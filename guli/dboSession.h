/*
 * dboSession.h
 *
 *  Created on: Nov 15, 2014
 *      Author: sven
 */

#ifndef DBOSESSION_H_
#define DBOSESSION_H_

#include <string>
#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/Session.h>



namespace  dbo = Wt::Dbo;


namespace GULI {
	namespace Dbo {

		class Session : public Wt::Dbo::Session {
		public:
			Session () : _connected(false){};
			virtual ~Session () {}

			bool connect(const std::string &JSON);
			std::istream &connect(std::istream &JSON);

			virtual bool createInitialDatabase(bool drop = false) { return _createInitialDatabase(drop); }
protected:
			bool _createInitialDatabase(bool);

		private:
			bool _connected;
			const std::string defaultConnection;
		};
	}
}
#endif /* DATABASE_H_ */

