/*
 * utilNeedlemanWunsch.h
 *
 *  Created on: Mar 24, 2014
 *      Author: sven
 */

#ifndef UTILNEEDLEMANWUNSCH_H_
#define UTILNEEDLEMANWUNSCH_H_
#include <string>
#include "sequence.h"

namespace GULI {
	namespace UTIL {
		// Poor man's "optimal" alignment (Smith Waterman and Needleman Wunsch)
		// This is not optimized in any way, and is thought to be used if one needs
		// an occasional alignment.
		// There are much better libraries out there, e.g.
		// "SSW library: an SIMD Smith-Waterman C/C++ library for use in genomic applications." Zhao, Mengyao et. al. PLOS one 8(12) [2013] e82138
		class aligner {
		public:
			typedef struct {
				int score1, score2;
				size_t start1, end1;
				size_t start2, end2;
				string cigar1;
				string cigar2;
			} resultT;

			aligner(bool keepMem = false) : _M(0), _P(0), _r(0),  _keepMem(keepMem) {}
			virtual ~aligner();
			// smith-waterman (global = false) OR needleman-wunsch (global=true)
			void align(const sequence &ref, const sequence &seq, resultT &result, bool global);
		private:
			class intarray;
			class chararray;
			intarray *_M;
			chararray *_P;
			char *_r;
			bool _keepMem;
		};
	}
};



#endif /* UTILNEEDLEMANWUNSCH_H_ */
