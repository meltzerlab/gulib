/*
 * ngsBamPileupT.h
 *
 *  Created on: Dec 12, 2012
 *      Author: sven
 */

#ifndef NGSBAMPILEUPT_H_
#define NGSBAMPILEUPT_H_
#include "ngsPilupNtReadT.h"
#include "stdinc.h"

namespace GULI {
	namespace NGS {
		class pileup_t : public vector<ntRead_t> {
			/** A pileup at a given location is represented a a vector of ntRead_t objects.
			 * In addition, the vector class is extended with a method reference().
			 * This method returns the reference nucleotide used in the alignment
			 * __if__ the MD and CIGAR fields are both correct in the BAM file.
			 */
		public:
			pileup_t() :
					_reference('\0'),
					_clientReject(0), _baseQualityReject(0), _overlapReject(0), _dupReject(0),
					_statUpdated(false), _coverage(0)
			{}
			virtual ~pileup_t() {}
			char reference() const { return _reference;} /** Return reference nucleotide for pileup position */

			int ntCoverage() const;
			int ntCount(const char nt) const;
			int ntFwdCount(const char nt) const;
			int ntBwdCount(const char nt) const;
			double gmnNtQual(const char nt) const;
			double gmnAlQual(const char nt) const;
			int clientReject()      const { return _clientReject; }
			int baseQualityReject() const { return _baseQualityReject; }
			int overlapReject()     const { return _overlapReject; }
			int dupReject()     	const { return _dupReject; }
//			int reject()            const { return maskReject() + alignQcReject() + clientReject() + baseQualityReject() + overlapReject() + dupReject(); }

			void clear();

		private:
			friend class piler;

			int _ntidx(char) const;


			char reference(char r) { return _reference = r; }

			void _updateStat() const;

			char _reference;
			int   _clientReject, _baseQualityReject, _overlapReject, _dupReject;
			mutable bool _statUpdated;
			mutable int _coverage, _ntFreq[7], _ntFwdFreq[7];
			mutable double _gmnBqual[7], _gmnAqual[7];
		};
	}
}


#endif /* NGSBAMPILEUPT_H_ */
