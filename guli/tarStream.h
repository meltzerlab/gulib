/*
 * tarStream.h
 *
 *  Created on: Jun 24, 2010
 *      Author: sven
 */

#ifndef TARSTREAM_H_
#define TARSTREAM_H_
#include "stdinc.h"
#include <iostream>

#define NAMELEN 100
namespace GULI {
	/** A very simple tar-streamer, use this object by re-implementing the consume* methods
	 * which are called for each entry in the tar-file.
	 * The object does currently only support reading tar-streams
	 */
	class tarStream {
	public:
		class tarHeader {
		public:
			tarHeader();

			virtual ~tarHeader();

			int blocksize() const { return _blocksize; }

			int blocksize(int b);

			istream &operator<<(istream &I);

			bool isValid() const { return _isValid; }
			bool isEot() const;
			bool isFile() const { return isValid() && (_header().type == REGTYPE || _header().type == AREGTYPE);}
			bool isHardLink() const { return isValid() && _header().type == LLINKTYPE; }
			bool isSoftLink() const { return isValid() && _header().type == SYMTYPE;   }
			bool isCharSpecial() const { return isValid() && _header().type == CHRTYPE; }
			bool isBlockSpecial() const { return isValid() && _header().type == BLKTYPE; }
			bool isDir() const { return isValid() && _header().type == DIRTYPE; }
			bool isFifo() const { return isValid() && _header().type == FIFOTYPE; }
			int  type() const {
				int r = -1;
				if (isValid()) r =  _header().type;
				return r;
			}

			string name() const { return isValid() ? (_posixName != "" ? _posixName : _name) : ""; }
			string mode() const { return isValid() ? _header().mode : ""; }
			string modTime() const { return isValid() ? (_posixMtime != "" ? _posixMtime : _header().modTime ) : ""; }
			string atime() const { return _posixAtime; }
			string ctime() const { return _posixCtime; }


			string linkTarget() const {
				return isValid() ? (_posixLink != "" ? _posixLink : _header().target ) : "";
			}
			long int size() const {	return isValid() ? (_posixSize != "" ? _o2uint(_posixSize) : _o2uint(_header().size)) : 0; }
			int uid() const { return isValid() ? _o2uint(_header().uid) : 0; }
			int gid() const {return isValid() ? _o2uint(_header().gid) : 0;	}

			enum {	REGTYPE='0',
					AREGTYPE='\0',
					LNKTYPE='1',
					SYMTYPE='2',
					CHRTYPE='3',
					BLKTYPE='4',
					DIRTYPE='5',
					FIFOTYPE='6',
					CONTTYPE='7',
					XHDTYPE ='x',
					XGLTYPE ='h',
					LLINKTYPE ='L',
			};

		protected:
			bool _isLongLink() const {
				return isValid() && _header().type == LLINKTYPE;
			}

			bool _isXHdr() const {
				return isValid() && _header().type == XHDTYPE;
			}

			typedef struct {
				char name[NAMELEN];
				char mode[8];
				char uid[8];
				char gid[8];
				char size[12];
				char modTime[12];
				char checksum[8];
				char type;
				char target[100];
				char magic[6];
				char version[2];
				char uname[32];
				char gname[32];
				char devmajor[8];
				char devminor[8];
				char prefix[155];
			} _headerT;

			int _checksum() {
				return isValid() ? _o2uint(_header().size) : 0;
			}

			virtual unsigned int _calcChecksum();

			_headerT &_header() const {
				return *reinterpret_cast<_headerT *> (_buffer);
			}

			unsigned int _o2uint(const string &s) const;

		private:
			string _posixAtime, _posixCtime, _posixMtime, _posixLink, _posixSize, _posixName;
			int _blocksize;
			char *_buffer;
			bool _isValid;
			string _name;
		};

		tarStream() :
			_resync(true), _outofsync(false), _interrupt(false) {
		}

		virtual ~tarStream() {}

		bool reSync(bool b) {
			return _resync = b;
		}
		bool reSync() const {
			return _resync;
		}
		int blocksize(int b) {
			return _hdr.blocksize(b);
		}
		int blocksize() const {
			return _hdr.blocksize();
		}

		istream &operator<<(istream &I);

	protected:
		virtual int consumeSoftLink(const tarHeader &H) {
			return 0;
		}
		virtual int consumeHardLink(const tarHeader &H) {
			return 0;
		}
		virtual int consumeDirectory(const tarHeader &H) {
			return 0;
		}
		virtual int consumeBlockSpecial(const tarHeader &H) {
			return 0;
		}
		virtual int consumeCharSpecial(const tarHeader &H) {
			return 0;
		}
		virtual int consumeFifo(const tarHeader &H) {
			return 0;
		}
		virtual int consumeFile(const tarHeader &H, istream &I);
		virtual void stoppedReading(){}


		void _doneReading() {
			_interrupt = true;
		}


	private:
		tarHeader _hdr;
		bool _resync, _outofsync;
		bool _interrupt;
	};


	class _tarStreamBase : virtual public std::ios {
	public:
		class streambuf :  public std::streambuf {
		public:
			streambuf(const tarStream::tarHeader &H, istream &I);
			virtual ~streambuf();
			virtual int underflow();

		private:
			char                       *_buffer;
			std::istream               &_tstream;
			const int                  _blockSize;
			int                        _bytesLeft;
		};

		_tarStreamBase(const tarStream::tarHeader &H, istream &I) : buf(H, I) {
			init(&buf);
		}

		virtual ~_tarStreamBase(){};
		streambuf* rdbuf() { return &buf; }

	protected:
		streambuf buf;
	};

	class tarConsumerStream : public _tarStreamBase, public std::istream {
	public:
		tarConsumerStream (const tarStream::tarHeader &H, istream &I) : _tarStreamBase(H,I), std::istream(&buf) {}

		streambuf* rdbuf() {
			return _tarStreamBase::rdbuf();
		}
	};
}
#endif /* TARSTREAM_H_ */
