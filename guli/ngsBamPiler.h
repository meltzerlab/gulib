/*
 * ngsBamPileup.h
 *
 *  Created on: Aug 28, 2011
 *      Author: sven
 */

#ifndef NGSBAMPILEUP_H_
#define NGSBAMPILEUP_H_
#include <vector>
#include <map>
#include "samtools/sam.h"
#include "samtools/bam.h"
#include "bedRegion.h"
#include "bedTiling.h"
#include "ngsPileupT.h"
#include "ngsBamUtils.h"
#include "utilIdMap.h"
#include "seqsetwholegenome.h"
using std::vector;
using  GULI::NGS::BAMUTILS::bamAssoc;
struct _bamReaderDataS_;

namespace GULI {
	namespace UTIL {
		typedef struct {
				size_t operator()(const long long int l) const {
					return (size_t) l;
				}
		} longLongHashFunc; // Needed for APPLE gcc, is built in on linux gcc
	}

	namespace NGS {
		class piler {
			/** Implements access to a pileup of reads in a BAM file for a single genomic location **/
		public:
			class streamingClient {
			protected:
				friend class piler;
				virtual pileup_t *pilerStreamingSetup(uint32_t tid, uint32_t pos,  size_t fileIndex, bool &manage) = 0;
				virtual void positionStreamsComplete(uint32_t tid, uint32_t pos) {}
			};

			class  readStatsT {
			public:
				readStatsT() : reads(0), bases(0), inserts(0), pairs(0), rejectMask(0), rejectAlignQuality(0), rejectDup(0) { }
				virtual ~readStatsT() { }
				void clear(){ reads = bases = inserts = pairs = rejectMask = rejectAlignQuality = rejectDup = 0; }
				virtual double avReadLength() const    { return (double) bases    / (double) reads; }
				virtual double avInsertSize() const    { return (double) inserts  / (double) pairs; }
				virtual double fractionPaired() const  { return (double) pairs    / (double) reads; }
				long long int reads, bases, inserts, pairs, rejectMask, rejectAlignQuality, rejectDup;
			};
			piler();
			virtual ~piler() {
				detach();
				if(_ucscSeq) delete _ucscSeq;
			}
			size_t numberOfStreams() const {
				return _streams.size();
			}

			virtual int attach(const vector<samfile_t *> &streams, const vector<string> &fn);

			virtual void detach() {
				_flush();
				for (unsigned int i = 0; i < numberOfStreams(); ++i) _detach(i);
			}
			bam_index_t *bamIndex(size_t s) {
				assert(s < numberOfStreams());
				return _streams[s].idx;
			}
			void  stream(streamingClient *C, const bedTilingBase  &R);

			void clearStats();
			const readStatsT &readStats( size_t s, const char *rg=0);

			int maxProperInsertSize(int m) {
				return _maxinsert = m;
			}
			int maxProperInsertSize() const {
				return _maxinsert;
			}

			int minAlignQuality() const {
				return _minalignquality;
			}
			int minAlignQuality(int m) {
				_minalignquality = m;
				_flush();
				return m;
			}

			int minBaseQuality() const {
				return _minbasequality;
			}
			int minBaseQuality(int m) {
				_minbasequality = m;
				_flush();
				return m;
			}

			bool enableReferenceNt(bool ref) {
				return _getref = ref;
			}
			bool enableReferenceNt() const {
				return _getref;
			}

			bool collectStats(bool ref) {
				return _collectStats = ref;
			}
			bool collectStats() const {
				return _collectStats;
			}

			bool rmDup(bool ref) {
				_rmdup = ref;
				_flush();
				return _rmdup;
			}
			bool rmDup() const {
				return _rmdup;
			}

			bool clipOverlap() const {
				return _clipOverlap;
			}
			bool clipOverlap(bool b) {
				_clipOverlap = b;
				_flush();
				return b;
			}

			uint32_t mask() const {
				return _pileupmask;
			}
			uint32_t mask(uint32_t m) {
				_pileupmask = m;
				_flush();
				return _pileupmask;
			}

			bool readGroups() const {
				return _rgEn;
			}
			bool readGroups(bool r) {
				return _rgEn = r;
			}
			size_t nReadGroup(size_t s) const {
				assert(s < numberOfStreams());
				return _streams[s].rgs.nId();
			}
			const char *readGroup(int i, size_t s) const {
				assert(s < numberOfStreams());
				return _streams[s].rgs.id(i).c_str();
			}
			int readGroup(const char *r, size_t s) const {
				assert(s < numberOfStreams());
				if (r) return _streams[s].rgs.idIdx(r);
				else return -1;
			}

			uint32_t maxReadDepth() const {
				return _maxdepth;
			}
			uint32_t maxReadDepth(uint32_t m) {
				_maxdepth = m;
				_flush();
				return _maxdepth;
			}

			seqSetWholeGenome *ucscGenome(seqSetWholeGenome *G) { return _ucscGenome = G; }
			seqSetWholeGenome *ucscgenome() { return _ucscGenome; }

			size_t maxCntdStreamingGap() const   { return _maxCntdStreamingGap; }
			size_t maxCntdStreamingGap(size_t g) { return _maxCntdStreamingGap = g; }
			bam_index_t *bamIdx(size_t stream) { assert(stream < numberOfStreams()); return _streams[stream].idx; }

			int _bamReader(bam1_t *b, _bamReaderDataS_ *D);

			int maskReject(size_t s, int rg=-1)    const { return rg < 0 ? _streams[s].stats.rejectMask         : _streams[s].rgStats[rg].rejectMask;}
			int alignQcReject(size_t s, int rg=-1) const { return rg < 0 ? _streams[s].stats.rejectAlignQuality : _streams[s].rgStats[rg].rejectAlignQuality;}
			int dupReject(size_t s, int rg=-1)     const { return rg < 0 ? _streams[s].stats.rejectDup          : _streams[s].rgStats[rg].rejectDup;}


			static const char   *RGIDX_TAG;
			static const uint8_t maxRgIdx;

		protected:
			virtual bool _acceptableRead(const bam_pileup1_t &) {
				return true;
			}
			virtual void _detach(size_t s);
			virtual void _flush(){}
			virtual void _strategize(const bedTilingBase &input, bedRegion::listT &strategy);
			const char 	 *_chromosome(uint32_t tid, size_t s);
			int32_t   	  _tid(const char  *chromosome, size_t s);
			int32_t   	  _tid(const string &chromosome, size_t s) { return _tid(chromosome.c_str(), s); }

		protected:
			typedef std::unordered_map<string, uint32_t> _chrMapT;
			typedef struct {
				readStatsT stats;
				vector<readStatsT> rgStats;
				utilIdMap rgs;
				samfile_t *stream;
				bam_index_t *idx;
				bam_iter_t iter;
				_chrMapT  chr;
				int32_t   rpos;

			} _perStreamDataT;

			typedef vector<_perStreamDataT> _streamDataT;
			_streamDataT _streams;

		private:
			static const int _defaultMaxDepth = 8000;

			void _collectStats_(const bam1_t *b, size_t streamno);
			void _processPileup(uint32_t tid, uint32_t pos, size_t n_plp, const bam_pileup1_t *pl, size_t s);

			bool _readBam(uint32_t rtid, uint32_t start, uint32_t len);
			void _rmOverlap(size_t n_plp, const bam_pileup1_t *pl);
			void _rmDup(size_t n_plp, const bam_pileup1_t *pl);


			int _minalignquality, _minbasequality;
			bool _clipOverlap, _rgEn, _getref, _collectStats, _rmdup;
			uint32_t _pileupmask, _maxinsert, _maxdepth;
			size_t _maxCntdStreamingGap;
			bam_mplp_t _mplp;
			streamingClient *_client;
			const bedTilingBase *_streamRegions;
			bedRegion _curBed;

			typedef uint32_t _magicT;
			_magicT _clipMagic;
			vector<_magicT> _clipped;
			vector<uint32_t> _pos5;

			seqSetWholeGenome *_ucscGenome;
			sequence          *_ucscSeq;
			int               _inMemChr;
		};
	}
}

#endif /* NGSBAMPILEUP_H_ */
