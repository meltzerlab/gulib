/*
 * guliconfig.h
 *
 *  Created on: Apr 11, 2009
 *      Author: sven
 */

#ifndef GULICONFIG_H_
#define GULICONFIG_H_

#define MAX_CHROMOSOMES 255
#ifdef SVENS_MAC
#define DEFAULT_BASE_DIRECTORY "/Users/sven/analysis/Matchmaker/"
#else
#define DEFAULT_BASE_DIRECTORY "/home/public/Matchmaker/"
#endif
#endif /* GULICONFIG_H_ */
