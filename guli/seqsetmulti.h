/***************************************************************************
 *   Copyright (C) 2006 by Sven Bilke  					   *
 *   bilkes@mail.nih.gov   						   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "stdinc.h"
#ifndef __SEQUENCE_MULTI_SET_H
#define __SEQUENCE_MULTI_SET_H
#include "seqset.h"
class seqMultiSet : public seqSet {
  public:
    virtual ~seqMultiSet() {}
    virtual int nSet () const = 0;                   // -1 := unknown
    virtual void selectFirst()  const = 0;           // select first sub-set
    virtual bool select(const string &) const =0;    // select a specific item, return true if found
    virtual void next    (int stepsize=1) const = 0; // step stepsize sets forward
    virtual bool end() const = 0;                    // True if out-of-bound
    virtual const seqSet &currentSet() const { return *this; }
};

/** a simple adaptor to make a sequence set appear as a multi-set **/
class seqSetAdaptorMultiSet : public seqMultiSet {
  public:
    virtual ~seqSetAdaptorMultiSet() {}
    seqSetAdaptorMultiSet(const seqSet &S) : _content(S) {}

    virtual size_t size() const {
      return _content.size();
    }
    virtual const sequence operator[](size_t i) const {
      return _content[i];
    }

    virtual int nSet () const {
      return 1;
    }
    virtual void selectFirst() const  {
      _end = false;
    }
    virtual bool select(const string &s) const {
      return false;
    } // Not supported
    virtual void next   (int stepsize) const {
      if(stepsize)
        _end = true;
    }
    virtual bool end() const {
      return _end;
    }
  private:
    const seqSet &_content;
    mutable bool _end;
};
#endif
