/*
 * ngsTagDensity.h
 *
 *  Created on: Jan 26, 2013
 *      Author: sven
 */

#ifndef NGSTAGDENSITY_H_
#define NGSTAGDENSITY_H_
#include "ngsBamPiler.h"
#include "bedRegion.h"
#include "bedTiling.h"
namespace GULI {
	namespace NGS {

		class tagDensity : public piler, private piler::streamingClient {
		public:
			typedef vector<size_t> countT;
			typedef enum {ALL=0, FWD=1, BWD=2} dirT;

			tagDensity() : _pileups(0),  _targets(0) {};
			virtual ~tagDensity() {};
			void analyze(const bedTilingBase  &targets);

			const countT &density(size_t s, dirT dir,  size_t rg) {
				assert (s < numberOfStreams() && rg < _streamCount[s].perReadGroup[ALL].size());
				return _streamCount[s].perReadGroup[dir][rg];
			}

			const countT &density(size_t s, dirT dir,  const char *rg) {
				int i = readGroup(rg,s);
				assert(i >= 0);
				return density( s, dir, i);
			}

			const countT &density(size_t s, dirT dir) {
				assert(s < numberOfStreams());
				return _streamCount[s].total[dir];
			}

			const countT &density(dirT dir) { return  _total[dir]; }

			virtual void detach();

		private:
			typedef struct {
				countT total[3];
				vector<countT> perReadGroup[3];
			} _perStreamDataT;
			vector<_perStreamDataT> _streamCount;
			countT _total[3];
			pileup_t *_pileups;
			const bedTilingBase *_targets;

			virtual pileup_t *pilerStreamingSetup(uint32_t tid, uint32_t pos,  size_t fileIdx, bool &manage);
			virtual void      positionStreamsComplete(uint32_t tid, uint32_t pos);
		};
	}
}

#endif /* NGSTAGDENSITY_H_ */
