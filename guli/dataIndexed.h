/***************************************************************************
 dataWrapperIndexed.h  -  description
 -------------------
 begin                : Thu Jun 5 2003
 copyright            : (C) 2003 by sven
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2004 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __GULI_DATA_REDIRECT_H
#define __GULI_DATA_REDIRECT_H
#include "stdinc.h"
#include "dataIF.h"

namespace GULI {
	using GULI::data::dataIF;

	/** a dataIF  introducing INDEXED access to another dataIF
	 **/
	class redirect: public dataIF {
	public:
		virtual ~redirect() {
		}

		/** Introduce ROW-Indexed access with LINEAR column access
		 **/
		template<class it> redirect(it rb, it re, dataIF &target) :
			_target(target) {
			_rowIdx.resize(re - rb);
			std::copy(rb, re, _rowIdx.begin());
			_colIdx.resize(_target.cols());
			for (unsigned int i = 0; i < target.cols(); i++)
				_colIdx[i] = i;
		}

		/** Introduce COL-Indexed access with LINEAR row access
		 **/
		template<class it> redirect(dataIF &target, it cb, it ce) :
			_target(target) {
			_colIdx.resize(ce - cb);
			std::copy(cb, ce, _colIdx.begin());
			_rowIdx.resize(target.rows());
			for (unsigned int i = 0; i < target.rows(); i++)
				_rowIdx[i] = i;
		}

		/** Introduce COL-Indexed, ROW-indexed access
		 **/
		template<class it1, class it2>
		redirect(dataIF &target, it1 cb, it1 ce, it2 rb, it2 re) :
			_target(target) {
			_colIdx.resize(ce - cb);
			std::copy(cb, ce, _colIdx.begin());
			_rowIdx.resize(re - rb);
			std::copy(rb, re, _rowIdx.begin());
		}

		/** Return the number of Columns in the Data
		 *  Re-implemented for internal reasons
		 **/
		virtual const unsigned int cols() const {
			return _colIdx.size();
		}

		/** Return the number of Rows in the Data
		 *  Re-implemented for internal reasons
		 **/
		virtual const unsigned int rows() const {
			return _rowIdx.size();
		}

		/** return the Data content at column, row
		 *  Re-implemented for internal reasons
		 **/
		virtual float &Data(int col, int row) {
			return _target(_colIdx[col], _rowIdx[row]);
		}

		virtual float Data(int col, int row) const {
			return _target(_colIdx[col], _rowIdx[row]);
		}

		/** return the String ID for a row
		 *  Re-implemented for internal reasons
		 **/
		virtual std::string &rowId(int i) {
			return _target.rowId(_rowIdx[i]);
		}

		/** return the String ID for a column
		 *  Re-implemented for internal reasons
		 **/
		virtual std::string &colId(int i) {
			return _target.colId(_colIdx[i]);
		}

		/** return the String ID for a row (const)
		 **/
		virtual const std::string &rowId(int i) const {
			return _target.rowId(_rowIdx[i]);
		}

		/** return the String ID for a column (const)
		 **/
		virtual const std::string &colId(int i) const {
			return _target.colId(_colIdx[i]);
		}

	private:
		dataIF &_target;
		vector<int> _rowIdx, _colIdx;
	};
} // namespace
#endif // defined __redirectdata
