/*************************************************************************
 utilXmlGeneric.h  -  description
 --------------------------------
 begin                : Sun Nov 19 2000
 copyright            : (C) 2000-2009 by Sven Bilke
 email                : sven@thep.lu.se
 *************************************************************************/
/***************************************************************************
 *   Copyright (C) 2000-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __MULI_UTIL_XMLGENERIC_H
#define __MULI_UTIL_XMLGENERIC_H
#include <stack>
#include <string>
#include <vector>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include "guliconfig.h"

using std::stack;
using std::string;
using std::vector;

namespace GULI {
	/** A simple XML parser. It generates a "kind of" DOM representation
	 ** of the XML document. So, why not use DOM rightaway ? The difference is
	 ** that one _can_ "eat" the elements (called "token" in this code) rather
	 ** then putting them on the stack. This is useful when reading very large
	 ** files and one does not want to keep the whole tree in Memory. At the same
	 ** time it gives  more pre-processed data compared to the SAX interface.
	 ** Eating tokens is done by inheriting and re-implementing the
	 ** method _eatToken. A "true" return value indicates the token should not
	 ** be stored on the stack.
	 ** Sorry for introducing my own structures (tokentype, attributetype) rather
	 ** than using the DOM components, this should be changed
	 **/
	class xmlGeneric {
	public:
		/** Representation of ATTRIBUTES <tagname NAME="VALUE"/>
		 **/
		typedef struct {
			string name;
			string value;
		} attributetype;

		/** Representation of Tokens (aka Elements)
		 ** <name attribute="val"> <name1/> </name>
		 ** where this sample has one attribute and one embedded token
		 **/
		typedef struct TOKEN {
			string name;
			vector<attributetype> Attribute;
			vector<struct TOKEN> Tokens;
			string text;
			const TOKEN &token(const std::string &name, size_t *idx=0) const;
			std::string attribute(const std::string &name, size_t *idx = 0) const;
		} tokentype;

		bool parse(const char *file);
		bool parse(std::istream &);

		virtual ~xmlGeneric() {}


		/** Set up to search a TOKEN within a token. Do not yet start !
		 **/
		void startRepFindToken(const tokentype &, const string &id) const;
		/** Find next TOKEN set up by StartRepFindToken. Return true if successfull
		 **/
		bool findNextToken(tokentype &target) const;

		/** Find first TOKEN matching string. Return copy in second tokentype
		 **/
		bool findToken(const tokentype &, const string &, tokentype &) const;

		/** Return the value associated with Attribute id=string
		 **/
		const string &findAttribute(const tokentype &, const string &) const;

		static const string  UNKNOWNATTRIBUTE;

	private:
		friend void startElementProcessor(void *, const xmlChar *,
				const xmlChar **);
		friend void characterProcessor(void *, const xmlChar *, int);
		friend void endElementProcessor(void *, const xmlChar *);

		/** Interface to the XML2-SAX API
		 **/
		void _startElement(const xmlChar *Name, const xmlChar **attrs);
		/** Interface to the XML2-SAX API
		 **/
		void _processText(const xmlChar *Text, int len);
		/** Interface to the XML2-SAX API
		 **/
		void _endElement(const xmlChar *Name);

		/** Interface to CLIENTS !
		 **/
		virtual bool _eatToken(tokentype &) {
			return false;
		}

		mutable const tokentype *_search_token;
		mutable unsigned int _cur_token;
		mutable string _id;
		typedef stack<tokentype> tokenStackType;
		tokenStackType _stack;
	};
}// NAMESPACE MULI
#endif
