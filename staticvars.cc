/* Some static object variables. Collecting them here allows to control
   th eorder in which they are instantiated.
*/
#include "guli/seqgenomeregionset.h"
#include "guli/mmu.h"

string ucscGenome::_defaultBase = DEFAULT_BASE_DIRECTORY;
#ifdef HAVE_THREADS
bool mmu::managed::_mutex_initialized = false;
pthread_mutex_t mmu::managed::_mtx;
#endif
