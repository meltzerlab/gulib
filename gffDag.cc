/*
 * gffDag.cc
 *
 *  Created on: Sep 27, 2012
 *      Author: sven
 */
#include <sys/types.h>
#include <regex.h>
#include <fstream>
#include "guli/gffDag.h"


namespace GULI {
	const string gffDag::rootId = "ROOT";


	// Initialize an empty gffDag to contain a root element
	void gffDag::clear() {
		Dag::clear();
		GULI::gffDag::Node::textual rootNode;
		rootNode.attribute["ID"] = rootId;
		_addNode(new Node(rootNode));
		return;
	}

	gffDag::Node *gffDag::operator[](const string &id) const {
		return dynamic_cast<Node *> (Dag::operator[](idAndVersionT(id).id));
	}

	bool  gffDag::exists(const string &id) const {
		return Dag::exists(idAndVersionT(id).id);
	}

	void gffDag::addNode(Node *N) {
			_addNode(static_cast<Dag::Node *>(N), idAndVersionT(N->id()).id);
		}

	const char gffDag::Node::textual::reservedAttributes [][20] = {
			"ID", "Parent", ""
	};

	gffDag::Node::Node(const bedRegion &B, const string &feature, const string &source, char frame) :
		bedRegion(B),
		_type(feature),
		_source(source),
		_frame(frame)
	{}

	gffDag::Node::Node(const textual &T) :
			bedRegion(T.seqname, "", T.start, T.end, T.score, T.strand),
			_type(T.feature),
			_source(T.source),
			_frame(T.frame)
	{
		// parse attributes to get ID, gene name and attributes not otherwise absorbed
		for(textual::attributeT::const_iterator i=T.attribute.begin(); i != T.attribute.end(); ++i) {

			int token = -1;
			for(int j=0; *textual::reservedAttributes[j]; ++j) {
				if(i->first == textual::reservedAttributes[j]) {
					token = j;
					break;
				}
			}

			switch(token) {
				case -1: addReplaceAttribute(i->first, i->second); // Not handled here
						 break;

				case 0:  bedRegion::id(i->second);
						break;

				default: break; // this includes parents, treated elsewhere
			}
		}
	}

	ostream &gffDag::Node::operator<<(ostream &O) {
		O << chromosome() << "\t" << source() << "\t" << feature() << "\t"
          << start() << "\t" << end() <<  "\t" << score() << "\t" << orient() << "\t"
		  << frame() << "\t" << "ID=" << id();

		bool labeled = false;
		for( size_t i=0; i < nIncoming(); ++i) {
			if(incoming(i)->id() != gffDag::rootId) {
				if(!labeled) {
					labeled = true;
					O << ";Parent=" << incoming(i)->id();
					continue;
				} else {
				 O << ',' << incoming(i)->id();
				}
			}
		}

		vector<string>  A = knownAttributes();
		for(size_t i=0; i < A.size(); ++i)
			O << ";" << A[i] << "=" << attribute(A[i]);
		return O;
	}


	static void chomp(string &s) {
		size_t left = s.find_first_not_of(' ');
		if(left && left != string::npos) s = s.substr(left, string::npos);
		size_t l = s.length();
		while(l && s[l] == ' ') --l;
		if(l != s.length()) s = s.substr(0, l);
	}

	gffDag::Node::textual::attributeT gffDag::Node::textual::parseAttributeString(const string text) {
		attributeT R;
		string expr,  id, value;
		for(size_t spos = 0; spos != string::npos; )  {
			size_t lpos = text.find_first_of(';',spos);
			expr = text.substr(spos, lpos-spos);
			chomp(expr);
			size_t eq   = expr.find_first_of(" =");
			if(eq == string::npos || eq >= expr.length()) {
				std::cerr << "Can not parse attribute field " << expr << ". Ignoring this instance\n";
				continue;
			}
			id     = expr.substr(0, eq);
			value  = expr.substr(eq+1, string::npos);

			chomp(id);
			chomp(value);

			if(value[0] == '"') {
				value = value.substr(1,value.length()); // remove "
				if(value[value.length()-1] == '"') value = value.substr(0,value.length()-1); // remove "
			}

			if(R.find(id) != R.end()) { //merge tags if they occur more than once
				R[id] += "," + value;
			} else {
				R[id] = value;
			}
			if(lpos == string::npos) break;
			spos = text.find_first_not_of(' ', lpos+1); // eat white-spaces
		}

		return R;
	}

} // NAMESPACE

