/*
 * attributedDagNode.cc
 *
 *  Created on: Oct 18, 2013
 *      Author: sven
 */
#include "guli/utilAttributeFunctionality.h"

vector<string> GULI::utilAttributeFunctionality::knownAttributes() const {
	vector<string> r;
	for(_attributeT::const_iterator i=__attributes.begin(); i != __attributes.end(); ++i) {
		r.push_back(i->first);
	}
	return r;
}

const string &GULI::utilAttributeFunctionality::attribute(const string &id) const {
	_attributeT::const_iterator i = __attributes.find(id);
	if(i==__attributes.end()) return UNKNOWNATTRIBUTE;
	return i->second;
}


const char *GULI::utilAttributeFunctionality::getAttribute(const string &id)  const{
		_attributeT::const_iterator i = __attributes.find(id);
		if(i == __attributes.end()) return 0;
		return i->second.c_str();
}


const string GULI::utilAttributeFunctionality::UNKNOWNATTRIBUTE="UNKNOWN_ATTRIBUTE";


