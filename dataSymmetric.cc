/***************************************************************************
 dataSymmetric.cc  -  description
 -------------------
 begin                : Thu Jun 5 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <assert.h>
#include "guli/stdinc.h"
#include "guli/dataSymmetric.h"

namespace GULI {
		dataSymmetric::dataSymmetric(unsigned int size) :
			_size(size) {
			int memsize = _size * (_size + 1) / 2;
			_data = new float[memsize];
			_id.resize(size);
			;
		}

		dataSymmetric::dataSymmetric(dataSymmetric &init) :
			_size(init._size) {
			int memsize = _size * (_size + 1) / 2;
			_data = new float[memsize];
			std::copy(init._data, init._data + memsize, _data);
			std::copy(init.beginRowLabel(), init.endRowLabel(), beginRowLabel());
		}

		dataSymmetric::dataSymmetric(dataIF &init) :
			_size(init.cols()) {
			assert(init.rows() == _size);
			int memsize = _size * (_size + 1) / 2;
			_data = new float[memsize];
			_id.resize(_size);
			for (unsigned int i = 0; i < _size; i++) {
				for (unsigned int j = i; j < _size; j++)
					Data(j, i) = init(j, i);
			}
			std::copy(init.beginRowLabel(), init.endRowLabel(), beginRowLabel());
		}

		dataSymmetric::~dataSymmetric() {
			delete[] _data;
		}

		float &dataSymmetric::Data(int col, int row) {
			int idx;
			if (col > row) {
				idx = (col) * (col + 1) / 2 + row;
			} else {
				idx = row * (row + 1) / 2 + col;
			}
			return _data[idx];
		}

		float dataSymmetric::Data(int col, int row) const {
			int idx;
			if (col > row) {
				idx = (col) * (col + 1) / 2 + row;
			} else {
				idx = row * (row + 1) / 2 + col;
			}
			return _data[idx];
		}

} // namespace SB::data
