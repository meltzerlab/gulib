/*
 * ngsBamUtils.cc
 *
 *  Created on: Jun 9, 2011
 *      Author: sven
 */

/*
 * bamFuncs.cc
 *
 *  Created on: May 5, 2011
 *      Author: sven
 */
//#include <strstream>
#include <sstream>
#include <iostream>
#include <assert.h>

#include <htslib/hts.h>
#include <htslib/sam.h>
#include <htslib/khash.h>

#include "guli/ngsBamUtils.h"



using namespace GULI::NGS::BAMUTILS;


static 	void _clipMD (bam1_t *b, size_t qpos,  orient_t orient) {
	// find update position for MD-Tag string
	bool xd = false;
	uint8_t *MD = bam_aux_get(b, "MD");
	if(!MD) {
		MD = bam_aux_get(b, "XD"); // Just use the alternative
		xd = true;
	}
	if(!MD) return; // nothing to do

	vector<MDop_T> mda = parseMD(bam_aux2Z(MD));
	size_t Epos = 0;
	unsigned int mdpos = 0;
	for(mdpos=0; mdpos < mda.size(); ++mdpos) {
		switch(mda[mdpos].op) {
		case MATCH:
		case MISSMATCH:
			Epos += mda[mdpos].len;
			break;
		case DELETE:
			break;
		default:
			std::cerr << "Unknown MD tag operation for " << bam1_qname(b) << std::endl;
			break;
		}
		if(Epos >= qpos) break;
	}
	assert(mdpos <  mda.size()); // this should never happen?!

	MDop_T &cur = mda[mdpos];

	if(orient == RIGHT) { // cap on right
		cur.len =  qpos - (Epos - cur.len);
		if(mdpos < mda.size()  - 1) mda.erase(mda.begin() + mdpos + 1, mda.end());
	} else { // cap on left
		int q = Epos - qpos;
		int delta = cur.len - q;
		cur.len =  q;
		if(cur.op == DELETE && delta)
			std::copy(cur.nt.begin() + delta, cur.nt.begin() + delta + cur.len, cur.nt.begin());
		if(mdpos) mda.erase(mda.begin(), mda.begin() + mdpos + (cur.len ?  0 : 1 ));
	}
	if(cur.op == DELETE && cur.len < cur.nt.size()) // trim insertion string
		cur.nt.erase(cur.nt.begin() + cur.len, cur.nt.end());
	char *newmd = createMD(mda);
	editTag(b, xd ? "XD" : "MD", newmd);
	delete [] newmd;
	return;
}

static bool _softClip(bam1_t *b, size_t pos,   orient_t orient) {
	if (b->core.flag & BAM_FUNMAP)  return false; // not Aligned
	uint32_t ncigar  = b->core.n_cigar;
	uint32_t *cigar  = bam1_cigar(b);
	const unsigned int p_start = b->core.pos;
	const unsigned int p_end   = bam_calend(&b->core, cigar);
	if(pos < p_start || pos > p_end) return false;

	const unsigned int dist = pos - p_start;

	// find update position for CIGAR string
	unsigned int cigarPos = 0;
	unsigned int iCigar;
	int qpos = 0;   // the position within the MD string
	int cqpos = 0;  // position in the bam-sequence, differs from qpos (referencing the MD position) because
	                // the md-string does not account for inserts, but we need the position in the query string to
	                // calculate the length of the soft-clipped region
	int clen  = 0;  // Count number of already clipped nt's on the left

	for(iCigar=0; iCigar < ncigar; ++iCigar) {
		const int l = cigar[iCigar] >> BAM_CIGAR_SHIFT;
		const int op = cigar[iCigar] & BAM_CIGAR_MASK;
		if(op == BAM_CMATCH)                                          qpos     += l;
		if(op == BAM_CMATCH || op == BAM_CINS)                        cqpos    += l;
		if(op == BAM_CMATCH || op == BAM_CDEL || op == BAM_CREF_SKIP) cigarPos += l;
		if(op == BAM_CSOFT_CLIP)                                      clen     += l;
		if(cigarPos >= dist) break;
	}
	assert(cigarPos >= dist);

	// adjust positions to start of cigar operation and calculate remainder
	const unsigned int step      = (cigar[iCigar] >> BAM_CIGAR_SHIFT);
	const unsigned int op        = cigar[iCigar] & BAM_CIGAR_MASK;
	const unsigned int cPartial  = dist + step - cigarPos;
	qpos                        -= (op == BAM_CMATCH) ? step - cPartial : 0;
	cqpos                       -= (op == BAM_CMATCH) ? step - cPartial : 0;
	const unsigned int Slen      = (orient == RIGHT)  ? bam_cigar2qlen(&b->core, cigar) - clen - cqpos
			                                          : cqpos + clen;
	const unsigned int clipOP    = BAM_CSOFT_CLIP | (Slen << BAM_CIGAR_SHIFT);


	if(orient == RIGHT) { // cap on right
		cigar[iCigar] = (cPartial << BAM_CIGAR_SHIFT) | op;
		insertCigarOperations(b, iCigar + 1, 1, &clipOP);
		if(iCigar < ncigar - 1) deleteCigarOperations(b, iCigar + 2, ncigar - iCigar - 1);
	} else { // cap on left
		const unsigned int leftover = step - cPartial;
		if(leftover) {
			cigar[iCigar] = (leftover << BAM_CIGAR_SHIFT) | op;
			if(iCigar >  0) deleteCigarOperations(b, 0, iCigar);
		} else { // there should always be a leftover? Do we need this arm?
			deleteCigarOperations(b, 0, iCigar + 1);
		}
		insertCigarOperations(b, 0, 1, &clipOP);
		b->core.pos = pos; // adjust alignment position
	}

	// adjust MD string
	_clipMD (b, qpos,   orient);
	return true;
}


static const char nt_bam2ascii[] = { '?', 'A', 'C', '?', 'G', '?', '?', '?',
									'T', '?', '?', '?', '?', '?', '?', 'N'
};

namespace GULI {
	namespace NGS {
		namespace BAMUTILS {

			long int idxaligned(bam_header_t *header, bam_index_t *idx, int i) {
				 uint64_t u, v;
			     hts_idx_get_stat(idx, i, &u, &v);
			     return u;
			}


			uint8_t mapAscii2Bam(char a) {
				switch(a) {
					case 'A':
					case 'a': return 0x1;
					case 'C':
					case 'c': return 0x2;
					case 'G':
					case 'g': return 0x4;
					case 'T':
					case 't': return 0x8;
					default:  return 0xF;
				}
			}

			char mapBam2Ascii(uint8_t bnt) {
				char nt = nt_bam2ascii[bnt & 0xF];
				return  nt;
			}


			/** Calculate the chromosome position associated with a query position **/
			uint32_t q2chrPos(const bam_pileup1_t &CUR) {
				const bam1_t *b = CUR.b;
				uint32_t pos  = b->core.pos;
				int32_t covered = 0;
				const uint32_t *cigar = bam1_cigar(b);

				for(int i=0; i < b->core.n_cigar; ++i) {
					uint32_t op = cigar[i] & BAM_CIGAR_MASK;
					uint32_t l  = cigar[i] >> BAM_CIGAR_SHIFT;

					switch(op) {
						case BAM_CSOFT_CLIP:
						case BAM_CINS:
							assert(covered < CUR.qpos); // This should not happen by the semantics of qpos and CIGAR
				 			covered += l;
				 			break;

						case BAM_CDEL:
							if(CUR.is_del && covered == CUR.qpos) return pos;
							pos += l;
							break;

						case BAM_CMATCH:
							if(covered == CUR.qpos) return pos;
				 			covered += l;
				 			pos += l;
							if(covered > CUR.qpos) {
								pos -= covered - CUR.qpos;
								return pos;
							}
				 			break;
						default: 	break;
					}
				}
				assert(0); //This should not happen by the semantics of qpos and CIGAR
				return 0;  // keep gcc quiet
			}


			uint8_t referenceNT(const bam_pileup1_t &pl, uint32_t offset) {
				bam1_t * const b = pl.b;
				// position coordinate used in the pl structure  count soft-clipped nucleotides
				// qstart now points to the first position AFTER leading clipping, i.e. the position corresponding
				// to the beginning of the MD string.
				// Next : MD string does not contain information about inserts, so need to adjust for this
				// need to sync query position with representation in MD string.
				// check MD string to see if we hit a variant
				// specs are not clear (to me) about treatment of nt's within deletions.
				// experimentally,  semantic of qpos changes in deletions, where the "delete" operation
				// needs to be counted towards the MD string position IF and ONLY IF
				// the NT of interest is inside the deletion. I am not sure what happens if there
				// are two disjoint deletions

				uint8_t *MD = bam_aux_get(b, "MD");
				if(!MD)  MD = bam_aux_get(b, "XD");
				if(!MD) return mapAscii2Bam('N'); // Can not extract reference from MD string


				uint32_t adj_qpos = pl.qpos;
				int qstart = 0, covered = 0;
				uint32_t *cigar = bam1_cigar(b);

				for(int i=0; i < b->core.n_cigar; ++i) {
					uint32_t op = cigar[i] & BAM_CIGAR_MASK;
					uint32_t l  = cigar[i] >> BAM_CIGAR_SHIFT;
					switch(op) {
						case BAM_CSOFT_CLIP:  qstart  += l;
											  covered += l;
											  break;

						case BAM_CINS:   adj_qpos  -= l; // insertions are not marked up in MD string, but are counted towards the Cigar
						                 covered   += l;
						                 break;

						case BAM_CMATCH: covered   += l; break;

						default: break;
					}
					if(covered >= pl.qpos) break;
				}



				// check MD string to see if we hit a variant
				// specs are not clear (to me) about treatment of nt's within deletions.
				// experimentally,  semantic of qpos changes in deletions, where the "delete" operation
				// needs to be counted towards the MD string position IF and ONLY IF
				// the NT of interest is inside the deletion. I am not sure what happens if there
				// are two disjoint deletions
				const char *md = bam_aux2Z(MD);
				const vector<MDop_T> mda = parseMD(md);
				for(unsigned int opPos = 0; opPos < mda.size(); ++opPos) {
					if(qstart + mda[opPos].len     >   adj_qpos) {
						switch(mda[opPos].op) {
							case MISSMATCH: return mapAscii2Bam(mda[opPos].nt[0]);
							case MATCH:     return bam1_seqi(bam1_seq(b), pl.qpos);
							case DELETE:    if(pl.is_del && qstart == adj_qpos)
												return mapAscii2Bam(mda[opPos].nt[ adj_qpos - qstart + offset]);
											break;
						}
					}
					if (mda[opPos].op == MATCH || mda[opPos].op == MISSMATCH) qstart  += mda[opPos].len;
				}
				return 0XF; // this should not happen
			}

   			int updateCigar(bam1_t *b, const uint32_t *cigar, const size_t len) {
				const int change = len - b->core.n_cigar;
				if(change < 0) {
					if(deleteCigarOperations(b, 0, -change)) return -1;
				} else if(change > 0) {
					if(insertCigarOperations(b, 0, change, (uint32_t *) 0)) return -1;
				}

				const size_t L = len * sizeof(uint32_t);
				char *target = (char *) bam1_cigar(b);
				char *source = (char *) cigar;
				memcpy(target, source, L);
				return 0;
			}

			int insertCigarOperations(bam1_t *b, const size_t cigarPosition, const size_t nOp, const uint32_t *op) {
				const size_t S = sizeof(uint32_t) * nOp;
				const size_t L = sizeof(uint32_t) * cigarPosition;

				int needmem = b->data_len + S;
				if(needmem >= b->m_data) {
					kroundup32(needmem);
					b->data = (uint8_t*)realloc(b->data, needmem);
					b->m_data = needmem;
				}

				char *cigarInsertPos =  (char *) bam1_cigar(b) +  L;
				for(char *p= (char *) b->data + b->data_len-1; p >= cigarInsertPos; --p) *(p+S) = *p;
				if(op) for(size_t i=0; i < S; ++i) cigarInsertPos[i] = *((char *) op + i);
				b->data_len     += S;
				b->core.n_cigar += nOp;
				return 0;
			}

			int deleteCigarOperations(bam1_t *b, const size_t cigarPosition, const size_t nOp) {
				const size_t S = sizeof(uint32_t) * nOp;
				const size_t L = sizeof(uint32_t) * cigarPosition;
				if(b->core.n_cigar < nOp) {
					std::cerr << "Attempt to remove more CigarOPs than what is actually present " << std::endl;
					return -1;
				}

				for(char *p= (char *) bam1_cigar(b) + L; p < (char *) b->data + b->data_len - S; ++p) *p = *(p+S);
				b->data_len     -= S;
				b->core.n_cigar -= nOp;
				return 0;
			}

			int editTag(bam1_t *b, const tagId_t id, const string &text) {
				uint8_t *existingTag = bam_aux_get(b, id);
				if(existingTag) bam_aux_del(b, existingTag);
				bam_aux_append(b, id, 'Z', text.length()+1, (uint8_t *) text.c_str());
				return 0;
			};

			const char *getStringTag(const bam1_t *b, const tagId_t id) {
				uint8_t *tag = bam_aux_get(b, id);
				return tag ? bam_aux2Z(tag) : 0;
			}

			char *createMD(const vector<MDop_T> &MD) {
				std::stringstream O;

				const int L=MD.size();
				for(int i =0; i < L; ++i) {
					// insert 0-length matches were required
					const MDop_T &cur = MD[i];

					if(cur.op != MATCH && ( (i && MD[i-1].op != MATCH) || !i))  O << '0';
					switch(cur.op) {
						case MATCH:        O << cur.len; break;
						case MISSMATCH:    O << cur.nt[0]; break;
						case DELETE:       {
							O << '^';
							for(size_t j=0; j < cur.len; ++j) O << cur.nt[j];
							break;
						}
						default:
							std::cerr << "Unknown MD operation?" << std::endl;
							break;
					}
				}
				if(MD[L-1].op != MATCH) O << '0';
				O << std::ends;
				size_t l = O.str().length();
				char *buffer = new char[l + 1];
				strcpy(buffer, O.str().c_str());
				return buffer;
			}

			vector<MDop_T> parseMD(const char *md) {
				vector<MDop_T> R;
				char *next;
				for(const char *ppos = md; *ppos; ppos = next) {
					size_t len = strtol(ppos, &next, 10);
					if(len) {
						MDop_T E = { MATCH, len };
						R.push_back(E);
					}
					if(*next) { // start next op
						switch(*next) {
							case '^': {
								vector<char> insert;
								char *p;
								for(p=next+1; *p; ++p) {
									if(isdigit(*p)) break;
									insert.push_back(*p);
								}
								if(R.size() && R[R.size() - 1].op == DELETE) { // merge adjacent delete operations
									MDop_T &C = R[R.size() - 1];
									C.nt.insert(C.nt.end(), insert.begin(), insert.end());
									C.len = C.nt.size();
								} else {
									MDop_T E = { DELETE, insert.size(), insert };
									R.push_back(E);
								}
								next = p;
								break;
							}
							default:
								vector<char> nt;
								nt.push_back(*next);
								MDop_T E = { MISSMATCH, 1, nt };
								R.push_back(E);
								++next;
								break;
						}
					}
				}
				return R;
			}

			bool softClip(bam1_t *b,  const size_t position, orient_t orient) {
				// to keep it at least somewhat readable, process is split in cigar, md=string components above
				return _softClip(b, position, orient);
			}

			int32_t genomicCoords(const bam1_t *b, vector<int32_t> &result) {
				const uint32_t *cigar = bam1_cigar(b);
				const uint32_t L      = bam_cigar2qlen(&b->core, cigar);
				result.resize(L);
				uint32_t pos = b->core.pos;
				uint32_t  iq = 0;
				for(size_t iCigar=0; iCigar < b->core.n_cigar; ++iCigar) {
					const size_t l  = cigar[iCigar] >> BAM_CIGAR_SHIFT;
					const int   op  = cigar[iCigar] & BAM_CIGAR_MASK;
					switch(op) {
						case BAM_CMATCH:
							for(size_t i=0; i < l; ++i)	result[iq++] = pos++;
							break;

						case BAM_CINS:
						case BAM_CSOFT_CLIP:
						case BAM_CHARD_CLIP:
						case BAM_CPAD: // Haven't seen a datafile using padding, check this if you run into trouble
							for(size_t i=0; i < l; ++i)	result[iq++] = -1;
							break;

						case BAM_CREF_SKIP:
						case BAM_CDEL:
							pos += l;
							break;

						default:
							assert(0);
							break;
					}
				}

#undef BUGME
#ifdef BUGME
				assert(iq == L);
				assert(pos ==  bam_calend(&b->core, cigar));
#endif
#undef BUGME
				return pos;
			}

			char baseQuality(const bam_pileup1_t &pl)  {
				if(pl.is_del) return '!'; // deletions don't have base quality assigned to it
				char *q = reinterpret_cast<char *>(bam1_qual(pl.b));
				return q[pl.qpos];
			}


		} // BAMUTILS
	}     // NGS
}         // GULI

#undef DEBUG_REF_NT
#ifdef DEBUG_REF_NT
#include "guli/ngsBamPiler.h"
#include "guli/bedTilingInstances.h"
namespace GULI {
	namespace DEBUG {
		class debugRefNT : public GULI::NGS::piler::streamingClient {
		public:
			debugRefNT() : _OUT(0) {};
			~debugRefNT(){}
			virtual GULI::NGS::pileup_t *pilerStreamingSetup(uint32_t tid, uint32_t pos,  size_t fileIndex, bool &manage) {
				buffer.clear();
				manage = false;
				return &buffer;
			}

			virtual void positionStreamsComplete(uint32_t tid, uint32_t pos) {
				const size_t MAXCODE=15;
				int count[MAXCODE+1];
				std::fill(count, count + MAXCODE + 1, 0);
				for(size_t i=0; i < buffer.size(); ++i) {
					if(buffer[i].is_del) continue;
					uint8_t r = GULI::NGS::BAMUTILS::referenceNT(buffer[i],0);
					++count[r];
				}
				if(!buffer.size()) return;

				int c = 0, M=0;
				uint8_t m = MAXCODE;
				for(size_t i =0; i < MAXCODE; ++i) {
					if(count[i]) ++c;
					if(count[i] > M) { m = i; M = count[i]; }
				}
				if(c==1) return;

				for(size_t i=0; i < buffer.size(); ++i) {
					if(buffer[i].is_del) continue;
					uint8_t r = GULI::NGS::BAMUTILS::referenceNT(buffer[i],0);
					if(r != m) {
						GULI::NGS::ntRead_t &cur = buffer[i];
						std::cerr << bam1_qname(cur.b) << "\t" << _OUT->header->target_name[tid] << "\t" << pos << "\t" << cur.qpos;
						std::cerr << "\t" << nt_bam2ascii[r] << "\t" << nt_bam2ascii[m] <<"\n";
						samwrite(_OUT, cur.b);
						// write out data to a bam-file
					}
				}
			}
			GULI::NGS::pileup_t buffer;
			samfile_t *_OUT;

			int main(int argc, char **argv) {
				samfile_t *IN;
				IN =  samopen(argv[1], "rb", 0);
				if(!IN) {
					std::cerr << "Can not open " << argv[1] << std::endl;
					return -1;
				}

				_OUT =  samopen(argv[2], "w", IN->header);
				if(!_OUT) {
					std::cerr << "Can not open " << argv[2] << std::endl;
					return -1;
				}


				vector<samfile_t *> streams;
				streams.push_back(IN);
				vector<string> FN;
				string fn = argv[1];
				FN.push_back(fn);

				GULI::NGS::piler P;
				if(P.attach(streams, FN)) return -1;

				GULI::bedRegion::listT feature;

				for(int c = 0; c <  IN->header->n_targets; ++c) {
					GULI::bedRegion B(IN->header->target_name[c], IN->header->target_name[c], 0, IN->header->target_len[c]);
					feature.push_back(B);
				}

				GULI::constBedListTiling *T = new GULI::constBedListTiling (feature.begin(), feature.end());
				P.stream(this, *T);
				delete T;

				samclose(_OUT);
				samclose(IN);
				return 0;
			}
		};
		int mainDebugRefNT(int argc, char **argv) {
			debugRefNT R;
			return R.main(argc, argv);
		}
	}


}

/*
 * This can go into your main file if you want to invoke the test routines
namespace GULI {
  namespace DEBUG {
    extern int mainDebugRefNT(int argc, char **argv);
  }
}
int main(int argc, char **argv) {
  return GULI::DEBUG::mainDebugRefNT(argc, argv);
}
*/
#endif
