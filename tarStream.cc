/*
 * tarStream.cc
 *
 *  Created on: Jun 29, 2010
 *      Author: sven
 */
#include <errno.h>
#include <stdio.h>
#include <numeric>
#include <string.h>
#include <cstring>
#include "guli/tarStream.h"

namespace GULI {

	istream &tarStream::operator<<(istream &I) {
		_interrupt = false;
		while (!(I.eof() || I.fail() || I.bad() || _interrupt)) {
			_hdr << I;
			int r = 1;

			// end of tape
			if (_hdr.isEot()) break;

			if (_hdr.isValid()) {
				_outofsync = false;
				switch (_hdr.type()) {
					case tarHeader::REGTYPE:
					case tarHeader::AREGTYPE:
						r = consumeFile(_hdr, I);
						break;
					case tarHeader::LNKTYPE:
						r = consumeHardLink(_hdr);
						break;
					case tarHeader::SYMTYPE:
						r = consumeSoftLink(_hdr);
						break;
					case tarHeader::CHRTYPE:
						r = consumeCharSpecial(_hdr);
						break;
					case tarHeader::BLKTYPE:
						r = consumeBlockSpecial(_hdr);
						break;
					case tarHeader::DIRTYPE:
						r = consumeDirectory(_hdr);
						break;
					case tarHeader::FIFOTYPE:
						r = consumeFifo(_hdr);
						break;

					default:
						if (!_outofsync) std::cerr
								<< "This does not look like a valid tar header (file type), searching next header\n";
						_outofsync = true;
						r = 0;
						break;
				}
			} else {
				if (_resync) {
					if (!_outofsync) std::cerr
							<< "This does not look like a valid tar header, searching next header\n";
					_outofsync = true;
					r = 0;
				} else {
					if (r) I.setstate(std::ios_base::failbit);
				}
			}
			if (r) I.setstate(std::ios_base::badbit);
		}
		stoppedReading();
		return I;
	}

	int tarStream::consumeFile(const tarHeader &H, istream &I) {
		char *buffer = new char[blocksize()];
		int blocksToRead = (H.size() + blocksize() - 1) / blocksize();
		for (int i = 0; i < blocksToRead; ++i)
			I.read(buffer, blocksize());
		delete[] buffer;
		return 0;
	}

	static bool _getToken(string &r, char *buffer, const char *id) {
		char *label, *dstart;
		string LABEL, TEXT;
		errno = 0;

		int len = strtol(buffer, &label, 10);
		if(errno) return false;

		while(*label && *label == ' ') label++;
		if(!*label) return false;

		for(dstart=label; *dstart && *dstart != '='; dstart++);
		if(!*dstart) return false;
		LABEL.assign(label, dstart - label);

		++dstart; if(!*dstart) return false;
		len -= dstart - buffer + 1;
		char *next = dstart + len + 1;
		if(LABEL != id) {
			if(*next) return _getToken(r, next, id);
			return false;
		}
		r.assign(dstart, len);
		return true;
	}


	istream &tarStream::tarHeader::operator<<(istream &I) {
		char namebuffer[NAMELEN+1];
		_posixAtime = _posixCtime = _posixMtime = _posixLink = _posixSize = _posixName = "";

		while(1){
			I.read(_buffer, _blocksize);
			unsigned int c1 = _calcChecksum();
			unsigned int c2 = _o2uint(_header().checksum);
			_isValid = ((!I.fail()) && (c1 == c2));
			strncpy(namebuffer, _header().name, NAMELEN);
			namebuffer[NAMELEN]='\0';
			_name = namebuffer;

			if (_isLongLink()) {
				int blocksToRead = (size() + blocksize() - 1) / blocksize();
				int need2read = blocksToRead * blocksize();
				char *buffer = new char[need2read];
				I.read(buffer, need2read);
				_name = buffer;
				delete[] buffer;
//TODO: check that this is the right behavior below, will overwrite the header!. Need a TAR file utilizing a longLink header to test this....
				I.read(_buffer, _blocksize); //
			}  else if(_isXHdr()) {
				int blocksToRead = (size() + blocksize() - 1) / blocksize();
				int need2read = blocksToRead * blocksize();
				char *buffer = new char[need2read];
				I.read(buffer, need2read);
				_getToken(_posixName, buffer, "path"); // update name (if updated in that block, that is)
				_getToken(_posixAtime, buffer,"atime"); // update posix-2001 atime (if updated in that block, that is)
				_getToken(_posixCtime, buffer, "ctime"); // update posix-2001 ctime (if updated in that block, that is)
				_getToken(_posixMtime, buffer, "mtime"); // update posix-2001 mtime (if updated in that block, that is)
				_getToken(_posixLink,  buffer, "linkpath"); // update posix-2001 linkpath (if updated in that block, that is)
				_getToken(_posixSize,  buffer, "size"); // update posix-2001 size (if updated in that block, that is)
				delete[] buffer;
				continue;
			}
			break;
		}
		return I;
	}

	int tarStream::tarHeader::blocksize(int b) {
		if (_blocksize != b) {
			delete[] _buffer;
			_buffer = new char[b];
		}
		_blocksize = b;
		return _blocksize;
	}

	unsigned int tarStream::tarHeader::_calcChecksum() {
		unsigned int r = std::accumulate(_buffer, _buffer + _blocksize,
				(unsigned int) 0);
		// Checksum does not include checksum itself, but rather " ", so correct for this
		unsigned int r1 = std::accumulate(_header().checksum,
				_header().checksum + 8, (unsigned int) 0);
		return r - r1 + 8 * ' ';
	}

	unsigned int tarStream::tarHeader::_o2uint(const string &s) const {
		unsigned long int r;
		sscanf(s.c_str(), "%lo", &r);
		return r;
	}

	bool tarStream::tarHeader::isEot() const {
		return 0 == std::accumulate(_buffer, _buffer + _blocksize,  (unsigned int) 0);
	}

	tarStream::tarHeader::tarHeader() : _blocksize(512), _isValid(false)   {
		_buffer = new char[_blocksize];
	}

	tarStream::tarHeader::~tarHeader() {
		delete[] _buffer;
	}

	// This is derived from
	// gzstream, C++ iostream classes wrapping the zlib compression library.
	// Copyright (C) 2001  Deepak Bandyopadhyay, Lutz Kettner
	// which in turn was derived from
	// Standard streambuf implementation following Nicolai Josuttis, "The
	// Standard C++ Library".
	_tarStreamBase::streambuf::streambuf(const tarStream::tarHeader &H, istream &I):
			_tstream(I),
			_blockSize(H.blocksize()),
			_bytesLeft(H.size()) {
		const int bufferSize = _blockSize + 4;
		_buffer = new char[bufferSize];
		setp(_buffer, _buffer + (bufferSize - 1));
		setg(_buffer + 4, // beginning of putback area
			 _buffer + 4, // read position
			 _buffer + 4); // end position
	}


	int _tarStreamBase::streambuf::underflow() { // used for input buffer only
		static const int PUTBACK = 4;
		if (gptr() && (gptr() < egptr())) return *reinterpret_cast<unsigned char *> (gptr());

		if ((_bytesLeft==0) || _tstream.fail() || _tstream.eof()) return EOF;

		// Josuttis' implementation of inbuf
		int n_putback = gptr() - eback();
		if (n_putback > PUTBACK) n_putback = PUTBACK;
		memcpy(_buffer + (4 - n_putback), gptr() - n_putback, n_putback);

		_tstream.read(_buffer + PUTBACK, _blockSize);
		if (_tstream.fail() || _tstream.eof()) return EOF;

		int num = _blockSize < _bytesLeft ? _blockSize : _bytesLeft;
		_bytesLeft -= num;

		// reset buffer pointers
		setg(_buffer + (PUTBACK - n_putback), // beginning of putback area
			 _buffer + PUTBACK,               // read position
			 _buffer + PUTBACK + num);        // end of buffer

		return *reinterpret_cast<unsigned char *> (gptr());
	}


	_tarStreamBase::streambuf::~streambuf() {
		if(_bytesLeft) { // read to end of file-stream
			int blocksToRead = (_bytesLeft + _blockSize - 1) / _blockSize;
			for (int i = 0; i < blocksToRead; ++i)
				_tstream.read(_buffer, _blockSize);
		}
		delete [] _buffer;
	}
}
