/*************************************************************************
 tabdelimited.cpp  -  description
 --------------------------------
 begin                : Sun Nov 19 2000
 copyright            : (C) 2000-2018 by Sven Bilke
 email                : sven@gmail.com
 *************************************************************************/
/*__ LICENSE_BLABLA*/
#include <string.h>
#include "guli/stdinc.h"
#include "guli/tabDelimited.h"

namespace GULI {
	class _tabLineParser {
	typedef char *CP;
	public:
		_tabLineParser(tabDelimited &master) : _M(master) {}
		virtual ~_tabLineParser() {}

		void parseLine(string &line) {

			const char DELIMITER   = _M.DELIMITER();
			const char STDELIMITER = _M.STDELIMITER();
			const bool OS          = _M.observeStringDelimiter();

			const size_t l = line.length();
			const char *debug = &_Linebuffer[0];
			_Linebuffer.resize(l + 1);
			std::copy(line.begin(), line.end(), _Linebuffer.begin());
			_Linebuffer[l] = 0;
			if ((l>=1) && (_Linebuffer[l - 1] == '\n')) _Linebuffer[l - 1] = 0;

			_token.clear();
			if (_Linebuffer[0] && (_Linebuffer[0] != '#' || !_M.ignoreComment())) {
				_token.push_back(&_Linebuffer[0]);
				bool inString = false;
				for (int i = 0; _Linebuffer[i]; i++) {
					if(OS && _Linebuffer[i] ==  STDELIMITER) inString ^= true;
					if (_Linebuffer[i] == DELIMITER && !inString) {
						_Linebuffer[i] = 0;
						if (_Linebuffer[i + 1]) _token.push_back(&_Linebuffer[i + 1]);
					}
				}
			}
			return;
		}

		char **argv() { return &_token[0]; }
		int argc()    { return _token.size(); }
	private:
		tabDelimited &_M;
		std::vector<char> _Linebuffer;
		std::vector<char *>   _token;
	};



	tabDelimited::tabDelimited(const char DEL) :
		_lines_read(0) {
		_delimiter = DEL;
		_stDelimiter= '"';
		_ignoreComment = true;
		_observeStringDelimiter = false;
	}

	tabDelimited::~tabDelimited() {}

	int tabDelimited::consumeData(int nSource, argcT argc[], argvT argv[]) {
		std::cerr << "One day you will wake up and ask yourself \"How did I get here?\" (Calling pure virtual, that is....)\n";
		assert(0);
		return -1;
	}

	int tabDelimited::parse(vector<istream *> &in) {
		int retval = 0;
		const unsigned int nStream = in.size();
		if(!nStream) return retval;

		argvT argv[nStream];
		argcT argc[nStream];

		vector<_tabLineParser *> P(nStream);
		for(unsigned int i =0; i < nStream; ++i) P[i] = new _tabLineParser(*this);

		while (1) {
			int eof = 0;
			for(unsigned int i=0; i < nStream; ++i) {
				std::getline(*in[i], _line);
				P[i]->parseLine(_line);
				argv[i] = P[i]->argv();
				argc[i] = P[i]->argc();
				if(in[i]->eof()) ++ eof;
			}
			if(consumeData(nStream, argc, argv) ) {retval = -1; break; }
			++_lines_read;
			if(eof) break;
		}
		for(unsigned int i=0; i<nStream; ++i) delete P[i];
		return retval;
	}

	istream &tabDelimited::parse(istream &in) {
		_tabLineParser  P(*this);
		while (1) {
			std::getline(in, _line);
			P.parseLine(_line);
			consumeData(P.argc(), P.argv());
			++_lines_read;
			if(in.eof() || in.bad()) return in;
		}
		return in;
	}


	int _getline(const char *in, int len, string &l) {
		const char *t = in;
		while(len--) if(*t++ == '\n') break;
		int l1 = t - in;    // skip \n
		l.assign(in, l1-1); // but don't include it in the line
		return l1;
	}


	void tabDelimited::parse(const char *in, int len) {
		_tabLineParser  P(*this);
		while (len > 0 && *in) {
			int l = _getline(in,  len, _line);
			P.parseLine(_line);
			consumeData(P.argc(), P.argv());
			++_lines_read;
			in += l;
			len -=l;
		}
	}


	int tabDelimited::parse(vector<tabDelimited::memoryFile> in) {
		int retval = 0;
		const unsigned int nStream = in.size();
		if(!nStream) return retval;

		argvT argv[nStream];
		argcT argc[nStream];

		vector<_tabLineParser *> P(nStream);
		for(unsigned int i =0; i < nStream; ++i) P[i] = new _tabLineParser(*this);

		while (1) {
			int eof = 0;
			for(unsigned int i=0; i < nStream; ++i) {
				int l = _getline(in[i].p, in[i].l, _line);
				P[i]->parseLine(_line);
				argv[i] = P[i]->argv();
				argc[i] = P[i]->argc();
				in[i].p  += l;
				in[i].l  -= l;
				if(!(in[i].l && *in[i].p)) {
					++eof;
				}
			}
			if(consumeData(nStream, argc, argv) ) {retval = -1; break; }
			++_lines_read;
			if(eof) break;
		}
		for(unsigned int i=0; i<nStream; ++i) delete P[i];
		return retval;
	}
} // NAMESPACE


