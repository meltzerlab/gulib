/*
 * ngsDnaTemplate.cc
 *
 *  Created on: Dec 13, 2013
 *      Author: sven
 */

#include "guli/ngsDnaTemplate.h"
#include <sstream>

static void _copySeq(bam1_t *segment, vector<uint8_t> &_seq, vector<uint8_t>  &_qual) {
	size_t _seqlen = segment->core.l_qseq;
	const size_t l = (1 + segment->core.l_qseq) >> 1;
	_seq.resize(l);

	_qual.resize(_seqlen);
	if(segment->core.flag & BAM_FREVERSE) {
		std::fill(_seq.begin(), _seq.end(), 0);
		for(size_t i=0; i < _seqlen; ++i) {
			_seq[i>>1] |= GULI::NGS::DNAtemplate::Read::_complement[bam1_seqi(bam1_seq(segment), _seqlen - i - 1)] << (i & 0x1 ? 0 : 4);
		}
		std::copy(bam1_qual(segment), bam1_qual(segment) + _seqlen, _qual.rbegin());
	} else {
		std::copy(bam1_seq(segment), bam1_seq(segment) + l, _seq.begin());
		std::copy(bam1_qual(segment), bam1_qual(segment) + _seqlen, _qual.begin());
	}
}

GULI::NGS::DNAtemplate::Read::~Read()    {
	for(size_t i = 0; i < _alns.size(); ++i) delete _alns[i];
}

GULI::NGS::DNAtemplate::Read::Read(const Read &R) : _seq(R._seq), _qual(R._qual), _keepSequence(R._keepSequence), _keepAux(R._keepAux) {
	_alns.resize(R._alns.size());
	for(size_t i=0; i < R._alns.size(); ++i) _alns[i] = new _alignmentDataT(*R._alns[i]);
}


GULI::NGS::DNAtemplate::Read::Read(bam1_t * segment, bool ks, bool ka)  : _keepSequence(ks), _keepAux(ka)   {
	_alns.push_back(new _alignmentDataT);
 	_alns[0]->reserve(1);
	_alns[0]->push_back(seg_core_t(segment, _keepAux));
	if(_keepSequence) {
		_copySeq(segment, _seq, _qual);
	}
}

static int segstart_qPos(const bam1_t *b) {
	int p = 0;
	uint32_t *c  = bam1_cigar(b);
	size_t i =0;
	while( i < b->core.n_cigar && (c[i] & BAM_CIGAR_MASK) != BAM_CMATCH && (c[i] & BAM_CIGAR_MASK) != BAM_CINS) {
		if( (c[i] & BAM_CIGAR_MASK) == BAM_CSOFT_CLIP) p = (c[i] >> BAM_CIGAR_SHIFT);
		++i;
	}
	if(b->core.flag & BAM_FREVERSE) {
		for( ;i < b->core.n_cigar; ++i) {
			if ((c[i] & BAM_CIGAR_MASK) == BAM_CMATCH)  p+= c[i] >> BAM_CIGAR_SHIFT;
			if ((c[i] & BAM_CIGAR_MASK) == BAM_CINS)    p+= c[i] >> BAM_CIGAR_SHIFT;
		}
		p = b->core.l_qseq - 1 - p;
	}
	if(b->core.flag & BAM_FREAD2)   p = -p;
	return p;
}


GULI::NGS::DNAtemplate::Read::Read(vector<vector<bam1_t *> >&segments, bool ks, bool ka)
: _keepSequence(ks), _keepAux(ka)   {
	if(!segments.size()) return; // no reads
	bool haveSeq = false;

	for(size_t i=0; i < segments.size(); ++i) {
		if(_keepSequence && segments[i].size() && !haveSeq) _copySeq(segments[i][0], _seq, _qual);

		_alns.push_back(new _alignmentDataT);
		if(segments[i].size() == 1) {
			_alns[i]->push_back(seg_core_t(segments[i][0], _keepAux));
			continue;
		}

		// this read alignment has multiple segments...., just figure out a consistent representation
		std::sort(segments[i].begin(), segments[i].end(),
				[](const bam1_t *a, const bam1_t *b) {  return segstart_qPos(a) < segstart_qPos(b); });


		for(size_t b=0; b < segments[i].size(); ++b) {
			_alns[i]->push_back(seg_core_t(segments[i][b], _keepAux));
		}
	}
	return;
}

GULI::NGS::DNAtemplate::Read::seg_core_t::seg_core_t(bam1_t *src, bool keepAux) {
	tid   = src->core.tid;
    pos   = src->core.pos;
	flag  = src->core.flag;
	bin   = src->core.bin;
	isize = src->core.isize;
	qual  = src->core.qual;
	cigar.resize(src->core.n_cigar);
	std::copy(bam1_cigar(src), bam1_cigar(src)+src->core.n_cigar, cigar.begin());
	if(keepAux) {
#ifndef USE_OLD_BAM
		const int32_t l_aux = bam_get_l_aux(src);
#else
		const int32_t l_aux = src->l_aux;
#endif
		auxData.resize(l_aux);
		std::copy(bam1_aux(src), bam1_aux(src) + l_aux, auxData.begin());
	}
	return;
}

void GULI::NGS::DNAtemplate::Read::_covers(bedRegion::listT &R, size_t al, size_t seg) const {
	if(segment(al,seg).flag & BAM_FUNMAP) return;
	bool valid = false;
	const seg_core_t &S = segment(al, seg);
	bedRegion tmp;
	tmp.start(S.pos);
	tmp.end(S.pos);
	if(S.flag & BAM_FREAD2)
		tmp.orient((S.flag & BAM_FREVERSE) ? '+' : '-');
	else
		tmp.orient((S.flag & BAM_FREVERSE) ? '-' : '+');

	const cigarT &cigar = segment(al, seg).cigar;
	for(size_t c=0; c < cigar.size(); ++c) {
		const size_t  l  = cigar[c] >> BAM_CIGAR_SHIFT;
		const int32_t op = cigar[c] &  BAM_CIGAR_MASK;
		switch(op) {
			case BAM_CMATCH: valid = true; tmp.end(tmp.end() + l); break;
			case BAM_CREF_SKIP:
			case BAM_CDEL:    {
					if(valid)R.push_back(tmp);
					tmp.start(tmp.end()+l);
					tmp.end(tmp.end()+l);
					valid = false;
			} break;

			default: break;
		}

	}
	if(valid) R.push_back(tmp);
	return;
}

bam1_t *GULI::NGS::DNAtemplate::Read::_bam( size_t s, size_t al, const char *id) const {
	if(s > _alns[al]->size()) return 0;
	const seg_core_t &c  = segment(al,s);
	bam1_t *b         = bam_init1();;
	b->core.tid       = c.tid;
	b->core.pos       = c.pos;
	b->core.bin       = c.bin;
	b->core.qual      = c.qual;
	b->core.l_qname   = strlen(id) + 1;
	b->core.flag      = c.flag;
	b->core.n_cigar   = c.cigar.size();
	b->core.l_qseq    = seqlen();
	b->core.isize     = c.isize;
	// setup DATA memory
	b->data_len       = 4 * c.cigar.size() +  b->core.l_qname + b->core.l_qseq + (b->core.l_qseq + 1) / 2 + c.auxData.size();
	b->m_data  = b->data_len;
	kroundup32(b->m_data);
	b->data = (uint8_t *)  realloc(b->data,  b->m_data);

	// fill DATA memory
	strcpy(bam1_qname(b), id);
	std::copy(c.cigar.begin(), c.cigar.end(), bam1_cigar(b));
	std::copy(c.auxData.begin(), c.auxData.end(), bam1_aux(b));
	if(_keepSequence) {
		if(b->core.flag & BAM_FREVERSE) {
			std::copy(_qual.rbegin(), _qual.rend(), bam1_qual(b));
			std::fill(bam1_seq(b), bam1_seq(b) + ((seqlen() + 1) >>1), 0);
			for(size_t i=0; i < seqlen(); ++i) bam1_seq(b)[i/2] |= (seqi(i, s, al) << ( (i  & 0x1) ? 0 : 4)) & 0xFF;
		} else {
			std::copy(_qual.begin(), _qual.end(), bam1_qual(b));
			std::copy(_seq.begin(), _seq.end(), bam1_seq(b));
		}
	}
	return b;
}

bam1_t *GULI::NGS::DNAtemplate::read(bool rd, size_t al, size_t s) const {
	const Read &cur = read(rd);
	size_t iLast = cur.nSegment(al) - 1;
	const Read::seg_core_t &p = (s==iLast) ? read(!rd).segment(al, 0) : read(rd).segment(al,s+1);
	bam1_t *r = cur._bam( s, al, _id.c_str());
	r->core.mtid = p.tid;
	r->core.mpos = p.pos;
	return r;
}

static std::pair<int, int> qregion(const bam1_t *b) {
	std::pair<int,int> R;
	const uint32_t *cigar = bam1_cigar(b);
	int p = 0;
	int start = -1, end = -1;
	for(size_t i=0; i < b->core.n_cigar; ++i) {
		const int32_t op  = cigar[i] & BAM_CIGAR_MASK;
		const int32_t len = cigar[i] >>  BAM_CIGAR_SHIFT;
		switch(op) {
			case BAM_CMATCH:  		if(start < 0) start = p;
							  	  	p += len;
							  	  	end = p;
							  	  	break;

			case BAM_CSOFT_CLIP:
			case BAM_CINS:         p += len; break;

			case BAM_CDEL:
			case BAM_CREF_SKIP:
			case BAM_CHARD_CLIP:
			case BAM_CPAD:         break;
		}
	}

	if(b->core.flag & BAM_FREVERSE) {
		R.first  = b->core.l_qseq - end - 1;
		R.second = b->core.l_qseq - start - 1;
	} else {
		R.first = start;
		R.second = end;
	}
	return R;
}

// check if two reads are plausibly a "dangling" pair of the same read
static bool plausibleDangler(const bam1_t *a, const bam1_t *b) {
	if((a->core.flag ^ b->core.flag) & (BAM_FREAD1 | BAM_FREAD2)) return false;
	std::pair<int,int> R1 = qregion(a);
	std::pair<int,int> R2 = qregion(b);
	int s = R1.first  >= R2.first  ? R1.first  : R2.first;
	int e = R1.second <= R2.second ? R1.second : R2.second;
	if(s-e > -4) return true;         // we allow at most 4 bases overlap..... Uiiiii, check this
	return false;
}

typedef struct workT{
	vector<bam1_t *> &alns;
	vector<vector<bam1_t *> > T1, T2;
	vector<int> idx;
	vector<int> NH, HI;
	vector<bool> used;

	workT(vector<bam1_t *> &a) : alns(a),  NH(alns.size()), HI(alns.size()), used(alns.size(), false) {
		for(size_t i=0; i < alns.size(); ++i) {
			uint8_t *NHt = bam_aux_get(alns[i], "NH");
			uint8_t *HIt = bam_aux_get(alns[i], "HI");
			NH[i]   = NHt ? bam_aux2i(NHt) : 1;
			HI[i]   = HIt ? bam_aux2i(HIt) : 1;
		}
	}


	bool dangler(int me) {
		if(alns[me]->core.flag & BAM_FUNMAP) return false;
		for(size_t j=0; j < alns.size(); ++j) {
			if(alns[j]->core.mpos != alns[me]->core.pos || alns[j]->core.pos != alns[me]->core.mpos) continue;
			if((alns[me]->core.flag ^ alns[j]->core.flag) & BAM_FSECONDARY) continue;
			if((alns[me]->core.flag ^ alns[j]->core.flag) & (BAM_FREAD1 | BAM_FREAD2)) return false;
		}
		return true;
	}


	void pairMe(int me) {
		if(dangler(me)) return;
		bool lonely = true;
		used[me] = true;
		vector<bam1_t *> v1(1), v2(1);
		v1[0] = alns[me];


		for(size_t j=0; lonely && j < alns.size(); ++j) {
			if(used[j]) continue;

			if(alns[me]->core.flag & BAM_FUNMAP) {
				// need to deal with the special case that the current read is not aligned
				// as we can not find the partner by searching for someone referencing us
				if(used[j] || !(alns[j]->core.flag & BAM_FMUNMAP)) continue;
				if(alns[me]->core.flag & alns[j]->core.flag & (BAM_FREAD1 | BAM_FREAD2))  continue; // same read
				v2[0] = alns[j];
				used[j] = true;
				lonely  = false;
			}

			if(alns[j]->core.flag &  BAM_FMUNMAP) continue; // FMUNMAP requires "me" is unaligned, this case is treated above

			if(alns[j]->core.flag & BAM_FUNMAP) {
				if(alns[me]->core.flag & BAM_FMUNMAP) {
					if(alns[me]->core.flag & alns[j]->core.flag & (BAM_FREAD1 | BAM_FREAD2))  continue; // same read
					v2[0]   = alns[j];
					used[j] = true;
					lonely  = false;
				};
				continue;
			}

			// we get to this point only if both partners are aligned,  we can finally trust positions
			if(NH[me] != NH[j]) continue;
			if(alns[me]->core.pos  != alns[j]->core.mpos || alns[me]->core.tid  != alns[j]->core.mtid) continue;
			if(alns[me]->core.mpos != alns[j]->core.pos  ||	alns[me]->core.mtid != alns[j]->core.tid) continue;
			if(!((alns[me]->core.flag ^ alns[j]->core.flag) & (BAM_FREAD1 | BAM_FREAD2))) continue;
			if((alns[me]->core.flag ^ alns[j]->core.flag) & BAM_FSECONDARY) continue;

			// We are on different reads AND we mutually point to each other, so
			// unless something else comes up, this is an ordinary mate-pair partner
			int mrPerfect = -1;
			if(HI[me] != HI[j]) {
					// MOST of the time STAR has HI==HI for the matches, but sometimes it does not
					// so check if in this instance the only possible pairing is with HI != HI
					// or  if there is a better match out there
					for(size_t k=0; k < alns.size(); ++k) {
						if(k==j || used[k] || NH[j] != NH[k] ) continue;
						if(alns[k]->core.flag & (BAM_FMUNMAP | BAM_FUNMAP )) continue ;
						if(alns[j]->core.mpos != alns[k]->core.pos  && alns[j]->core.mtid != alns[k]->core.tid)  continue;
						if(alns[j]->core.pos  != alns[k]->core.mpos && alns[j]->core.tid  != alns[k]->core.mtid) continue;
						if((alns[k]->core.flag ^ alns[j]->core.flag) & BAM_FSECONDARY) continue;
						if(((alns[j]->core.flag ^ alns[k]->core.flag) & BAM_FREAD1) && HI[j] == HI[k])  {
							mrPerfect = k;
							break;
						}
					}
			}
			if(mrPerfect != -1 && mrPerfect != me) continue;
			v2[0] = alns[j];
			used[j] = true;
			lonely = false;
		}
		if(lonely) assert(0); // We should ALWAYS find a partner....
		T1.push_back( (alns[me]->core.flag & BAM_FREAD1) ? v1 : v2);
		T2.push_back( (alns[me]->core.flag & BAM_FREAD1) ? v2 : v1);
		idx.push_back(me);
		return;
	}


	void dangleMe(size_t me) {
		size_t j;
		if(alns[me]->core.flag & (BAM_FUNMAP | BAM_FMUNMAP)) assert(0); // These should have been handled by now
		for(j=0; j < idx.size(); ++j) {
			if(NH[idx[j]] != NH[me]) continue;

			if(~(T1[j][0]->core.flag & BAM_FMUNMAP)) {
				if(alns[me]->core.mpos == T1[j][0]->core.pos && alns[me]->core.mtid == T1[j][0]->core.tid) {
					bool relink = alns[me]->core.pos == T1[j][0]->core.mpos && alns[me]->core.tid == T1[j][0]->core.mtid;
					bool mates  = (alns[me]->core.flag ^ T1[j][0]->core.flag) & BAM_FREAD1;
					bool fsec   = (alns[me]->core.flag ^ T1[j][0]->core.flag) & BAM_FSECONDARY;
					if( fsec || !(mates && relink)) {
						if(plausibleDangler(alns[me], mates ? T2[j][0] : T1[j][0])) break;
					}
				}
			}

			if(~(T2[j][0]->core.flag & BAM_FMUNMAP)) {
				if(alns[me]->core.mpos == T2[j][0]->core.pos && alns[me]->core.mtid == T2[j][0]->core.tid) {
					bool relink = alns[me]->core.pos == T2[j][0]->core.mpos && alns[me]->core.tid == T2[j][0]->core.mtid;
					bool mates  = (alns[me]->core.flag ^ T2[j][0]->core.flag) & BAM_FREAD1;
					bool fsec   = (alns[me]->core.flag ^ T2[j][0]->core.flag) & BAM_FSECONDARY;
					if(fsec || ! (mates && relink) ) {
						if(plausibleDangler(alns[me], mates ? T1[j][0] : T2[j][0])) break;
					}
				}
			}
		}
		if(j == idx.size()) assert(0); // Did not find dangling partner
		((alns[me]->core.flag ^ T1[j][0]->core.flag) & BAM_FREAD1) ? T2[j].push_back(alns[me]) : T1[j].push_back(alns[me]);
	}

} workT;

GULI::NGS::DNAtemplate::DNAtemplate(std::vector<bam1_t *> &alignments, bool keepSequence,  bool keepAuxFields) {
	if(alignments.size()) _id = bam1_qname(alignments[0]);
	if(alignments.size() == 2 && ((alignments[0]->core.flag ^  alignments[1]->core.flag) &  BAM_FREAD1)) {
		// Treat normal reads here thus avoiding the overhead for the more complex cases for the majority of reads
		vector<vector<bam1_t *> > T(1);
		T[0].resize(1);
		T[0][0]= (alignments[0]->core.flag & BAM_FREAD1) ? alignments[0] : alignments[1];
		_R[0] = new Read(T,  keepSequence, keepAuxFields);
		T[0][0]= (alignments[0]->core.flag & BAM_FREAD1) ? alignments[1] : alignments[0];
		_R[1] = new Read(T, keepSequence, keepAuxFields);
		return;
	}


	workT W(alignments);
	for(size_t i=0; i < alignments.size(); ++i) {
		if(W.used[i] || W.dangler(i)) continue;
		W.pairMe(i);
	}

	for(size_t i=0; i < alignments.size(); ++i) {
		if(W.used[i]) continue;
		W.dangleMe(i);
	}
	assert(W.T1.size() && W.T2.size());

	_R[0] = new Read(W.T1, keepSequence, keepAuxFields);
	_R[1] = new Read(W.T2, keepSequence, keepAuxFields);
}

GULI::NGS::DNAtemplate::DNAtemplate(bam1_t *b, bool keepSequence,  bool keepAuxFields) {
	 _id = bam1_qname(b);

	 vector<vector<bam1_t *> > T(1), E;
	Read *Empty = new Read(E,  false, false);

	T[0].resize(1);
	T[0][0] = b;

	if(b->core.flag & BAM_FREAD1) {
		_R[0] = new Read(T,  keepSequence, keepAuxFields);
		_R[1] = Empty;
	} else {
		_R[0] = Empty;
		_R[1] = new Read(T,  keepSequence, keepAuxFields);
	}
	return;
}

GULI::NGS::DNAtemplate::DNAtemplate(const DNAtemplate &D) : _id(D._id) {
	_R[0] = new Read(D.read(0));
	_R[1] = new Read(D.read(1));
}

GULI::NGS::DNAtemplate::~DNAtemplate() {
	delete _R[0];
	delete _R[1];
}

int GULI::NGS::DNAtemplate::covers(bool rd, size_t al, size_t s, char **chrIds, bedRegion::listT &R) const {
	int n = 0;
	R.clear();
	assert(al < nAlignment());
	assert(s < read(rd).nSegment(al));
	read(rd)._covers(R, al, s);
	for(size_t i=0; i < R.size(); ++i) {
		R[i].chromosome(chrIds[read(rd).segment(al,s).tid]);
		n += R[i].end() - R[i].start();
	}
	return n;
}

size_t GULI::NGS::DNAtemplate::nAlignment() const {
	size_t r = read(false)._nAL();
	if(r) return r;
	r = read(true)._nAL();
	return r;
}

const uint8_t GULI::NGS::DNAtemplate::Read::_complement[] =
	{ 15,  8,  4, 15,  2, 15, 15, 15,
	   1, 15, 15, 15, 15, 15, 15, 15 };
