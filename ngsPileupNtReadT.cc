/*
 * ngsPileupNtReadT.cc
 *
 *  Created on: Dec 12, 2012
 *      Author: sven
 */

#include <assert.h>
#include "guli/ngsBamUtils.h"
#include "guli/ngsBamPiler.h"
#include "guli/ngsPilupNtReadT.h"

static void unlock(const bam1_t *);
static const char *_readGroup(const bam1_t *);
static const bam1_t *lock(const bam1_t *b);

static const char nt_bam2asci[] = { '?', 'A', 'C', '?', 'G', '?', '?', '?',
									'T', '?', '?', '?', '?', '?', '?', 'N',
									'D', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I'
};

static const char nt_bam2asciL[] = { '?', 'a', 'c', '?', 'g', '?', '?', '?',
									't', '?', '?', '?', '?', '?', '?', 'n',
									'D', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'?', '?', '?', '?', '?', '?', '?', '?',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
									'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
};


namespace GULI {
	// the definition of these objects is at the end of the file for readability
	// so you get just a few forward declarations
	class  managementT;
	class  bamIntHash;

	namespace NGS {

		char ntRead_t::nt() const {
		  const char *x = strand() ? nt_bam2asciL : nt_bam2asci;
		  const int nt = bnt();
		  return x[nt];
		}

		unsigned short int ntRead_t::bnt() const {
			if(this->indel > 0)  return 64;
			if(this->is_del)	 return 16;
			return bam1_seqi(bam1_seq (this->b), this->qpos);
		}

		ntRead_t::~ntRead_t() {
			if(_managed) unlock(this->b);
		}

		ntRead_t &ntRead_t::operator=(const bam_pileup1_t &pl) {
			if(_managed) unlock(this->b);
			memcpy(static_cast<bam_pileup1_t *>(this), &pl, sizeof(bam_pileup1_t));
			this->b = _managed ? static_cast<bam1_t *>((void *) lock(pl.b)) : pl.b;
			return *this;
		}

		unsigned int  ntRead_t::apos() const {
			return   bam1_strand(this->b) ?
					 bam_calend(&this->b->core, bam1_cigar(this->b)) :
					 this->b->core.pos;
		}

		const char *ntRead_t::rg() const {
			return _readGroup(this->b);
		}

		int ntRead_t::rgTag() const {
			if(this->b->data_len < 4) return -3;
			if(this->b->data[b->data_len-4] != piler::RGIDX_TAG[0] || this->b->data[b->data_len-3] != piler::RGIDX_TAG[1]) return -2;
			uint8_t val = b->data[b->data_len -1];
			if(val >= piler::maxRgIdx) return -1;
			return val;
		}

	}

	class managementT {
		typedef GULI::NGS::BAMUTILS::bamAssoc<int> _storageT;
		typedef _storageT::iterator _it;
		_storageT _store;

	public:

		void unlock(const bam1_t *b) {
			if(!b) return;
#ifdef HAVE_THREADS
		//TODO: make thread safe
#endif
			_it f = _store.find(b);
			if(f != _store.end()) {
				if(--f->second <= 0) {
					bam1_t *b = static_cast<bam1_t *>( (void *) f->first); // Release bam1_t from its cage....
					_store.erase(f);
					if(f->second ==0) bam_destroy1(b);
				}
			}
			else {
				bool deletingAlreadyDeletedBam1Object = true;
				assert(!deletingAlreadyDeletedBam1Object);
			}
#ifdef HAVE_THREADS
		//TODO: make thread safe
#endif
			return;
		}

		const bam1_t *lock(const bam1_t *b) {
			if(!b) return 0; // nothing to manage
#ifdef HAVE_THREADS
		//TODO: make thread safe
#endif
			_it f = _store.find(b);
			if(f != _store.end()) {
				f->second++;
			}
			else {
				f = _store.insert(bam_dup1(b),1).first;
			}
#ifdef HAVE_THREADS
		//TODO: make thread safe
#endif
			return  f->first;
		}
	};


} // namespace GULI


static GULI::managementT universe;
// lock SHOULD return const bam1_t, but the (external) bam library coded in C requires bam_1t to be r/w in bam1_pileup
static const bam1_t *lock(const bam1_t *b) { return universe.lock(b); }
static void unlock(const bam1_t *b)  { universe.unlock(b); }

static const char *_readGroup(const bam1_t *b) {
	uint8_t *aux = bam_aux_get(b, "RG");
	return aux ? bam_aux2Z(aux) : 0;
}



