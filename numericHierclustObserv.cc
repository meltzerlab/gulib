/***************************************************************************
 numericHierclustObserv.cc  -  description
 -------------------
 begin                : Wed Apr 16 2003
 copyright            : (C) 2003-2009 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <assert.h>
#include "guli/stdinc.h"
#include "guli/dataIF.h"
#include "guli/dataSymmetric.h"
#include "guli/numericHierclustObserv.h"
#include "guli/numericHierclust.h"
namespace GULI {
	typedef unsigned long int pointer;
	typedef hierClust::node node;
	namespace hierClustTools {
		int depth(node *start) {
			class measDepth: public hierClust::visitor {
			public:
				virtual void *visit(node *n, void *leftRes, void *rightRes) {
					if (n->isLeaf()) return (void *) 1;
					int l = (pointer) leftRes;
					int r = (pointer) rightRes;
					int s = (r > l) ? r : l;
					return (void *) (s + 1);
				}
				int operator()(node *start) {
					return (pointer) start->visit(this);
				}
				virtual ~measDepth(){}
			};
			measDepth D;
			return D(start);
		}

		int countLeafs(node *start) {
			class countLeaf: public hierClust::visitor {
			public:
				virtual void *visit(node *n, void *, void *) {
					if (n->isLeaf()) _n++;
					return 0;
				}
				int operator()(node *start) {
					_n = 0;
					start->visit(this);
					return _n;
				}

				virtual ~countLeaf(){}
			private:
				int _n;
			};

			countLeaf C;
			return C(start);
		}

		int countNodes(node *start) {
			class countNodes: public hierClust::visitor {
			public:
				virtual ~countNodes(){}
				virtual void *visit(node *n, void *, void *) {
					_n++;
					return 0;
				}
				int operator()(node *start) {
					_n = 0;
					start->visit(this);
					return _n;
				}
			private:
				int _n;
			};

			countNodes C;
			return C(start);
		}

		void _drawTree(drawDevice &, node *, int, int, int);
		void drawTree(node *start, drawDevice &d) {
			const int maxdepth = -1;
			int h = depth(start);
			if (maxdepth > 0) h = (h > maxdepth) ? maxdepth : h;
			int l = countLeafs(start->leftChild());
			int r = countLeafs(start->rightChild());

			int my_x = 2 * l;
			int my_y = 1;

			d.start((l + r) * 2, h);
			d.drawNode(my_x, my_y);
			d.drawVerticalLine(my_x, 0, my_y);

			int x0 = my_x - l;
			int x1 = my_x + r;
			d.drawHorizontalLine(x0, x1, 1);

			_drawTree(d, start->leftChild(), x0, 1, h);
			_drawTree(d, start->rightChild(), x1, 1, h);

			d.end();
			return;
		}

		void _drawTree(drawDevice &d, hierClust::node *cur, int x,
				int parent_depth, int final_depth) {

			int my_depth = final_depth - depth(cur) + 1;

			d.drawVerticalLine(x, parent_depth, my_depth);

			if (cur->isLeaf()) {
				d.drawLeaf(x, my_depth, cur->label());
				return;
			} else {
				d.drawNode(x, my_depth);
			}

			int l = countLeafs(cur->leftChild());
			int r = countLeafs(cur->rightChild());

			int my_x = x + l - r;
			int x0 = my_x - l;
			int x1 = my_x + r;
			d.drawHorizontalLine(x0, x1, my_depth);

			_drawTree(d, cur->leftChild(), x0, my_depth, final_depth);
			_drawTree(d, cur->rightChild(), x1, my_depth, final_depth);
			return;
		}

		dataIF *calcDistMatrix(node *start) {
			class measureDistance {
			protected:
				class initUserVar: public hierClust::visitor {
				public:
					~initUserVar(){}
					virtual void *visit(node *n, void *, void *) {
						n->uservar() = _init;
						return 0;
					}
					void operator()(node *root) {
						_init = -1;
						root->visit(this);
					}

				private:
					int _init;
				};
			public:
				measureDistance() :
					_magic(0) {
				}

				dataIF *operator()(node *root) {
					initUserVar INIT;
					INIT(root);
					LEAFS(root);

					dataSymmetric &result = *new dataSymmetric(LEAFS.size());
					_result = &result;
					for (unsigned int i = 0; i < LEAFS.size(); i++) {
						_magic++;
						result.rowId(i) = LEAFS[i]->label();
						result.colId(i) = LEAFS[i]->label();
						_Visit(LEAFS[i], LEAFS[i], 0);
					}
					return _result;
				}

			private:
				void _Visit(hierClust::node *start, node *cur, int d) {
					dataIF &res = *_result;
					if (cur->isLeaf()) {
						res(LEAFS.getIndex(start->id()), LEAFS.getIndex(
								cur->id())) = static_cast<float> (d);
					}
					cur->uservar() = _magic;
					if (cur->leftChild() && (cur->leftChild()->uservar()
							< _magic)) _Visit(start, cur->leftChild(), d + 1);

					if (cur->rightChild() && (cur->rightChild()->uservar()
							< _magic)) _Visit(start, cur->rightChild(), d + 1);
					//				std::cerr << "Visiting "<< start->id() << " vs. " << cur->id() << std::endl;

					if (cur->parent() && (cur->parent() ->uservar() < _magic)) _Visit(
							start, cur->parent(), d + 1);
					return;
				}
				int _magic;
				dataIF *_result;
			};
			measureDistance M;
			return M(start);
		}

		/**  Calculate a matrix with density between any two _leafs_ in the tree
		 **/
		dataIF *calcDensMatrix(node *start) {
			class measureDensity {
			public:
				measureDensity() :
					_magic(-1) {
				}

				class initUserVar: public hierClust::visitor {

				public:
					~initUserVar(){}
					virtual void *visit(node *n, void *, void *) {
						n->uservar() = _init;
						_visited[_init] = -1;
						_leafs[_init] = countLeafs(n);
						_init++;
						return 0;
					}

					void operator()(node *root) {
						_init = 0;
						root->visit(this);
					}

					initUserVar(vector<int> &v, vector<int> &l) :
						_visited(v), _leafs(l) {
					}
				private:
					int _init;
					vector<int> &_visited, &_leafs;
				};

				dataIF *operator()(node *root) {
					int N = countLeafs(root);
					int NNODE = countNodes(root);
					_visited.resize(NNODE);
					_nleafs.resize(NNODE);
					initUserVar INIT(_visited, _nleafs);
					INIT(root);
					LEAFS(root);

					dataSymmetric &result = *new dataSymmetric(LEAFS.size());
					_result = &result;
					for (unsigned int i = 0; i < LEAFS.size(); i++) {
						result.rowId(i) = LEAFS[i]->label();
						result.colId(i) = LEAFS[i]->label();
						result(i, i) = 0.;
						_magic++;
						_markUpwards(LEAFS[i]);
						for (unsigned int j = i + 1; j < LEAFS.size(); j++) {
							node *common = _searchUpwards(LEAFS[j]);
							result(i, j) = (float) _nleafs[common->uservar()]
									/ (float) N;
							result(j, i) = result(i, j);
						}
					}
					return _result;
				}

				void _markUpwards(node *start) {
					_visited[start->uservar()] = _magic;
					if (start->parent()) _markUpwards(start->parent());
				}

				node *_searchUpwards(node *start) {
					if (_visited[start->uservar()] == _magic) return start;
					assert(start->parent() != 0);
					return _searchUpwards(start->parent());
				}

			private:
				int _magic;
				dataIF *_result;
				vector<int> _visited, _nleafs;
			};
			measureDensity M;
			return M(start);
		}

		leafFinder::leafFinder() {}

		typedef struct {
			bool operator()(const node *a, const node *b) {
				return a->id() < b->id();
			}
		} LESSNODE;

		void leafFinder::operator()(node *start) {
			class locateLeaf: public hierClust::visitor {
			public:
				virtual ~locateLeaf(){};
				virtual void *visit(node *n, void *, void *) {
					if (n->isLeaf()) _leafs.push_back(n);
					return 0;
				}
				int operator()(node *start) {
					return (pointer) start->visit(this);
				}
				locateLeaf(vector<node *> &l) :
					_leafs(l) {
					_leafs.clear();
				}
			private:
				vector<node *> &_leafs;
			};

			LESSNODE LESS;

			locateLeaf L(_leafs);
			L(start);
			std::sort(_leafs.begin(), _leafs.end(), LESS);

			_minidx
					= (*std::min_element(_leafs.begin(), _leafs.end(), LESS))->id();
			_maxidx
					= (*std::max_element(_leafs.begin(), _leafs.end(), LESS))->id();
			assert(_maxidx >= _minidx && _minidx >= 0);
			_reverseIndex.resize(_maxidx - _minidx + 1);
			std::fill(_reverseIndex.begin(), _reverseIndex.end(), -1);
			for (unsigned int i = 0; i < _leafs.size(); i++)
				_reverseIndex[_leafs[i]->id() - _minidx] = i;

			return;
		}

		unsigned int leafFinder::size() {
			return _leafs.size();
		}

		node *leafFinder::operator[](int n) {
			return _leafs[n];
		}

		int leafFinder::getIndex(int nodeindex) {
			assert(nodeindex >= _minidx && nodeindex <= _maxidx);
			int r = _reverseIndex[nodeindex - _minidx];
			assert(r >= 0);
			return r;
		}
		;

		leafFinder LEAFS;
	}
}

