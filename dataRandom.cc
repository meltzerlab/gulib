/***************************************************************************
 dataRandom.cc  -  description
 -------------------
 begin                : Mon Mar 29 2004
 copyright            : (C) 2004 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <gsl/gsl_randist.h>
#include "guli/stdinc.h"
#include <stdio.h>
#include "guli/dataRandom.h"

namespace GULI {
	dataRandom::dataRandom(int cols, int rows, double sigma, double av) :
		dataBasic(cols, rows) {
		gsl_rng_env_setup();
		ranT = gsl_rng_default;
		rangen = gsl_rng_alloc(ranT);
		randomize(sigma, av);
		_setLabel();
	}

	void dataRandom::_setLabel() {
		char buffer[1024];
		const int R = rows();
		for (int i = 0; i < R; i++) {
			sprintf(buffer, "row%d", i + 1);
			rowId(i) = buffer;
		}
		const int C = cols();
		for (int i = 0; i < C; i++) {
			sprintf(buffer, "col%d", i + 1);
			colId(i) = buffer;
		}
		return;
	}

	void dataRandom::randomize(double sigma, double av) {
		const int R = rows();
		const int C = cols();

		for (int r = 0; r < R; r++) {
			for (int c = 0; c < C; c++) {
				Data(c, r) = static_cast<float> (gsl_ran_gaussian_ratio_method(
						rangen, sigma) + av);
			}
		}
		return;
	}
} // Namespace
