/*
 * utilNeedlemanWunsch.cc
 *
 *  Created on: Mar 24, 2014
 *      Author: sven
 */
#include "guli/utilAligner.h"

static const char equal = '=';
static const char diff  = ':';
static const char del   = '-';
static const char ins   = '+';

static  int _simMatrix[5][5] = {
		// A  C  T  G  N
		{1, -2,  -2, -2, -2},  // A
		{-2,  1, -2, -2, -2},  // C
		{-2, -2,  1, -2, -2},  // T
		{-2, -2, -2,  1, -2},  // G
		{-2  -2, -2, -2,  0}   // N
};
static int _gapPenalty = -3;

namespace GULI {
	namespace HELPER {
		template<class valueType> class memarray {
		public:
			memarray(size_t cols, size_t rows) : _rows(rows), _cols(cols) {
				_mem = new valueType[cols * rows];
			}

			~memarray() { delete [] _mem; }

			valueType &operator()(size_t col, size_t row) {
				return _mem[col * _rows + row];
			}

			size_t cols() const { return _cols; }
			size_t rows() const { return _rows; }

		private:
			size_t _rows, _cols;
			valueType *_mem;
		};
	}

	namespace UTIL {
		class aligner::intarray : public GULI::HELPER::memarray<int32_t> {
		public:
			intarray(int32_t c, int32_t r) : GULI::HELPER::memarray<int32_t>(c,r) {}
		};

		class aligner::chararray : public GULI::HELPER::memarray<char> {
		public:
			chararray(int32_t c, int32_t r) : GULI::HELPER::memarray<char>(c,r) {}
		};

		aligner::~aligner() {
			if(_M) {
				delete _M;
				delete _P;
				delete [] _r;
			}
		}


	 void aligner::align(const sequence &ref, const sequence &seq, resultT &result, bool global) {
			enum {UP=0, LEFT=1, DIAG=3, STOP=4};
			const size_t R = ref.length();
			const size_t S = seq.length();

			if(!_M || _M->cols() < R+1 || _M->rows() < S+1) {
				 if(_M) {
					 delete _M;
					 delete _P;
					 delete [] _r;
				 }
				 _M = new intarray(R+1, S+1);
				 _P = new chararray(R+1, S+1);
				 _r = new char[R + S + R + S];
			}

			intarray &M(*_M);
			chararray &P(*_P);

			for(size_t i1=0; i1 <= R; ++i1) { M(i1, 0) = 0; P(i1,0) = STOP; }
			for(size_t i2=0; i2 <= S; ++i2) { M(0 ,i2) = 0; P(0,i2) = STOP; }

			size_t i1Max = 0;
			size_t i2Max = 0;
			int max = 0;

			for(size_t i1=1; i1 <= R; ++i1) {
				for(size_t i2=1; i2 <= S; ++i2) {
					int match = M(i1-1, i2-1) + _simMatrix[(int)  ref[i1-1]][(int) seq[i2-1]];
					int del   = M(i1-1, i2)   + _gapPenalty;
					int ins   = M(i1, i2-1)   + _gapPenalty;

					int score;
					char dir;
					if(match >= del) {
						dir = DIAG;
						score = match;
					} else  {
						dir = UP;
						score = del;
					}
					if(score < ins ) {
						dir = LEFT;
						score = ins;
					}
					M(i1,i2) = score;
					P(i1,i2) = dir;

					if(score > max) {
						max = score;
						i1Max = i1;
						i2Max = i2;
					}
				}
			}

			result.end1 = global ? R : i1Max;
			result.end2 = global ? S : i2Max;
			result.score1 = M(result.end1, result.end2);
			result.score2 = result.score1;
			// Backtrack
			size_t i1 = result.end1;
			size_t i2 = result.end2;

			char *r1 = _r;
			char *r2 = r1 + (R + S);
			size_t i = 0;

			while(P(i1,i2) != STOP) {
				if(P(i1,i2) == DIAG) {
					r1[i] = (ref[i1-1] == seq[i2-1] ? equal : diff) ;
					r2[i] = (ref[i1-1] == seq[i2-1] ? equal : diff);
					--i1;
					--i2;
				} else if (P(i1,i2) == UP) {
					r1[i] =  ins;
					r2[i] =  del;
					--i1;
				} else if (P(i1,i2) == LEFT) {
					r1[i] = del;
					r2[i] = ins;
					--i2;
				}
				++i;
			}

			size_t p1 = i;
			size_t p2 = i;
			if(global) {
				for( ; i1 > 0; --i1) { r1[p1++] = '-'; result.score1 += _gapPenalty; }
				for( ; i2 > 0; --i2) { r2[p2++] = '-'; result.score2 += _gapPenalty; }
			}
			result.start1 = i1;
			result.start2 = i2;


			result.cigar1.resize(p1);
			result.cigar2.resize(p1);
			std::copy(r1, r1+p1, result.cigar1.rbegin());
			std::copy(r2, r2+p2, result.cigar2.rbegin());
			if(!_keepMem) {
				 delete _M;
				 delete _P;
				 delete _r;
				 _M = 0;
			}
		 }
	} // namespace UTIL
} // NAMESPACE GULI

