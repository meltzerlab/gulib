/*
 * gffAlgo.cc
 *
 *  Created on: Oct 31, 2012
 *      Author: sven
 */

#include <regex.h>
#include <iostream>
#include <sstream>
#include "guli/gffDag.h"
#include "guli/dagIterator.h"
#include "guli/bedRegion.h"
#include "guli/bedTiling.h"
#include "guli/gffAlgo.h"

namespace GULI {


	void gffPingPong(gffDag::Node *start, gffNodeListT &same_level, gffNodeListT &feature_level, string feature_regex, bool dir) {
		typedef std::unordered_map<string, gffDag::Node *> idT ;
		idT lower_level, upper_level;

		if(dir) {
			lower_level[start->id()] = start;
		} else {
			upper_level[start->id()] = start;
		}
		bool running = false;
		while(1) {
			if(running || dir)  {
				bool foundNewUpper = false;
				for(idT::const_iterator i=lower_level.begin(); i != lower_level.end(); ++i) {
					gffNodeListT tr = gffFindFeatures(GFF_DOWN, i->second, feature_regex);
					for(size_t j=0; j < tr.size(); ++j) {
						if(upper_level.find(tr[j]->id()) == upper_level.end()) {
							upper_level[tr[j]->id()] = tr[j];
							foundNewUpper = true;
						}
					}
				}
				if(!foundNewUpper) break;
			}
			running = true;
			bool foundNewLower = false;
			for(idT::const_iterator i=upper_level.begin(); i != upper_level.end(); ++i) {
				gffNodeListT cd;
				gffFindFeatures(GFF_UP, cd, i->second, start->feature());
				for(size_t j=0; j < cd.size(); ++j) {
					if(lower_level.find(cd[j]->id()) == lower_level.end()) {
						lower_level[cd[j]->id()] = cd[j];
						foundNewLower = true;
					}
				}
			}
			if(!foundNewLower) break;
		}
		same_level.clear();
		feature_level.clear();
		if(dir) {
			same_level.reserve(lower_level.size());
			for(idT::const_iterator i=lower_level.begin(); i != lower_level.end(); ++i) {
				same_level.push_back(i->second);
			}

			feature_level.reserve(upper_level.size());
			for(idT::const_iterator i=upper_level.begin(); i != upper_level.end(); ++i) {
				feature_level.push_back(i->second);
			}
		} else {
			same_level.reserve(upper_level.size());
			for(idT::const_iterator i=upper_level.begin(); i != upper_level.end(); ++i) {
				same_level.push_back(i->second);
			}

			feature_level.reserve(lower_level.size());
			for(idT::const_iterator i=lower_level.begin(); i != lower_level.end(); ++i) {
				feature_level.push_back(i->second);
			}
		}
		return;
	}

	void gffAddNonOverlappingNodes(gffDag &D, const string featureSelection, string newFeatureType) {
		int serial = 0;
		typedef const constGffNodeListTiling tilingT;
		typedef const tilingT::tileListT tileListT;
		gffNodeListT feature;
		gffFindFeatures(GFF_DOWN, feature, D.root(), featureSelection);

		tilingT tiles(feature.begin(), feature.end());
		const tileListT &L = tiles.getTiling();

		for(size_t t=0; t < L.size(); ++t) {
			bedRegion outline(L[t]);
			std::stringstream ids;
			ids << newFeatureType << "_#" << ++serial;
			outline.id(ids.str());
			gffDag::Node *N = new gffDag::Node(outline, newFeatureType, "GULI", '.');
			D.addNode(N);

			// update ancestry
			Dag::Link Lchild(N, Dag::Link::OUTGOING);
			for(size_t j = 0; j < L[t].member().size(); ++j) {
				gffDag::Node *F = feature[L[t].member()[j]];
				F->addLink(Lchild);
				Dag::Link Lparent(*F, Dag::Link::INCOMING);
				N->addLink(Lparent);
			}
		}
		return;
	}

	template<> bedTiling<gffNodeListT::iterator>::bedAccess::operator const bedRegion*() { return *_i; }
	template<> const bedRegion *bedTiling<gffNodeListT::iterator>::bedAccess::operator->() { return static_cast<bedRegion*> (*_i); }
	template<> bedTiling<gffNodeListT::const_iterator>::bedAccess::operator const bedRegion*() { return *_i; }
	template<> const bedRegion *bedTiling<gffNodeListT::const_iterator>::bedAccess::operator->() { return static_cast<bedRegion*> (*_i); }
}
