/*
 * bgsBamPileup.cc
 *
 *  Created on: Aug 28, 2011
 *      Author: sven
 */
#include "guli/ngsBamPiler.h"
#include <limits>



struct _bamReaderDataS_ {
	size_t s;
	GULI::NGS::piler *object;
	_bamReaderDataS_() : s(0), object(0) {}
	_bamReaderDataS_(size_t S, GULI::NGS::piler *o) : s(S), object(o) {}
};

// This function reads a BAM alignment from one BAM file.
static int bamReader(void *data, bam1_t *b) {
	struct _bamReaderDataS_ *D = reinterpret_cast<struct _bamReaderDataS_ *>(data);
	return D->object->_bamReader(b,D);
}



namespace GULI {

	namespace NGS {

		const char *piler::RGIDX_TAG = "ZG";
		const uint8_t piler::maxRgIdx = 0x3F;

		piler::piler() :
			  _minalignquality(10),
			  _minbasequality(0),
			  _clipOverlap(false),
			  _rgEn(false),
			  _getref(false),
			  _collectStats(true),
			  _rmdup(false),
			  _pileupmask(BAM_DEF_MASK),
			  _maxinsert(600),
			  _maxdepth(_defaultMaxDepth),
			  _maxCntdStreamingGap(20000000),
			  _mplp(0),
			  _client(0),
			  _streamRegions(0),
			  _clipMagic(std::numeric_limits<_magicT>::max()),
			  _ucscGenome(0),
			  _ucscSeq(0),
			  _inMemChr(-1)
		{}

		int piler::attach(const vector<samfile_t *> &streams, const vector<string> &fn) {
			assert(streams.size() == fn.size());
			detach();
			_streams.resize(streams.size());
			for(size_t i=0; i < numberOfStreams(); ++i) {
				_streams[i].stream = 0;
				_streams[i].idx    = 0;
			}

			bool error = false;
			for(size_t i=0; i < numberOfStreams() && !error; ++i) {
				_perStreamDataT &D = _streams[i];
				D.stream = streams[i];
				if(D.idx) _detach(i);
				D.idx = bam_index_load(fn[i].c_str());
				for(int32_t c = 0; c < D.stream->header->n_targets; ++c) {
					D.chr[D.stream->header->target_name[c]] = c;
				}
				if(!D.idx) error = true;
			//TODO: update read-groups from the header, if the header contains RG information
			}
			if(!error) return 0;
			detach();
			_streams.resize(0);
			return -1;
		}

		void piler::_detach(size_t s) {
			assert((unsigned int) s < numberOfStreams());
			_perStreamDataT &D = _streams[s];
			if(D.idx) bam_index_destroy(D.idx);
			D.idx = 0;
			D.rgs.clear();
			D.rgStats.clear();
			D.chr.clear();
		}

		void  piler::stream(streamingClient *C, const bedTilingBase &R) {
			_streamRegions = &R;
			bedRegion::listT workplan;
			_strategize(R, workplan);

			_client = C;
			for(size_t i =0; i < workplan.size(); ++i) {
				bool warningIssued = false;
				_curBed = workplan[i];
				int32_t tid = _tid(_curBed.chromosome(), 0);
				if(tid < 0) continue;
				if(_readBam(tid, _curBed.start(), _curBed.end() - _curBed.start() + 1) && !warningIssued)  {
					std::cerr << "Warning::piler::stream: An error occurred while streaming the bam file in at least one region.\n";
					std::cerr << "Warning::piler::stream: Probably the region is not known across all bam-files.\n";
					std::cerr << "Warning::piler::stream: I keep going, but be warned (and check your derived object to capture this before it happens next time).";
					std::cerr << std::endl;
					warningIssued = true;
				}
			}
			if(_ucscSeq) {
				delete _ucscSeq;
				_ucscSeq = 0;
			}
			return;
		}

		void piler::_strategize(const bedTilingBase  &input, bedRegion::listT &strategy) {
			strategy.clear();
			const bedTilingBase::tileListT &tiles = input.getTiling();
			if(!tiles.size()) return;


			bedRegion cur = tiles[0];
			for(size_t i = 0; i < tiles.size(); ++i) {
				if(!cur.overlap(tiles[i])) {
					if(cur.chromosome() != tiles[i].chromosome() || tiles[i].start() - cur.end() > _maxCntdStreamingGap ) {
						strategy.push_back(cur);
						cur = tiles[i];
						continue;
					}
					if(tiles[i].end() > cur.end()) cur.end(tiles[i].end());
				}
			}
			strategy.push_back(cur);
			return;
		}

		const piler::readStatsT &piler::readStats( size_t s, const char *rg)  {
			static const readStatsT _error;
			assert(s < numberOfStreams());
			_perStreamDataT &D = _streams[s];
			if(!rg) return D.stats;
			int i = D.rgs.idIdx(rg);
			if(i < 0) return _error;
			return D.rgStats[i];
		}

		void  piler::clearStats() {
			for(size_t s=0; s < numberOfStreams(); ++s)  {
				_perStreamDataT &D = _streams[s];
				D.stats.clear();
				for(size_t i=0; i < D.rgStats.size(); ++i) D.rgStats[i].clear();
			}
		}

		void  piler::_collectStats_(const bam1_t *b, size_t streamno) {
			assert(streamno < numberOfStreams());
			int rg_idx = -1;
			_perStreamDataT &D = _streams[streamno];
			if(_rgEn) {
				uint8_t rg = b->data[b->data_len -1];
				if(rg <  maxRgIdx) {
					rg_idx = rg;
					++D.rgStats[rg_idx].reads;
				}
			}

			if( (b->core.flag & BAM_FPAIRED)
					&& (b->core.tid == b->core.mtid)
					&& (abs(b->core.isize) <= (int) _maxinsert)
			){
				++D.stats.pairs;
				D.stats.inserts += abs(b->core.isize);
				if(rg_idx >= 0) {
					++D.rgStats[rg_idx].pairs;
					++D.rgStats[rg_idx].inserts += abs(b->core.isize);
				}
			}

			const int ncigar = b->core.n_cigar;
			uint32_t *cigar = bam1_cigar(b);

			for(int j=0; j < ncigar; ++j) {
				uint32_t op = cigar[j] &  BAM_CIGAR_MASK;
				if(op == BAM_CMATCH || op ==  BAM_CINS) {
					const int nb = (cigar[j] & ~BAM_CIGAR_MASK) >> BAM_CIGAR_SHIFT;
					D.stats.bases += nb;
					if(rg_idx >= 0 ) D.rgStats[rg_idx].bases += nb;
				}
			}
		}

		static const uint32_t BAM_DUP_CHECKED =0x8000;

		void piler::_rmDup(size_t n_plp, const bam_pileup1_t *pl) {
			if(_pos5.size() < n_plp) {
				_pos5.resize(2 * n_plp); // get us a margin so we don;t have to re-allocate too often
			}
			for(size_t i=0; i < n_plp; ++i) {
				if(pl[i].b->core.flag & BAM_FREVERSE) _pos5[i] = bam_calend(&pl[i].b->core, bam1_cigar(pl[i].b));
			}


			for(size_t i=0; i < n_plp; ++i) {
				bam1_core_t &c1 = pl[i].b->core;
				if( (c1.flag & BAM_FDUP) || (c1.flag & (BAM_DUP_CHECKED | BAM_FREVERSE)) == BAM_DUP_CHECKED) {
					continue;
				}
				c1.flag |= BAM_DUP_CHECKED;

				for(size_t j=i+1; j < n_plp; ++j) {
					bam1_core_t &c2 = pl[j].b->core;
					if(c2.flag & BAM_DUP_CHECKED) continue;
					if(( c1.flag ^ c2.flag) & BAM_FREVERSE) continue;
					if( (c1.flag & BAM_FMUNMAP) || !(c1.flag & BAM_FPAIRED) || (c2.flag & BAM_FMUNMAP) || !(c2.flag & BAM_FPAIRED) ) {
						if(c2.flag & BAM_FREVERSE) {
							if(_pos5[i] != _pos5[j]) continue;
						}  else {
							if(c1.pos != c2.pos) continue;
						}
					} else {
						if( ((c1.flag ^ c2.flag) & BAM_FMREVERSE) || c1.isize != c2.isize ) continue;
						if(c1.flag & BAM_FREVERSE) {
							if(c1.mpos != c2.mpos) continue;
						} 	else  {
							if(c1.pos  != c2.pos)  continue;
						}
					}

					if(c1.qual < c2.qual) {
						c1.flag |= BAM_FDUP;
						break;
					}	else {
						c2.flag |= BAM_FDUP;
					}
				}
			}
		}

		void piler::_rmOverlap(size_t n_plp, const bam_pileup1_t *pl) {
			if(_clipped.size() < n_plp) {
				_clipMagic = std::numeric_limits<_magicT>::max() - 1;
				_clipped.resize(2 * n_plp); // get us a margin so we don;t have to re-allocate too often
			}
			if(++_clipMagic == std::numeric_limits<_magicT>::max()) {
				std::fill(_clipped.begin(), _clipped.end(), 0);
				_clipMagic = 1;
			}

			for(size_t i=0; i < n_plp; ++i) {
				bam1_core_t &c1 = pl[i].b->core;
				if(c1.flag & (BAM_FDUP | BAM_FMUNMAP)) continue;
				const char q1 = bam1_qual(pl[i].b)[pl[i].qpos];
				for(size_t j=i+1; j < n_plp; ++j) {
					bam1_core_t &c2 = pl[j].b->core;
					if(c2.flag & (BAM_FDUP | BAM_FMUNMAP)) continue;
					if(c2.mpos == c1.pos && c1.mpos == c2.mpos && !strcmp(bam1_qname(pl[i].b), bam1_qname(pl[j].b))) {
						if(q1 < bam1_qual(pl[j].b)[pl[j].qpos]) {
							_clipped[i] = _clipMagic;
						} else {
							_clipped[j] = _clipMagic;
						}
					}
				}
			}


		}

		void piler::_processPileup(uint32_t tid, uint32_t pos, size_t n_plp, const bam_pileup1_t *pl, size_t s) {
			assert(s < numberOfStreams());

			// get a buffer
			bool manage = false;
			pileup_t *plup = _client->pilerStreamingSetup(tid, pos,  s, manage);
			if(!plup) return; // client does not provide a buffer
			pileup_t &P(*plup);
			P.clear();
			if(!_streamRegions->doesOverlap(bedRegion(_streams[s].stream->header->target_name[tid], "", pos, pos+1))) return;

			if(rmDup()) _rmDup(n_plp, pl);
			if(clipOverlap()) _rmOverlap(n_plp, pl);

			for(size_t i=0; i < n_plp; ++i) {
				if(clipOverlap() && _clipped[i] == _clipMagic) {
					P._overlapReject++;
					continue;
				}

				const bam_pileup1_t &CUR = pl[i];
				if(rmDup() && (CUR.b->core.flag & BAM_FDUP)) {
					P._dupReject++;
					continue;
				}
				if(!_acceptableRead(CUR)) {P._clientReject++; continue;}
				if(!CUR.indel) {
					const char *q = reinterpret_cast<const char *>(bam1_qual(CUR.b));
					if(q[pl->qpos]< minBaseQuality()) {
						P._baseQualityReject++;
						continue;
					}
				}

				if(enableReferenceNt() && !P.reference()) {
					char nt;
					if(_ucscGenome) {
						if((int) tid != _inMemChr) {
							if(_ucscSeq) delete _ucscSeq;
							_inMemChr = tid;
							string chr = _streams[s].stream->header->target_name[tid];
							_ucscSeq = new sequence((*_ucscGenome)[chr]);
						}
						nt = pos < _ucscSeq->length() ? (*_ucscSeq)(pos) : 'N';
					} else {
						if(CUR.is_del) {
							// for deletions, qpos ALWAYS refers to the
							// beginning of the deletion, EVEN if we are somewhere in the middle of it
							// in the external (genomic) coordinate. Lets find the offset within the deletion
							int32_t offset = pos - BAMUTILS::q2chrPos(CUR);
							assert(offset >= 0);
							nt = BAMUTILS::mapBam2Ascii(BAMUTILS::referenceNT(CUR, offset));
						} else {
	#undef DEBUG_q2chrPOS
	#ifdef DEBUG_q2chrPOS
							int32_t offset = BAMUTILS::q2chrPos(CUR) - pos;
							if(offset) {
								std::cerr << "Issue " << pos <<  " " << CUR.qpos << " " << offset << " " << bam1_qname(CUR.b) << std::endl;
							}
							assert(!offset);
	#endif
							nt = BAMUTILS::mapBam2Ascii(BAMUTILS::referenceNT(CUR));
						}
					}
					P.reference( nt );
				}
//#define DEBUG_REFNT
#undef DEBUG_REFNT
#ifdef DEBUG_REFNT
				if(enableReferenceNt()) {
					char nt;
					if(CUR.is_del) {
						// for deletions, qpos ALWAYS refers to the
					    // beginning of the deletion, EVEN if we are somewhere in the middle of it
					    // in the external (pos) coordinate.
						int32_t offset = pos - BAMUTILS::q2chrPos(CUR);
						assert(offset >= 0);
						nt = BAMUTILS::mapBam2Ascii(BAMUTILS::referenceNT(CUR, offset));
					} else {
						nt = BAMUTILS::mapBam2Ascii(BAMUTILS::referenceNT(CUR));
					}

					if(nt != 'N') assert(P.reference() == nt);
				}
#endif

				++_streams[s].stats.reads;
				if(collectStats()) _collectStats_(CUR.b, s);
				ntRead_t d(CUR, manage);
				P.push_back(d);
			}
		}

		bool piler::_readBam(uint32_t rtid, uint32_t start, uint32_t len) {
			typedef const bam_pileup1_t *pileupPointer;
			typedef struct _bamReaderDataS_ *dataPointerT;
			int  tid, pos;
			bool error = false;
			const size_t ns = numberOfStreams();
			pileupPointer *pileup = new pileupPointer[ns];
			dataPointerT  *data = new dataPointerT[ns];
			int *n_plp = new int[ns];

			for(size_t s=0; s < ns; ++s) {
				tid = _tid(_chromosome(rtid, s), s);
				if(tid < 0) {
					error = true;
					for(size_t i=0; i < s; ++i) {
						bam_iter_destroy(_streams[i].iter);
						delete data[i];
					}
					goto error;
				}
				data[s] = new struct _bamReaderDataS_(s, this);
				_streams[s].iter = bam_iter_query(bamIndex(s), rtid, start, start + len);
			}

			_mplp = bam_mplp_init(ns, bamReader, (void **) data);
			bam_mplp_set_maxcnt(_mplp, _maxdepth);

			while (bam_mplp_auto(_mplp, &tid, &pos, n_plp,  pileup) > 0) {
				for(size_t s = 0; s < numberOfStreams(); ++s) {
					_processPileup(tid, pos, n_plp[s], pileup[s], s);
				}
				_client->positionStreamsComplete(tid, pos);
			}

			bam_mplp_destroy(_mplp);
			for(size_t s=0; s < ns; ++s) {
				bam_iter_destroy(_streams[s].iter);
				delete data[s];
			}
error:
			delete [] n_plp;
			delete [] data;
			delete [] pileup;
			return error;
		}

		int piler::_bamReader(bam1_t *b, _bamReaderDataS_ *D) {
			//TODO: implement local re-alignment
			int r;
			_perStreamDataT &cstream(_streams[D->s]);


			while(1) {
				uint8_t rgidx = maxRgIdx;
				r = bam_iter_read(cstream.stream->x.bam, cstream.iter, b);
				if(r < 0) break;

				if(_rgEn) {
					const char *rg =BAMUTILS::getStringTag(b, "RG");
					if(rg) {
						bool created;
						rgidx  = cstream.rgs.idIdx(rg, true, created);
						if(created) {
							readStatsT S;
							cstream.rgStats.push_back(S);
						}
						if(rgidx >= maxRgIdx) {
							std::cerr << "ngsBamPileup::warning: found more than the max supported " << maxRgIdx << " read-groups." << std::endl;
							assert(0==1);
						}
					}
					bam_aux_append(b, RGIDX_TAG, 'i', 1, &rgidx); // this is slow but where else could we keep this in a bam object?
						                                          // it is probably a little dangerous, but we should be able to extract
						                                          // this information as (uint8_t) b->data[b->data_len -1].
					// TODO: Remove next line when you are sure that the below shortcut (see debug1 variable) works.
					//       Re-test this for every update of the BAM library
					//uint8_t debug1 = b->data[b->data_len -1];
					//uint8_t *debug = bam_aux_get(b, RGIDX_TAG);
					//assert(debug && *(debug+1) == debug1);
				}

				if( (b->core.flag  & mask())) {
					++cstream.stats.rejectMask;
					if(rgidx < maxRgIdx) ++cstream.rgStats[rgidx].rejectMask;
					continue;
				}

				if(b->core.qual < minAlignQuality() )  {
					++cstream.stats.rejectAlignQuality;
					if(rgidx < maxRgIdx) ++cstream.rgStats[rgidx].rejectAlignQuality;
					continue;
				}


				//TODO: REMOVE next line. It is a HACK to avoid a bug in a different of Sven's codes.
				if (b->core.n_cigar && b->core.l_qseq != (int32_t)bam_cigar2qlen(&b->core, bam1_cigar(b))) continue; //
				break;
			}
			return r;
		}

		const char *piler::_chromosome(uint32_t tid, size_t stream) { return _streams[stream].stream->header->target_name[tid];}

		int32_t piler::_tid(const char *chromosome, size_t stream) {
			_chrMapT &chr(_streams[stream].chr);
			_chrMapT::iterator it = chr.find(chromosome);
			if(it == chr.end()) return -1;
			return it->second;
		}

	}  // namespace NGS
} // NAMESPACE GULI


