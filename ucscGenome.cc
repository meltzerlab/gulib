/***************************************************************************
 *   Copyright (C) 2006 by Sven Bilke                                           *
 *   bilkes@mail.nih.gov                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "dirent.h"
#include "guli/ucscGenome.h"

ucscGenome::ucscGenome() :  _curopen(-1), _error(false) {
    _base = _defaultBase;
}

ucscGenome::ucscGenome(const string &g) :  _curopen(-1), _error(false) {
    _base = _defaultBase;
    select(g);
}

ucscGenome::~ucscGenome() {
    if (_curopen >= 0) _CHRSTREAM.close();
}

int ucscGenome::select(const string &dir) {
	struct dirent *pDirent;
	DIR *pDir;
	_index.clear();
	_knownChromosomes.clear();
	_curDir = "";
	string fdir = _base + "/" + dir;
	pDir = opendir (fdir.c_str());
	if (pDir == NULL) {
		std::cerr << "Can not open directory " << fdir << std::endl;
		return -1;
	}

	_curDir = dir;
	int idx = 0;
	while ((pDirent = readdir(pDir)) != NULL) {
		string fnam = pDirent->d_name;
		if(fnam.length() < 3) continue;
		if(fnam.substr(fnam.length() - 3) != ".fa") continue;
		_chrInfoT info;
		ifstream CHR;
		string fqn = fdir + "/" + fnam;
		CHR.open(fqn.c_str(), std::ios_base::in);
		info.id = fnam.substr(0, fnam.length()-3);
        if (!CHR.good() || CHR.eof() || !CHR.is_open()) {
            info.chrlen = 0;
            info.linelen = 1;
            info.offset = 0;
            std::cerr << "Warning: could not open chromosome " << fqn << std::endl;
        } else {
            string line;
            CHR.seekg(0, std::ios_base::beg);
            std::getline(CHR, line); // FASTA-HEADER
            info.offset = CHR.tellg();
            std::getline(CHR, line); // Get length of DATA line in FASTA file
            info.linelen = line.length(); // Count CR as well
            CHR.seekg(0, std::ios_base::end);
            int rawlen = static_cast<int>(CHR.tellg()) - info.offset;
            info.chrlen = rawlen - rawlen / (info.linelen + 1); // remove CR
        }
	    if(CHR.is_open()) CHR.close();
	    _index[info.id] = idx++;
	    _knownChromosomes.push_back(info);
	}
	closedir (pDir);
	return 0;
}

string ucscGenome::getSeq(unsigned int chr, unsigned int pos, unsigned int len) const {
    string retval, line;
    _error = true;
    if (chr != _curopen) {
        if (_curopen >= 0) _CHRSTREAM.close();
        _curopen = -1;
        const std::string fn = fnam(chr);
        _CHRSTREAM.open(fn.c_str(), std::ios_base::in);
        if ( (!_CHRSTREAM.good()) || _CHRSTREAM.eof() || !_CHRSTREAM.is_open()) {
        	return "ERROR:OPEN_FILE";
        }
        _curopen = chr;
    }
    const _chrInfoT &I = _knownChromosomes[chr];
    int p = I.offset + pos + pos / I.linelen; // ADD CR's
    _CHRSTREAM.seekg(p, std::ios_base::beg);
    if(_CHRSTREAM.fail()) {
    	return "ERROR:SEEK";
    }

    while (!(_CHRSTREAM.eof() | _CHRSTREAM.fail()) ) {
        std::getline(_CHRSTREAM, line);
        retval += line;
        if (retval.length() >= len) {
        	_error = false;
            return retval.substr(0, len);
        }
    }
    if(!_CHRSTREAM.fail()) _error = false;
    return retval;
}

int ucscGenome::chrLength(unsigned int i) const {
    assert(i < nChromosomes());
    return _knownChromosomes[i].chrlen;
}

string ucscGenome::fnam(unsigned int i) const {
     string r = _base + "/" + _curDir + "/" + _knownChromosomes[i].id + ".fa";
    return r;
}

int ucscGenome::chrIndex(string chr) const {
//	if(chr.length() > 3 && chr.substr(0,3) == "chr") chr = chr.substr(3, string::npos);
    std::map<string,int>::const_iterator f = _index.find(chr);
    if (f == _index.end()) return -1;
    return f->second;
}

string ucscGenome::chrId(unsigned int i) const {
    if (i >= nChromosomes() ) return "UNKNOWN";
    return _knownChromosomes[i].id;
}
