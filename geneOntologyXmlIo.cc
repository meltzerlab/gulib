/***************************************************************************
 ontologyXmlIO.cc  -  description
 -------------------
 begin                : Wed Apr 16 2003
 copyright            : (C) 2003-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2003-2009 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "guli/stdinc.h"
#include "guli/geneOntologyXmlIo.h"
namespace GULI {
	geneOntology::textual *ontologyXmlIO::read(const char *filename) {
		_target = new geneOntology::textual;
		if (!parse(filename)) {
			std::cerr << "Warning: error reading Ontology file" << std::endl;
			delete _target;
			return 0;
		}
		return _target;
	}

	bool ontologyXmlIO::_eatToken(tokentype &token) {
		if ((token.name != "go:term") && (token.name != "sb:term")) return false;

		geneOntology::textualNode tnode;

		for (unsigned int i = 0; i < token.Tokens.size(); i++) {
			string & name = token.Tokens[i].name;
			if (name.length() < 3) continue;
			string domain = name.substr(0, 3);
			if (!((domain == "go:") || (domain == "sb:"))) continue;
			string key = name.substr(3, name.length() - 3);
			for (int j = 0; Keys[j].method; j++) {
				if (!strcmp(key.c_str(), Keys[j].key)) {
					(this->*Keys[j].method)(tnode, token.Tokens[i]);
					break;
				}
			}
		}
		if (tnode.name == "" || tnode.identifier == "") {
			std::cerr << "Encountered ontology with missing name or id. ";
			std::cerr << "Ontology ignored" << std::endl;
			//	    for(unsigned int i=0; i < token.Tokens.size(); i++) {
			//		string &name = token.Tokens[i].name;
			//		if(name.length() < 3) continue;
			//		std::cerr << " Key " << i << name << ": "
			//                        << token.Tokens[i].text << std::endl;
			//	    }
			return true;
		}
		_target->push_back(tnode);
		return true;
	}

	void ontologyXmlIO::_process_relation(geneOntology::textualNode &t, xmlGeneric::tokentype &to) {
		static const string rootAlias = "#all";
		for (unsigned int i = 0; i < to.Attribute.size(); i++) {
			if (to.Attribute[i].name == "rdf:resource") {
				string target = to.Attribute[i].value;
				if (target.find("#obsolete") != string::npos) {
					target = geneOntology::obsoleteId;
				} else {
					// They have finally introduced a root-node.... Sorry for the work-around
					if (target.substr(target.length() - 4, 4) == rootAlias) {
						target = geneOntology::rootId;
					} else {
						target = target.substr(target.length() - 10, 10);
						if (to.name.length() < 3) continue;
					}
				}
				string type = to.name.substr(3, to.name.length() - 3);

				if (type == "is_a") {
					t.isa.push_back(target);
					return;
				}
				if (type == "part_of") {
					t.partof.push_back(target);
					return;
				}
				if (type == "contains") {
					t.contains.push_back(target);
					return;
				}
			}
		}
		std::cerr << "Did not find \"rdf:resource\" in isa/partof/contains"
				<< std::endl;
	}

	const ontologyXmlIO::keyword ontologyXmlIO::Keys[] = {
			{ "is_a",      &ontologyXmlIO::_process_relation },
			{ "part_of",   &ontologyXmlIO::_process_relation },
			{ "contains",  &ontologyXmlIO::_process_relation },
			{ "accession", &ontologyXmlIO::_process_accession },
			{ "name",      &ontologyXmlIO::_process_name },
			{ "definition",&ontologyXmlIO::_process_description },
			{ "",          0 }
	};

/* Some leftovers from Debug */
/*
 using std::cout;
 using std::endl;
 static void print_node(textualNode &t) {
 cout << t.identifier << " " << t.name << endl;;
 cout << "   Descr:  " << t.description << endl;
 cout << "      isa ";
 for(unsigned int i=0; i < t.isa.size(); i++)
 cout << " " << t.isa[i];
 cout << endl;
 cout << "     part ";
 for(unsigned int i=0; i < t.partof.size(); i++)
 cout << " " << t.partof[i];
 cout << endl;
 cout << "     contains ";
 for(unsigned int i=0; i < t.contains.size(); i++)
 cout << " " << t.contains[i];
 cout << endl;
 cout << "-------------------------------------------------" << endl;
 }
 */
} // NAMESPACE
