/*
 * JsonCommandInterpreter.cpp
 *
 *  Created on: Mar 4, 2015
 *      Author: sven
 */
#ifdef HAVE_WT

#include <fstream>
#include <set>
#include <strstream>
#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/Session.h>
#include <Wt/Dbo/Field.h>
#include <Wt/Dbo/WtSqlTraits.h>
#include <Wt/Json/Object.h>
#include <Wt/Json/Parser.h>
#include "guli/dboJsonCommandInterpreter.h"

namespace GULI {
	namespace Dbo {

		const std::string jsonCommandInterpreter::whoAmI = "JsonCommandInterpreter:: ";

		void jsonCommandInterpreter::update(const Wt::Json::Object &arg) const { assert(0);}

		void jsonCommandInterpreter::remove(const Wt::Json::Object &arg) const { assert(0);}

		void jsonCommandInterpreter::include(const Wt::Json::Object &arg) const {
			const std::string FILENAMEKEY = "file";
			std::string IAm = whoAmI + "include:";

			auto _include = [IAm, this](const std::string &fn) {
				std::ifstream IN(fn);
				if(!IN)
					throw Wt::Json::ParseError(IAm + " can not open include file " + fn);
				this->stream(IN);
				IN.close();
				return;
			};

			if(arg.contains(FILENAMEKEY)) {
				switch(arg.type(FILENAMEKEY)) {
					case Wt::Json::Type::String: {
						_include(arg.get(FILENAMEKEY).toString());
					} break;

					case Wt::Json::Type::Array: {
						const Wt::Json::Array &A = arg.get(FILENAMEKEY);
						for(size_t i=0; i < A.size(); ++i) {
							if(A[i].type() == Wt::Json::Type::String) {
								_include(A[i].toString());
							} else {
								throw Wt::Json::ParseError(whoAmI + "ill-formed string array." );
							}
						}
					} break;

					default:
						throw Wt::Json::ParseError(IAm + " Ill formed argument FILENAMEKEY" );
				}
			} else {
				throw Wt::Json::ParseError(IAm + " Missing argument '" + FILENAMEKEY );
			}
		}

		void jsonCommandInterpreter::add(const Wt::Json::Object &arg) const {
			std::string IAm = whoAmI + "add:";
			if(arg.names().size() != 1) {
				std::string names;
				for(auto n : arg.names()) { names += n + " "; }
				throw Wt::Json::ParseError(IAm + " ill-formed argument " + names);
			}
			const std::string what = *arg.names().begin();
			if(_tables.find(what) == _tables.end())
				throw Wt::Json::ParseError(IAm + " unknown table " + what);

			switch (arg.type(what)) {

			case Wt::Json::Type::Object:
				_tables.find(what)->second->instantiate(arg.get(what), _S, _modeT::CREATE);
				break;

			case Wt::Json::Type::Array: {
				const Wt::Json::Array &A = arg.get(what);
				for(size_t i=0; i < A.size(); ++i) {
					if(A[i].type() == Wt::Json::Type::Object) {
						_tables.find(what)->second->instantiate(A[i], _S, _modeT::CREATE);
					} else {
						throw Wt::Json::ParseError(whoAmI + "ill-formed object array while analyzing argument for table " + what);
					}
				}
			} break;

			default:
				throw Wt::Json::ParseError(whoAmI + "ill-formed object array while analyzing argument for table " + what);
				break;
			}
		}


	void jsonCommandInterpreter::parseObjectString(const std::string &json) const {
			Wt::Json::Object cmdO;
			Wt::Json::ParseError  E;

			if(!Wt::Json::parse(json, cmdO ,E , true)) throw E;
			const std::set<std::string> &names = cmdO.names();

			for(std::string cmd : names){
				if(_commands.find(cmd) == _commands.end()) {
					throw Wt::Json::ParseError(whoAmI + "command not found: " + cmd);
				}

				_serviceRoutine exec = _commands.find(cmd)->second;;

				switch(cmdO.type(cmd)) {
					case Wt::Json::Type::Object: {
						(this->*exec)(cmdO.get(cmd));
						break;
					}
					case Wt::Json::Type::Array: {
						const Wt::Json::Array &A = cmdO.get(cmd);
						for(size_t i=0; i < A.size(); ++i) {
							if(A[i].type() == Wt::Json::Type::Object) {
								(this->*exec)(A[i]);
							} else {
								throw Wt::Json::ParseError(whoAmI + "ill-formed object array while analyzing argument of " + cmd);
							}
						}
					} break;

					default: throw Wt::Json::ParseError(whoAmI + "ill-formed argument for " + cmd);
				}
			}
			return;
		}

	std::istream &jsonCommandInterpreter::stream(std::istream &IN) const {
		while(! (IN.eof() || IN.bad())) {
			std::string buffer;
			bool done = false;
			int openBraces = 0;
			for(std::istreambuf_iterator<char> I(IN); I != std::istreambuf_iterator<char>() && !done; ++I) {
				buffer.push_back(*I);
				if(*I == '{') ++openBraces;
				else if(*I == '}') {
					--openBraces;
					if(openBraces <=0) done = true;
				}
			}

			if(openBraces < 0)
				throw Wt::Json::ParseError(whoAmI + "Unbalanced curly braces");

			if(buffer.find_first_of("{") == std::string::npos) break;
			parseObjectString(buffer);
		}
		return IN;
	}


		const std::map<std::string, jsonCommandInterpreter::_serviceRoutine> jsonCommandInterpreter::_commands = {
				{"add", 	&jsonCommandInterpreter::add},
				{"remove",	&jsonCommandInterpreter::remove},
				{"update",	&jsonCommandInterpreter::update},
				{"include",	&jsonCommandInterpreter::include},
		};

	}
}
#endif

