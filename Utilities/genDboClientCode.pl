#!/usr/bin/perl -w
####################################################################
# Parse an XML File and build a "kind of" DOM Tree. The major difference
# with respect to the DOM is that you can issue a "commit" argument, so that
# every time we see a </COMMIT> in the XML stream we invoke a client routine
# in this way you don't have to load the full document into memory.
# This is _only_ usefull if you have very many of the <COMIT> tokens in the
# data stream, because all objects LOWER in the hierarchy get loaded into
# Memory.
#
# The code was designed to parse the ontology XML File. So, if you happen to find
# references to Ontology, don't be confused. The code is (can be used) more generic
##############################################################################
my $CLASSID = "parseXml";

package parseXml;
use strict;
use Data::Dumper;
use XML::Parser::PerlSAX;

sub parseXml {
######################################################################
 # The main entry point. This parses a XML file and calls a "client: routine
 # upon encountering the :</$COMMIT> end-token. BEWARE: The rest of the file,
 # i.e. those parts OUTSIDE <$COMMIT> .... </COMMIT> gets silently discarded
 # AND (in the current implementation) loaded into memore temporarily ! This may
 # be HUGE !
 # First argument: FILENAME
 # second argument: $COMMIT,
 # third argument: the client OBJECT
######################################################################
	my $filename    = $_[0];
	my $commitToken = "DATABASE";
	$commitToken = $_[1] if ( defined( $_[1] ) );
	my %self;
	$self{"commit"} = $commitToken;
	my $this   = \%self;
	my $client = $this;
	$client = $_[2] if defined( $_[2] );
	$this->{"client"} = $client;

	bless( $this, $CLASSID );
	$this->{"_stack"} = [];
	$this->{_FILENAME} = $filename;

	my $parser = XML::Parser::PerlSAX->new(
		"ErrorHandler" => sub {
			print STDERR "An error occured while parsing XML file\n";
			die "Giving up";
		},
		"Handler" => $this
	);

	open INPUT, $filename;
	$parser->parse( Source => { "ByteStream" => *INPUT } );
	close INPUT;
	return;
}

sub start_element {
	my @empty;
	my $this  = shift;
	my $stack = $this->{"_stack"};
	$_[0]->{"Data"}   = "";
	$_[0]->{"fields"} = \@empty;
	push( @{$stack}, $_[0] );
	return;
}

sub end_element {
	my $this  = shift;
	my $stack = $this->{"_stack"};
	my $n     = $#{$stack};
	my $r     = $stack->[ $#{$stack} ];
	splice( @{$stack}, $n, 1 );
	print "Uups. END tag is different from start-Tag ?\n"
	  if ( !$r->{"Name"} eq $_[0]->{"Name"} );
	if ( $r->{"Name"} eq "DATABASE" ) {
		$this->{"client"}->consumeXMLdatabase($r);
	}
	else {
		push( @{ $stack->[ $n - 1 ]->{"fields"} }, $r ) if ( $n > 0 );
	}
	return;
}

sub characters {
	my $this  = shift;
	my $stack = $this->{"_stack"};
	$stack->[ $#{$stack} ]->{"Data"} .= $_[0]->{"Data"};
	return;
}

#**************************************************************************/
#* Generate Source code and header files from the XML description         */
#**************************************************************************/

#IO streams and current indentation level
my ( $sindent, $source );
my ( $hindent, $header );

sub consumeXMLdatabase {
	my $this   = shift;
	my $tree   = $_[0];
	my $dbname = $$tree{Attributes}{name};
	open $source, ">$dbname.cc";
	$sindent = 0;
	&genSourceFile($tree);
	close $source;

	# fill in the gloabl include file
	open $header, ">" . $dbname . ".h";
	$hindent = 0;
	&genIncludeFile($tree);
	close $header;
}

# A bunch of methods to extract frequently used features form the XML tree
my $_field = sub {
	my $f     = shift;
	my $field = shift;
	die "Missing \"$field\" attribute in database description " . Dumper($f)
	  if ( ( !defined $$f{Attributes} )
		|| ( !defined $$f{Attributes}{$field} ) );
	return $$f{Attributes}{$field};
};

sub update {
	return &{$_field}( $_[0], "update" );
}

sub  type {
	return &{$_field}( $_[0], "type" );
}

sub function {
	return &{$_field}( $_[0], "function" );
}

sub initialize {
	return undef
	  if (
		!(
			defined( $_[0]{Attributes} ) && defined( $_[0]{Attributes}{initialize} )
		)
	  );
	return $_[0]{Attributes}{initialize};
}

sub invalidId {
	return &{$_field}( $_[0], "invalidId" );
}

sub linker {
	return undef
	  if (
		!(
			defined( $_[0]{Attributes} ) && defined( $_[0]{Attributes}{linker} )
		)
	  );
	return $_[0]{Attributes}{linker};
}

sub references {
	return ""
	  if (
		!(
			   defined( $_[0]{Attributes} )
			&& defined( $_[0]{Attributes}{references} )
		)
	  );
	return &{$_field}( $_[0], "references" );
}

sub size {
	return -1
	  if (
		!( defined( $_[0]{Attributes} ) && defined( $_[0]{Attributes}{size} ) )
	  );
	return $_[0]{Attributes}{size};
}

sub primaryKey {
	my $this = shift;
	return 0
	  if (
		!(
			   defined( $$this{Attributes} )
			&& defined( $$this{Attributes}{primaryKey} )
		)
	  );
	return &{$_field}( $this, "size" );

}

sub isKey {
	my $this = shift;
	return 1
	  if ( defined( $$this{Attributes} )
		&& defined( $$this{Attributes}{key} ) );
	return &primaryKey($this);
}

sub name {
	return $_[0]->{Name};
}


sub printFileHeader {
	my $OSTREAM = shift;
	my $dbXML   = shift;
	my $dbname  = shift;
	my $table   = shift;

	print $OSTREAM "/********************************************/\n";
	print $OSTREAM "/* Autogenerated file, do not edit. 	    */\n";
	print $OSTREAM "/********************************************/\n";
	if ( defined $dbname ) {
		my $key = $dbname;
		$key .= "_" . $table if ( defined $table );
		print $OSTREAM "#ifndef __DATABASE_" . $key . "__\n";
		print $OSTREAM "#define __DATABASE_" . $key . "__\n";
	}
	print $OSTREAM "#include <climits>\n";
	if ( defined $dbXML ) {
		foreach my $f ( @{ $$dbXML{fields} } ) {
			next if ( &type($f) ne "HEADER" );
			my $txt = $$f{Data};
			$txt =~ s/\n\s*/\n/g;
			$txt =~ s/^\s*//;
			print $OSTREAM $txt . "\n";
		}
	}
}

sub sprint {
	my $text = shift;
	my @line = split "\n", $text;
	foreach (@line) {
		print $source "\t" x $sindent if ($sindent);
		print $source $_ . "\n";
	}
}

sub hprint {
	my $text = shift;
	my @line = split "\n", $text;
	foreach (@line) {
		print $header "\t" x $hindent if ($hindent);
		print $header $_ . "\n";
	}
}

my $initialize = {
	"std::string" => "\"_NULL_\"",
	"float"       => "1e30",
	"double"      => "1e30",
	"int"         => "INT_MIN",
	"long long"	  => "LLONG_MIN"
};

sub genClassInitializer {
	my %enumTypes;
	my $dsc = shift;
	my $d   = $$dsc{fields};
	my $id  = $$dsc{Name};
	my $buf = $id . "::" . $id . "()";
	my $lc  = 0;
	
	foreach my $e ( @{$d} ) {
		next if ( &function($e) ne "ENUM" );
		next if(!defined ($$e{Attributes}{default}));
		$enumTypes{&name($e)."T"} = $$e{Attributes}{default};
	}
		
	foreach my $e ( @{$d} ) {
		next if ( &function($e) ne "FIELD" );
		my $init = &initialize($e);
		$init = $$initialize{ &type($e) } if(!defined $init);

		if ( (defined $init ) || defined $enumTypes{&type($e)} ) {
			if ( !$lc++ ) {
				$buf .= ":";
				sprint($buf);
				++$sindent;
			}
			else {
				$buf .= ",";
				sprint($buf);
			}
			my $init = (defined $init) ? $init : $enumTypes{&type($e)}; 
			$buf = '_' . &name($e) . "($init)";
		}
	}
	sprint($buf);
	--$sindent if ($lc);
	sprint("{");
	++$sindent;
	sprint("if(!_initialized)\n{");
	++$sindent;
	sprint( "_initialized=true;\n"
		  . "getFieldNameAction A;\n"
		  . "persist(A);\n"
		  . "_names  = A.names;\n"
		  . "_idfield = A.idField;\n" );
	foreach my $e ( @{$d} ) {
		next if ( &function($e) ne "METHOD" );
		my $methodName =$$e{Name};
		sprint( "_methods.insert(\"$methodName\");\n");  
	}	  
	--$sindent;
	sprint("}");
	--$sindent;
	sprint("}\n\n");

}

sub genSourceFile {
	my $tree   = $_[0];
	my $dbname = $$tree{Attributes}{name};
	&printFileHeader( $source, $tree );
	&sprint( '#include "' . $dbname . ".h\"" );

	#	&sessionIncludes($tree);
	&sprint("\n\n");

# re-implement Session, json2Table classes with all tables registered at instantiation
	&sprint("namespace $dbname {\n");

	++$sindent;
	&sprint("Session::Session() {");
	++$sindent;
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		my $t = &name($f);
		&sprint( "Session::mapClass<$t>(" . $t . "::tablename());" );
	}
	--$sindent;
	&sprint("}\n\n");

	&sprint(
"jsonCommandInterpreter::jsonCommandInterpreter(Session &S) : GULI::Dbo::jsonCommandInterpreter(S) {"
	);
	++$sindent;
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		my $t = &name($f);
		&sprint(
			"jsonCommandInterpreter::mapClass<$t>(" . $t . "::tablename());" );
	}
	--$sindent;
	&sprint("}\n\n");

	#generate class initializers
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		&genClassInitializer($f);
	}

	#generate static variables
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		my $t = &name($f);
		&sprint( "bool " . $t . "::_initialized = false;" );
		&sprint( "std::set<std::string> " . $t . "::_methods;" );
		&sprint( "std::set<std::string> " . $t . "::_names;" );
		&sprint( "std::string " . $t . "::_idfield;" );
	}
	--$sindent;
	sprint("} // NAMESPACE");
}

sub genSqlProperties {
	my $f = shift;
	my $dbname = shift;
	my $idfield;
	foreach my $e ( @{ $$f{fields} } ) {
		if ( ((&function($e) eq "FIELD" || (&function($e) eq "BASEREF")) && &primaryKey($e)> 0  )) {
			$idfield = $e;
		}
	}
	
	
	
# need to generate sql properties to (a) remove the version field and (b) 
# re-define a natural id-field for this table if needed
	my $class = $dbname."::".$$f{Name};
	&hprint("template <> struct dbo_traits<$class> : dbo_default_traits {");
	++$hindent;
	&hprint( "static const char *versionField() { return 0; }" );
	if(defined $idfield) {
#		print STDERR Dumper($f)."\n".Dumper($idfield)."\n-----------------------\n";
		&hprint( "typedef " . &type($idfield) . " IdType;" );
		&hprint("static const char *surrogateIdField() { return 0; };");
		&hprint("static IdType invalidId() { return "
			  . &invalidId($idfield)
		  	  . "; }\n" );
	}
	--$hindent;
	&hprint("};");
#	&hprint("template <> struct dbo_traits<const $id> : dbo_traits<$id> {};");
}

sub genIncludeFile {
	my $tree   = $_[0];
	my $dbname = $$tree{Attributes}{name};

	&printFileHeader( $header, $tree, $dbname );
	&hprint('#include <guli/dboTableBase.h>');
	&hprint('#include <guli/dboSession.h>');
	&hprint('#include <guli/dboJsonCommandInterpreter.h>');
	&hprint("using GULI::Dbo::tableBase;\nusing GULI::Dbo::tableJSON;");

	&hprint("namespace $dbname {\n");
	++$hindent;

	# generate forward declarations
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		die "Table names _must_ begin with a captial letter: $$f{Name}"
		  if ( !( $$f{Name} =~ /^[A-Z]/ ) );
		hprint( "class " . $$f{Name} . ";" );
	}

	&hprint("class Session : public GULI::Dbo::Session {\npublic:");
	++$hindent;
	&hprint("Session();\nvirtual ~Session(){}");
	--$hindent;
	&hprint("};\n\n");
	&hprint(
"class jsonCommandInterpreter : public GULI::Dbo::jsonCommandInterpreter {\npublic:"
	);
	++$hindent;
	&hprint("jsonCommandInterpreter(Session &);\n"
		  . "virtual ~jsonCommandInterpreter(){}" );
	--$hindent;
	&hprint("};\n\n");

	--$hindent;
	hprint "}//Namespace\n\n";

# generate SQL properties
	&hprint("namespace Wt {");
	++$hindent;
	hprint "namespace Dbo {";
	++$hindent;
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		&genSqlProperties($f, $dbname);
	}
	--$hindent;
	hprint("} // Namespace Dbo");
	--$hindent;
	hprint("} // Namespace Wt");

# generate class implementation
	foreach my $f ( @{ $$tree{fields} } ) {
		next if ( &type($f) ne "TABLE" );
		&hprint("\n\nnamespace $dbname {\n");
		++$hindent;
		&genTableClass($f);
		--$hindent;
		&hprint("}//NAMESPACE");
		&genMethodStub($f, $dbname);
		&hprint("\n\n");
	}

	hprint("#endif");
}

my $referenceVarType = {
	hasOne => sub {
		my $e = shift;
		my $N = &name($e);
		my $R = &references($e);
		$R = $N if ( $R eq "" );
		return "Wt::Dbo::weak_ptr<$R>";
	},

	hasMany => sub {
		my $e = shift;
		my $N = &name($e);
		my $R = &references($e);
		$R = $N if ( $R eq "" );
		return "Wt::Dbo::collection<Wt::Dbo::ptr<$R> >";
	},

	belongsTo => sub {
		my $e = shift;
		my $N = &name($e);
		my $R = &references($e);
		$R = $N if ( $R eq "" );
		return "Wt::Dbo::ptr<$R>";
	},

	many2Many => sub {
		my $e = shift;
		my $N = &name($e);
		my $R = &references($e);
		$R = $N if ( $R eq "" );
		return "Wt::Dbo::collection<Wt::Dbo::ptr<$R> >";
	  }
};

my $genReferencePersist = {
	hasOne => sub {
		my $e  = shift;
		my $me = shift;
		my $N  = &name($e);
		my $L  = &linker($e);
		$L = $me if ( !defined $L );
		&hprint("Wt::Dbo::hasOne(a, _$N,  \"$L\");");
	},

	hasMany => sub {
		my $e  = shift;
		my $me = shift;
		my $N  = &name($e);
		my $L  = &linker($e);
		$L = $me if ( !defined $L );
		&hprint("Wt::Dbo::hasMany(a, _$N, Wt::Dbo::ManyToOne, \"$L\");");
	},

	belongsTo => sub {
		my $e = shift;
		my $N = &name($e);
		my $L = &linker($e);
		$L = $N if ( !defined $L );
		&hprint(
			"Wt::Dbo::belongsTo(a, _$N, \"$L\",  Wt::Dbo::OnDeleteCascade);");
	},

	many2Many => sub {
		my $e = shift;
		my $N = &name($e);
		my $L = &linker($e);
		die "Need to state linker for Many2Many relation $N" if ( !defined $L );
		hprint("Wt::Dbo::hasMany(a, _$N, Wt::Dbo::ManyToMany, \"$L\");");
	  }
};

sub genTableClass {
	my $dsc = shift;
	my %baseClass;

	my $d  = $$dsc{fields};
	my $id = $$dsc{Name};

	my $idField = "id";    #default label

	foreach my $e ( @{$d} ) {
		my $func = &function($e);
		if ( $func eq "BASECLASS" ) {
			my $bcd = {
				access  => $$e{Attributes}{access},
				persist => $$e{Attributes}{persist}
			};
			$baseClass{ $$e{Name} } = $bcd;
		}

		if ( $func eq "FIELD" || $func eq "BASEREF" ) {
			# check to see if we have a self-defined id-label
			if ( &isKey($e) ) {
				$idField = &name($e) ;
			}
		}
	}

	my $base = "public tableBase";
	foreach my $bc ( keys %baseClass ) {
		my $acc = $baseClass{$bc}{access};
		$acc = "private" if ( ( !defined $acc ) || $acc eq "" );
		$base .= ", $acc $bc ";
	}

	&hprint("class $id : $base {");
	&hprint("public:");
	++$hindent;
	&hprint("$id();");
	&hprint("static const char *tablename() { return \"$id\"; }\n"
		  . "static std::string idField()   { return \"$idField\"; }\n"
		  . "virtual const std::set<std::string> &methodNames() const { return _methods;}\n"
		  . "virtual const std::set<std::string> &fieldNames() const { return _names;}\n\n"
	);

	foreach my $e ( @{$d} ) {
		next if ( &function($e) ne "ENUM" );
		my $D = $$e{Data};

		$D =~ s/^\s+//;
		$D =~ s/\s+$//;
		$D =~ s/\s//g;

		my @options = split /,/, $$e{Data};
		&hprint( "enum " . &name($e) . "E {" );
		++$hindent;

		for ( my $i = 0 ; $i <= $#options ; ++$i ) {
			my $o = $options[$i];
			$o =~ s/\n//;
			$o =~ s/^\s*//;
			$o .= "," if ( $i != $#options );
			&hprint($o);
		}
		--$hindent;
		&hprint("};");
		&hprint( "typedef int " . &name($e) . "T;\n" );
	}

	# generate persistence template
	hprint("template<class Action> void persist(Action& a){");
	++$hindent;
	foreach my $e ( @{$d} ) {
		if ( &function($e) eq "FIELD" ) {
			if ( &primaryKey($e) ) {
				&hprint("Wt::Dbo::id(a, _"
					  . &name($e) . ", \""
					  . &name($e) . "\","
					  . &primaryKey($e)
					  . ");" );
			}
			else {
				&hprint("Wt::Dbo::field(a, _"
					  . &name($e) . ", \""
					  . &name($e)
					  . "\");" );
			}
		}


		if ( &function($e) eq "BASEREF" ) {
			if ( &primaryKey($e) ) {
				&hprint("Wt::Dbo::id(a, "
					  . &references($e) . ", \""
					  . &name($e) . "\","
					  . &primaryKey($e)
					  . ");" );
			} else {
				&hprint("Wt::Dbo::field(a, "
					. &references($e) . ", \""
				  	. &name($e)
					. "\");" );
			}
		}

		if ( &function($e) eq "REFERENCE" ) {
			my $t    = &type($e);
			my $n    = &name($e);
			my $proc = $$genReferencePersist{$t};
			die "Unknown reference type $t?" if ( !defined $proc );
			&{$proc}( $e, $id );
		}
	}

	foreach my $bc ( keys %baseClass ) {
		my $persist = $baseClass{$bc}{persist};
		next if ( ( !defined $persist ) || $persist eq "" );
		&hprint("$persist(a);");
	}
	--$hindent;
	&hprint("}\n");

	# generate getters and setters
	foreach my $e ( @{$d} ) {
		if ( &function($e) eq "FIELD" )
		{    #BASEREF does not implement getters and setters
			my $T = &type($e);
			my $N = &name($e);
			&hprint("const $T &$N() const  { return _$N; }");
			&hprint("$T &$N() { return _$N; }");
		}

		if ( &function($e) eq "REFERENCE" ) {
			my $t    = &type($e);
			my $N    = &name($e);
			my $proc = $$referenceVarType{$t};
			die "Unknown reference type $t?" if ( !defined $proc );
			my $ctype        = &{$proc}($e);
			my $functionName = $N;
			substr( $functionName, 0, 1 ) =~ tr/[A-Z]/[a-z]/;
			&hprint("const $ctype &$functionName() const { return _$N; }");
			&hprint("$ctype &$functionName()  { return _$N; }");
		}
	}
	--$hindent;
	hprint("private:");
	++$hindent;
	foreach my $e ( @{$d} ) {
		if ( &function($e) eq "FIELD" ) {
			&hprint( &type($e) . "\t_" . &name($e) . ";" );
		}
		if ( &function($e) eq "REFERENCE" ) {
			my $t    = &type($e);
			my $proc = $$referenceVarType{$t};
			die "Unknown reference type $t?" if ( !defined $proc );
			&hprint( &{$proc}($e) . "\t_" . &name($e) . ";" );
		}
	}
	&hprint("static bool _initialized;\n"
		  . "static std::set<std::string> _names;\n"
		  . "static std::set<std::string> _methods;\n"
		  . "static std::string _idfield;\n" );
	--$hindent;
	&hprint("};\n");
}

sub genMethodStub {
	my $dsc = shift;
	my $dbname = shift;

	my $d  = $$dsc{fields};
	my $id = $dbname."::".$$dsc{Name};
	
	foreach my $e ( @{$d} ) {
		my $func = &function($e);
		
		if ( $func eq "METHOD" ) {
			&hprint("template <> void GULI::Dbo::tableBase::callMethod<$id>(Wt::Dbo::ptr<$id> &, const std::string &, const Wt::Json::Object &arg);\n");
			return;
		}
	}
	# No need to print a methods stub
	return;
}
&parseXml::parseXml( $ARGV[0], "TABLE" );
