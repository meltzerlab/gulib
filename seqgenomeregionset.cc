/***************************************************************************
 *   Copyright (C) 2007 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <algorithm>
#include <sstream>
#include "guli/seqgenomeregionset.h"
#include "guli/seqsetwholegenome.h"
#include "guli/mmu.h"
#include "guli/bedRegion.h"
#include "guli/errorcodes.h"

using GULI::bedRegion;

seqSetGenomeRegion::seqSetGenomeRegion() :  _dsc(0), _quiet(true){
	_cache.setMaxMem(0);
	return;
}

seqSetGenomeRegion::~seqSetGenomeRegion() {}

void seqSetGenomeRegion::select(const selectionT &r, const ucscGenome &d) {
	_dsc = &d;
	_region.clear();
	selectionT empty;
	_region.swap(empty);
	_region.reserve(r.size());


	// Check validity of the regions
	for (size_t i = 0; i < r.size(); ++i) {
		bool ok = true;
		const compositeRegion &C = r[i];
		for(size_t j=0; ok && j < C.size(); ++j) {
			const bedRegion &t = C[j];

			int idx = _dsc->chrIndex(t.chromosome());
			if (idx < 0) {
				if (!_quiet)
					std::cerr << "Warning: Chromosome label " << t.chromosome() << " not recognized, region ignored\n";
				ok = false;
				break;
			}

			unsigned int chrlen = _dsc->chrLength(idx);

			if (chrlen < t.end()) {
				if (!_quiet) {
					std::cerr
						<< "Warning: Region outside chromosome. Region ignored:"
						<< std::endl;
					std::cerr << "Chromosome " << t.chromosome() << "(l=" << chrlen << "): " << t.start() << "-" << t.end()
						<< std::endl;
				}
				ok = false;
				break;
			}
		}
		if(ok) _region.push_back(C);
	}
	_cache.select(_region, *_dsc);
	return;
}

void seqSetGenomeRegion::select(const bedRegion &b, const ucscGenome &d) {
	selectionT tmp;
	tmp.push_back(compositeRegion(b));
	select(tmp, d);
}


void seqSetGenomeRegion::select(const bedRegion::listT &r, const ucscGenome &d) {
	selectionT tmp;
	tmp.resize(r.size());
	for(size_t i=0; i < r.size(); ++i) {
		tmp[i].id() = r[i].id();
		tmp[i].push_back(r[i]);
	}
	select(tmp, d);
	return;
}

void seqSetGenomeRegion::clear() {
	_region.clear();
	select(_region, *_dsc);
	return;
}

void seqSetGenomeRegion::cache::_load2mem(sequence::seqData &D, size_t i) const {
//	std::cerr << "Requesting " << i << " of " << _region->size() << " entries.\n";
	bool error = false;
	const compositeRegion &S = (*_region)[i];
	string seq_ascii = "";
	for(size_t j = 0; j < S.size(); ++j) {
		const bedRegion &t = S[j];
		// Read sequence from disk
		int idx = _dsc->chrIndex(t.chromosome());
		assert(idx >= 0);
		const unsigned int l = abs((int) (t.start()-t.end()));
		seq_ascii += _dsc->getSeq(idx, t.start(), l);
		error |= _dsc->error();
	}

	D.valid() = !error;
	sequence::storageType &seq_int = D.content();

	const size_t l = seq_ascii.size();
	seq_int.resize(l);
	for (size_t i = 0; i < l; i++) {
		int ascii = static_cast<int> (seq_ascii[i]);
		seq_int[i] = (_mask) ? sequence::c2n_MASKED[ascii] : sequence::c2n[ascii];
	}


	// setup ID
	if (S.id() == "") {
		std::stringstream s;
		for(size_t i=0; i < S.size(); ++i) {
			if(i) s << ',';
			s << S[i].chromosome() << ":" << S[i].start() << "-" << S[i].end();
		}
		D.id() = s.str();
	} else {
		D.id() = S.id();
	}

	_usedmem += seq_int.size();
	++_nmem;
	return;
}

void seqSetGenomeRegion::cache::_minimizeMemFootprint(sequence::seqData &D,	size_t i) const {
	_usedmem -= (unsigned int) D.content().size();
	--_nmem;
	sequence::seqData swapedOut;
	D = swapedOut;
}


void seqSetGenomeRegion::cache::select(const selectionT &r, const ucscGenome &s) {
	clear();
	_region = &r;
	_dsc = &s;
	resize((unsigned int) _region->size());
	prefetch(_prf);
}


void seqSetGenomeRegion::cache::prefetch(bool p) {
	_prf = p;
	if (_prf) {
		for (size_t i = 0; i < _region->size(); ++i) {
			(*this)[i]; // Force loading the data
			int avMem = _usedmem / (i + 1);
			if (_usedmem >= 10 * avMem + _usedmem) 	break; // Hea????
		}
	}
}

void seqSetGenomeRegion::cache::empty() {
	mmu::swapedVector<datT, sequence>::empty();
	_nmem = 0;
	_usedmem = 0;
}

