/*
 * numericKaplanMeier.cc
 *
 *  Created on: Dec 30, 2015
 *      Author: sven
 */
#include <assert.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include "guli/numericKaplanMeier.h"
namespace GULI {
	class kaplanMeier::multiEvent : public kaplanMeier::event {
	public:
		multiEvent(const event &E) {
			_T = E.T();
			_nCensored = E.nCensored();
			_nEvent    = E.nEvent();
		}

		multiEvent() : _T(0), _nCensored(0), _nEvent(0){}

		void addEvent(const event &E) {
			assert(E.T() == _T);
			_nCensored += E.nCensored();
			_nEvent    += E.nEvent();
		}

		virtual unsigned int T() 		const { return _T; }
		virtual unsigned int nCensored() const { return _nCensored; }
		virtual unsigned int nEvent()    const { return _nEvent; }

	private:
		unsigned int _T, _nCensored, _nEvent;
	};

	kaplanMeier::kaplanMeier() : _events(*new std::vector<multiEvent>), _N(0)  {_events.push_back(multiEvent());}

	kaplanMeier::~kaplanMeier() { delete &_events;}

	void kaplanMeier::addEvent(const event &EV ) {
		_N += EV.nEvent() + EV.nCensored();
		auto R = std::equal_range(_events.begin(), _events.end(), EV);
		if(R.first == _events.end()) {
			_events.push_back(multiEvent(EV));
			return;
		}

		if(R.first->T() != EV.T()) {
			_events.insert(R.first, multiEvent(EV));
		} else {
			R.first->addEvent(EV);
		}
	}

	float kaplanMeier::eventFreeEstimate(size_t i) const {
		double S = 1.;
		size_t N_i = _N;

		for(size_t t = 0; t <= i; ++t) {
			if(t > 0) N_i -= _events[t-1].nCensored() + _events[t-1].nEvent();
			S *= (double) (N_i - _events[t].nEvent()) / (double) N_i;
		}
		return S;
	}

	size_t kaplanMeier::size() 	const { return _events.size(); }

	unsigned int kaplanMeier::T(size_t i) const { return _events[i].T(); }

	const kaplanMeier::event &kaplanMeier::operator[](size_t i) const { return _events[i]; }

	unsigned int kaplanMeier::Di(size_t i) const {return _events[i].nEvent();}

	unsigned int kaplanMeier::DiAtTime(unsigned int t) const {
		const size_t i = _time2index(t);
		if(_events[i].T() != t) return 0.;
		return  Di(i);
	}

	unsigned int kaplanMeier::Ni(size_t i) const {
		size_t N_i = _N;

		for(size_t t =0; t < i; ++t) {
			N_i -= _events[t].nCensored() + _events[t].nEvent();
		}
		return N_i;
	}

	unsigned kaplanMeier::NiAtTime(unsigned int t) const {
		const size_t idx = _time2index(t);
		unsigned int r = Ni(idx);
		if(_events[idx].T() < t) r -=  _events[idx].nCensored() + _events[idx].nEvent();
		return r;
	}

	double kaplanMeier::logRank(const kaplanMeier &A, const kaplanMeier &B) {
		//setup a common event list
		kaplanMeier combined;
		typedef struct statT {
			statT(double O, double E, double V) : Oj1(O), Ej1(E), Vj(V) {}
			double Oj1, Ej1, Vj;
		} statT;

		auto stat = [&combined, &A, &B](size_t j) {
			const size_t T   	= combined.T(j);
			const double Nj  	= combined.Ni(j);
			if(Nj == 1) return statT(0,0,0);
			const double Oj 	= combined.Di(j);
			const double Nj1 	= A.NiAtTime(T);
			const double Oj1 	= A.DiAtTime(T);
			const double Ej1  = Oj / Nj * Nj1;
			const double Vj   = Oj * (Nj1 / Nj) * (1. - Nj1/Nj) * (Nj - Oj) / (Nj - 1.);
			return statT(Oj1, Ej1, Vj);
		};

		for(size_t i=0; i < A.size(); ++i) combined.addEvent(A[i]);
		for(size_t i=0; i < B.size(); ++i) combined.addEvent(B[i]);

		double Znum = 0., Zdenum = 0.;

		for(size_t i=0; i < combined.size(); ++i) {
			statT S = stat(i);
			Znum    += S.Oj1 - S.Ej1;
			Zdenum  += S.Vj;
		}
		double Z = Znum / sqrt(Zdenum);
		return  0.5 * (1. + gsl_sf_erf(Z/ sqrt(2.)));
	}

	size_t kaplanMeier::_time2index(unsigned int t) const {
		struct weakOrder {
			bool operator()(const unsigned int &t, const multiEvent &EV) { return t < EV.T(); }
			bool operator()(const multiEvent &EV, const unsigned int &t) { return EV.T() < t; }
		};
		auto  R = std::equal_range(_events.begin(), _events.end(), t, weakOrder());
		if(R.first == _events.end()) return _events.size()-1;
		size_t idx = R.first - _events.begin();
		if(R.first->T() > t) --idx;
		return idx;
	}
} // NAMESPACE GULI




