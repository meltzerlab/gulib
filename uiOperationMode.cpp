/*
 * peakfinderMode.cpp
 *
 *  Created on: Dec 26, 2008
 *      Author: sven
 */
#include <assert.h>
#include "guli/uiOperationMode.h"

namespace GULI {
	void uiOperationMode::_regstr() {
		if(!instances) instances = new sT;
		if (instances->find(id()) != instances->end()) {
			std::cerr << "Can only create one instance of " << _id << std::endl;
			assert(0);
		}
		instances->operator[](_id) = this;
	}

	void uiOperationMode::_unregstr() {
		sT::iterator w = instances->find(id());
		if (w == instances->end()) {
			std::cerr << "Can not unregister as a module " << _id << std::endl;
			assert(0);
		}
		instances->erase(w);
		if(!instances->size()) {
			delete instances;
			instances = 0;
		}
	}

	int uiOperationMode::entryPoint(int argc, char ** argv) {
		uiOperationMode *module;
		if (argc < 2) goto help;

		// First try if we are called under a module name
		module = mode(argv[0]);
		if (module) return module->main(argc, argv);

		module = mode(argv[1]);
		if (module) return module->main(argc - 1, argv + 1);
		help: std::cerr << "ERROR: Unknown mode " << argv[1] << "\n"
				"Usage: " << argv[0] << " mode ...." << std::endl
				<< "Try \"" << argv[0]
				<< " help\" to get a list of valid operation modes"
				<< std::endl
				<< "A detailed description for each mode can be obtained with "
				<< argv[0] << " mode -help" << std::endl;
		return -1;
	}

	vector<string> uiOperationMode::knownModes() {
		vector<string> r(instances->size());
		int j = 0;
		for (sT::iterator i = instances->begin(); i != instances->end(); ++i)
			r[j++] = i->first;
		return r;
	}

	uiOperationMode *uiOperationMode::mode(const string &id) {
		sT::iterator w = instances->find(id);
		if (w != instances->end()) return w->second;
		return 0;
	}

	uiOperationMode::sT *uiOperationMode::instances = 0;
}
