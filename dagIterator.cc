/*
 * dagIteratorBaic.cc
 *
 *  Created on: Sep 30, 2012
 *      Author: sven
 */
#include "guli/dag.h"
#include "guli/dagIterator.h"


void GULI::Dag::baseIterator::increment() {
	while (1) {
		const Node *cur = (Node *) this->operator->();
		unsigned int n_idx = __current.aidx++;

		if (n_idx < ( (_dir == Link::OUTGOING) ? cur->nOutgoing() :  cur->nIncoming()))  {
			// move forward to next ancestor ,
			// save current state to stack
			__stack.push_back(__current);
			__current.aidx = 0;
			__current.target = (_dir == Link::OUTGOING) ? cur->outgoing(n_idx) : cur->incoming(n_idx);
			if (!visited()) {
				markVisited();
				return;
			}
		}

pop_entry: if (!__stack.size()) { // Anything left to do on the stack ?
			__current.target = (Node *) -1; // NO, return END
			return;
		} else {
			// Restore last State from the stack
			__current = __stack.back();
			__stack.pop_back();
			if (__current.aidx < 0) { // put on stack by massign, continue from here...
				__current.aidx = 0;
				if (visited()) goto pop_entry;
				// This node has already been visited via an alternative pathway....
				// Sorry for the "goto":  Believe me -- a while(1) look is even more terrible here because we
				// have to break it 99% of the cases.....
				markVisited();
				return;
			}
		}
	} // Start all over
}


unsigned int GULI::Dag::uniqueIteratorState::newMagic() {
	if (++_magic >= reinit_magic) {
		for(_hashT::iterator i=_buffer.begin(); i != _buffer.end(); ++i) i->second = 0;
		_magic = 1;
	}
	return _magic;
}

