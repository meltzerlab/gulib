/***************************************************************************
 randomSubsetData.cpp  -  description
 -------------------
 begin                : Sun Feb 8 2004
 copyright            : (C) 2004 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 by Sven Bilke                                      *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <assert.h>
#include <algorithm>
#include "guli/dataRandomSubset.h"
#include "guli/stdinc.h"

namespace GULI {
	/** Transparent access to the full dataset in both directions **/
	void dataRandomSubset::reset() {
		_rowIdx.resize(_target.rows());
		for (unsigned int i = 0; i < _target.rows(); i++)
			_rowIdx[i] = i;
		_colIdx.resize(_target.cols());
		for (unsigned int i = 0; i < _target.cols(); i++)
			_colIdx[i] = i;
		_subR = _target.rows();
		_subC = _target.cols();
	}

	/** Select a random subset in row-direction, where every row is selected with probability P
	 * Thes method is depreciated and only provided for backward compatibility with an older version of randomSubset
	 **/
	void dataRandomSubset::randomRows(double P) {
		int N = static_cast<int> (P * _target.rows());
		randomRows(N);
	}

	/** Select a random subset in row-direction, where every row is selected with probability P
	 * Thes method is depreciated and only provided for backward compatibility with an older version of randomSubset
	 **/
	void dataRandomSubset::randomCols(double P) {
		int N = static_cast<int> (P * _target.cols());
		randomCols(N);
	}

	/** Present a subset of N  randomly chosen rows via this dataIF interface. With every call a new random
	 * subset is selected
	 **/
	void dataRandomSubset::randomRows(int N) {
		if (N < 0) N = _target.rows();
		assert(static_cast<unsigned int>(N) <= _target.rows());
		_subR = N;
		std::random_shuffle(_rowIdx.begin(), _rowIdx.end());
	}

	/** Present a subset of N  randomly chosen columns via this dataIF interface. With every call a new random
	 * subset is selected
	 **/
	void dataRandomSubset::randomCols(int N) {
		if (N < 0) N = _target.cols();
		assert(static_cast<unsigned int>(N) <= _target.cols());
		_subC = N;
		std::random_shuffle(_colIdx.begin(), _colIdx.end());
	}


    void dataRandomSubset::syncRandomSelections(dataRandomSubset &r, int mode) {
    	if(mode & ROW) {
    		assert(_target.rows() == r._target.rows());
            std::copy(r._rowIdx.begin(), r._rowIdx.end(), _rowIdx.begin());
            _subR = r._subR;
        }

        if(mode & COL) {
            assert(_target.cols() == r._target.cols());
            std::copy(r._colIdx.begin(), r._colIdx.end(), _colIdx.begin());
            _subC = r._subC;
        }

    }
} // namespace SB::data::wrapper
