/*
 * ngsBamRandomAccessPileup.cc
 *
 *  Created on: Jan 19, 2013
 *      Author: sven
 */
#include "guli/ngsBamRandomAccessPileup.h"
#include "guli/bedTiling.h"
namespace GULI {
	namespace NGS {

		static size_t DefaulRandomAccessPileupCacheSize = 4096;

		randomAccessPileup::randomAccessPileup() :
			_cache_tid(-1),
			_cache_start(-DefaulRandomAccessPileupCacheSize),
			_cache_len(DefaulRandomAccessPileupCacheSize)
		{}

		const pileup_t &randomAccessPileup::get(int tid, int pos, size_t s) {
			assert(s < numberOfStreams());
			if (!(tid == _cache_tid && pos >= _cache_start 	&& pos < _cache_start + _cache_len)) {
				_cache_start = pos;
				_cache_tid = tid;
				suggestReadahead(_cache_len); // Clears the cache
				bedRegion b(_chromosome(tid, 0), "randomAccessPilup::get", 	_cache_start, _cache_start + _cache_len);
				bedRegion::listT B(1, b);
				bedTiling<bedRegion::listT::const_iterator> T(B.begin(), B.end());
				stream(this, T);
			}
			return _cache[s][pos - _cache_start];
		}

		pileup_t *randomAccessPileup::pilerStreamingSetup(uint32_t tid, uint32_t pos,  size_t fileIndex, bool &manage) {
			if (pos < _cache_start || pos > _cache_start + _cache_len) return 0;
			manage = true;
			return &_cache[fileIndex][pos - _cache_start];
		}


		bool randomAccessPileup::get(pileup_t &result, int tid, int pos, const char *rg, size_t s) {
			const pileup_t &C = get(tid, pos, s);
			result.clear();
			int idx = readGroup(rg, s);
			if (idx < 0) return false; // Unknown read group
			const size_t N = C.size();
			for (size_t i = 0; i < N; ++i) {
				if (!strcmp(C[i].rg(), rg)) result.push_back(C[i]);
			}
			return true;
		}

		void randomAccessPileup::suggestReadahead(int w) {
			_cache_len = w;
			_cache_tid = -1;
			_cache_start = -w - 1;
			for (size_t i = 0; i < numberOfStreams(); ++i) {
				_cache[i].resize(_cache_len);
				for (int j = 0; j < _cache_len; ++j) {
					_cache[i][j].clear();
				}
			}
		}

		void randomAccessPileup::_flush() {
			for (size_t s = 0; s < numberOfStreams(); ++s) {
				for (int i = 0; i < _cache_len; ++i) {
					_cache[s][i].clear();
				}
			}
			_cache_tid = -1;
		}
	}
}

