/***************************************************************************
 *   Copyright (C) 2005-2007 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "guli/seqset.h"
#include <sstream>



	istream &seqMemSet::operator>>(istream &I) {
		sequence::seqData mem;
		while (1) {
			I >> mem;
			_content.push_back(mem);
			if (I.eof()) return I;
		}
		//    return I;
	}

	seqSet &seqMemSet::operator=(const seqSet &S) {
		const size_t sz = S.size();
		_content.resize(sz);
		for (size_t i = 0; i < sz; i++) _content[i] = S[i];
		return *this;
	}

	ostream &seqMemSet::operator<<(ostream &A) const {
		for (size_t i = 0; i < size(); i++) A << _content[i];
		return A;
	}

	istream &operator>>(istream &A, seqMemSet &b) { return b.operator>>(A); }
	ostream &operator<<(ostream &A, const seqMemSet &b) {return b.operator<<(A);	}
