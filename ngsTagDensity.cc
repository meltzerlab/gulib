/*
 * ngsTagDensity.cc
 *
 *  Created on: Jan 27, 2013
 *      Author: sven
 */

#include "guli/ngsTagDensity.h"
#include "guli/bedTiling.h"

namespace GULI {
	namespace NGS {
		void tagDensity::analyze(const bedTilingBase  &targets) {
			_targets = &targets;
			_streamCount.resize(numberOfStreams());
			for(size_t d=ALL; d <= BWD; ++d) {
				_total[d].resize(_targets->nBins());
				std::fill(_total[d].begin(), _total[d].end(), 0);
				for(size_t s=0; s < numberOfStreams(); ++s) {
					_streamCount[s].total[d].resize(_targets->nBins());
					_streamCount[s].perReadGroup[d].clear();
				}
			}
			_pileups = new pileup_t[numberOfStreams()];

			clearStats();
			stream(this, targets);

			delete [] _pileups;
			return;
		}

		pileup_t *tagDensity::pilerStreamingSetup(uint32_t tid, uint32_t pos,  size_t fileIdx, bool &manage) {
			manage = false;
			_pileups[fileIdx].clear();
			return &_pileups[fileIdx];
		}

		void tagDensity::detach() {
			_streamCount.clear();
			_total[ALL].clear();
			_total[FWD].clear();
			_total[BWD].clear();
			clearStats();
		}

		void      tagDensity::positionStreamsComplete(uint32_t tid, uint32_t pos) {
			bedRegion cur(_streams[0].stream->header->target_name[tid], "",  pos, pos+1);
			bedTilingBase::targetListT bins;
			_targets->overlaps(cur, bins);
			const size_t nBin = bins.size();
			if(!nBin) return;
			for(size_t s = 0; s < numberOfStreams(); ++s) {
				for(size_t i=0; i < _pileups[s].size(); i++) {
					if(_pileups[s][i].is_del || _pileups[s][i].is_refskip) continue;
					if(readGroups()) {
						if(_streamCount[s].perReadGroup[ALL].size() < nReadGroup(s)) {
							size_t old = _streamCount[s].perReadGroup[ALL].size();
							for(size_t rg = ALL; rg <= BWD; ++rg) {
								vector<countT> &RG = _streamCount[s].perReadGroup[rg];
								RG.resize(nReadGroup(s));
								for(size_t r=old; r < nReadGroup(s); ++r) {
									RG[r].resize(_total[ALL].size());
									std::fill(RG[r].begin(), RG[r].end(), 0);
								}
							}
						}
					}

					int RG = -1;
					if(readGroups()) {
						const char *rg = readGroups() ? BAMUTILS::getStringTag(_pileups[s][i].b, "RG") : 0;
						if(rg) {
							RG = readGroup(rg, s);
							for(size_t bb=0; bb < nBin; ++bb) {
								const size_t b = bins[bb];
								_streamCount[s].perReadGroup[ALL][RG][b]++;
							}
						}
					}

					int dir = _pileups[s][i].strand() ? BWD : FWD;
					for(size_t bb=0; bb < nBin; ++bb) {
						const size_t b = bins[bb];
						_total[ALL][b]++;
						_streamCount[s].total[ALL][b]++;
						_total[dir][b]++;
						_streamCount[s].total[dir][b]++;
						if(RG >=0) _streamCount[s].perReadGroup[dir][RG][b]++;
					}
				} // pileup loop
			} // stream loop
		} // function
	} // namespace NGS
}  // namespace GULI

