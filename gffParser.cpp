/*
 * gffParser.cpp
 *
 *  Created on: Oct 9, 2012
 *      Author: sven
 */
#define USE_LEGACY_REGEX
#ifdef USE_LEGACY_REGEX
#include <regex.h>
#else
#include <regex>
#endif

#include <sstream>
#include "guli/gffDag.h"
#include "guli/gffParser.h"


static const GULI::gffParser::parserMode::synthesizeFeatureT nativeSynthesis[] = {
		{ 0, }
};

static const GULI::gffParser::parserMode::featureSelectorT nativeFeatureSelector[] = {
		{ ".*", "ID", 0},
		{ 0, }
};

static const GULI::gffParser::parserMode::attributeSelectorT nativeAttributeSelector[] = {
		{ ".*", ".*", true},  // keep everything
};


static const GULI::gffParser::parserMode::namespaceMapT nativeMapNamespace[] = {
		{ 0, } // rename nothing
};


static const GULI::gffParser::parserMode::synthesizeFeatureT EnsemblSynthesis[] = {
		{"(exon|CDS)", "gene",       "gene_id"},
		{"(exon|CDS)", "transcript", "transcript_id"},
		{ 0, }
};

static const GULI::gffParser::parserMode::featureSelectorT EnsemblFeatureSelector[] = {
		{ "gene", "gene_id", 0 },
		{ "transcript", "transcript_id", 0 },
		{ "(exon)|(CDS)|(start_codon)|(stop_codon)", "", "(transcript_id)|(exon_number)" },
//		{ ".*", 0 }, // If you don't want warnings, turn them off here
		{ 0, } // We should have captured all features, warn about everything else
};

static const GULI::gffParser::parserMode::attributeSelectorT EnsemblAttributeSelector[] = {
		{ "gene", ".*gene.*",                     true}, // keep everything gene related
		{ "gene", ".*",                           false}, // drop everything else

		{ ".*", "transcript_id",                  true}, // All objects need this, either as ID or parent
		{ "transcript", "gene_id",                true}, // keep the gene_id as parent field
		{ "transcript", ".*transcript.*",         true}, // keep all transcript related information
		{ "transcript", ".*",                     false}, // drop everything else

		{ ".*", "ID",      						  true},  //
		{ ".*", "(.*gene.*)|(.*transcript.*)",    false}, // drop everything gene/transcript related (except transcript_id above)
		{ ".*", ".*", true},                              // keep everything not yet digested
		{ 0, } // We should have captured all features, warn about everything else
};

static const GULI::gffParser::parserMode::namespaceMapT EnsemblMapNamespace[] = {
		{ "gene", "gene_id", "ID"},
		{ "gene", "gene_name", "Name"},
		{ "gene", "gene_biotype", "Type"},

		{ "transcript", "transcript_id", "ID"},
		{ "transcript", "gene_id", "Parent"},
		{ "transcript", "transcript_name", "Name"},

		{ ".*", "transcript_id", "Parent"},        //  Associate with transcript
		{ 0, }
};


static const GULI::gffParser::parserMode::synthesizeFeatureT GencodeSynthesis[] = { // gencode includes genes/transcripts
		{ 0, }
};

static const GULI::gffParser::parserMode::featureSelectorT GencodeFeatureSelector[] = {
		{ "gene", "gene_id", 0 },
		{ "transcript", "transcript_id", 0 },
		{ "exon", "exon_id", "(transcript_id)|(exon_number)" },
		//		{ "(CDS)|(start_codon)|(stop_codon)", "", "(transcript_id)|(exon_id)|(exon_number)"  },
		{ "(CDS)", "", 0  },
		{ "(start_codon)|(stop_codon)", "", "(transcript_id)|(exon_id)|(exon_number)"  },
		{ "(UTR)|(Selenocysteine)", "", "transcript_id"  },
		//		{ ".*", 0, 0 }, // If you don't want warnings, turn them off here
		{ 0, } // We should have captured all features, warn about everything else
};


static const GULI::gffParser::parserMode::attributeSelectorT GencodeAttributeSelector[] = {
		{ "gene",       								"gene.*",                       true},  // keep everything gene related
		{ "gene",       								"(tag)|(ccdsid)|(ont)|(level)", true},  // keep gene specific information
		{ "gene",       								".*",                           false}, // drop everything else
		{ "transcript", 								"gene_id",                		true},  // keep everything gene related
		{ "transcript", 								"gene_.*",                		false}, // drop everything (else) gene related
		{ "transcript", 								"transcript_.*",           		true},  // accept everything transcript related
		{ "transcript",       							"ont", 							false},  // keep ontology information
		{ "transcript",       							"level",						true},  // keep ontology information
		{ "transcript", 								".*",                     		false}, // drop everything else
		{ "exon", 										"exon_.*", 						true},  // Accept everything exon specific
		{ "CDS",										"(ccdsid)|(exon_id)|(exon_number)",						true},
		{ "(start_codon)|(stop_codon)", 				"(exon_id)|(exon_number)",		true},  //
		{ ".*",                                     	"transcript_id",                true},
//		{ ".*",       									"ID",  				  		    true},  //
		{ ".*",         								".*((gene)|(transcript)|(exon)).*",  false}, // drop everything gene/transcript/exon related (except mentioned above)
		{ ".*",         								"(tag)|(ccdsid)|(ont)|(level)", false}, // drop other gene specific information
		{ ".*",         								".*", 							true},  // keep everything not yet digested
		{ 0, } // End of Rules
};


static const GULI::gffParser::parserMode::namespaceMapT GencodeMapNamespace[] = {
		{ "gene", "gene_id", "ID"},
		{ "gene", "gene_name", "Name"},
		{ "gene", "gene_type", "Type"},
		{ "gene", "gene_status", "Status"},

		{ "transcript", "transcript_id", "ID"},
		{ "transcript", "gene_id", "Parent"}, // associate with Gene
		{ "transcript", "transcript_name",   "Name"},
		{ "transcript", "transcript_type",   "Type"},
		{ "transcript", "transcript_status", "Status"},

		{ "exon", "exon_id", "ID"},
		{ "exon", "transcript_id", "Parent"},

		{ "(CDS)|(start_codon)|(stop_codon)", "transcript_id", "Parent"},  //  Associate with Exon
		{ "(Selenocysteine)|(UTR)", "transcript_id", "Parent"},     //   Associate with Transcript
		{ 0, }
};



namespace GULI {
	const gffParser::parserMode gffParser::Native   = { nativeSynthesis,  nativeFeatureSelector,  nativeAttributeSelector,  nativeMapNamespace };
	const gffParser::parserMode gffParser::Ensembl  = { EnsemblSynthesis, EnsemblFeatureSelector, EnsemblAttributeSelector, EnsemblMapNamespace };
	const gffParser::parserMode gffParser::Gencode  = { GencodeSynthesis, GencodeFeatureSelector, GencodeAttributeSelector, GencodeMapNamespace };

	namespace HELPER {
#ifdef USE_LEGACY_REGEX
		class key {
		public:
			key() : _key(""), _error(true) {};
			key(const key &k) : _key(""),  _error(true) { setKey(k._key); }
			key(const string &k) : _key(""),  _error(true) { setKey(k); }
			virtual ~key() { if(!_error) regfree(&_regex); }

			void setKey(const string &k) {
				_key = k;
				_error = regcomp(&_regex, _key.c_str(), REG_EXTENDED);
			}
			bool match(const string &k)  {
				regmatch_t m;
				return !regexec(&_regex, k.c_str(), 1, &m, 0);
			}
			bool ok() const { return !_error; }
		private:
			string  _key;
			regex_t _regex;
			bool _error;
		};

#else
		class key {
		public:
			key() : _key("") {};
			key(const key &k) : _key("")    { setKey(k._key); }
			key(const string &k) : _key("") { setKey(k); }
			virtual ~key() {  }

			void setKey(const string &k) {
				_key = k;
				_regex.assign(_key, std::regex_constants::extended);
			}
			bool match(const string &k)  {
				return std::regex_search(k, _regex);
			}
			bool ok() const { return true; } // leftover from an earlier implementation
		private:
			string  _key;
			std::regex _regex;
		};
#endif

		class gffParserImplementation;


	} // NAMESAPCE HELPER

	class HELPER::gffParserImplementation :  private tabDelimited {
	public:
		istream &parse(istream &IN, gffDag::Node::table &target, const gffParser::parserMode &M) {
			_target = &target;
			_mode = &M;
			// compile regular expressions
			for(size_t i=0; _mode->synthesis[i].featureRegex; ++i) {
				key K(_mode->synthesis[i].featureRegex);
				if(!K.ok()) {
					std::cerr << "Error setting up regular expression " << _mode->synthesis[i].featureRegex << std::endl;
					assert(0);
				}
				_synthesis_regexp.push_back(K);
			}

			for(size_t i =0; _mode->idSelector[i].featureRegex; ++i) {
				const char *featureRegex = _mode->idSelector[i].featureRegex;
//				const char *uniquify     = _mode->idSelector[i].uniqifyRegex ? _mode->idSelector[i].uniqifyRegex : "";
				const char *uniquify     = _mode->idSelector[i].uniqifyRegex ? _mode->idSelector[i].uniqifyRegex : "__DO_NOT_MATCH__";
				attribute_regexpT R(featureRegex, uniquify);
				if(! (R.feature.ok() && R.attribute.ok())) {
					std::cerr << "Error setting up regular expression " << featureRegex << "," << uniquify
							  << std::endl;
					assert(0);
				}
				_featureSelector_regexp.push_back(R);
			}

			for(size_t i =0; _mode->attributeSelector[i].featureRegex; ++i) {
				const char *featureRegex = _mode->attributeSelector[i].featureRegex;
				const char *attribute    = _mode->attributeSelector[i].attributeRegex;
				attribute_regexpT R(featureRegex, attribute);
				if(! (R.feature.ok() && R.attribute.ok())) {
					std::cerr << "Error setting up regular expression " << featureRegex << "," << attribute
		          			  << std::endl;
					assert(0);
				}
				_attributeSelector_regexp.push_back(R);
			}


			for(size_t i =0; _mode->mapNames[i].featureRegex; ++i) {
				 attribute_regexpT R(_mode->mapNames[i].featureRegex, _mode->mapNames[i].attributeRegex);
				if(! (R.feature.ok() && R.attribute.ok())) {
					std::cerr << "Error setting up regular expression " << _mode->mapNames[i].featureRegex << ","
							  << _mode->mapNames[i].attributeRegex << std::endl;
					assert(0);
				}
				_attributeNamespace_regexp.push_back(R);
			}

			tabDelimited::parse(IN);
			_mapNamespace();

			_syntheticNameSerial.clear();
			_synthesized.clear();
			_synthesis_regexp.clear();
			_featureSelector_regexp.clear();
			_attributeSelector_regexp.clear();
			_attributeNamespace_regexp.clear();
    		_idxTable.clear();
			return IN;
		}
	private:
		// tab-delimited API
		void consumeData(int argc, char **argv) {
			if(argc < 9) return;
			gffDag::Node::textual R;
			R.seqname = argv[0];
			R.source  = argv[1];
			R.feature = argv[2];
			R.start   = atoi(argv[3]);
			R.end     = atoi(argv[4]) + 1;   // GFF files use [start,end] notaton, the BED object operations use [start, end[ notation. Need to adjust for this
			R.score   = (*argv[5] == '.') ? 0. : atof(argv[5]);
			R.strand  = *argv[6];
			R.frame   = *argv[7];
			R.attribute = gffDag::Node::textual::parseAttributeString(argv[8]);

			if(!_featureSelection(R)) return; // feature not used otherwise added to *_target by _featureSelection
			_synthesize(R);          // check if there are any derived, implicit features
		}

		void _synthesize(gffDag::Node::textual R) {
			// see if we have any synthetic features in need of updating
			for(size_t s = 0; s < _synthesis_regexp.size(); ++s) {
				if(_synthesis_regexp[s].match(R.feature)) {
					string idAttribute = _mode->synthesis[s].idAttribute;
					if(R.attribute.find(idAttribute) == R.attribute.end()) {
						std::cerr << "Warning::gffParser in synthesis step: can not find ID attribute " << idAttribute
								  << " while synthesizing " << _mode->synthesis[s].feature << ". Won't create (or re-use) that feature here" << std::endl;
						continue;
					}

					string id = R.attribute[_mode->synthesis[s].idAttribute];
					if(_synthesized.find(id) == _synthesized.end()) { // not seen this one before
						gffDag::Node::textual S = R;
						S.frame = '.';
						S.score = 0.;
						S.feature = _mode->synthesis[s].feature;
						if(!_featureSelection(S)) continue;
						_synthesized[id] = _target->size() - 1;
					}

					// check if we need to adjust boundaries of the synthetic object to cover all associated features
					gffDag::Node::textual &T = _target->operator[](_synthesized[id]);
					if(R.seqname != T.seqname) {
						std::cerr << "Warning: Inconsistent seqname for " << id << std::endl;
					} else {
						if(T.start > R.start)    T.start = R.start;
						if(T.end   < R.end)      T.end   = R.end;
					}
				}
			}
		}

		bool _featureSelection(gffDag::Node::textual N) {
			int f = -1;
			for(size_t ff = 0; ff < _featureSelector_regexp.size(); ++ff) {
				if(_featureSelector_regexp[ff].feature.match(N.feature)) {
					f = ff;
					break;
				}
			}

			if(f < 0) {
				std::cerr << "Warning: featureId selection reached end of rule-set without a match.\n"
						  <<  "Using default DROP (i.e. discarding this entity) for feature " << N.feature
						  << std::endl;
				return false;
			}

			const char *idAttribute = _mode->idSelector[f].idAttribute;
			if(!idAttribute) return false; // DROP requested

			// uniquify
			string nodeId;
			if(*idAttribute) {
				gffDag::Node::textual::attributeT::iterator I = N.attribute.find(idAttribute);
				if(I == N.attribute.end()) {
					std::cerr << "Warning::gffParser in process step: can not find ID attribute " << idAttribute << std::endl;
					return false;
				}
				nodeId = N.attribute[idAttribute];

				// uniquify on named entities does not work on outline, but on ID
				if(_mode->idSelector[f].uniqifyRegex) {
					if(_idxTable.find(nodeId) != _idxTable.end()) {
						gffDag::Node::textual &p = _target->operator[](_idxTable[nodeId]);
						typedef gffDag::Node::textual::attributeT::iterator atIterator;
						for(atIterator I = N.attribute.begin(); I != N.attribute.end(); ++I) {
							if(_featureSelector_regexp[f].attribute.match(I->first)) {
								if(p.attribute.find(I->first) != p.attribute.end()) {
									p.attribute[I->first] += "," + I->second;
								} else {
									p.attribute[I->first] = I->second;
								}
							}
						}
						return true;
					} else {
						_idxTable[nodeId] = _target->size();
					}
				}
			} else {
				// if uniquify is requested, we may not have to create a new entity
				// test  if we have already seen a feature with the same outline
				bedRegion bed(N.seqname, "__NOID__", N.start, N.end, N.score, N.strand);
				if(_mode->idSelector[f].uniqifyRegex) {
					const bedRegion::listT &U = _uniqueFeature[N.feature][N.seqname];
					bedRegion::constListIterator F = std::lower_bound(U.begin(), U.end(), bed);
					if(F!= U.end() && *F == bed) { // feature exists
						gffDag::Node::textual &p = _target->operator[](_idxTable[F->id()]);
						typedef gffDag::Node::textual::attributeT::iterator atIterator;
						for(atIterator I = N.attribute.begin(); I != N.attribute.end(); ++I) {
							if(_featureSelector_regexp[f].attribute.match(I->first)) {
								if(p.attribute.find(I->first) != p.attribute.end()) {
									p.attribute[I->first] += "," + I->second;
								} else {
									p.attribute[I->first] = I->second;
								}
							}
						}
						return true;
					}
				}

			// synthesize ID for this feature
				if(_syntheticNameSerial.find(N.feature) == _syntheticNameSerial.end()) _syntheticNameSerial[N.feature] = 1;
				std::stringstream ids;
				ids << N.feature << "_#" << _syntheticNameSerial[N.feature]++;
				nodeId = ids.str();
				N.attribute["ID"] = nodeId;

				if(_mode->idSelector[f].uniqifyRegex) {
					bedRegion::listT &T = _uniqueFeature[N.feature][N.seqname];
					bed.id(nodeId);
					bedRegion::listT::iterator I = std::lower_bound(T.begin(), T.end(), bed);
					T.insert(I, bed); // we never get here if bed already exists, so no need to check *I != bed here
					_idxTable[nodeId] = _target->size();
				}
			}

			_attributeSelection(N);
			_target->push_back(N);
			return true;
		}

		void _attributeSelection(gffDag::Node::textual &N) {
			gffDag::Node::textual::attributeT A;
			for(gffDag::Node::textual::attributeT::const_iterator I=N.attribute.begin(); I != N.attribute.end(); ++I) {
				string id    = I->first;
				string value = I->second;
				bool found = false;
				for(size_t i=0; i < _attributeSelector_regexp.size(); ++i) {
					if(_attributeSelector_regexp[i].feature.match(N.feature) && _attributeSelector_regexp[i].attribute.match(id)) {
						found = true;
						if(!_mode->attributeSelector[i].keep) {
//							std::cerr << "Dropping " << N.feature << " attribute " << id << std::endl;
							break; // drop attribute
						}
						A[id] = value;
//						std::cerr << "Accepting " << N.feature << " attribute " << id << std::endl;
						break;
					}
				}
				if(found) continue;
					std::cerr << "Warning: attribute selection reached end of rule-set without a match.\n"
							 "Using default DROP (i.e. discarding this attribute)" << id << " for node " << N.attribute["ID"] << std::endl;
			}
			N.attribute.swap(A);
		}

		void _mapNamespace() {
			for(size_t i=0; i  < _target->size(); ++i) {
				gffDag::Node::textual &N = _target->operator[](i);
				gffDag::Node::textual::attributeT A;

				for(gffDag::Node::textual::attributeT::const_iterator I=N.attribute.begin(); I != N.attribute.end(); ++I) {
					string id    = I->first;
					string value = I->second;
					bool found = false;
					for(size_t i=0; i < _attributeNamespace_regexp.size(); ++i) {
						if(_attributeNamespace_regexp[i].feature.match(N.feature) && _attributeNamespace_regexp[i].attribute.match(id)) {
							if(!_mode->mapNames[i].targetAttribute) break; // drop attribute
							if(*_mode->mapNames[i].targetAttribute) id = _mode->mapNames[i].targetAttribute;
							A[id] = value;
							found = true;
							break;
						}
					}
					if(!found) A[id]  = value;
				}
				N.attribute.swap(A);
			}
		}

		typedef std::unordered_map<string, int> _syntcT;
		typedef std::unordered_map<string, bedRegion::listT> _s2bedT;
		typedef std::unordered_map<string, _s2bedT>           _s2s2bedT;

		_syntcT _synthesized, _syntheticNameSerial, _idxTable;
		_s2s2bedT _uniqueFeature;

		typedef struct attribute_regexpS {
			key feature;
			key attribute;
			attribute_regexpS(string f, string a) : feature(f), attribute(a) {}
			attribute_regexpS(const attribute_regexpS &A) : feature(A.feature), attribute(A.attribute) {}
		} attribute_regexpT;

		gffDag::Node::table *_target;
		vector<key> _synthesis_regexp;
		vector<attribute_regexpS>  _featureSelector_regexp, _attributeSelector_regexp, _attributeNamespace_regexp;
		const gffParser::parserMode *_mode;


	};


	istream &gffParser::parse(istream &I, gffDag::Node::table &target, const gffParser::parserMode &M) {
		HELPER::gffParserImplementation P;
		return P.parse(I, target, M);
	}

	istream &gffParser::parse(istream &I, gffDag &target, const gffParser::parserMode &M) {
		gffDag::Node::table T;
		parse(I, T, M);
		textual2Gff(T, target);
		return I;
	}

	// NOTE: Currently we assume that, for each identifier, there is only one version (ID.version) substring
	// The code does not, in a smart way, deal with versioning. It may be required to update the code if the
	// assumption of coherent versioning with a given gff file is not correct
	void gffParser::textual2Gff(const gffDag::Node::table &T, gffDag &target) {
		typedef gffDag::Node Node;
		typedef gffDag::Link Link;
		target.clear();

		// turns out, unique ID's are not quite unique in all files
		// so we may need to re-name nodes on the fly and try our best to disambiguate Parent/Child relations
		typedef std::unordered_map<string, vector<Node *>> findT;
		findT F;

		for (size_t i = 0; i < T.size(); i++) {
			Node::textual cur = T[i];
			Node *N = new Node(cur);
			F[N->id()].push_back(N);
		}

		for(findT::iterator i=F.begin(); i != F.end(); ++i)  {
			vector<Node *> &N = i->second;
			if(N.size() == 1) {
				target.addNode(N[0]);
			} else {
				for(size_t i=0; i < N.size(); ++i) {
					std::stringstream ids;
					ids << N[i]->id() << '@' << i+1;
					dynamic_cast<bedRegion *>(N[i])->id(ids.str()); // rename node
					target.addNode(N[i]);
				}
			}
		}
		Node &root = *target.root();
		// fill in links
		for (size_t i = 0; i < T.size(); i++) {
			Node::textual curt = T[i];
			string nodeId = curt.attribute["ID"];
			// check if we need to disambiguate
			findT::iterator I = F.find(nodeId);
			if(I == F.end()) {
				std::cerr << "ERROR: Orphaned node (" << nodeId << "), trying to recover " << std::endl;
				continue;
			}
			vector<Node *> & N = I->second;;
			if(N.size() > 1) { // need to disambiguate
				for(size_t j=0; j < N.size(); ++j) {
					if(curt.start == N[j]->start() && curt.end == N[j]->end() &&  curt.seqname == N[j]->seqname()) {
						nodeId = N[j]->id();
						break;
					}
				}
			}

			assert(nodeId != Node::UNKNOWNATTRIBUTE);
			Node &curn = *dynamic_cast<Node *>(target[nodeId]);

			// now, we need to potentially disambiguate the parent relationship
			const string &A = curt.attribute["Parent"];
			if(!A.length()) {
				curn.addLink(Link(root, Link::INCOMING));
				root.addLink(Link(curn, Link::OUTGOING));
			} else {
				vector<string> parents;
				for(size_t pos = 0; pos < A.length(); ) {
					size_t to = A.find(',',pos);
					if(to == string::npos) to = A.length();
					string anc = A.substr(pos, to - pos);
					pos = to + 1;
					if(anc != "") {
						// check if we need to disambiguate
						findT::iterator i = F.find(anc);
						if(i == F.end()) {
							std::cerr << "Orphaned Child (" << nodeId << "),  parent named " << anc << std::endl;
							continue;
						}
						vector<Node *> & N = i->second;;
						if(N.size() == 1) {
							parents.push_back(anc);
						} else {
							bool found = false;
							for(size_t j=0; j < N.size(); ++j) {
								if(N[j]->inside(curn)) {
									parents.push_back(N[j]->id());
									found = true;
									break;
								}
							}
							if(!found) {
								std::cerr << "Child has more than one parent, but none of them overlaps its position. RANDOMLY assigning one parent" << std::endl;
								parents.push_back(N[0]->id());
							}
						}
					}
				}
				target.updateRelations(curn, parents,  Link::INCOMING, (void *) 0);
			}
		}
	}

} // NAMESPACE
