/*
 * numericSpectralFilter.cc
 *
 *  Created on: Aug 27, 2013
 *      Author: sven
 */

#include <math.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_fit.h>
#include "guli/stdinc.h"
#include "guli/numericSpectralFilter.h"
#undef DEBUG_SPECTRAL_FILTER

namespace GULI {
	namespace SPECTRAL {
		void _filter(double *buffer, const int N, filterCore &A) {
			// transform to frequency domain
			gsl_fft_real_wavetable *real;
			gsl_fft_real_workspace *work;
			work = gsl_fft_real_workspace_alloc(N);
			real = gsl_fft_real_wavetable_alloc(N);
			gsl_fft_real_transform(buffer, 1, N, real, work);
			gsl_fft_real_wavetable_free(real);

			// Apply filter in frequency domain
			A.estimate(buffer, N);
			for (int i = 0; i < N; i++) {
				buffer[i] = A.coefficient(i, buffer[i]) * buffer[i];
			}

			// inverse transform
			gsl_fft_halfcomplex_wavetable *hc;
			hc = gsl_fft_halfcomplex_wavetable_alloc(N);
			gsl_fft_halfcomplex_inverse(buffer, 1, N, hc, work);
			gsl_fft_halfcomplex_wavetable_free(hc);
			gsl_fft_real_workspace_free(work);
			return;
		}

		void _spectralize(double *buffer, const int N) {
			// transform to frequency domain
			gsl_fft_real_wavetable *real;
			gsl_fft_real_workspace *work;
			work = gsl_fft_real_workspace_alloc(N);
			real = gsl_fft_real_wavetable_alloc(N);
			gsl_fft_real_transform(buffer, 1, N, real, work);
			gsl_fft_real_wavetable_free(real);
			gsl_fft_real_workspace_free(work);
		}

		void percentileFilter::estimate(double *S, int N) {
			double Y[N];
			for (int i = 0; i < N; i++) {
				Y[i] = S[i] * S[i];
			}
			std::sort(Y, Y + N);
			int C = (int) ((double) N * fraction);
			C = C >= N ? N - 1 : C;
			thresh = Y[C];
		}

		double percentileFilter::coefficient(int F, double C) {
			double S2 = C * C - thresh;
			return (S2 < 0.) ? 0. : S2 / (S2 + thresh);
		}

	}
} // NAMESPACE

#ifdef DEBUG_SPECTRAL_FILTER
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
int main(int argc, char **argv) {
	const gsl_rng_type * T;
	gsl_rng * r;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc (T);

	double test[20000];
	double a1, a2, a3;
	a1 = 200.;
	a2 = 300.;
	a3 = 700;

	for(int i=0; i < 20000; i++) {
		test[i] = sin( i / a1) + cos( i / a2) + sin(i / a3);
		test[i] += gsl_ran_gaussian_ratio_method (r, 1.);
	}

	for(int i=0; i < 20000; i++) {
		std::cerr << i << "\t" << test[i] << std::endl;
	}

	wienerFilter::removePercentile A(0.96);
	wienerFilter::filter(test, test+20000, A);

	for(int i=0; i < 20000; i++) {
		std::cout << i << "\t" << test[i] << std::endl;
	}
}

#endif
