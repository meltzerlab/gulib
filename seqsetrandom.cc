/***************************************************************************
 *   Copyright (C) 2005-2007 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <algorithm>
#include "guli/seqsetrandom.h"

seqSetRandom::seqSetRandom(int s, const seqSet *d) :
	_data(d), _n_selected(0), _nset(s) {
}

seqSetRandom::~seqSetRandom() {
}

void seqSetRandom::_randomize() const {
	std::random_shuffle(_index.begin(), _index.end());
	return;
}

/*
 seqSetRandom &seqSetRandom::operator=(const seqSet &S) {
 _data=S;
 transparent();
 return *this;
 }
 */

void seqSetRandom::transparent() {
	_n_selected = _data->size();
	_index.resize(_n_selected);
	for (unsigned int i = 0; i < _index.size(); i++)
		_index[i] = i;
	return;
}

