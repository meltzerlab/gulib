#include <assert.h>
#include <math.h>
#include "guli/stdinc.h"
#include "guli/dataSymmetric.h"
#include "guli/numericHierclust.h"

namespace GULI {
	hierClust::node::node(int id, node *c1, node *c2, node *parent, Mode &P,
			const string &l, normalizeData *N) :
		_leftChild(c1), _rightChild(c2), _parent(parent), _id(id), _label(l) {
		assert(_leftChild->_p.size() == _rightChild->_p.size());
		_p.resize(_leftChild->_p.size());

		P(_leftChild->_p, _rightChild->_p, _p);
		_leftChild->_p.clear();
		_rightChild->_p.clear();
		if (N) (*N)(_p.begin(), _p.end());
	}

	hierClust::node::~node() {
		if (!isLeaf()) {
			delete _leftChild;
			delete _rightChild;
		}
	}

	void *hierClust::node::visit(visitor *V) {
		void *l = 0, *r = 0;

		if (!isLeaf()) {
			l = _leftChild->visit(V);
			r = _rightChild->visit(V);
		}
		return V->visit(this, l, r);
	}

	hierClust::node *hierClust::cluster(dataIF &D, orientation O) {
		//Create Leaf-Nodes
		if (O == COLUMNS) {
			for (unsigned int i = 0; i < D.cols(); i++) {
				_nodes.push_back(new node(i, D.beginCol(i), D.endCol(i),
						D.colId(i)));
			}
		} else {
			for (unsigned int i = 0; i < D.rows(); i++) {
				_nodes.push_back(new node(i, D.beginRow(i), D.endRow(i),
						D.rowId(i)));
			}
		}

		// Setup similarity Matrix
		_index.resize(_nodes.size());
		_next_nb.resize(_nodes.size());
		_next_d.resize(_nodes.size());
		std::fill(_next_d.begin(), _next_d.end(), (float) -2.);

		metric.setCacheSize(_nodes.size());
		for (unsigned int i = 0; i < _nodes.size(); i++) {
			_index[i] = i;
			metric.prepareCache(_nodes[i]->_p, i);
		}

		_distance = new GULI::dataSymmetric(_nodes.size());
		for (unsigned int i = 0; i < _nodes.size() - 1; i++)
			_updateDistanceSlice(i, i + 1);

		vector<float>::iterator maxI = std::max_element(_next_d.begin(),
				_next_d.end());
		int max1 = maxI - _next_d.begin();
		int max2 = _next_nb[max1];

#ifdef DEBUG
		_debug_cache_consistency();
#endif
		node *r = _cluster(max1, max2);
		delete _distance;
		return r;
	}

	hierClust::node *hierClust::_cluster(int i, int j) {
		// Link Node i,j. remove these two node and insert a new node
		// representing the two
		int new_loc = _nodes.size();
		int new_id = _nodes[_index[i]]->id(); // Re-use old slice from i
		int ix_new = _index.size() - 2;
		int ixs = _index.size() - 1;
		node *new_node = new node(new_id, _nodes[_index[i]], _nodes[_index[j]],
				0, mode);
		_nodes[_index[i]]->_parent = new_node;
		_nodes[_index[j]]->_parent = new_node;

		metric.prepareCache(new_node->_p, new_node->id());

		if (ixs == 1) { // Found last link, we are done
			_nodes.clear(); // Clean-up, remove data
			_index.clear(); // used internally by the
			_next_nb.clear(); // algorithm
			_next_d.clear(); //
			return new_node;
		}

		_nodes.push_back(new_node);

		// Search and destroy distance-cache references to i,j
		_mob_up(i, j);
		_mob_up(j, i);

		// Move  the two dirty entries out of scope from index space
		_move2(i, ixs);
		if (j == ixs) j = i; // Special case: we have just moved j in the previous statements....
		_move2(j, ix_new);

		// Destroy i,j
		_index.pop_back(); // Remove the node from index space
		_next_nb.pop_back();
		_next_d.pop_back();
		_index[ix_new] = new_loc; // .. re-use reference moved to j, point to new node

		// Calculate distance-matrix slice for new node
		// Update nearest-distance cache
		_next_d[ix_new] = -2.;
		_updateDistanceSlice(ix_new);

		// Find next link, continue recursively
		vector<float>::iterator maxI = std::max_element(_next_d.begin(),
				_next_d.end());
		int max1 = maxI - _next_d.begin();
		int max2 = _next_nb[max1];

#ifdef DEBUG
		_debug_cache_consistency();
#endif
		return _cluster(max1, max2);
	}

	void hierClust::_mob_up(int dirty1, int dirty2) {
		// Find reference_r_s to dirty index dirty1 in the distance cache.
		// This one and dirty2 is about to be removed, therefore find a new minimal distance
		// which is not dirty1 and not dirty2
		dataIF &distance = *_distance;

		vector<int>::iterator b = _next_nb.begin();
		vector<int>::iterator e = _next_nb.end();
		vector<int>::iterator s;
		int ixs = _index.size();

		for (s = std::find(b, e, dirty1); s < e; s
				= std::find(s + 1, e, dirty1)) {
			int idx = s - b;
			if (idx == dirty2) continue;
			int cid = _nodes[_index[idx]]->id();
			_next_d[idx] = -2.;
			_next_nb[idx] = -1;
			for (int k = 0; k < ixs; k++) {
				if (k == idx || k == dirty1 || k == dirty2) continue;
				float d = distance(cid, _nodes[_index[k]]->id());
				if (_next_d[idx] < d) {
					_next_d[idx] = d;
					_next_nb[idx] = k;
				}
			}
		}
		return;
	}

	void hierClust::_updateDistanceSlice(unsigned int i, unsigned int start2nd) {
		dataIF &distance = *_distance;
		node *new_node = _nodes[_index[i]];
		int new_id = new_node->id();
		for (unsigned int k = start2nd; k < _index.size(); k++) {
			if (k == i) continue;
			node *cur = _nodes[_index[k]];
			float d = metric(new_node->_p, cur->_p, new_id, cur->id());
			distance(new_id, cur->id()) = d;
			if (_next_d[k] < d) {
				_next_d[k] = d;
				_next_nb[k] = i;
			}
			if (_next_d[i] < d) {
				_next_d[i] = d;
				_next_nb[i] = k;
			}
		}
	}

	void hierClust::_move2(int to, int from) {
		_index[to] = _index[from];
		_next_d[to] = _next_d[from];
		_next_nb[to] = _next_nb[from];
		std::replace(_next_nb.begin(), _next_nb.end(), from, to);
	}

	vector<int> *hierClust::indexTable(node *root) {
		class assign: public visitor {
		public:
			assign(vector<int> &target) :
				_target(target) {
			}
			virtual ~assign() {}
			virtual void *visit(node *cur, void *, void *) {
				if (cur->isLeaf()) _target.push_back(cur->id());
				return 0;
			}
		private:
			vector<int> &_target;
		};
		vector<int> *target = new vector<int> ;
		assign A(*target);
		root->visit(&A);
		return target;
	}

	const string hierClust::node::_intermediate("");

	void commonMetric::hcpearson::prepareCache(data &d, int key) {
		dscstat A(d.begin(), d.end());
		_avg[key] = static_cast<float> (A.average());
		_var[key] = static_cast<float> (A.variance());
	}

	float commonMetric::hcpearson::operator()(data &d1, data &d2, int key1,
			int key2) {
		int n = d1.size();
		float sum = 0;
		for (int i = 0; i < n; i++) {
			float v1 = d1[i] - _avg[key1];
			float v2 = d2[i] - _avg[key2];
			sum += v1 * v2;
		}
		sum /= n;
		sum /= static_cast<float> (sqrt(_var[key1] * _var[key2] + 1e-6));
		// Regularize constant data
		return sum;
	}

#ifdef DEBUG
#include <iostream>
void hierClust::_debug_cache_consistency() {
	dataIF &distance = *_distance;
	static int iteration = 0;
	bool err = false;

	std::cerr << "Enter Debug consistency #" << ++iteration << std::endl;

	for(int i=0; i < _index.size(); i++) {

		int idx = _index[i];
		if(! _nodes[idx]->_p.size()) {
			std::cerr << "Index " << i << " referencing an abondoned node" << std::endl;
			err = true;
		}

		int iid = _nodes[idx]->id();
		if(! _nodes[_index[_next_nb[i]]]->_p.size()) {
			std::cerr << "nearest neighbor " << _next_nb[idx] << " referenced by " << i << " [" << iid << "] is an abondoned node" << std::endl;
			err = true;
		}
		float D = -2.;
		int max = -1;
		for(int k=0; k < _index.size(); k++) {
			if(k==i) continue;
			if(distance(iid,_nodes[_index[k]]->id()) > D) {
				D = distance(iid,_nodes[_index[k]]->id());
				max = k;
			}
		}

		if((_next_d[i] - D) > 1e-4) {
			std::cerr << "cached distance " << _next_d[idx] << " differs from matrix value " << D
			<< " for index " << i << " [" << iid << "]" << std::endl;
			err = true;
		}

		if(_next_nb[i] != max) {
			std::cerr << "cached neighbor " << _next_nb[idx] << " differs from matrix lookup " << max
			<< " for index " << i << " [" << iid << "]" << std::endl;
			err = true;
		}
	}

#ifdef DETAIL_DEBUG
	for(int i=0; i < _index.size(); i++) {
		std::cerr << i << ":\t" << _index[i] << " ["<< _nodes[_index[i]]->id() << "]\t"
		<< _next_nb[i] << "\t" << _next_d[i];
		for(int k=0; k < _index.size(); k++)
		std::cerr << "\t" << distance(_nodes[_index[i]]->id(), _nodes[_index[k]]->id());
		std::cerr << std::endl;
	}
#endif
	if(err) exit(-1);
}
#endif
} // Namespace
#ifdef DEBUG
#include <dataWrapperIndexed.h>
#include <dataBasic.h>
#include <dataIO.h>
#include <iostream>
int main(int argc, char **argv) {
	basicData IN;
	if( dataIO::read(argv[1], IN)) {
		std::cerr << "Can not open datafile " << argv[1] << std::endl;
		return -1;
	}

	commonModes::averageLinkage mode;
	commonMetric::pearson metric;
	hierClust C(mode, metric);

	hierClust::node *root = C.cluster(IN, hierClust::ROWS);
	vector<int> *order = C.indexTable(root);
	redirectData OUT(order->begin(), order->end(), IN);
	delete order;

	std::cerr << "I find " << C.countLeafs(root) << " leafs.\n";
	std::cerr << "The depth is  " << C.depth(root) << ".\n";
	delete root;
	dataIO::write(argv[2], OUT);
	return 0;
}

#endif //ifdef DEBUG
