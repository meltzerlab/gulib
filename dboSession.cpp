/*
 * Database.cpp
 *
 *  Created on: Nov 15, 2014
 *      Author: sven
 */
#ifdef HAVE_WT
#include<memory>
#include <Wt/Json/Object.h>
#include <Wt/Json/Parser.h>
#include <Wt/Dbo/WtSqlTraits.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include <Wt/Dbo/backend/MySQL.h>
#include "guli/dboSession.h"

namespace GULI {
	namespace Dbo {

		bool Session ::_createInitialDatabase(bool drop) {
			if(drop) {
				try {
					 dropTables();
				 } catch(Wt::Dbo::Exception &e) {/* If we can't delete; there is nothing */		 }
			}
			try {
				 createTables();
			 } catch(Wt::Dbo::Exception &e) {
				std::cerr << e.what() << std::endl;
				std::cerr << "while creating database tables, re-using existing database" << std::endl;
				return false;
			 }
			 return true;
		}



		bool Session::connect(const std::string &json) {
			static const char *dbkey = "dbparms";
			enum { mysql, sqlite, postgres};

			Wt::Json::Object o;
			Wt::Json::ParseError  E;


			if(!Wt::Json::parse(json,o ,E , true)) {
				std::cerr << "Error parsing JSON config definition\n";
				std::cerr << E.what() << std::endl;
				return false;
			}

			if(!o.contains(dbkey)) {
				std::cerr << "Did not find " << dbkey << " object\n";
				return false;
			}
			const Wt::Json::Object p = o.get(dbkey);

			std::string host     = p.get("host").toString().orIfNull("");
			std::string user     = p.get("user").toString().orIfNull("");
			std::string password = p.get("password").toString().orIfNull("");
			std::string database = p.get("database").toString().orIfNull("");
			std::string socket   = p.get("socket").toString().orIfNull("");
			int         port     = p.get("port").toNumber().orIfNull(0);
			std::string driver = p.get("driver").toString().orIfNull("sqlite");
			int backend = driver == "mysql" ? mysql : sqlite;

			try {
				switch (backend) {
				case mysql:
					setConnection(std::make_unique<Wt::Dbo::backend::MySQL>(database, user, password, host, port, socket));
					break;
				case sqlite:
					setConnection(std::make_unique<Wt::Dbo::backend::Sqlite3>(database));
					break;

				default:
					assert(0);
					break;
				}
			} catch (Wt::Dbo::Exception& e) {
				std::cerr << e.what() << std::endl;
				std::cerr << "while connecting to  database";
				return false;
			}
			_connected = true;
			return true;
		}

		std::istream &Session::connect(std::istream &JSON) {
			std::string J;
			J.assign(std::istreambuf_iterator<char>(JSON), std::istreambuf_iterator<char>());
			bool r =  connect(J);
//			if(!r) 	JSON.setstate(std::ios::bad());
			return JSON;
		}

	}
}
#endif

