/*
 * ontologySimplifiedAPI.cpp
 *
 *  Created on: Apr 23, 2009
 *      Author: sven
 */
#include "guli/ontologySimplifiedAPI.h"
#include "guli/geneOntologyXmlIo.h"
#include "guli/tabDelimited.h"
//OBSOLETE, SHOULD GO SOOON

namespace GULI {

	unsigned int GULI::ontologySimplifiedAPI::goSize() const {
		return tree().size();
	}

/*
	const string &GULI::ontologySimplifiedAPI::goId(int i) const {
		return node(i).id();
	}

	const string &ontologySimplifiedAPI::goDescription(int i) const {
		return node(i).description();
	}
*/
//	const geneOntology::Node &ontologySimplifiedAPI::node(int i) const {
//		return *(*dagGO)[i];
//	}

	int ontologySimplifiedAPI::getTreeFromXML(const string &file) {
		ontologyXmlIO IN;
		dagGO->clear();
		geneOntology::textual *t = IN.read(file.c_str());
		if (!t) return -1;
		dagGO->textual2Node(*t);
		delete t;
		return 0;
	}

	void ontologySimplifiedAPI::setId2goMap(const one2manyMap &m) {
		typedef one2manyMap::const_iterator mapit;
		typedef one2many::const_iterator    o2mit;

		_id2go = m;
		//setup inverse matrix
		_go2id.clear();
		for(mapit i = m.begin(); i != m.end(); ++i) {
			for(o2mit j=i->second.begin(); j != i->second.end(); ++j) {
				_go2id[*j].push_back(i->first);
			}
		}
		return;
	}

	int ontologySimplifiedAPI::readId2GO(const string fnam, one2manyMap &M) {
		class parser : private tabDelimited {
		public:
			parser(const string &fnam, one2manyMap &M) : _M(M), _error(false) {
				ifstream IN(fnam.c_str());
				if(!IN) return;
				parse(IN);
				IN.close();
				return;
			}

			bool error() { return _error; }
		protected:
			virtual void consumeData(int argc, char **argv) {
				if(argc < 2) {
//					_error = true; // just ignore errors
					return;
				}
				one2many  &cur = _M[argv[0]];
				for(int i=1; i < argc; ++i) {
					string go =  argv[i];
					string domain = go.substr(0, 3);
					if (domain != "GO:") {
						std::cerr << "Ontology not in the expected format (GO|SB):nnnnnnnn: " << go << std::endl;
						_error = true;
						return;
					}
//					string key = go.substr(3, go.length() - 3);
//TODO: fix the syntax problem in the next line to allow checking for duplicate entries
//					if(std::find(cur.begin(). cur.end(), key)) continue; // entry already exists
//					cur.push_back(key);
					cur.push_back(go);
				}
				return;
			}
		private:
			one2manyMap &_M;
			bool _error;
		};

		parser P(fnam, M);
		return P.error() ? -1 : 0;
	}

	string ontologySimplifiedAPI::path(const geneOntology::Node &n, const geneOntology &go) {
/*
 * NEEDS WORK AFTER CHANGES TO GO itsel
		vector<string> namestack;
		geneOntology::uniqueBackwardIterator I(&n, &_b);
		for (; I != go.end() && I->id() != go.rootId; ++I)
			namestack.push_back(I->name());

		string retval;
		if (namestack.empty()) {
			retval = "/";
			return retval;
		}
		retval = namestack[namestack.size() - 1];
		for (int i = namestack.size() - 2; i >= 0; i--) {
			retval += ":";
			retval += namestack[i];
		}
		return retval;
*/
	}


	void ontologySimplifiedAPI::id2Go(const string &id, one2many &r, 	bool descend) const {
		one2many tmp;
		tmp.push_back(geneOntology::allId);
		one2manyMap::const_iterator I = _id2go.find(id);
		if (I == _id2go.end()) {
			tmp.push_back(geneOntology::unknownId);
		} else {
			const one2many &go = I->second;
			tmp.insert(tmp.end(), go.begin(), go.end());
		}

		if (descend) {
			geneOntology::uniqueBackwardIterator I(*(*dagGO)[geneOntology::rootId]);
			typedef vector<Node *> nlT;
			nlT startOntologies;
			for (unsigned int i = 0; i < tmp.size(); i++) {
				Node &cur = *tree()[tmp[i]];
				startOntologies.push_back(&cur);
			}
			I.massign(startOntologies.begin(), startOntologies.end());
			r.clear();
			for (; I != tree().end(); ++I) {
				r.push_back(I->id());
			}
		} else {
			r.swap(tmp);
		}

		return;
	}

	void ontologySimplifiedAPI::go2Id(const string goid,
			const vector<string> ids, one2manyI &r, bool ascend) const {

		r.clear();
		// create list of known ids  associated with GO term
		map<string, bool> M; // use map to filter out duplicate IDS
		if (ascend) {
			geneOntology::uniqueForwardIterator I(tree()[goid]);
			if (I->id() != goid) return; // GO term not found
			for (; I != tree().end(); ++I) {
				const string &goid = I->id();
				one2manyMap::const_iterator f = _go2id.find(goid);
				if (f != _go2id.end()) {
					const one2many &m = f->second;
					for (unsigned int i = 0; i < m.size(); ++i) {
						M[m[i]] = true;
					}
				}
			}
		} else {
			one2manyMap::const_iterator f = _go2id.find(goid);
			if (f != _go2id.end()) {
				const one2many &m = f->second;
				for (unsigned int i = 0; i < m.size(); ++i) {
					M[m[i]] = true;
				}
			}
		}

		// create return index table
		for (unsigned int i = 0; i < ids.size(); ++i) {
			if (M.find(ids[i]) != M.end()) {
				r.push_back(i);
			}
		}
		return;
	}


}
//NAMESPACE
