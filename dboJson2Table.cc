/*
 * Json2Table.cc
 *
 *  Created on: Feb 26, 2015 //
 *      Author: sven
 */
#ifdef HAVE_WT
#include "guli/dboJson2Table.h"

namespace GULI {
	namespace Dbo {
		template <> void Json2Table::act<int>(FieldRef<int> F) {
			if(!_O->contains(F.name())) return;
			F.setValue(_O->get(F.name()).toNumber());
		}

		template <> void Json2Table::act<long long>(FieldRef<long long> F) {
			if(!_O->contains(F.name())) return;
			F.setValue(_O->get(F.name()).toNumber());
		}

		template <> void Json2Table::act<float>(FieldRef<float> F) {
			if(!_O->contains(F.name())) return;
			F.setValue((double) _O->get(F.name()).toNumber());
		}

		template <> void Json2Table::act<double>(FieldRef<double> F) {
			if(!_O->contains(F.name())) return;
			F.setValue(_O->get(F.name()).toNumber());
		}

		template <> void Json2Table::act<std::string>(FieldRef<std::string> F) {
			if(!_O->contains(F.name())) return;
			F.setValue(_O->get(F.name()).toString());
		}

		template <> void Json2Table::act<bool>(FieldRef<bool> F) {
			if(!_O->contains(F.name())) return;
			F.setValue(_O->get(F.name()).toBool());
		}

		template <> void Json2Table::act<Wt::Json::Object>(FieldRef<Wt::Json::Object> F) {
			// vacuum up all unknown JSON fields
			Wt::Json::Object R;

			const auto names = _O->names();
			for(auto i = names.begin(); i != names.end(); ++i) {
				if(_T->knownColumn(*i)) continue;
				if(_T->knownMethod(*i)) continue;
				R[*i] = _O->get(*i);
			}
			F.setValue(R);
		}

		template <> void Json2Table::act<Wt::WDate>(FieldRef<Wt::WDate> F) {
			if(!_O->contains(F.name())) return;
			std::string in =  _O->get(F.name()).toString();
			Wt::WDate D = Wt::WDate::fromString(in, _dateFormat);
			F.setValue(D);
		}

		template <> void Json2Table::actId<std::string>(std::string &v, const std::string &name, int size) {
			_idLabel = name;
			if(!_O->contains(name)) return;
			v = _O->get(_idLabel).toString().orIfNull("");
		}

	}
}
#endif


