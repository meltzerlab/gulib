/***************************************************************************
 numericHistogram.cc  -  description
 -------------------
 begin                : Thu Jul 18 2002
 copyright            : (C) 2002-2005 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2002-2005 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "guli/stdinc.h"
#include "guli/numericHistogram.h"
#undef DEBUG_HISTO
#ifdef DEBUG_HISTO
#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#endif // DEBUG_HISTO
namespace GULI {
	bool Partition::API::operator==(const Partition::API &B) {
		return (signature() == B.signature() && lower() == B.lower() && upper()
				== B.upper() && delta() == B.delta() && _specific_compare(B));
	}

	int Partition::linear::bin(double x) const {
		if (x < _x0 || x > _xN) return -1;
		return (int) ((x - _x0) / _delta);
	}

	int Partition::linear::Nbin() const {
		return (int) ((_xN - _x0) / _delta) + 1;
	}

	double Partition::linear::center(int b) const {
		return _x0 + (b + 0.5) * _delta;
	}

	double Partition::linear::width(int b) const {
		return _delta;
	}

	Partition::logarithmic::logarithmic(double x0, double xN, double delta,
			double x0Min) :
		_x0(x0), _xN(xN), _delta(delta) {
		if (_x0 >= x0Min) {
			_x0Min = _x0;
			_c0 = 0.5 * _x0 * (1 + exp(_delta));
			_w0 = _x0 * (exp(_delta) - 1.);
		} else {
			_x0Min = x0Min;
			_c0 = 0.5 * (_x0 + _x0Min * exp(_delta));
			_w0 = _x0Min * exp(_delta) - _x0;
		}
	}

	int Partition::logarithmic::bin(double x) const {
		if (x < _x0 || x > _xN) return -1;
		if (x < _x0Min) return 0;
		return (int) (log(x / _x0Min) / _delta);
	}

	int Partition::logarithmic::Nbin() const {
		return (int) (log(_xN / _x0Min) / _delta);
	}

	double Partition::logarithmic::center(int b) const {
		if (b) return _x0Min * exp(_delta * (b + 0.5));
		return _c0;
	}

	double Partition::logarithmic::width(int b) const {
		double r;
		if (b) r = (exp(_delta * (b + 1.)) - exp(_delta * b)) * _x0Min;
		else r = _w0;
		return r;
	}

	Partition::API *Partition::logarithmic::clone() const {
		return new logarithmic(_x0, _xN, _delta, _x0Min);
	}

	histogram &histogram::operator =(const histogram &B) {
		if (_partition) delete _partition;
		_partition = B.partition().clone();
		buffer->resize(partition().Nbin());
		std::copy(B.buffer->begin(), B.buffer->end(), buffer->begin());
		return *this;
	}

} // NAMESPACE

#ifdef DEBUG_HISTO
using namespace GULI::numeric;
int main(int argc, char **argv) {
	Partition::logarithmic Pb(0, 3., 0.5, 1e-4);
	Partition::linear Pa(0, 3., 0.01);
	histogram A(Pa);
	histogram B(Pb);
	const gsl_rng_type * T;
	gsl_rng * r;
	gsl_rng_env_setup();
	T = gsl_rng_taus;
	r = gsl_rng_alloc (T);
	for (int i = 0; i < 10000000; i++) {
		//    double ran = gsl_ran_flat (r, 0., 2.0);
		double ran = gsl_ran_exponential (r, 1.);
		A.event(ran);
		B.event(ran);
	}
	A.normalize();
	B.normalize();

	//  for(int i=0; i < A.Nbin(); i++) {
	//  cout << A.center(i) << " " << A.entries(i) << endl;
	// }

	for(int i=0; i < B.Nbin(); i++) {
		cout << B.center(i) << " " << B.entries(i) << endl;
	}
}
#endif // DEBUG_HISTO
