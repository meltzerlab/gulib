/***************************************************************************
 bedRegion.cpp  -  description
 -------------------
 begin                : Wed Dec 27 2008
 copyright            : (C) 2008-2009 by Sven Bilke
 email                : bilkes@mail.nih.gov
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2008-2015 by Sven Bilke                                 *
 *   bilkes@mail.nih.gov                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <limits.h>
#include <algorithm>
#include <numeric>
#include <sstream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "guli/bedRegion.h"
#include "guli/bedTiling.h"
#include "guli/tabDelimited.h"
#include "guli/errorcodes.h"
#include "assert.h"
#include <sys/types.h>
#include <unistd.h>



static const gsl_rng_type *T = 0;
static gsl_rng *r = 0;
static void _initRandom() {
	if(r) return;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc (T);
	time_t a = time(0);
	pid_t  n = getpid();
	int seed = ((int) a | (int) n) + 1;
	if(seed < 0) seed *= -1;
	gsl_rng_set(r, seed); // seed with random number for clib
}


namespace GULI {
	class bedFile: private tabDelimited {
	public:
		int readRegions(const string fnam, vector<bedRegion> &R) {
			_regions = &R;
			ifstream IN(fnam.c_str());
			if (!IN) return -ENOTFOUND;
			parse(IN);
			IN.close();
			std::sort(R.begin(), R.end());
			return -ESUCCESS;
		}

		int readRegions(istream &IN, vector<bedRegion> &R) {
			_regions = &R;
			parse(IN);
			std::sort(R.begin(), R.end());
			return -ESUCCESS;
		}

	private:
		virtual void consumeData(int argc, char **argv) {
			if (argc < 3) return;
			string id = argc >= 4 ? argv[3] : "NONE";
			float val = argc >= 5 ? atof(argv[4]) : 1;
			char orient = argc >= 6 ? *argv[5] : '+';
//todo: Add support for fields beyond col 6
			_regions->push_back(bedRegion(argv[0], id, atoi(argv[1]),
					atoi(argv[2]),val,orient));
			return;
		}

		vector<bedRegion> *_regions;
	};

	bedRegion::bedRegion(const bam1_t *b, const bam_header_t *H)  :
			_chr("NotSet"), _id("NotSet"), _start(0), _end(0),  _thickStart(0), _thickEnd(0), _val(0), _orient('?') {
		assert(0);
		/*
 *
 		if(b->core.flag & BAM_FUNMAP) return;
		_id     = bam1_qname(b);
		_start  = b->core.pos;
		_end    = bam_calend(&b->core, bam1_cigar(b));
		_orient = bam1_strand(b) ? '-' : '+';
		_val    = b->core.qual;
		_chr    = H->target_name[b->core.tid];
*/
	}

	int bedRegion::readList(listT &R, const string &fnam) {
		bedFile rdr;
		return rdr.readRegions(fnam, R);
	}

	int bedRegion::readList(listT &R, istream &IN) {
		bedFile rdr;
		return rdr.readRegions(IN, R);
	}

	int bedRegion::writeList(const listT &R, const string &fnam) {
		ofstream OUT(fnam.c_str());
		if (!OUT) return -ECREATE;
		writeList(R, OUT);
		OUT.close();
		return -ESUCCESS;
	}

	int bedRegion::writeList(const listT &R, ostream &OUT) {
		const unsigned int N = R.size();
		for (unsigned int i = 0; i < N; ++i) {
			const bedRegion &r = R[i];
//			OUT << r.chromosome() << "\t" << r.start() << "\t" << r.end();
//			if (r.id() != "") OUT << "\t" << r.id();
			OUT << r;
			OUT << "\n";
		}
		return -ESUCCESS;
	}

	bool bedRegion::operator<(const bedRegion &b) const {
		if (b.chromosome() == chromosome()) {
			if(start() < b.start()) return true;
			if(start() == b.start() && end() < b.end()) return true;
			return false;
		}
		return chromosome() < b.chromosome();
	}

	bool bedRegion::operator>(const bedRegion &b) const {
		return b < *this;
	}

	bool bedRegion::inside(const bedRegion &b) const {
		if(b.chromosome() != chromosome()) return false;
		return (start() <= b.start()) 	&& (end() >= b.end());
	}

	bool bedRegion::overlap(const bedRegion &b) const {
		if(b.chromosome() != chromosome())                 return false;
		if((start() <= b.start()) 	&& (end() >= b.end())) return true;
		if(b.start() <= start() && b.end() > start())      return true;
		if(b.start() < end()   && b.end() >= end())        return true;
		return false;
	}

	int bedRegion::centerDistance(const bedRegion &b) const {
		int d = chromosome().compare(b.chromosome());
		if(d < 0) return  OTHERCHROMOSOMELARGER;
		if(d > 0) return  OTHERCHROMOSOMESMALLER;
		float c1 = (b.start() + b.end()) / 2.;
		float c2 = (start() + end()) / 2.;
		return static_cast<int> (c2 - c1 + 0.5);
	}

	int bedRegion::outerDistance(const bedRegion &b) const {
		int d = chromosome().compare(b.chromosome());
		if(d < 0) return  OTHERCHROMOSOMELARGER;
		if(d > 0) return  OTHERCHROMOSOMESMALLER;
		// overlap ?
		if((start() <= b.start()) 	&& (end() >= b.end())) return 0;
		if(b.start() <= start() && b.end() >= start())     return 0;
		if(b.start() < end()   && b.end() >= end())        return 0;

		int d1 = start() - b.end()-1;
		if(d1 >= 0) return -d1;
		return b.start() - end() - 1;
	}


	bedRegion::listT bedRegion::randomize(bedRegion::listT R) {
		// There appears to be a bug in that randomization occurs only up to and including
		// the largest input coordinate within a given chromosome, rather tan to the end of a chromosome
		_initRandom();
		std::sort(R.begin(), R.end());
		int start = -1;
		string chr = "";
		unsigned int max = 0;
		unsigned int i = 0;
		do {
			if( (R[i].chromosome() != chr) || (i == R.size() - 1) ) {
				if(start >= 0) {
					for(unsigned int c = start; c < i; ++c) {
						int size = R[c].end() - R[c].start();
						int st = gsl_ran_flat(r, 0., max + 0.5 - size);
						assert(st >=0);
						R[c].start(st);
						R[c].end(R[c].start() +  size);
					}
				}
				start = i;
				chr = R[i].chromosome();
				max = R[i].end();
			}
			if(R[i].end() > max) max = R[i].end();
		} while(++i < R.size());
		return R;
	}

	/*  LEGACY, to be rempved
	bedRegion::listT bedRegion::randomize(bedRegion::listT R, const alignment &background) {
		_initRandom();
		std::sort(R.begin(), R.end());
		int start = -1;
		string chr = "";
		unsigned int max = 0;
		unsigned int i = 0;
		do {
			if( (R[i].chromosome() != chr) || (i == R.size() - 1) ) {
				if(start >= 0) {
					const alignment::gvalueLT *D = background.density(chr);
					float  S = std::accumulate(D->begin(), D->end(), 0.);
					if(D->size()) {
						vector<double> P(D->size());
						P[0] = (*D)[0] / S;
						for(unsigned int j=1; j < D->size(); ++j) {
							P[j] = P[j-1] + (*D)[j] / S;
						}

						for(unsigned int c = start; c < i; ++c) {
							int size = R[c].end() - R[c].start();
							double st = gsl_ran_flat(r, 0., D->size() - size - 1) / (double) D->size();
							int start = std::lower_bound(P.begin(), P.end(), st) - P.begin();
							R[c].start(start);
							R[c].end(start +  size);
						}
					}
					delete D;
				}
				start = i;
				chr = R[i].chromosome();
				max = R[i].end();
			}
			if(R[i].end() > max) max = R[i].end();
		} while(++i < R.size());
		return R;
	}
*/
	bedRegion::listT bedRegion::mergeOverlap(bedRegion::listT R) {
		typedef listT::iterator it;
		std::sort(R.begin(), R.end());
		for(bool stable=false; !stable; ) {
			string newid;
			stable = true;
			it first = R.begin();
			for(it i = first + 1; true; ++i) {
				if(i != R.end() && first->overlap(*i)) {
					newid  = first->id() + ",";
					newid += i->id();
					first->id(newid);
					if(first->end() < i->end()) first->end(i->end());
					stable = false;
				} else {
					if(!stable) {
						R.erase(first+1, i);
						break;
					} else {
						first = i;
					}
				}
				if(i==R.end()) break;
			}
		}
		return R;
	}

	const bedRegion &bedRegion::operator=(const bedRegion &b) {
		chromosome(b.chromosome());
		id(b.id());
		start(b.start());
		end(b.end());
		val(b.val());
		orient(b.orient());
		rgb(b.rgb());
		feature(b.feature());
		thickStart(b.thickStart());
		thickEnd(b.thickEnd());
		return *this;
	}

	string bedRegion::defaultId() const {
		std::stringstream O;
		O << chromosome() << ":" << start() << "-" << end();
		return O.str();
	}

	const int bedRegion::OTHERCHROMOSOMELARGER  = INT_MAX;
	const int bedRegion::OTHERCHROMOSOMESMALLER = INT_MIN + 1;
}


ostream &operator<<(ostream &O, const GULI::bedRegion &b) {
	const size_t N = b.feature().size();
	O << b.chromosome() << "\t" << b.start() << "\t" << b.end() << "\t"  << b.id() << "\t" << b.val() << "\t" << b.orient();
	if(b.thickStart() > 0) {
		O << "\t" << b.thickStart() << "\t" << b.thickEnd() << "\t";
	} else {
		if(!N) return O;
		O << "\t" << b.start() << "\t" << b.end() << "\t";
	}
 	O  << (int) b.rgb().r << "," << (int) b.rgb().g << "," << (int) b.rgb().b << "\t";
	if(N) {
		O << N << "\t";
		for(size_t i = 0; i < N; ++i) {
			if(i) O << ',';
			O <<  (int) b.feature()[i].size ;
		}
		O << "\t";
		for(size_t i = 0; i < N; ++i) {
			if(i) O << ',';
			O <<  (int) b.feature()[i].pos ;
		}
	}
  return O;
}
