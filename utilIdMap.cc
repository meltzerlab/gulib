/*
 * genomeDensity.cc
 *
 *  Created on: Dec 30, 2008
 *      Author: sven
 */

#include <algorithm>
#include "guli/utilIdMap.h"

namespace GULI {
	bool utilIdMap::idExist(const string &id) const {
		return idIdx(id) >= 0;
	}

	void utilIdMap::clear() {
		_id2idx.clear();
		_ids.clear();
	}

	const string &utilIdMap::id(unsigned int i) const {
		return _ids[i];
	}

	int utilIdMap::idIdx(const string &id) const {
		_mapT::const_iterator f = _id2idx.find(id);
		if (f != _id2idx.end()) return f->second;
		return -1;
	}

	int utilIdMap::idIdx(const string &id, bool create, bool &created) {
		created = false;
		_mapT::iterator f = _id2idx.find(id);
		if (f != _id2idx.end()) return f->second;
		if (!create) return -1;
		created = true;
		_ids.push_back(id);
		int r = _ids.size() - 1;
		_id2idx[id] = r;
		return r;
	}

	int utilIdMap::idIdx(const string &id, bool create) {
		bool dummy;
		return idIdx(id, create, dummy);
	}


	unsigned int utilIdMap::nId() const {
		return _ids.size();
	}

}
