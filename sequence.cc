#include <sstream>
#include <string.h>
#include "guli/stdinc.h"
#include "guli/sequence.h"

static void shrinkToFit(sequence::storageType &v) {
	if (v.capacity() > (1. + sequence::MEM_OVERALLOCATION_MARGIN) * v.size()) {
		sequence::storageType(v).swap(v);
	}
}


	sequence::sequence(const sequence::seqData &d) :
		referer(&d), _ownSeqData(false) {
		_sd = &d;
	}

	sequence::sequence(const sequence &a) :
		referer(a._sd), _ownSeqData(a.isManaged() ? false : true) {
		if (_ownSeqData) {
			_sd = new seqData(*a._sd);
		} else {
			_sd = a._sd;
		}
	}

	sequence::sequence(const inverseSequence &a) :
		referer(0), _ownSeqData(true) {
		seqData *d = new seqData;
		d->content().resize(a.length());
		d->id() = a.id();
		d->valid() = a.valid();
		std::copy(a.begin(), a.end(), d->content().begin());
		_sd = d;
	}

	sequence::sequence(const storageType &s, const string id) :
		referer(0), _ownSeqData(true) {
		seqData *d = new seqData;
		d->content() = s;
		d->id() = id;
		d->valid() = true;
		_sd = d;
	}

	sequence::sequence(const char *s, const string id) :
		referer(0), _ownSeqData(true) {
		seqData *d = new seqData;
		d->id() = id;
		d->valid() = true;
		const size_t L = strlen(s);
		d->content().resize(L);
		for(size_t i=0; i < L; ++i)
			d->content()[i] = _maskRepeats ? c2n_MASKED[(unsigned char) s[i]] : c2n[(unsigned char) s[i]] ;
		_sd = d;
	}

	sequence::~sequence() {
		if (_ownSeqData) delete _sd;
	}

	const sequence sequence::subSequence(unsigned int start, unsigned int len, const string &id) const {
		assert ( length() >= start + len );
		//   const storageType &s = _sd->content();
		storageType t;
		t.resize(len);
		std::copy(begin() + start, begin() + start + len, t.begin());
		sequence r(t, id);
		return r;
	}

	sequence::seqData::seqData() : _valid(false) {}

	sequence::seqData::seqData(const seqData &A) : _valid(false) {
		_content    = A.content();
	}

	sequence::seqData::~seqData() {}

	istream &sequence::seqData::operator>>(istream &IN) {
		_content.clear();
		_valid = false;
		string line;
		while(! (IN.eof() || IN.fail())) {
			std::getline(IN, line);
			if(!line.length()|| (line[0] == '#')) continue;
			break;
		}
		if(IN.eof() || IN.fail()) return IN;

		if (line[0] != '>') {
			std::cerr << "Syntax error while parsing FASTA: " << line
					<< std::endl;
			IN.setstate(std::ios_base::failbit | IN.rdstate());
			return IN;
		}
		/** TODO: Remove trailing spaces when reading a sequence identifier
		 in FASTA format **/
		id() = line.substr(1, string::npos);

		_valid = true;
		while (! (IN.eof() || IN.fail())) {
			char c;
			IN.get(c);
			if (IN.eof()) return IN;
			IN.putback(c);
			if ('>' == c) return IN;
			std::getline(IN, line);
			if ((!line.length()) || ('#' == line[0])) continue;
			size_t s = line.find_first_not_of(" \t");
			size_t e = line.find_first_of(" \t", s);
			e = (e > line.length()) ? line.length() : e;

			if (_maskRepeats) {
				for (size_t i = s; i < e; i++) {
					content().push_back(c2n_MASKED[(unsigned char) (line[i])]);
				}
			} else {
				for (size_t i = s; i < e; i++) {
					content().push_back(c2n[(unsigned char) (line[i])]);
				}
			}
		}
		if(IN.fail()) _valid = false;
		shrinkToFit(_content);
		return IN;
	}

	sequence::seqData &sequence::seqData::operator=(const seqData &src) {
		content() = src.content();
		id() = src.id();
		shrinkToFit(content());
		valid() = src.valid();
		return *this;
	}

	ostream &sequence::seqData::operator<<(ostream &OUT) const {
		const size_t sz = _content.size();
		OUT << ">" << id() << std::endl;
		size_t i;

		for (i = 0; i < sz; i++) {
			if (i && !(i % 79)) OUT << std::endl;
			OUT << n2c[(int) _content[i]];
		}
		if (i % 79) OUT << std::endl;
		return OUT;
	}

	istream &operator>>(istream &A, sequence::seqData &b) {
		return b.operator>>(A);
	}
	ostream &operator<<(ostream &A, const sequence::seqData &b) {
		return b.operator<<(A);
	}

	ostream &operator<<(ostream &A, const sequence &b) {
		return b.operator<<(A);
	}

	/* Transcoding tables */

	/* character to internal */
	const sequence::nucleotide sequence::c2n[256] = { A, C, G, T, N, N, N, N,
			N, N, N, N, N, N, N, N, //0x00
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x10
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x20
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x30
			N, A, N, C, N, N, N, G, N, N, N, N, N, N, N, N, //0x40
			N, N, N, N, T, N, N, N, N, N, N, N, N, N, N, N, //0x50
			N, A, N, C, N, N, N, G, N, N, N, N, N, N, N, N, //0x60
			N, N, N, N, T, N, N, N, N, N, N, N, N, N, N, N, //0x70
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x80
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x90
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xA0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xB0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xC0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xD0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xE0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xF0
			};

	const sequence::nucleotide sequence::c2n_MASKED[256] = { A, C, G, T, N, N,
			N, N, N, N, N, N, N, N, N, N, //0x00
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x10
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x20
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x30
			N, A, N, C, N, N, N, G, N, N, N, N, N, N, N, N, //0x40
			N, N, N, N, T, N, N, N, N, N, N, N, N, N, N, N, //0x50
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x60
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x70
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x80
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0x90
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xA0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xB0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xC0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xD0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xE0
			N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, //0xF0
			};

	/* internal to character */
	const char sequence::n2c[5] = { 'A', 'C', 'G', 'T', 'N' };

	/* complement sequence */
	const inverseSequence::nucleotide inverseSequence::_trans[5] = {
			sequence::T, sequence::G, sequence::C, sequence::A, sequence::N };

	bool sequence::_maskRepeats = true;
	float sequence::MEM_OVERALLOCATION_MARGIN = (float) 0.1;


