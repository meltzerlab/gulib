#include <string.h>
#include <algorithm>
#include "guli/stdinc.h"
#include "guli/errorcodes.h"
#include "guli/xmlGeneric.h"
namespace GULI {
	void startElementProcessor(void *udat, const xmlChar *Name,
			const xmlChar **attrs) {
		xmlGeneric *target = reinterpret_cast<xmlGeneric *> (udat);
		target->_startElement(Name, attrs);
	}

	void characterProcessor(void *udat, const xmlChar *Text, int len) {
		xmlGeneric *target = reinterpret_cast<xmlGeneric *> (udat);
		target->_processText(Text, len);
	}

	void endElementProcessor(void *udat, const xmlChar *Name) {
		xmlGeneric *target = reinterpret_cast<xmlGeneric *> (udat);
		target->_endElement(Name);
	}

	void xmlGeneric::_startElement(const xmlChar *Name, const xmlChar **attrs) {
		tokentype token;
		attributetype attr;

		token.name = (const char *) Name;
		if (attrs) {
			for (int i = 0; attrs[i]; i += 2) {
				attr.name = (const char *) attrs[i];
				attr.value = (const char *) attrs[i + 1];
				token.Attribute.push_back(attr);
			}
		}
		_stack.push(token);
		return;
	}

	void xmlGeneric::_processText(const xmlChar *Text, int len) {
		char *buffer = new char[len + 1];
		tokentype &CUR = _stack.top();
		strncpy(buffer, (const char *) Text, len);
		buffer[len] = 0;
		CUR.text += buffer;
		delete [] buffer;
	}

	void xmlGeneric::_endElement(const xmlChar *Name) {
		string NAME = (const char *) Name;

		tokentype CUR = _stack.top();
		_stack.pop(); // Discard original token

		if (CUR.name != NAME) {
			std::cerr << "Error parsing XML file while ending token " << NAME;
			std::cerr << ", expected " << CUR.name << std::endl;
			return;
		}
		if (_eatToken(CUR)) return;

		_stack.top().Tokens.push_back(CUR); // Attach to parent-Node
	}

	bool xmlGeneric::parse(const char *file) {
		bool retval = true;
		tokentype ROOT;
		_stack.push(ROOT);

		xmlSAXHandler SAXHandler;
		char *p = (char *) &SAXHandler;

		// Setup the SAX Data-Structure. All we need is BEGIN-ELEMENT and END-ELEMENT
		for (unsigned int i = 0; i < sizeof(xmlSAXHandler); i++)
			*p++ = 0;
		SAXHandler.startElement = startElementProcessor;
		SAXHandler.endElement = endElementProcessor;
		SAXHandler.characters = characterProcessor;

		// Pass reference to this Object to via the XML Pareser
		xmlParserCtxtPtr ctxt;
		ctxt = xmlCreateFileParserCtxt(file);
		if (!ctxt) return false;
		ctxt->sax = &SAXHandler;

		ctxt->userData = (void *) this;
		xmlParseDocument(ctxt);

		if (!ctxt->wellFormed) { // do something about bad xml-code !
			//		retval = false;
		}
		ctxt->sax = NULL;
		xmlFreeParserCtxt(ctxt);

		return retval;
	}

	static  std::istream *_I;

	static int _readStream(void *context, char *buffer, int len) {
		_I->read(buffer, (size_t) len);
		return _I->gcount();
	}

	static int _closeStream(void *context) {
		return 0;
	}

	bool xmlGeneric::parse(std::istream &I) {
		bool retval = true;
		tokentype ROOT;
		_stack.push(ROOT);

		xmlSAXHandler SAXHandler;
		char *p = (char *) &SAXHandler;

		// Setup the SAX Data-Structure. All we need is BEGIN-ELEMENT and END-ELEMENT
		for (unsigned int i = 0; i < sizeof(xmlSAXHandler); i++)
			*p++ = 0;
		SAXHandler.startElement = startElementProcessor;
		SAXHandler.endElement = endElementProcessor;
		SAXHandler.characters = characterProcessor;

		// Pass reference to this Object to via the XML Parser
		xmlParserCtxtPtr ctxt;

		_I = &I;
		ctxt = xmlCreateIOParserCtxt(&SAXHandler, 0, _readStream, _closeStream, 0, XML_CHAR_ENCODING_ASCII);
		if (!ctxt) return false;
		ctxt->sax = &SAXHandler;

		ctxt->userData = (void *) this;
		xmlParseDocument(ctxt);

		if (!ctxt->wellFormed) { // do something about bad xml-code !
			//		retval = false;
		}
		ctxt->sax = NULL;
		xmlFreeParserCtxt(ctxt);

		return retval;
	}

	bool xmlGeneric::findToken(const tokentype &S, const string &I,
			tokentype &T) const {
		for (unsigned int i = 0; i < S.Tokens.size(); i++) {
			if (S.Tokens[i].name == I) {
				T = S.Tokens[i];
				return true;
			}
		}
		return false;
	}

	std::string  xmlGeneric::tokentype::attribute(const std::string &I, size_t *idx) const {
		for (size_t i =  idx ? *idx : 0; i < Attribute.size(); i++) {
			if (Attribute[i].name == I) {
				if(idx) *idx=i+1;
				return Attribute[i].value;
			}
		}
		if(idx) *idx = 0;
		std::string err = "xmlGeneric: Attribute ";
		err += I + " not found in token  " + name;
		throw guliNotFoundException(err);
	}


	const xmlGeneric::tokentype &xmlGeneric::tokentype::token(const std::string &I, size_t *idx) const {
		for (size_t i =  idx ? *idx : 0; i < Tokens.size(); i++) {
			if (Tokens[i].name == I) {
				if(idx) *idx=i + 1;
				return Tokens[i];
			}
		}
		if(idx) *idx = 0;
		std::string err = "xmlGeneric: token ";
		err += I + " not found in token " + name;
		throw guliNotFoundException(err);
	}

	const string &xmlGeneric::findAttribute(const tokentype &S, const string &I) const {
		for (unsigned int i = 0; i < S.Attribute.size(); i++) {
			if (S.Attribute[i].name == I) return S.Attribute[i].value;
		}
		return  UNKNOWNATTRIBUTE;
	}

	void xmlGeneric::startRepFindToken(const tokentype &T, const string &id) const {
		_search_token = &T;
		_id = id;
		_cur_token = 0;
	}

	bool xmlGeneric::findNextToken(tokentype &target) const {
		for (; _cur_token < _search_token->Tokens.size(); _cur_token++)
			if (_search_token->Tokens[_cur_token].name == _id) {
				target = _search_token->Tokens[_cur_token++];
				return true;
			}
		return false;
	}

	const string xmlGeneric::UNKNOWNATTRIBUTE = "Unknown Attribute (xmlgeneric)";
} // NAMESPACE GUILIB
